import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OurDetailsRoutingModule } from './our-details-routing.module';
import { OurDetailsComponent } from './our-details.component';


@NgModule({
  declarations: [OurDetailsComponent],
  imports: [
    CommonModule,
    OurDetailsRoutingModule
  ]
})
export class OurDetailsModule { }
