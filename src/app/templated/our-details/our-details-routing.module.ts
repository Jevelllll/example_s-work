import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {OurDetailsComponent} from './our-details.component';


const routes: Routes = [{path: '', component: OurDetailsComponent}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OurDetailsRoutingModule {
}
