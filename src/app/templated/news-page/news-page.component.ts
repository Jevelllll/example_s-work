import {Component, ComponentFactoryResolver, OnDestroy, OnInit, Renderer2, ViewChild, ViewContainerRef} from '@angular/core';
import {Meta, MetaDefinition, Title} from '@angular/platform-browser';
import {ContainerComponent} from './container/container.component';
import {InnerContainerComponent} from './inner-container/inner-container.component';
import {NewsPageService} from '../../services/templated/news-page.service';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {SpinnerService} from '../../services/spinner.service';
import {PreviewService} from '../../services/preview.service';
import {Observable} from 'rxjs/internal/Observable';
@Component({
  selector: 'app-news-page',
  templateUrl: './news-page.component.html',
  styleUrls: ['./news-page.component.scss']
})
export class NewsPageComponent implements OnInit, OnDestroy {
  destroyed$ = new Subject<void>();
  public previewImg: Observable<string> = new Observable<string>();
  @ViewChild('dynamicNews', {static: true, read: ViewContainerRef}) dynamicNews: ViewContainerRef;
  private newsDatas: any [] = [];

  constructor(private newsService: NewsPageService,
              private renderer: Renderer2,
              private spinnerService: SpinnerService,
              private componentFactoryResolver: ComponentFactoryResolver,
              private titleService: Title,
              private previewService: PreviewService,
              private meta: Meta) {

    this.titleService.setTitle('Новости "???"');
  }

  private metaGenerate() {

    const keywords: MetaDefinition = {
      name: 'keywords',
      content: '??? новости, Новости ???, новости сыщик инфо'
    };
    const description: MetaDefinition = {
      name: 'description',
      content: 'Новостная лента ???'
    };

    this.meta.updateTag(keywords);
    this.meta.updateTag(description);
  }

  private customFactory(dynamic: any) {
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(dynamic);
    this.dynamicNews.clear();
    this.dynamicNews.createComponent(componentFactory);
  }

  private newsView(): void {
    this.customFactory(ContainerComponent);
  }

  private news_innerView(): void {
    this.customFactory(InnerContainerComponent);
  }

  ngOnInit() {

    this.previewImg = this.previewService.getPreview('news');

    this.spinnerService.show();
    this.metaGenerate();
    this.newsService.getNews()
      .pipe(takeUntil(this.destroyed$))
      .subscribe(value => {
          this.spinnerService.hide();
          value.map(item => this.newsDatas.push(item));
        },
        error => this.spinnerService.hide(),
        () => {
          this.spinnerService.hide();
        });

    this.newsService.sendExtendData(this.newsDatas);

    this.newsView();

    this.newsService.getBack()
      .pipe(takeUntil(this.destroyed$))
      .subscribe(back => {
        this.newsView();
        this.spinnerService.hide();
      });

    this.newsService.getDetail()
      .pipe(takeUntil(this.destroyed$))
      .subscribe(value => {
        this.newsService.sendDataInner({full_data: this.newsDatas, select_data: value});
        this.news_innerView();
        this.spinnerService.hide();
      });

  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }

}
