import {Component, OnDestroy, OnInit} from '@angular/core';
import {NewsPageService} from '../../../services/templated/news-page.service';
import {DomSanitizer} from '@angular/platform-browser';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';

@Component({
  selector: 'app-inner-container',
  templateUrl: './inner-container.component.html',
  styleUrls: ['./inner-container.component.scss']
})
export class InnerContainerComponent implements OnInit, OnDestroy {
  destroyed$ = new Subject<void>();
  // tslint:disable-next-line:variable-name
  public li_class = 'inner_landing__container__news__list';
  // tslint:disable-next-line:variable-name
  public data_to_inner: any;
  // tslint:disable-next-line:variable-name
  public model_inner: any;
  // tslint:disable-next-line:variable-name
  public html_test: any;
  public SANITIZER: any;

  constructor(private newsService: NewsPageService, private sanitizer: DomSanitizer) {
    this.SANITIZER = this.sanitizer;
  }

  public data_main(model) {

    this.model_inner = model;
  }

  public goBack() {

    this.newsService.sendBack(true);
  }

  ngOnInit() {

    // const city = this.getRegion.filter(function (el) {
    //     //     return (el.$.name === region_name);
    //     // });
    this.newsService.getDataInner()
      .pipe(takeUntil(this.destroyed$))
      .subscribe(value => {

        this.data_to_inner = value;
        this.model_inner = value.select_data;
      });
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }
}
