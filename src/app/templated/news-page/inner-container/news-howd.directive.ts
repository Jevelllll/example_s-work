import {Directive, ElementRef, HostListener, Input, Renderer2} from '@angular/core';

@Directive({
  selector: '[appNewsHowd]',
  // host: {
  //     '(mouseenter)': 'onMouseEnter()',
  //     '(mouseleave)': 'onMouseLeave()'
  // }
})
export class NewsHowdDirective {
// tslint:disable-next-line:variable-name
  private _defaultBColor = '#66aa2f';
  // tslint:disable-next-line:variable-name
  private _defaultColor = '#292c37';
  private el: HTMLElement;
  private clEl: HTMLElement;
  @Input('appNewsHowd') appNewsHowd: any;

  constructor(el: ElementRef, private renderer: Renderer2) {
    this.el = el.nativeElement;

  }

  @HostListener('click', ['$event']) onClick($event) {
// tslint:disable-next-line:variable-name
    let el_target: any;

    if ($event.target.toString() === '[object HTMLLIElement]') {

      el_target = $event.target;
    } else {

      el_target = $event.target.parentNode;
    }

    const parentElement = el_target.parentNode;
    const focus = parentElement.childNodes;
    focus.forEach(value => {

      if (value.getAttribute === undefined) {
      } else {

        this.renderer.removeClass(value, 'active');
      }

    });

    this.renderer.addClass(el_target, 'active');

  }

}
