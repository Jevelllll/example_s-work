import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NewsPageRoutingModule } from './news-page-routing.module';
import { NewsPageComponent } from './news-page.component';
import { ContainerComponent } from './container/container.component';
import { InnerContainerComponent } from './inner-container/inner-container.component';
import {NewsPageService} from '../../services/templated/news-page.service';
import {NewsHowdDirective} from './inner-container/news-howd.directive';
import { NgxPaginationModule } from 'ngx-pagination';


@NgModule({
  declarations: [NewsPageComponent, ContainerComponent, InnerContainerComponent, NewsHowdDirective],
  providers: [NewsPageService],
  imports: [
    NgxPaginationModule,
    CommonModule,
    NewsPageRoutingModule
  ],
  exports: [NewsHowdDirective]
})
export class NewsPageModule { }
