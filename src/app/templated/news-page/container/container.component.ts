import {Component, OnDestroy, OnInit, Renderer2} from '@angular/core';
import {NewsPageService} from '../../../services/templated/news-page.service';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

@Component({
  selector: 'app-container',
  templateUrl: './container.component.html',
  styleUrls: ['./container.component.scss']
})
export class ContainerComponent implements OnInit, OnDestroy {
  destroyed$ = new Subject<void>();
  p1 = 1;
  // @Output() onChanged = new EventEmitter<any>();
  // tslint:disable-next-line:variable-name
  public mode_up: any;
  // tslint:disable-next-line:variable-name
  public li_class = 'news_landing__head__right__main__item';
  public title = '';
  // tslint:disable-next-line:variable-name
  public img_C = '';
  // tslint:disable-next-line:variable-name
  public time_create = '';
  public newsData: any[] = [];
  private datesendNews: any;

  constructor(private newsService: NewsPageService,
              private renderer: Renderer2) {
  }

  public clDate(model: any) {
    this.newsService.sendDetail(model);
  }

  ngOnInit() {

    this.newsService.getDataExtend()
      .pipe(takeUntil(this.destroyed$))
      .subscribe(value => {
        this.newsData = value;
      });

  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }

}
