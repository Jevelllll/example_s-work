import {AfterViewInit, Component, ElementRef, OnInit, Renderer2, ViewChild} from '@angular/core';

@Component({
  selector: 'app-new-year',
  templateUrl: './new-year.component.html',
  styleUrls: ['./new-year.component.scss']
})
export class NewYearComponent implements OnInit, AfterViewInit {

  @ViewChild('nums') nums: ElementRef;

  constructor(private renderer: Renderer2) {
  }

  ngAfterViewInit(): void {
    setInterval(() => this.gir(), 500);
  }

  ngOnInit(): void {

  }

  gir(): void {

    if (this.nums) {
      const nums = Number(document.getElementById('nums_1').innerHTML);
      const elementByIdGir = document.getElementById('gir');
      if (nums === 1) {
        elementByIdGir.className = 'gir_1';
        this.nums.nativeElement.innerHTML = '2';
      }
      if (nums === 2) {
        elementByIdGir.className = 'gir_2';
        this.nums.nativeElement.innerHTML = '3';
      }
      if (nums === 3) {
        elementByIdGir.className = 'gir_3';
        this.nums.nativeElement.innerHTML = '1';
      }
    }
  }
}
