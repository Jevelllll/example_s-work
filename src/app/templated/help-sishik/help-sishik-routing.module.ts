import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {HelpSishikComponent} from './help-sishik.component';


const routes: Routes = [{path: '', component: HelpSishikComponent}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HelpSishikRoutingModule {
}
