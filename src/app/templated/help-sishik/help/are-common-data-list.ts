// tslint:disable-next-line:import-spacing
import  * as moment_js_date_ls from 'moment';

// tslint:disable-next-line:variable-name
const time_at = moment_js_date_ls().format('YYYY');
export class AreCommonDataList {
  public mothList = [];
  // tslint:disable-next-line:variable-name
  private __montYearList = [];
  // tslint:disable-next-line:variable-name
  private __dayList = [];
  constructor() {
    this.mothList = monthList;
    // tslint:disable-next-line:radix
    this.montYearList = Number.parseInt(time_at);
    this.dayList = 31;
  }
  set montYearList(montEnd) {
    const rangeStart = 1920;
    for (let index = rangeStart; index <= montEnd; index++) {
      this.__montYearList.push(index);
    }
  }
  get montYearList(): any {
    return this.__montYearList;
  }
  set dayList(montEnd) {
    const rangeStart = 1;
    for (let index = rangeStart; index <= montEnd; index++) {
      if (index.toString().length === 1) {
        this.__dayList.push(`0${index}`);
      } else {
        this.__dayList.push(`${index}`);
      }

    }
  }
  get dayList(): any {
    return this.__dayList;
  }
}
const monthList = [
  {
    value: '01',
    nameRu: 'Январь',
  }, {
    value: '02',
    nameRu: 'Февраль',
  }, {
    value: '03',
    nameRu: 'Март',
  }, {
    value: '04',
    nameRu: 'Апрель',
  }, {
    value: '05',
    nameRu: 'Май',
  }, {
    value: '06',
    nameRu: 'Июнь',
  }, {
    value: '07',
    nameRu: 'Июль',
  }, {
    value: '08',
    nameRu: 'Август',
  }, {
    value: '09',
    nameRu: 'Сентябрь',
  }, {
    value: '10',
    nameRu: 'Октябрь',
  }, {
    value: '11',
    nameRu: 'Ноябрь',
  }, {
    value: '12',
    nameRu: 'Декабрь',
  },
];
