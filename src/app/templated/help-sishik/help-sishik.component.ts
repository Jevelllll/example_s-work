import {Component, OnDestroy, OnInit} from '@angular/core';
import {DataHelp, HelpModel} from '../../shared/templData';
import {DomSanitizer, Meta, MetaDefinition, Title} from '@angular/platform-browser';
import {InfoHelpM, InformationDataService} from '../../services/templated/information-data.service';
import {Subject, Observable} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {SpinnerService} from '../../services/spinner.service';
import {PreviewService} from '../../services/preview.service';

@Component({
  selector: 'app-help-sishik',
  templateUrl: './help-sishik.component.html',
  styleUrls: ['./help-sishik.component.scss']
})
export class HelpSishikComponent implements OnInit, OnDestroy {
  destroyed$ = new Subject<void>();
  // tslint:disable-next-line:variable-name
  public data_help: HelpModel[] = [];
  // tslint:disable-next-line:variable-name
  public h_model: InfoHelpM[] = [];
  public SANITIZER: any;
  public previewImg: Observable<string> = new Observable<string>();
  constructor(private titleService: Title,
              private meta: Meta,
              private previewService: PreviewService,
              private spinnerService: SpinnerService,
              private sanitizer: DomSanitizer,
              // tslint:disable-next-line:variable-name
              private info_i: InformationDataService) {
    this.SANITIZER = this.sanitizer;
    this.titleService.setTitle('Помощь в пользовании "???"');
  }

  private metaGenerate() {

    const keywords: MetaDefinition = {
      name: 'keywords',
      content: '??? help, помочь с ???, помочь с сыщиком, ЧАВО ???, ' +
        'ЧАВО Сыщик инфо, Как зарегистрироваться в Сышике, Как зарегистрироваться в ???,' +
        'FAQ ???, F.A.Q. ???, FAQ ???, F.A.Q. ???, как пользоваться sishik info'
    };
    const description: MetaDefinition = {
      name: 'description',
      content: 'Часто задаваемые вопросы (Как зарегистрироваться в ???,...)'
    };

    this.meta.updateTag(keywords);
    this.meta.updateTag(description);
  }

  ngOnInit() {
    this.previewImg = this.previewService.getPreview('inform/faq');
    this.spinnerService.show();
    this.metaGenerate();

    this.data_help = DataHelp;
    this.info_i.get_i('help')
      .pipe(takeUntil(this.destroyed$))
      .subscribe(value => {
        this.h_model = value;
        this.spinnerService.hide();
        window.scrollTo(0, 67);
      });
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }

}
