import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HelpSishikRoutingModule } from './help-sishik-routing.module';
import {HelpSishikComponent} from './help-sishik.component';
import {SanitizeHtmlPipe} from './help/sanitize-html.pipe';


@NgModule({
  declarations: [HelpSishikComponent, SanitizeHtmlPipe],
  imports: [
    CommonModule,
    HelpSishikRoutingModule
  ],
  exports: [SanitizeHtmlPipe]
})
export class HelpSishikModule { }
