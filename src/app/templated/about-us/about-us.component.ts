import {Component, OnDestroy, OnInit} from '@angular/core';
import {InfoAbRules, InformationDataService} from '../../services/templated/information-data.service';
import {DomSanitizer, Meta, MetaDefinition, Title} from '@angular/platform-browser';
import {Subject, Observable} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {SpinnerService} from '../../services/spinner.service';
import {PreviewService} from '../../services/preview.service';

@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.scss']
})
export class AboutUsComponent implements OnInit, OnDestroy {
  destroyed$ = new Subject<void>();
  // tslint:disable-next-line:variable-name
  public h_model: InfoAbRules;
  public SANITIZER: any;
  public previewImg: Observable<string> = new Observable<string>();
  constructor(private titleService: Title,
              // tslint:disable-next-line:variable-name
              private meta: Meta, private info_i: InformationDataService,
              private spinnerService: SpinnerService,
              private previewService: PreviewService,
              private sanitizer: DomSanitizer) {
    this.SANITIZER = this.sanitizer;
    this.titleService.setTitle('О Сайте "???"');
  }

  private metaGenerate() {

    const keywords: MetaDefinition = {
      name: 'keywords',
      content: 'О ???, o сыщик инфо, о сыщик инфо, о ???, О Нас'
    };
    const description: MetaDefinition = {
      name: 'description',
      content: 'О сайте ???'
    };

    this.meta.updateTag(keywords);
    this.meta.updateTag(description);
  }

  ngOnInit(): void {
    this.previewImg = this.previewService.getPreview('inform/about');
    this.spinnerService.show();
    this.info_i.get_i('about')
      .pipe(takeUntil(this.destroyed$))
      .subscribe(value => {
        this.h_model = value[0];
        this.spinnerService.hide();
      });
    this.metaGenerate();
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }

}
