import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {FeaturesAppRoutingModule} from './features-app-routing.module';
import {FeaturesAppComponent} from './features-app.component';
import {FeaturesDDirective} from './features-d.directive';
import {LaptopModule} from '../laptop/laptop.module';


@NgModule({
  declarations: [FeaturesAppComponent, FeaturesDDirective],
  imports: [
    CommonModule,
    FeaturesAppRoutingModule,
    LaptopModule
  ]
})
export class FeaturesAppModule {
}
