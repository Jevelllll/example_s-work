import {Component, Inject, OnInit, PLATFORM_ID, Renderer2} from '@angular/core';
import {FeaturesModel} from '../../shared/leave-request/leave/stepsmodel';
import {FEATURENEWSDATA} from '../../shared/howStepsdata';
import {BrowServices} from '../../services/brow-services.service';

@Component({
  selector: 'app-features-app',
  templateUrl: './features-app.component.html',
  styleUrls: ['./features-app.component.scss']
})
export class FeaturesAppComponent implements OnInit {
  lCl = 'laptop';
  timeSrx = '';
  public iterate = 0;
  // tslint:disable-next-line:variable-name
  public featuresList: FeaturesModel[] = FEATURENEWSDATA;
  // tslint:disable-next-line:variable-name
  constructor(private br_service: BrowServices,
              private renderer: Renderer2,
              @Inject(PLATFORM_ID) private platformId: any) { }

  public getClick(index: number, clickListn: any = false) {
    this.iterate = index;
    this.timeSrx = this.featuresList[index].img_src;
    this.renderer.addClass(clickListn, 'active');
    this.setGetActiveMethod(clickListn);
    this.br_service.addDataFeatures(index);
  }
  // tslint:disable-next-line:variable-name
  setGetActiveMethod(el_target) {
    const parentElement = el_target.parentNode;
    const focus = parentElement.childNodes;
    focus.forEach(value => {
      if (value.getAttribute === undefined) {} else {
        this.renderer.removeClass(value, 'active');
      }
    });
    this.renderer.addClass(el_target, 'active');
  }
  ngOnInit() {
    this.timeSrx = this.featuresList[0].img_src;
  }

}
