import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {FeaturesAppComponent} from './features-app.component';


const routes: Routes = [{path: '', component: FeaturesAppComponent}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FeaturesAppRoutingModule {
}
