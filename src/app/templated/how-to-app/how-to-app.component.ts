import {AfterViewInit, Component, OnInit} from '@angular/core';
import {CurrentPhase, interpreter} from '../../services/auth/application-phases.service';

declare let $;

@Component({
  selector: 'app-how-to-app',
  templateUrl: './how-to-app.component.html',
  styleUrls: ['./how-to-app.component.scss']
})
export class HowToAppComponent implements OnInit, AfterViewInit {

  phaseContainer = [];
  interpreterM = interpreter;

  constructor() {
  }


  ngOnInit() {
    const phase = new CurrentPhase();
    this.phaseContainer = Object.keys(phase).map((item) => {
      if (!['final', 'stop'].includes(item)) {
        return item;
      }
    }).filter((it) => it);
  }

  ngAfterViewInit(): void {
    this.tabStyle();
    window.scrollTo(0, 67);
  }

  tabStyle() {
    setTimeout(() => {
      const $aria = $('mat-step-header');
      [].slice.call($aria).map((item) => {
        $(item).addClass('not-select');
        $(item).addClass('b-Def');
        $(item).removeClass('b-Col');
        const attr = $(item).attr('aria-selected');
        if (attr == 'true') {
          $(item).addClass('b-Col');
          $(item).removeClass('b-Def');
        }
      });
    }, 4);

  }

}
