import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HowToAppRoutingModule } from './how-to-app-routing.module';
import { HowToAppComponent } from './how-to-app.component';
import {MatStepperModule} from '@angular/material/stepper';
import {LaptopModule} from '../laptop/laptop.module';


@NgModule({
  declarations: [HowToAppComponent],
  imports: [
    CommonModule,
    HowToAppRoutingModule,
    MatStepperModule,
    LaptopModule
  ]
})
export class HowToAppModule { }
