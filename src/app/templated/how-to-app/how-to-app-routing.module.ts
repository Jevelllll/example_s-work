import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HowToAppComponent} from './how-to-app.component';


const routes: Routes = [{path: '', component: HowToAppComponent}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HowToAppRoutingModule {
}
