import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-laptop',
  templateUrl: './laptop.component.html',
  styleUrls: ['./laptop.component.scss']
})
export class LaptopComponent implements OnInit {
  @Input() laptopClass = 'laptop';
  @Input() downPanel = 'down-panel';
  @Input() laptopContainer = 'laptop-container';

  constructor() {
  }

  ngOnInit(): void {
  }

}
