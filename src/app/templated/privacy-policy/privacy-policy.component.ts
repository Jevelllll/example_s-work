import {Component, ComponentFactoryResolver, OnDestroy, OnInit, Renderer2} from '@angular/core';
import {InfoAbRules, InformationDataService} from '../../services/templated/information-data.service';
import {DomSanitizer, Meta, Title} from '@angular/platform-browser';
import {Subject, Observable} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {SpinnerService} from '../../services/spinner.service';
import {PreviewService} from '../../services/preview.service';

@Component({
  selector: 'app-privacy-policy',
  templateUrl: './privacy-policy.component.html',
  styleUrls: ['./privacy-policy.component.scss']
})
export class PrivacyPolicyComponent implements OnInit, OnDestroy {
  destroyed$ = new Subject<void>();
  // tslint:disable-next-line:variable-name
  public h_model: InfoAbRules;
  public SANITIZER: any;
  public previewImg: Observable<string> = new Observable<string>();
  constructor(private renderer: Renderer2,
              private componentFactoryResolver: ComponentFactoryResolver,
              private titleService: Title,
              private meta: Meta,
              private sanitizer: DomSanitizer,
              private spinnerService: SpinnerService,
              private previewService: PreviewService,
              // tslint:disable-next-line:variable-name
              private info_i: InformationDataService) {
    this.SANITIZER = this.sanitizer;
    this.titleService.setTitle('Политика конфиденциальности"');
  }

  ngOnInit(): void {
    this.previewImg = this.previewService.getPreview('inform/private-policy');
    this.spinnerService.show();
    this.info_i.get_i('security_doc')
      .pipe(takeUntil(this.destroyed$))
      .subscribe(value => {
        this.h_model = value[0];
        this.spinnerService.hide();
      });
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }

}
