import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ContactRoutingModule } from './contact-routing.module';
import {ContactComponent} from './contact.component';
import {ReactiveFormsModule} from '@angular/forms';
import {InputsModule} from 'angular-bootstrap-md';


@NgModule({
  declarations: [ContactComponent],
  imports: [
    CommonModule,
    ContactRoutingModule,
    ReactiveFormsModule,
    InputsModule
  ]
})
export class ContactModule { }
