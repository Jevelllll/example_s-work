import {Component, OnDestroy, OnInit} from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {SpinnerService} from '../../services/spinner.service';
import {Observable, Subject} from 'rxjs';
import {catchError, map, takeUntil} from 'rxjs/operators';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {PreviewService} from '../../services/preview.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit, OnDestroy {
  destroyed$ = new Subject<void>();
  private url = environment.api_url;
  public mail = '???';
  public phone = '???';
  public data: any = {};
  // tslint:disable-next-line:variable-name
  public contact_model: FormGroup;
  public labelStyle = {left: '15px', 'font-size': '1.2rem'};
  submitted = false;
  public previewImg: Observable<string> = new Observable<string>();
  onSubmit() {
    // this.submitted = true;
  }

  // tslint:disable-next-line:variable-name
  constructor(
    private previewService: PreviewService,
    private http: HttpClient,
    private spinnerService: SpinnerService,
    // tslint:disable-next-line:variable-name
    private _fb: FormBuilder) {
  }

  // tslint:disable-next-line:variable-name
  save(contact_model): void {
    this.data.name = contact_model.get('name').value;
    this.data.email = contact_model.get('email').value;
    // this.data['mobile'] = contact_model.mobile;
    this.data.message = contact_model.get('message').value;
    setTimeout(function() {
      this.send_message(contact_model.get('name').value, contact_model.get('email').value, contact_model.get('message').value)
        .pipe(takeUntil(this.destroyed$))
        .subscribe(value => {
          this.submitted = value;
        }, error2 => {
        }, () => {
        });
    }.bind(this), 700);
  }

  ngOnInit() {
    this.previewImg = this.previewService.getPreview('inform/contact-us');
    this.contact_model = this._fb.group({
      name: [null, Validators.required],
      email: [null, Validators.required],
      message: [null, Validators.required]
    });
  }

  send_message(name, email, message): Observable<boolean> {
    // tslint:disable-next-line:variable-name variable-name
    const contact_to = '???';
    // tslint:disable-next-line:variable-name
    const contact_from = {name, email};
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        'Client-Service': 'frontend-client',
        'Auth-Key': '???i'
      })
    };
    const preBody = 'contact_to=' + contact_to
      + '&contact_from=' + JSON.stringify(contact_from) + '&contact_message=' + message;
    return this.http.post(this.url + 'mail_us/send_serv',
      preBody, httpOptions).pipe(map((response: Response) => {
      // const mes = response.json();
      if (response['message'] === 'success') {
        return true;
      } else {
        return false;
      }
    }), catchError(this.handleError));
  }

  /*
     *
     * @param error
     */
  private handleError(error: HttpErrorResponse | any) {
    // TODO: seems we cannot use messageService from here...
    const errMsg = (error.message) ? error.message : 'Server error';
    if (error.status === 401) {
      window.location.href = '/';
    }
    return Observable.throw(errMsg);
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }
}
