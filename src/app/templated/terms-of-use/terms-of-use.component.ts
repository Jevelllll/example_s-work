import {Component, OnDestroy, OnInit} from '@angular/core';
import {InfoAbRules, InformationDataService} from '../../services/templated/information-data.service';
import {DomSanitizer, Meta, MetaDefinition, Title} from '@angular/platform-browser';
import {takeUntil} from 'rxjs/operators';
import {Subject, Observable} from 'rxjs';
import {SpinnerService} from '../../services/spinner.service';
import {PreviewService} from '../../services/preview.service';

@Component({
  selector: 'app-terms-of-use',
  templateUrl: './terms-of-use.component.html',
  styleUrls: ['./terms-of-use.component.scss']
})
export class TermsOfUseComponent implements OnInit, OnDestroy {
  destroyed$ = new Subject<void>();
  // tslint:disable-next-line:variable-name
  public h_model: InfoAbRules;
  public SANITIZER: any;
  public previewImg: Observable<string> = new Observable<string>();
  constructor(private titleService: Title,
              private meta: Meta,
              // tslint:disable-next-line:variable-name
              private info_i: InformationDataService,
              private spinnerService: SpinnerService,
              private previewService: PreviewService,
              private sanitizer: DomSanitizer) {
    this.SANITIZER = this.sanitizer;
    this.titleService.setTitle('??? правила пользование сайтом');
  }

  private metaGenerate() {

    const keywords: MetaDefinition = {
      name: 'keywords',
      content: '??? правила пользования, Пользовательское соглашение ???,' +
        'пользовательское соглашение ???'
    };
    const description: MetaDefinition = {
      name: 'description',
      content: 'Пользовательское соглашение ???'
    };

    this.meta.updateTag(keywords);
    this.meta.updateTag(description);
  }

  ngOnInit() {
    this.previewImg = this.previewService.getPreview('inform/rules');
    this.spinnerService.show();
    this.metaGenerate();
    this.info_i.get_i('user_doc')
      .pipe(takeUntil(this.destroyed$))
      .subscribe(value => {
        this.h_model = value[0];
        this.spinnerService.hide();
        window.scrollTo(0, 67);
      });
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }

}
