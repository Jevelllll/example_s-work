import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';


const routes: Routes = [
  {path: 'homes', pathMatch: 'full', redirectTo: ''},
  {
    path: '',
    loadChildren: () => import('./topbar/topbar-homes.module').then(m => m.TopbarHomesModule)
  },
  {
    path: 'type/:characteristic',
    loadChildren: () => import('./first-level-options/first-level-options.module').then(m => m.FirstLevelOptionsModule)
  },
  {
    path: 'leave',
    loadChildren: () => import('./leave-request/base/base.module').then(m => m.BaseModule)
  },
  {
    path: 'news',
    loadChildren: () => import('./templated/news-page/news-page.module').then(m => m.NewsPageModule)
  },
  {
    path: 'about',
    loadChildren: () => import('./templated/about-us/about-us.module').then(m => m.AboutUsModule)
  },
  {
    path: 'help',
    loadChildren: () => import('./templated/help-sishik/help-sishik.module').then(m => m.HelpSishikModule)
  },
  {
    path: 'rules',
    loadChildren: () => import('./templated/terms-of-use/terms-of-use.module').then(m => m.TermsOfUseModule)
  },
  {
    path: 'privacy-policy',
    loadChildren: () => import('./templated/privacy-policy/privacy-policy.module').then(m => m.PrivacyPolicyModule)
  },
  {
    path: 'contact_us',
    loadChildren: () => import('./templated/contact/contact.module').then(m => m.ContactModule)
  },
  {
    path: 'account',
    loadChildren: () => import('./account/base-account/base-account.module').then(m => m.BaseAccountModule)
  },
  {
    path: 'account/:characteristic',
    loadChildren: () => import('./account/base-account/base-account.module').then(m => m.BaseAccountModule)
  },
  {
    path: 'features',
    loadChildren: () => import('./templated/features-app/features-app.module').then(m => m.FeaturesAppModule)
  },
  {
    path: 'how-to-app',
    loadChildren: () => import('./templated/how-to-app/how-to-app.module').then((m) => m.HowToAppModule)
  },
  {
    path: 'price',
    loadChildren: () => import('./topbar/price-app/price-app.module').then(m => m.PriceAppModule)
  },
  {
    path: 'special',
    loadChildren: () => import('./special/special.module').then(m => m.SpecialModule)
  },
  {
    path: 'sto',
    loadChildren: () => import('./sto/sto.module').then(m => m.StoModule)
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
