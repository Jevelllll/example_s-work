import {Component, OnDestroy, OnInit} from '@angular/core';
import {SpecialService} from '../../../services/priority/special.service';
import {CraftCategory} from '../../../services/priority/resModel';
import {Observable} from 'rxjs/internal/Observable';
import {tap} from 'rxjs/internal/operators/tap';
import {Subject} from 'rxjs/internal/Subject';
import {takeUntil} from 'rxjs/internal/operators/takeUntil';

@Component({
  selector: 'app-category-bar',
  templateUrl: './category-bar.component.html',
  styleUrls: ['./category-bar.component.scss']
})
export class CategoryBarComponent implements OnInit, OnDestroy {
  destroyed$ = new Subject<void>();
  public activeModel = {};
  public craftCategory: Observable<CraftCategory[]> = new Observable<CraftCategory[]>();

  constructor(private specialService: SpecialService) {
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }

  ngOnInit(): void {
    this.craftCategory = this.specialService.getCraftCategoriesMore().pipe(tap((data) => {
      data.forEach((item, index) => {
        this.activeModel[index] = false;
      });
    }));
  }

  addActive(craftCat: CraftCategory, index: number) {

    if (this.activeModel[index]) {
      this.clearActive();
      this.specialService.getItemByCategoryId(null)
        .pipe(takeUntil(this.destroyed$)).subscribe((it) => {
      });
    } else {
      this.specialService.getItemByCategoryId(craftCat.id)
        .pipe(takeUntil(this.destroyed$)).subscribe((it) => {
      });
      this.clearActive();
      this.activeModel[index] = true;
    }
  }

  private clearActive() {
    Object.keys(this.activeModel).forEach((ind) => {
      this.activeModel[ind] = false;
    });
  }
}
