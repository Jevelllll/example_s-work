import {Component, OnDestroy, OnInit} from '@angular/core';
import {SpecialService} from '../../services/priority/special.service';
import {Subject} from 'rxjs/internal/Subject';
import {takeUntil} from 'rxjs/internal/operators/takeUntil';


@Component({
  selector: 'app-base-craft',
  templateUrl: './base-craft.component.html',
  styleUrls: ['./base-craft.component.scss']
})
export class BaseCraftComponent implements OnInit, OnDestroy {

  destroyed$ = new Subject<void>();

  constructor(private specialService: SpecialService) {
  }

  ngOnInit(): void {
    this.specialService.getItemByCategoryId().pipe(takeUntil(this.destroyed$)).subscribe(() => {
    });
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }
}
