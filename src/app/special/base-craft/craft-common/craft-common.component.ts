import {Component, OnDestroy, OnInit} from '@angular/core';

@Component({
  selector: 'app-craft-common',
  templateUrl: './craft-common.component.html',
  styleUrls: ['./craft-common.component.scss']
})
export class CraftCommonComponent implements OnInit, OnDestroy {

  constructor() {
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
  }
}
