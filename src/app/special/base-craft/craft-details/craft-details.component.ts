import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subject} from 'rxjs/internal/Subject';
import {SpecialService} from '../../../services/priority/special.service';
import {CraftItem, CraftItemImages} from '../../../services/priority/resModel';
import {ActivatedRoute, Router} from '@angular/router';
import {switchMap} from 'rxjs/internal/operators/switchMap';
import {map} from 'rxjs/internal/operators/map';
import {takeUntil} from 'rxjs/internal/operators/takeUntil';
import {DomSanitizer} from '@angular/platform-browser';
import {MatDialog} from '@angular/material/dialog';
import {LoginService} from '../../../services/auth/login.service';
import {Observable} from 'rxjs/internal/Observable';
import {ContactComponent} from '../../../leave-request/container/contact/contact.component';
import {Application_phases} from '../../../services/auth/application-phases.service';
import {CreateformdataService} from '../../../services/auth/createformdata.service';
import {SpinnerService} from '../../../services/spinner.service';
import {take} from 'rxjs/internal/operators/take';
import {distinctUntilChanged} from 'rxjs/internal/operators/distinctUntilChanged';

@Component({
  selector: 'app-craft-details',
  templateUrl: './craft-details.component.html',
  styleUrls: ['./craft-details.component.scss']
})
export class CraftDetailsComponent implements OnInit, OnDestroy {
  public href: string;
  public showDialog = false;
  destroyed$ = new Subject<void>();
  public craftItems: CraftItem;
  public craftImages: CraftItemImages[];
  public SANITIZER: any;
  public isLogin: Observable<boolean>;

  constructor(private specialService: SpecialService,
              private sanitizer: DomSanitizer,
              private loginService: LoginService,
              private dialog: MatDialog,
              private router: Router,
              private phaseService: Application_phases,
              private toValidservice: CreateformdataService,
              private spinnerService: SpinnerService,
              private activateRoute: ActivatedRoute) {
  }

  ngOnInit(): void {
    const regExp = new RegExp(/[-()/\\]/g);
    this.phaseService.obsFinishContactForm.pipe(takeUntil(this.destroyed$))
      .subscribe(isSubmitContactForm => {
        if (isSubmitContactForm) {
          this.spinnerService.show();
          this.phaseService.get_phase_contact().pipe(takeUntil(this.destroyed$))
            .subscribe((userC) => {
              let user = userC;
              user['phone'] = userC['phone'].replace(regExp, '')
              this.toValidservice.postContact([user]).subscribe((result) => {
                if (result) {
                  if (user) {
                    this.loginService.login(user['phone'], user['password'])
                      .pipe(take(1), distinctUntilChanged()
                        , takeUntil(this.destroyed$)
                      )
                      .subscribe((respIsLogin) => {
                        if (respIsLogin) {
                          this.router.navigate(['/account']);
                          this.spinnerService.hide();
                          this.dialog.closeAll();
                        }
                      }, error => {
                        this.dialog.closeAll();
                        this.spinnerService.hide();
                      });
                  }
                }
              });
            });
        }
      });
    this.isLogin = this.loginService.isLogin();

    this.SANITIZER = this.sanitizer;
    this.activateRoute.paramMap.pipe(takeUntil(this.destroyed$),
      switchMap(params => {
        return params.getAll('id');
      }), map((currentCraftItemId) => {
        this.specialService.getCraftItem().pipe(takeUntil(this.destroyed$), switchMap(dT => {
          return dT.filter((item) => Number(item.id) === Number(currentCraftItemId));
        })).pipe(takeUntil(this.destroyed$))
          .subscribe((resultICrItems => {
            this.craftItems = resultICrItems;
            if (Array.isArray(resultICrItems.images)) {
              this.craftImages = resultICrItems.images;
            }
          }));
        return this.craftItems;
      })
    ).subscribe((f) => {
      window.scrollTo(0, 67);
    });
  }

  nexTStep(isLogin: boolean): void {
    switch (isLogin) {
      case true:
        this.router.navigate(['/account']);
        break;
      case false:
        this.phaseService.setOneRegistration(true);
        this.dialog.open(ContactComponent, {panelClass: 'custom-modalbox-one-reg'});
        break;
    }
  }

  openImage(urlString, template: any) {
    this.dialog.open(template, {panelClass: 'custom-modalbox-craft', backdropClass: 'custom-overlay-craft'});
    this.href = urlString;
  }

  closeDialog(event) {
    this.loginService.setIsLoginForm('0');
    this.showDialog = event;
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }
}
