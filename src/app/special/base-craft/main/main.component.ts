import {Component, OnInit} from '@angular/core';
import {SpecialService} from '../../../services/priority/special.service';
import {Observable} from 'rxjs/internal/Observable';
import {CraftItem} from '../../../services/priority/resModel';
import { tap } from 'rxjs/internal/operators/tap';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {
  p1 = 1;
  public craftItems: Observable<CraftItem[]> = new Observable<CraftItem[]>();

  constructor(private specialService: SpecialService) {
  }

  ngOnInit(): void {
    this.craftItems = this.specialService.getCraftItem().pipe(tap(v => this.p1 = 1));
  }

}
