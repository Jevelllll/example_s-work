import {AfterViewInit, Component, ElementRef, OnDestroy, OnInit, Renderer2, ViewChild} from '@angular/core';
import {SpecialService} from '../../../services/priority/special.service';
import {Subject} from 'rxjs/internal/Subject';
import {takeUntil} from 'rxjs/internal/operators/takeUntil';
import {CraftInfo} from '../../../services/priority/resModel';
import {DomSanitizer} from '@angular/platform-browser';
import {animate, state, style, transition, trigger} from '@angular/animations';

@Component({
  selector: 'app-craft-info',
  templateUrl: './craft-info.component.html',
  styleUrls: ['./craft-info.component.scss'],
  animations: [
    trigger('openClose', [
      state('open', style({
        height: '100%',
      })),
      state('close', style({
        height: '200px',
      })),
      transition('open => closed', [
        animate('1s')
      ]),
      transition('closed => open', [
        animate('0.5s')
      ]),
    ])
  ]
})
export class CraftInfoComponent implements OnInit, AfterViewInit, OnDestroy {
  destroyed$ = new Subject<void>();
  craftInfo: CraftInfo;
  public SANITIZER: any;
  isOpen = false;
  @ViewChild('boxText') boxText: ElementRef;
  private myBtn: any;

  constructor(
    private specialService: SpecialService,
    private sanitizer: DomSanitizer,
    private renderer: Renderer2
  ) {
    this.SANITIZER = sanitizer;
  }

  toggle() {
    this.isOpen = !this.isOpen;
    this.removeButton();
    this.addButton();
  }

  ngAfterViewInit(): void {
    this.addButton();
  }

  private addButton() {
    const button = this.renderer.createElement('button');
    const buttonText = this.renderer.createText((this.isOpen ? 'скрыть часть текста' : 'показать весь текст'));
    this.renderer.addClass(button, 'c-btn');
    this.renderer.addClass(button, 'c-btn_primary');
    this.renderer.addClass(button, 'p-2');
    this.renderer.addClass(button, 'm-auto');
    this.renderer.listen(button, 'click', () => this.toggle());
    this.myBtn = button;
    this.renderer.appendChild(button, buttonText);
    this.renderer.appendChild(this.boxText.nativeElement, button);
  }

  private removeButton() {
    this.myBtn.remove();
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }

  ngOnInit(): void {
    this.specialService.repeatCraftInfoObs.pipe(takeUntil(this.destroyed$)).subscribe((val) => {
      if (val) {
        this.craftInfo = val.data;
      } else {
        this.specialService.getCraftInfos().pipe(takeUntil(this.destroyed$)).subscribe(() => {
        });
      }
      window.scrollTo(0, 67);
    });
  }

}
