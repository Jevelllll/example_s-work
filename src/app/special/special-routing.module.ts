import {RouterModule, Routes} from '@angular/router';
import {SpecialComponent} from './special.component';
import {NgModule} from '@angular/core';
import {BaseCraftComponent} from './base-craft/base-craft.component';
import {CraftDetailsComponent} from './base-craft/craft-details/craft-details.component';
import {CraftCommonComponent} from './base-craft/craft-common/craft-common.component';

const routes: Routes = [{
  path: '', component: SpecialComponent,
  children: [
    {
      path: '',
      redirectTo: 'craft',
      pathMatch: 'full',
    },
    {
      component: BaseCraftComponent,
      path: 'craft',
      children: [
        {
          path: '',
          redirectTo: 'main',
          pathMatch: 'full',
        },
        {
          path: 'main',
          component: CraftCommonComponent
        },
        {
          path: 'details/:id',
          component: CraftDetailsComponent
        }
      ]
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SpecialRoutingModule {
}
