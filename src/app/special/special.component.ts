import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-special',
  templateUrl: './special.component.html',
  styleUrls: ['./special.component.scss']
})
export class SpecialComponent implements OnInit {

  constructor() {
  }

  ngOnInit(): void {
    window.scrollTo(0, 67);
  }

}
