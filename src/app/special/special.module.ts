import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SpecialRoutingModule} from './special-routing.module';
import {SpecialComponent} from './special.component';
import { BaseCraftComponent } from './base-craft/base-craft.component';
import { MainComponent } from './base-craft/main/main.component';
import { CategoryBarComponent } from './base-craft/category-bar/category-bar.component';
import {SpecialService} from '../services/priority/special.service';
import { NgxPaginationModule } from 'ngx-pagination';
import { CraftDetailsComponent } from './base-craft/craft-details/craft-details.component';
import { CraftCommonComponent } from './base-craft/craft-common/craft-common.component';
import {MatDialogModule} from '@angular/material/dialog';
import {AplicationPhasesModule} from '../services/auth/aplication-phases.module';
import { CraftInfoComponent } from './base-craft/craft-info/craft-info.component';


@NgModule({
  declarations: [
    SpecialComponent,
    BaseCraftComponent,
    MainComponent,
    CategoryBarComponent,
    CraftDetailsComponent,
    CraftCommonComponent,
    CraftInfoComponent
  ],
  imports: [
    NgxPaginationModule,
    CommonModule,
    SpecialRoutingModule,
    MatDialogModule,
    AplicationPhasesModule
  ],
  providers: [
    SpecialService,
  ]
})
export class SpecialModule {
}
