import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MDBBootstrapModule} from 'angular-bootstrap-md';
import {NavAppComponent} from './topbar/nav-app/nav-app.component';
import {NavAppDirective} from './topbar/nav-app/nav-app.directive';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatSidenavModule} from '@angular/material/sidenav';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MatListModule} from '@angular/material/list';
import {SishikFooterAppComponent} from './sishik-footer-app/sishik-footer-app.component';
import {LiveChatComponent} from './sishik-footer-app/live-chat/live-chat.component';
import {ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {TopbarComponent} from './topbar/topbar.component';
import {TopbarHomesModule} from './topbar/topbar-homes.module';
import {ToastrModule} from 'ngx-toastr';
import {SharedModuleLogin} from './services/auth/login.module';
import {LoginComponent} from './login/login.component';
import {NewauthformComponent} from './login/newauthform/newauthform.component';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {AppdialogComponent} from './login/appdialog/appdialog.component';
import {MatTooltipModule} from '@angular/material/tooltip';
import {LoginViewPasswordModule} from './login/login-view-password.module';
import {FailedPageComponent} from './templated/failed-page/failed-page.component';
import {ThankPageComponent} from './templated/thank-page/thank-page.component';
import {ModalDataServiceModule} from './services/accounts/modal-data-service.module';
import {HintsModule} from './services/hints-module';
import {MatDialogModule} from '@angular/material/dialog';
import {NgxSpinnerModule} from 'ngx-spinner';
import {TextMaskModule} from 'angular2-text-mask';
import {GoogleAnalyticsService} from './services/google/google-analytics.service';
import {FacebookPixelService} from './services/facebook/FacebookPixelService';
import {QuestDescModule} from './services/leave-request/quest-desc/quest-desc.module';
import {WebGameModule} from './web-game/web-game.module';
import { NewYearComponent } from './templated/new-year/new-year.component';

@NgModule({
  declarations: [
    AppComponent,
    NavAppComponent,
    NavAppDirective,
    SishikFooterAppComponent,
    LiveChatComponent,
    TopbarComponent,
    LoginComponent,
    NewauthformComponent,
    AppdialogComponent,
    FailedPageComponent,
    ThankPageComponent,
    NewYearComponent
  ],
  imports: [
    TextMaskModule,
    ToastrModule.forRoot(),
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MDBBootstrapModule.forRoot(),
    SharedModuleLogin.forRoot(),
    ModalDataServiceModule.forRoot(),
    HintsModule.forRoot(),
    NgxSpinnerModule,
    LoginViewPasswordModule,
    MatDialogModule,
    MatToolbarModule,
    MatButtonModule,
    MatIconModule,
    MatSidenavModule,
    FlexLayoutModule,
    MatListModule,
    ReactiveFormsModule,
    TopbarHomesModule,
    MatProgressSpinnerModule,
    MatTooltipModule,
    QuestDescModule,
    WebGameModule
  ],
  providers: [GoogleAnalyticsService, FacebookPixelService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
