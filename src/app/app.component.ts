import {AfterViewInit, Component, Inject, OnDestroy, OnInit, PLATFORM_ID} from '@angular/core';
import {DOCUMENT, isPlatformBrowser, isPlatformServer} from '@angular/common';
import {ChatRoomService} from './services/chat-room/chat-room.service';
import {SpinnerService} from './services/spinner.service';
import {TariffService} from './services/tariff/tariff.service';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {NgxSpinnerService} from 'ngx-spinner';
import {Router} from '@angular/router';
import {Meta, MetaDefinition, Title} from '@angular/platform-browser';
import {environment} from '../environments/environment';
import {GoogleAnalyticsService} from './services/google/google-analytics.service';
import {FacebookPixelService} from './services/facebook/FacebookPixelService';
import {WebGameService} from './services/web-game.service';

declare var $: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, AfterViewInit, OnDestroy {
  title = '??';
  isAppChat = false;
  destroyed$ = new Subject<void>();
  public spinnerName = 'baseSpinner';

  constructor(
    public spinnerS: SpinnerService,
    private router: Router,
    private ngxSpinnerService: NgxSpinnerService,
    private titleService: Title, private metaService: Meta,
    private facebookPixelService: FacebookPixelService,
    private googleAnalyticsService: GoogleAnalyticsService,
    @Inject(DOCUMENT) private document: Document,
    private tariff: TariffService,
    private chatRoomService: ChatRoomService,
    @Inject(PLATFORM_ID) private platformId) {
    if (isPlatformBrowser(this.platformId)) {
      this.appendGaTrackingCode();
    }
    if (isPlatformServer(this.platformId)) {
    }
  }


  ngAfterViewInit(): void {
    console.log('nAft1');
  }

  ngOnInit(): void {

    this.titleService.setTitle(this.title);
    const keywords: MetaDefinition = {
      name: 'keywords',
      content: 'сыщик, сисик, сисхик, сыщик инфо, сисхик инфо, сыщик работа, сыщик недвижимость, сыщик найти работу, сыщик найти авто, машину, найти машину, сыщик найти машину, сыщик купить, сыщик купить авто, сыщик найти услугу, сыщик товары, сыщик найти товары, сыщик найти, csobr, csobr byaj, csobrbyaj, квартира, машина, авто, купить, аренда, снять, комната, гараж, работа'
    };
    const description: MetaDefinition = {
      name: 'description',
      content: 'Я ищу товары по вашему запросу. Чтобы найти выгодное предложение, я использую лучшие интернет-сервисы Украины.'
    };
    this.metaService.updateTag(keywords);
    this.metaService.updateTag(description);

    this.spinnerS.visibility.pipe(takeUntil(this.destroyed$)).subscribe((valSpinner) => {
      (valSpinner)
        ? this.ngxSpinnerService.show(this.spinnerName)
        : this.ngxSpinnerService.hide(this.spinnerName);
    });

    this.tariff.getTypeTariff().pipe(takeUntil(this.destroyed$)).subscribe(() => {
    });
    this.chatRoomService.getChatRoomState().subscribe((chatState) => {
      this.isAppChat = chatState;
    });


    if (isPlatformBrowser(this.platformId)) {
      this.buttonUp();
    }
  }

  isApp($event) {
    // this.chatRoomService.setChatRoomState($event);
  }

  buttonUp() {
    if (isPlatformBrowser(this.platformId)) {
      // tslint:disable-next-line:only-arrow-functions
      $(document).ready(function() {


        // hide #back-top first
        $('#back-top').hide();

        // fade in #back-top
        // tslint:disable-next-line:only-arrow-functions
        $(function() {
          $(window).scroll(function() {
            if ($(this).scrollTop() > 120) {
              $('#back-top').fadeIn();
            } else {
              $('#back-top').fadeOut();
            }
          });

          // scroll body to 0px on click
          // tslint:disable-next-line:only-arrow-functions
          $('#back-top a').click(function() {
            $('body,html').animate({
              scrollTop: 0
            }, 800);
            return false;
          });
        });

      });
    }
  }

  private appendGaTrackingCode() {
    try {
      const script = document.createElement('script');
      script.innerHTML = `
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
        ga('create', '` + environment.google_analytics_key + `', 'auto');
      `;
      document.head.appendChild(script);
    } catch (ex) {
      console.error('Error appending google analytics');
      console.error(ex);
    }
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }
}
