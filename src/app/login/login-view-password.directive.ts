import {Directive, ElementRef, Input, Renderer2} from '@angular/core';

@Directive({
  selector: '[appLoginViewPassword]'
})
export class LoginViewPasswordDirective {

  @Input('app-view-password') isChange: boolean;
  private readonly currentElement: HTMLElement;

  constructor(private element: ElementRef, private renderer: Renderer2) {
    this.currentElement = element.nativeElement;
  }

  protected ngOnChanges() {
    this.renderer.setAttribute(this.currentElement, 'type', (this.isChange) ? 'password' : 'text');
  }

}
