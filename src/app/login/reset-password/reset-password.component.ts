import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {LoginService} from '../../services/auth/login.service';
import {ToastrService} from 'ngx-toastr';
import {SpinnerService} from '../../services/spinner.service';
import {map, publishReplay, refCount} from 'rxjs/operators';
import {MustMatch} from '../../shared/other-help';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {

  // tslint:disable-next-line:no-output-on-prefix
  @Output() onChanged = new EventEmitter<any>();
  resetAuth: FormGroup;

  public isChangePassword = false;

  private cleanPhone: number;

  public passwordView = true;
  public passwordViewC = true;
  public areaCodeMask = ['(', /[0-9]/, /\d/, /\d/, ')', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];

  public isPasswordResetCode = false;
  // tslint:disable-next-line:variable-name
  public isPasswordSecret_key = false;

  // tslint:disable-next-line:variable-name
  constructor(private _fb: FormBuilder,
              private loginService: LoginService,
              private spinnerService: SpinnerService,
              private toastr: ToastrService) {
  }

  isResetForm() {
    // this.onChanged.emit('0');
    this.loginService.setIsLoginForm('0');
  }

  get phone() {
    return this.resetAuth.get('phone');
  }

  get newPassword() {
    return this.resetAuth.get('newPassword');
  }

  get confirmPassword() {
    return this.resetAuth.get('confirmPassword');
  }

  get secret_key() {
    return this.resetAuth.get('secret_key');
  }

  isPasswordSecretKey() {
    if (this.isPasswordResetCode) {
      this.isPasswordResetCode = !this.isPasswordResetCode;
      this.isPasswordSecret_key = true;
      this.loginService.esputnicResetKey(this.cleanPhone)
        // .pipe(takeUntil(componentDestroyed(this)))
        .subscribe((resRequest) => {
          if (resRequest) {
            this.toastr.info('Введите код сброса пароля из смс уведомления', '', {timeOut: 7000});
            this.isPasswordSecret_key = true;
          } else {
            this.isPasswordSecret_key = false;
          }
        }, error => {
          this.isPasswordSecret_key = false;
          this.toastr.error('Ошибка получения кода сброса пароля!', '', {
            timeOut: 3000,
          });
        });

    } else {
      this.isPasswordSecret_key = false;
    }
  }
  public createNewPassword() {

    const secretKey: any = this.resetAuth.controls.secret_key.value;
    const passwordN: any = this.resetAuth.controls.newPassword.value;
    if (this.isChangePassword && this.cleanPhone && secretKey) {
      this.loginService.esputnicUpdatePassword(this.cleanPhone, secretKey, passwordN)
        // .pipe(takeUntil(componentDestroyed(this)))
        .subscribe((result) => {
          if (result.status == 200) {
            this.toastr.success('Пароль обновлен', '', {timeOut: 5000});
            this.resetAuth.reset();
            const elementById = document.getElementById('toLog');
            elementById.click();
          } else {
            this.toastr.error('Ошибка. Повторите попытку позже', '', {timeOut: 5000});
          }
        }, error => {
          this.toastr.error('Ошибка. Повторите попытку позже', '', {timeOut: 5000});
        });
    } else {
      this.toastr.error('Ошибка. Повторите попытку позже', '', {timeOut: 5000});
      this.isChangePassword = false;
    }

    // esputnicUpdatePassword
  }
  public esputnicValidkeys() {

    if (this.isPasswordSecret_key && this.cleanPhone) {
      this.isPasswordSecret_key = false;
      const secretKey: any = this.resetAuth.controls.secret_key.value;
      if (secretKey) {
        this.loginService.esputnicValidkey(this.cleanPhone, secretKey)
          // .pipe(takeUntil(componentDestroyed(this)))
          .subscribe((result) => {
            if (result.msg === 'code_timed_out') {
              this.toastr.info('Время действия кода сброса пароля истекло', '', {timeOut: 7000});
              this.isChangePassword = false;
              this.resetAuth.reset();
            } else if (result.msg === 'ok') {
              this.isChangePassword = true;
              this.toastr.info('Введите новый пароль', '', {timeOut: 7000});
            }
          }, error => this.isChangePassword = false);
      } else {
        this.toastr.error('Ошибка. Повторите попытку позже', '', {timeOut: 5000});
        this.isChangePassword = false;
      }

    } else {
      this.isChangePassword = false;
      this.isPasswordSecret_key = false;
    }

  }

  resetMain() {
    this.resetAuth = this._fb.group({
      phone: [null, Validators.required],
      newPassword: [null, Validators.compose([Validators.minLength(5), Validators.required])],
      confirmPassword: [null, Validators.compose([Validators.minLength(5), Validators.required])],
      secret_key: [null, Validators.compose([Validators.required, Validators.minLength(6)])],
    }, {validator: MustMatch('newPassword', 'confirmPassword')});

    this.resetAuth.controls.phone.valueChanges
      .pipe(
        // takeUntil(componentDestroyed(this)),
        map((phone) => {
          if (phone) {
            const phoneCl = phone.replace(/[^\d;]/g, '');
            this.cleanPhone = phoneCl;
            return phoneCl;
          } else {
            return false;
          }
        }))
      .subscribe((phone) => {
        const phoneNumber = phone;
        const phoneNumberLength = phoneNumber.toString().length;

        if (phoneNumberLength === 10) {

          this.loginService.checkEsputnicPhone(phoneNumber)
            .pipe(
              publishReplay(1),
              refCount(),
              // takeUntil(componentDestroyed(this))
            )
            .subscribe((resCheck) => {
              if (resCheck['msg'] === 'user_exist') {
                this.toastr.info('Подтвердите смену пароля', '', {timeOut: 7000});
                this.isPasswordResetCode = true;
              } else {
                this.isPasswordResetCode = false;
                this.toastr.warning('Пользователь с таким номером не найден', '', {timeOut: 5000});
              }
            }, error => {
              this.toastr.error('Ошибка номера телефона!', '', {
                timeOut: 3000,
              });
              this.isPasswordResetCode = false;
            });

        } else {
          this.isPasswordResetCode = false;
        }

      });
  }
  ngOnInit(): void {
    this.resetMain();
  }

}
