import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LoginViewPasswordDirective} from './login-view-password.directive';
import {ResetPasswordComponent} from './reset-password/reset-password.component';
import {MDBRootModule} from 'angular-bootstrap-md';
import {MatTooltipModule} from '@angular/material/tooltip';
import {ReactiveFormsModule} from '@angular/forms';
import {TextMaskModule} from 'angular2-text-mask';


@NgModule({
  declarations: [LoginViewPasswordDirective, ResetPasswordComponent],
  imports: [
    TextMaskModule,
    CommonModule,
    MDBRootModule,
    MatTooltipModule,
    ReactiveFormsModule
  ],
  exports: [LoginViewPasswordDirective, ResetPasswordComponent]
})
export class LoginViewPasswordModule {
}
