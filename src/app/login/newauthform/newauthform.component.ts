import {Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {LoginService} from '../../services/auth/login.service';
import {ToastrService} from 'ngx-toastr';
import {SpinnerService} from '../../services/spinner.service';
import {AuthGuardService} from '../../services/login/auth-guard.service';
import {NgxSpinnerService} from 'ngx-spinner';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

@Component({
  selector: 'app-newauthform',
  templateUrl: './newauthform.component.html',
  styleUrls: ['./newauthform.component.scss'],
  animations: [
    trigger('success', [
      transition('void => *', [
        style({transform: 'scale3d(.3, .3, .3)'}),
        animate(350)
      ]),
      transition('* => void', [
        animate(350, style({transform: 'scale3d(.0, .0, .0)'}))
      ])
    ]),
    trigger('slideUpDown', [
      state('0', style({opacity: 1})),
      state('1', style({opacity: 0})),
      transition(':enter', animate('400ms ease-in-out')),
      transition('* => *', animate('400ms ease-in-out')),
    ])
  ]
})
export class NewauthformComponent implements OnInit, OnDestroy {
  destroyed$ = new Subject<void>();
  isResetForm: any = '0';
  title = 'Вход в личный кабинет';
  loginForm: FormGroup;
  public passwordView = true;
  public areaCodeMask = ['(', /[0-9]/, /\d/, /\d/, ')', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
  private messagesGetingPass = 'Сегодня осуществлялся вход в аккаунт. Введите ранее полученный код из смс.';
  private regExp = new RegExp(/[-()/\\]/g);
  @Output() closeDialog: any = new EventEmitter();

  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private loginService: LoginService,
              private toastr: ToastrService,
              private spinnerService: SpinnerService,
              private closeFormService: AuthGuardService
  ) {
  }

  ngOnInit(): void {
    this.loginService.getIsLoginForm()
      .pipe(takeUntil(this.destroyed$))
      .subscribe((isLForm) => {
        this.title = (isLForm == '0') ? 'Вход в личный кабинет' : 'Востановление пароля';
        this.isResetForm = isLForm;
      });

    this.formInit();
    this.closeFormService.getCloseLoginForm()
      .pipe(takeUntil(this.destroyed$))
      .subscribe(value => {
          this.loginForm.reset();
        }
      );
  }

  formInit() {
    this.loginForm = this.formBuilder.group({
      phone: [null, Validators.required],
      password: [null, Validators.required]
    });
  }

  onSubmit() {
    if (this.loginForm.invalid) {
      return;
    }
    this.login();
  }

  get phone() {
    return this.loginForm.get('phone');
  }

  get password() {
    return this.loginForm.get('password');
  }

  login() {
    this.spinnerService.show();
    setTimeout(function() {
      this.loading = true;
      this.loginService.login(this.loginForm.get('phone')
        .value.replace(this.regExp, ''), this.loginForm.get('password').value)
        .pipe(takeUntil(this.destroyed$))
        .subscribe(result => {
            if (result === true) {
              this.closeDialog.emit(false);
              this.router.navigate(['/account']);
              this.spinnerService.hide();
            } else {
              this.loading = false;
              this.spinnerService.hide();
            }
          }, error => {
            // this.showSuccess();
            // this.authForm.control;
            this.error = 'Не верный пароль!';
            this.loginForm.controls.password.patchValue('');
            this.toastr.error('Не верный пароль!', '', {
              timeOut: 3000,
            });
            this.spinnerService.hide();
          },
          () => {
            this.toastr.success('Добро пожаловать!', '', {
              timeOut: 3000,
            });
            this.loginForm.reset();
            this.spinnerService.hide();
          });
    }.bind(this), 700);
  }

  isResetForms() {
    this.loginService.setIsLoginForm('1');
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }

}
