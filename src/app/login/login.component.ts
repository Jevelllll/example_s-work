import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  @Output() closeD: any = new EventEmitter();

  closeDialog(event) {
    this.closeD.emit(event);
  }

  constructor() {
  }

  ngOnInit() {
  }

}
