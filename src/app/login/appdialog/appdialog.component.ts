import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {animate, style, transition, trigger} from '@angular/animations';
import {AuthGuardService} from '../../services/login/auth-guard.service';

@Component({
  selector: 'app-appdialog',
  templateUrl: './appdialog.component.html',
  styleUrls: ['./appdialog.component.scss'],
  animations: [
    trigger('dialog', [
      transition('void => *', [
        style({transform: 'scale3d(.3, .3, .3)'}),
        animate(350)
      ]),
      transition('* => void', [
        animate(350, style({transform: 'scale3d(.0, .0, .0)'}))
      ])
    ])
  ]
})
export class AppdialogComponent implements OnInit {
  @Input() closable = true;
  @Input() visible: boolean;
  @Output() visibleChange: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(private closeForm: AuthGuardService) {
  }

  ngOnInit(): void {
  }

  close() {
    this.closeForm.closeLogForm(true);
    this.visible = false;
    this.visibleChange.emit(this.visible);
  }
}
