import { NgModule } from '@angular/core';
import {WebGameComponent} from './web-game.component';
import { CommonModule } from '@angular/common';
import {NewGameDirective} from './new-game.directive';

@NgModule({
  declarations: [WebGameComponent, NewGameDirective],
  imports: [
    CommonModule,
  ],
  exports: [WebGameComponent],
})
export class WebGameModule { }
