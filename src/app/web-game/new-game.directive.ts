import {Directive, ElementRef, Inject, Input, OnInit, Renderer2} from '@angular/core';
import {DOCUMENT} from '@angular/common';

@Directive({
  selector: '[appRandomPlace]',
  host: {'(click)': 'hClick()'}
})
export class NewGameDirective implements OnInit {
  private element: HTMLElement;
  private isStart = false;
  private interval: any;

  constructor(private el: ElementRef, private renderer: Renderer2, @Inject(DOCUMENT) private document: Document) {
    this.element = el.nativeElement;
    this.pd();
  }

  ngOnInit(): void {

  }

  private initInterval() {
    this.interval = setInterval(() => {
      this.pd();
    }, 4000);
  }

  private stopInterval() {
    if (this.interval) {
      clearInterval(this.interval);
    }
  }

  private pd() {
    setTimeout(() => {
      const matDrSelector: any = document.querySelector('.mat-drawer-backdrop');
      if (matDrSelector) {
        // const collectionsChildDiv = [].slice.call(matDrSelector.querySelectorAll('div'));
        // const randomDiv = this.random(collectionsChildDiv);
        const matDrSelectorHeight: any = matDrSelector.offsetHeight;
        const matDrSelectorWidth: any = matDrSelector.offsetWidth;

        const elWidth: any = this.element.offsetWidth;
        const elHeight: any = this.element.offsetHeight;

        const heightMax: any = matDrSelectorHeight - elHeight;
        const widthMax: any = matDrSelectorWidth - elWidth;

        const left = Math.floor(Math.random() * widthMax);
        const top = Math.floor(Math.random() * heightMax);

        // this.renderer.appendChild(randomDiv, this.element);

        this.renderer.setStyle(this.element, 'left', `${left}px`);
        this.renderer.setStyle(this.element, 'top', `${top}px`);
      }
    }, 60);
  }

  private random(items) {
    return items[Math.floor(Math.random() * items.length)];
  }

  public hClick() {
    this.pd();
  }

  @Input() set __change(value: boolean) {
    this.pd();
  }

  @Input() set __isStart(val: boolean) {
    this.isStart = val;
    if (val) {
      this.initInterval();
    } else {
      this.stopInterval();
    }
  }
}
