import {Component, OnDestroy, OnInit} from '@angular/core';
import {WebGameService} from '../services/web-game.service';
import {Subject} from 'rxjs/internal/Subject';
import {takeUntil} from 'rxjs/internal/operators/takeUntil';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-web-game',
  templateUrl: './web-game.component.html',
  styleUrls: ['./web-game.component.scss']
})
export class WebGameComponent implements OnInit, OnDestroy {
  public result: any;
  public date = new Date().valueOf();
  public isLogin;
  public isStart: Observable<boolean> = new Observable<boolean>();
  // public isStart = false;
  destroyed$ = new Subject<void>();

  constructor(private webGameService: WebGameService) {
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }

  ngOnInit(): void {
    this.isStart = this.webGameService.obsIsStartGame;
    this.webGameService.obsDataGame.pipe(takeUntil(this.destroyed$))
      .subscribe((result) => {
        if (result) {
          this.result = result.link.link_id;
        }
      }, error => {
      });
  }

  notify() {
    this.webGameService.timerIsStart(false);
    this.webGameService.notifyGame(this.result).pipe(takeUntil(this.destroyed$)).subscribe((d) => {
      if (d['status'] === 401) {
        this.webGameService.timerIsStart(false);
      } else {
        if (d['link']['count_game'] === 0) {
          this.webGameService.timerIsStart(false);
        } else {
          this.webGameService.timerIsStart(true);
          this.webGameService.addDataGame(d);
        }
      }
    }, error => {
      this.webGameService.timerIsStart(false);
    });
  }

  private detectGame(gameMap) {
    if (gameMap) {
      console.log(gameMap);
      const fullLinkHost = `${window.location.href}/`;
      const fullLinkHref = window.location.href;
      const isset = [fullLinkHost, fullLinkHref].map((item) => {
        if (gameMap.indexOf(item) !== -1) {
          return item;
        }
      }).filter(it => it).shift();
      return isset;
    }
    return false;
  }
}
