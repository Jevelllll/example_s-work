import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Subject} from 'rxjs';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-board-rules',
  templateUrl: './board-rules.component.html',
  styleUrls: ['./board-rules.component.scss']
})
export class BoardRulesComponent implements OnInit {
  destroyed$ = new Subject<void>();
  public onClose: Subject<boolean>;
  public boardForm: FormGroup;
  align = '';

  // tslint:disable-next-line:variable-name
  constructor(private toastr: ToastrService) {
  }

  public onCancel(): void {
    this.onClose.next(false);
    // this._bsModalRef.hide();
  }

  public onConfirm(): void {
    this.onClose.next(true);
    // this._bsModalRef.hide();
  }

  ngOnInit() {

    setTimeout(() => {
      this.onClose = new Subject();
      this.boarfRulFormInit();
    }, 0);

  }

  public save(value, valid) {

    let msg = '';

    if (value.align === 'read') {

      msg = 'Cпасибо! за то, что вы уделили время и прочли наши правила';
      this.toastr.success(msg, '', {
        timeOut: 3000,
      });
    } else if (value.align === 'skip') {

      msg = 'Рекомендуем ознакомиться с нашими правилами';
      this.toastr.warning(msg, '', {
        timeOut: 3000,
      });

    }

    this.onCancel();
    this.boardForm.reset();
  }

  private boarfRulFormInit() {

    this.boardForm = new FormGroup({
      align: new FormControl(this.align, [Validators.required as any]),
    });
  }

}
