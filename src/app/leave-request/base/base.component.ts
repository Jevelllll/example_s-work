import {AfterViewInit, ChangeDetectorRef, Component, ElementRef, OnDestroy, OnInit, ViewChild, ViewChildren} from '@angular/core';
import {Router} from '@angular/router';
import {AuthGuardService} from '../../services/login/auth-guard.service';
import {Meta, MetaDefinition, Title} from '@angular/platform-browser';
import {LoginService} from '../../services/auth/login.service';
import {shareReplay, takeUntil, withLatestFrom} from 'rxjs/operators';
import {CurrencyTrService} from '../../services/currency-tr.service';
import {Application_phases, interpreter} from '../../services/auth/application-phases.service';
import {authExit} from '../../shared/authExist';
import {Observable, Subject} from 'rxjs';
import {NgxSpinnerService} from 'ngx-spinner';
import {animate, state, style, transition, trigger} from '@angular/animations';

@Component({
  selector: 'app-base',
  templateUrl: './base.component.html',
  styleUrls: ['./base.component.scss'],
  animations: [
    trigger('openClose', [
      state('open', style({
        opacity: 1,
      })),
      state('closed', style({
        opacity: 0,
        height: 0
      })),
      transition('open => closed', [
        animate('0.3s')
      ]),
      transition('closed => open', [
        animate('0.3s')
      ]),
    ])
  ]
})
export class BaseComponent implements OnInit, AfterViewInit, OnDestroy {
  destroyed$ = new Subject<void>();
  interpreterM = interpreter;
  phaseContainer = [];
  // tslint:disable-next-line:variable-name
  @ViewChild('search_tab', {static: true}) search_tab: ElementRef;
  @ViewChildren('stepper') stepper;
  // hwstepsdata = this.service.getWorks();
  isRegistration = false;

  public isHideStepper: Observable<boolean>;

  focusFunction(i): void {
    let child;
    child = this.search_tab.nativeElement.children;

    for (const ind in child) {
      if (ind !== 'length' && ind !== 'item' && ind !== 'namedItem') {
        if (child[i] === child[ind]) {
          child[i].style.transform = 'translateY(-7px)';
        } else {
          child[ind].style.transform = 'translateY(1px)';
        }
      }
    }
  }

  constructor(private router: Router,
              // private service: StepsService,
              private checkAccount: AuthGuardService,
              private titleService: Title,
              private cd: ChangeDetectorRef,
              public phase: Application_phases,
              private currencyTrService: CurrencyTrService,
              private spinnerService: NgxSpinnerService,
              private meta: Meta, private loginService: LoginService) {
    this.titleService.setTitle('??? создать заявку');
  }

  private metaGenerate() {

    const keywords: MetaDefinition = {
      name: 'keywords',
      content: '?? создать заявку, найти машину, найти планшет, дешевая квартира,' +
        'Сыщик инфо, Создать заявку поиска, Регистрация сыщик инфо, Регистрация ???,' +
        'регистрация на сыщик инфо'
    };
    const description: MetaDefinition = {
      name: 'description',
      content: '??? выберите категорию и создайте заявку'
    };
    this.meta.updateTag(keywords);
    this.meta.updateTag(description);
  }

  ngOnInit() {
    this.currencyTrService.currencyObs
      .pipe(takeUntil(this.destroyed$))
      .subscribe(currency => {
        if (currency) {
        } else {
          this.currencyTrService.getCurrencyList()
            .pipe(takeUntil(this.destroyed$))
            .subscribe(() => {
            });
        }
      });

    authExit(this.loginService, this.router, false);
    this.metaGenerate();
    this.isRegistration = this.checkAccount.checkLogin();
    // this.service.getWorks();
  }

  ngAfterViewInit() {

    setTimeout(() => {
      const message = this.stepper;
      const first: any = message.first;
      first.editable = false;
      this.cd.detectChanges();
      this.phase.get_current_phase()
        .pipe(
          shareReplay({bufferSize: 1, refCount: true}),
          takeUntil(this.destroyed$),
          withLatestFrom(this.loginService.isLogin()))
        .subscribe(([value, isLogin]) => {
          if (isLogin) {
            delete value.phaseContact;
          }
          this.phaseContainer = Object.keys(value).map((field) => {
            return {value: value[field], field};
          });
        });
      window.scrollTo(0, 67);
      this.isHideStepper = this.phase.getHideStepper();
    }, 0);
    const styleDiv: any = document.getElementsByClassName('timeline');
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }
}
