import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {BaseRoutingModule} from './base-routing.module';
import {BaseComponent} from './base.component';
import {CurrencyTrService} from '../../services/currency-tr.service';
import {AplicationPhasesModule} from '../../services/auth/aplication-phases.module';
import {MatStepperModule} from '@angular/material/stepper';
import {BoardRulesComponent} from '../board-rules/board-rules.component';
import {ContactComponent} from '../container/contact/contact.component';
import {ReactiveFormsModule} from '@angular/forms';
import {MDBBootstrapModule} from 'angular-bootstrap-md';
import {MatTooltipModule} from '@angular/material/tooltip';
import {LoginViewPasswordModule} from '../../login/login-view-password.module';
import {OtherProductComponent} from '../container/other-product/other-product.component';
import {OtherProductFormComponent} from '../container/other-product/other-product-form/other-product-form.component';
import {RealPropertyComponent} from '../container/real-property/real-property.component';
import {RealPropertyFormComponent} from '../container/real-property/real-property-form/real-property-form.component';
import {WorkingComponent} from '../container/working/working.component';
import {WorkingFormComponent} from '../container/working/working-form/working-form.component';
import {TransportComponent} from '../container/transport/transport.component';
import {TransportFormComponent} from '../container/transport/transport-form/transport-form.component';
import {SearchModule} from '../container/search/search.module';
import {TariffautoComponent} from '../container/tariff/tariffauto/tariffauto.component';
import {TariffotherComponent} from '../container/tariff/tariffother/tariffother.component';
import {TariffpropertyComponent} from '../container/tariff/tariffproperty/tariffproperty.component';
import {TariffworkComponent} from '../container/tariff/tariffwork/tariffwork.component';
import {BasetariffComponent} from '../container/tariff/basetariff.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {PointerModule} from '../../services/directivs/pointer.module';
import {NgMathPipesModule} from 'ngx-pipes';
import {ExpPipeModule} from '../../services/pipe_export/exp-pipe.module';
import {PriceAppModule} from '../../topbar/price-app/price-app.module';
import {NewBaseCommonMComponent} from '../container/new-base-common-m/new-base-common-m.component';
import {MatRadioModule} from '@angular/material/radio';
import {MatSelectModule} from '@angular/material/select';
import {OutputDisabledDirective} from '../../services/directivs/output-disabled-directive';
import {SkipparamsPipe} from '../container/skipparams.pipe';
import {WrapQueryComponent} from '../container/wrap-query/wrap-query.component';
import {PriorityModule} from '../../services/priority/priority.module';
import {MatDialogModule} from '@angular/material/dialog';
import {TextMaskModule} from 'angular2-text-mask';
import {DateAdapter, MAT_DATE_LOCALE, MatNativeDateModule} from '@angular/material/core';
import {MyDateAdapter} from '../container/tariff/my-date-adapter';
import {NgxSpinnerModule} from 'ngx-spinner';
import {HelpSishikModule} from '../../templated/help-sishik/help-sishik.module';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {BaseAccountModule} from '../../account/base-account/base-account.module';
import {SkipFilterDirective} from '../container/other-product/skip-filter.directive';
import {LegalComponent} from '../container/legal/legal.component';
import {LegalFormComponent} from '../container/legal/legal-form/legal-form.component';
import {TarifflegalComponent} from '../container/tariff/tarifflegal/tarifflegal.component';
import {LeaveUnsetSkipServicesPipe} from '../../services/leave-unset-skip-services.pipe';
import {QuestDescModule} from '../../services/leave-request/quest-desc/quest-desc.module';
import {TariffquestComponent} from '../container/tariff/tarriffquest/tariffquest.component';
import {QuestComponent} from '../container/quest/quest.component';
import {QuestFormComponent} from '../container/quest/quest-form/quest-form.component';
import {DialogConfirmComponent} from '../container/contact/dialog-confirm/dialog-confirm.component';

@NgModule({
  declarations: [
    BaseComponent,
    BoardRulesComponent,
    ContactComponent,
    OtherProductComponent,
    OtherProductFormComponent,
    RealPropertyComponent,
    RealPropertyFormComponent,
    WorkingComponent,
    WorkingFormComponent,
    TransportComponent,
    TransportFormComponent,
    TariffautoComponent,
    TariffotherComponent,
    TariffpropertyComponent,
    TariffworkComponent,
    TarifflegalComponent,
    TariffquestComponent,
    BasetariffComponent,
    LegalComponent,
    LegalFormComponent,
    QuestFormComponent,
    NewBaseCommonMComponent,
    OutputDisabledDirective,
    SkipparamsPipe,
    WrapQueryComponent,
    SkipFilterDirective,
    LeaveUnsetSkipServicesPipe,
    QuestComponent,
    DialogConfirmComponent
  ],
  imports: [
    PriorityModule,
    CommonModule,
    BaseRoutingModule,
    MatNativeDateModule,
    AplicationPhasesModule,
    MatStepperModule,
    ReactiveFormsModule,
    MDBBootstrapModule,
    MatTooltipModule,
    LoginViewPasswordModule,
    SearchModule,
    MatFormFieldModule,
    MatInputModule,
    MatCheckboxModule,

    MatDatepickerModule,
    PointerModule,
    NgMathPipesModule,
    ExpPipeModule,
    PriceAppModule,
    MatRadioModule,
    MatSelectModule,
    MatDialogModule,
    TextMaskModule,
    NgxSpinnerModule,
    HelpSishikModule,
    MatProgressSpinnerModule,
    BaseAccountModule,
    QuestDescModule
  ],
  providers: [
    CurrencyTrService,
    {provide: DateAdapter, useClass: MyDateAdapter},
    {provide: MAT_DATE_LOCALE, useValue: 'ru-Ru'},
  ]
})
export class BaseModule {
}
