import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AuthGuardService} from '../../services/login/auth-guard.service';
import {SearchComponent} from '../container/search/search.component';
import {BaseComponent} from './base.component';
import {TransportComponent} from '../container/transport/transport.component';
import {RealPropertyComponent} from '../container/real-property/real-property.component';
import {OtherProductComponent} from '../container/other-product/other-product.component';
import {WorkingComponent} from '../container/working/working.component';
import {LegalComponent} from '../container/legal/legal.component';
import {QuestComponent} from '../container/quest/quest.component';


const routes: Routes = [ {
  path: '', component: BaseComponent,
  children: [
    {
      path: '',
      redirectTo: 'search',
      pathMatch: 'full',
      canActivate: [AuthGuardService]
    },
    {
      path: 'search',
      component: SearchComponent,
      children: [
        {
          path: '',
          redirectTo: 'auto',
          pathMatch: 'full',
        },
        {
          path: 'auto',
          component: TransportComponent,
        },
        {
          path: 'auto/:type_id',
          component: TransportComponent,
        },
        {
          path: 'property',
          component: RealPropertyComponent
        },
        {
          path: 'property/:type_id',
          component: RealPropertyComponent
        },
        {
          path: 'other',
          component: OtherProductComponent
        },
        {
          path: 'work',
          component: WorkingComponent
        },
        {
          path: 'work/:type_id',
          component: WorkingComponent
        },
        {
          path: 'legal',
          component: LegalComponent
        },
        {
          path: 'quest',
          component: QuestComponent
        }
      ]
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BaseRoutingModule { }
