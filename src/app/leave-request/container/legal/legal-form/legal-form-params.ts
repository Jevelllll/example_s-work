import {Validators} from '@angular/forms';

export class LegalFormParams {

  static CONTROLSCONFIG = {
    query_category_legal: [null, Validators.compose([Validators.required])],
    query_region: [0],
  };
}

/**********************игициализация ранее введенными данными первой формы*************************/
export function predefinedData() {
  const dataPreDef = this.phasesService.phaseContainer;
  const serviceSelected = [];
  if (Array.isArray(dataPreDef)) {
    dataPreDef.forEach((element, index) => {
      this.serviceSelected[index] = this.phasesService.preDefinedService(element, serviceSelected, index);
      const formGroup = this.currentFormArray(index);
      Object.keys(element).forEach((field) => {
        const control = formGroup.get(field);
        try {
          control.setValue(element[field]);
        } catch (e) {
        }
      });
    });
  }

}
