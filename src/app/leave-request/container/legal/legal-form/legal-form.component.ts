import {AfterViewInit, Component, Input, OnDestroy, OnInit, Renderer2} from '@angular/core';
import {animate, style, transition, trigger} from '@angular/animations';
import {Observable, Subject} from 'rxjs';
import {ServiceListModel} from '../../../../services/auth/ServiceListModel';
import {FormArray, FormBuilder, FormGroup} from '@angular/forms';
import {PriorityLegalService} from '../../../../services/priority/legal.service';
import {LegalCategory, LegalCity, LegalRegion} from '../../../../services/priority/resModel';
import {LegalFormParams, predefinedData} from './legal-form-params';
import {LoginService} from '../../../../services/auth/login.service';
import {CommonService} from '../../../../services/common.service';
import {addNewForm, Application_phases} from '../../../../services/auth/application-phases.service';

@Component({
  selector: 'app-legal-form',
  templateUrl: './legal-form.component.html',
  styleUrls: ['./legal-form.component.scss'],
  animations: [
    trigger('dialog', [
      transition('void => *', [
        style({transform: 'scale3d(.1, .1, .1)'}),
        animate(50)
      ])
    ])
  ]
})
export class LegalFormComponent implements OnInit, AfterViewInit, OnDestroy {
  destroyed$ = new Subject<void>();
  cTitle = [];
  public wrapIndex: any;
  public tittle = null;
  public serviceSelected = [];
  public serviceList: ServiceListModel[] = [];
  private CATEGORY_NAME = 'legal';
  public JSON = JSON;
  public Object = Object;
  public Number = Number;
  public myForm: FormGroup;
  public items: FormArray;
  @Input() count = 1;
  public formOutputId = 0;
  public disabled = [];
  public typeNameBlocked = [];
  public InvoiceButtonHide = true;
  skipFilterWords: any = [];
  Array: any = Array;
  public patternService = [];
  public hoverShadow = false;
  public repeatLegalCityMore: Observable<LegalCity>;
  public repeatLegalCategoryMore: Observable<LegalCategory>;
  public repeatLegalRegionMore: Observable<LegalRegion>;

  constructor(
    // tslint:disable-next-line:variable-name
    private _fb: FormBuilder,
    private login: LoginService,
    private phasesService: Application_phases,
    private commonService: CommonService, private renderer: Renderer2,
    public priority: PriorityLegalService) {
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }

  ngOnInit(): void {
    this.repeatLegalCityMore = this.priority.getRepeatLegalCityMore();
    this.repeatLegalCategoryMore = this.priority.getRepeatLegalCategoryMore();

    this.repeatLegalRegionMore = this.priority.getRepeatLegalRegionMore();
    this.repeatLegalRegionMore.subscribe(v => {});

    for (let index = 0; index < this.count; index++) {
      if (index === this.count - 1) {
        this.cTitle[index] = 'Подтвердить';
      } else {
        this.cTitle[index] = 'Следующая заявка';
      }
    }

    this.formInitialize();

  }

  private createItem() {
    return this._fb.group(LegalFormParams.CONTROLSCONFIG);
  }

  addItem(): void {
    this.items = this.myForm.get('items') as FormArray;
    this.items.push(this.createItem());
  }

  private formInitialize() {
    this.myForm = this._fb.group({title: 'legal', items: this._fb.array([])});
    for (let index = 1; index <= this.count; index++) {
      this.disabled[index - 1] = true;
      this.serviceSelected.push({});
      this.addItem();
      // const skipFilterWo: any = new Set();
      this.skipFilterWords[index - 1] = new Set();
    }
  }

  public activeFromWrapQuery(data) {
    if (data) {
      this.formOutputId = data - 1;
    }
  }

  /**********работа с экземпляром форм******************/
  addNewForm(indexForm: number, confirm) {

    const preObj: any = {};
    const preForm: FormGroup = this.currentFormArray(indexForm);
    preForm.patchValue(preObj);

    addNewForm.apply(this, [indexForm]);
    if (indexForm === this.count - 1) {
      this.renderer.addClass(confirm, 'confirm_btn_success');
      this.renderer.removeClass(confirm, 'confirm_btn');
    } else {
      this.login.setScroll();
    }
  }

  /**********************игициализация ранее введенными данными первой формы*************************/
  predefinedData() {
    predefinedData.apply(this);
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      this.predefinedData();
    }, 0);
  }

  private currentFormArray(index) {
    const arrayControls = this.myForm.get('items') as FormArray;
    return arrayControls.controls[index] as FormGroup;
  }
}
