import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {DynamicInterface} from '../dynamicInterface';
import {Subject} from 'rxjs';
import {Application_phases} from '../../../services/auth/application-phases.service';
import {ConstingService} from '../../../services/consting/consting.service';
import {takeUntil} from 'rxjs/operators';
import {ContactComponent} from '../contact/contact.component';
import {BasetariffComponent} from '../tariff/basetariff.component';
import {LegalFormComponent} from './legal-form/legal-form.component';
import {TarifflegalComponent} from '../tariff/tarifflegal/tarifflegal.component';

@Component({
  selector: 'app-legal',
  templateUrl: './legal.component.html',
  styleUrls: ['./legal.component.scss']
})
export class LegalComponent implements OnInit, AfterViewInit, OnDestroy, DynamicInterface {
  destroyed$ = new Subject<void>();
  @ViewChild('dynamic', {static: true, read: ViewContainerRef}) dynamic: ViewContainerRef;
  public count = 1;

  constructor(private phasesService: Application_phases, private consting: ConstingService) {
  }

  ngOnInit(): void {
    this.consting.getTarrif()
      .pipe(takeUntil(this.destroyed$))
      .subscribe((tariff) => {
        if (tariff) {
          // tslint:disable-next-line:radix
          this.count = (Number.parseInt(tariff['isUnlimited']) === 1) ? 1 : 5;
        }
      });

    this.phasesService.get_current_phase()
      .pipe(takeUntil(this.destroyed$))
      .subscribe((currentPhase) => {
        if (currentPhase) {
          let goM = Object.keys(currentPhase).filter(it => currentPhase[it])[0];
          goM = `${goM}`;
          try {
            this[goM]();
          } catch (e) {
          }
        }
      });
  }

  ngAfterViewInit(): void {
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }

  phaseContact(): void {
    this.phasesService.customFactory(this.dynamic, ContactComponent);
  }

  phaseDataPay(): void {
    this.phasesService.customFactory(this.dynamic, BasetariffComponent);
  }

  phaseQuery(): void {
    this.phasesService.customFactory(this.dynamic, LegalFormComponent, this.count);
  }

  phaseTariff(): void {
    this.phasesService.customFactory(this.dynamic, TarifflegalComponent);
  }

}
