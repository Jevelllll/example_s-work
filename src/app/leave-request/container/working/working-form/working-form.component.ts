import {AfterViewInit, Component, Input, OnDestroy, OnInit, Renderer2} from '@angular/core';
import {ServiceListModel} from '../../../../services/auth/ServiceListModel';
import {FormArray, FormBuilder, FormGroup} from '@angular/forms';
import {Observable, Subject} from 'rxjs';
import {Application_phases, getServiceList, serviceListSelect, addNewForm} from '../../../../services/auth/application-phases.service';
import {CommonService} from '../../../../services/common.service';
import {ActivatedRoute} from '@angular/router';
import {LoginService} from '../../../../services/auth/login.service';
import {CreateformdataService} from '../../../../services/auth/createformdata.service';
import {PriorityWorkService} from '../../../../services/priority/work.service';
import {WorkingFormParams} from './working-form-params';
import {urlRusLat} from '../../../../shared/helper-container-leave';
import {first, takeUntil} from 'rxjs/operators';
import {animate, style, transition, trigger} from '@angular/animations';
import {SpinnerService} from '../../../../services/spinner.service';

@Component({
  selector: 'app-working-form',
  templateUrl: './working-form.component.html',
  styleUrls: ['./working-form.component.scss'],
  animations: [
    trigger('dialog', [
      transition('void => *', [
        style({transform: 'scale3d(.1, .1, .1)'}),
        animate(50)
      ])
    ])
  ]
})
export class WorkingFormComponent implements OnInit, OnDestroy, AfterViewInit {
  destroyed$ = new Subject<void>();
  cTitle = [];
  public wrapIndex: any;
  public tittle = null;
  public serviceSelected = [];
  public serviceList: ServiceListModel[] = [];
  private CATEGORY_NAME = 'query_work';
  public JSON = JSON;
  public Object = Object;
  public Number = Number;
  public myForm: FormGroup;
  public items: FormArray;
  @Input() count = 1;
  public formOutputId = 0;
  public disabled = [];
  public InvoiceButtonHide = true;
  public hoverShadow = false;
  public patternService = [];
  public priceMin;
  public priceMax;
  public labelStyle = {position: 'relative', left: '20px'};
  public typeNameBlocked = [];

  public typeIdS;
  // tslint:disable-next-line:variable-name
  public $_typeWork: Observable<any> = new Observable();
  // tslint:disable-next-line:variable-name
  public $_headings: Observable<any> = new Observable();
  // tslint:disable-next-line:variable-name
  public $_catHeadings: Observable<any> = new Observable();
  // tslint:disable-next-line:variable-name
  public $_workSchedule: Observable<any[]> = new Observable();
  // tslint:disable-next-line:variable-name
  public $_workCity: Observable<any[]> = new Observable();

  // tslint:disable-next-line:variable-name
  constructor(private _fb: FormBuilder,
              private phasesService: Application_phases,
              private commonService: CommonService,
              private renderer: Renderer2,
              private route: ActivatedRoute,
              private login: LoginService,
              private spinnerService: SpinnerService,
              private toValidservice: CreateformdataService, private priority: PriorityWorkService) {
    this.priceMin = 1;
    this.priceMax = 500000;
  }

  ngOnInit() {
    for (let index = 0; index < this.count; index++) {
      if (index === this.count - 1) {
        this.cTitle[index] = 'Подтвердить';
      } else {
        this.cTitle[index] = 'Следующая заявка';
      }
    }
    this.formInitialize();
    /************список сервисов***************************/
    getServiceList.call(this);
    this.getWorkType();
    this.getHeadings();
    this.getWorkSchedule();
    this.getCity();
  }

  private createItem() {
    return this._fb.group(WorkingFormParams.CONTROLSCONFIG);
  }

  addItem(): void {
    this.items = this.myForm.get('items') as FormArray;
    this.items.push(this.createItem());
  }

  public activeFromWrapQuery(data) {
    if (data) {
      this.formOutputId = data - 1;
    }
  }

  private formInitialize() {
    this.myForm = this._fb.group({title: 'work', items: this._fb.array([])});
    for (let index = 1; index <= this.count; index++) {
      this.disabled[index - 1] = true;
      this.serviceSelected.push({});
      this.addItem();
    }
  }

  // tslint:disable-next-line:variable-name
  public workType(type_id, index, typeName, subjSend = true) {

    this.typeNameBlocked[index] = urlRusLat(typeName);
    if (subjSend) {
      this.phasesService.typeName[index] = {type_id, typeName: urlRusLat(typeName)};
    }
    this.generalViewOfServiceRules(index);
    const formGroup = this.currentFormArray(index);
    if (this.patternService[index]) {
      this.phasesService.lockedFieldConditions(formGroup, this.patternService[index][this.typeNameBlocked[index]]);
    } else {
      this.phasesService.unlockedConditions(formGroup);
    }

  }

  /*****************Выбор Сервисов*******************************************/
  public serviceListSelect(serList, index) {
    serviceListSelect.apply(this, [serList, index]);
    this.typeIdS = this.route.snapshot.paramMap.get('type_id');
    if (this.typeIdS) {
      setTimeout(() => {
        this.currentFormArray(0).get('query_work_type').setValue(Number(this.typeIdS));
      },12);
    }
  }

  private currentFormArray(index) {
    const arrayControls = this.myForm.get('items') as FormArray;
    return arrayControls.controls[index] as FormGroup;
  }

  /**********работа с экземпляром форм******************/
  addNewForm(indexForm: number, confirm) {

    addNewForm.apply(this, [indexForm]);
    if (indexForm === this.count - 1) {
      this.renderer.addClass(confirm, 'confirm_btn_success');
      this.renderer.removeClass(confirm, 'confirm_btn');
    }else {
      this.login.setScroll();
    }
  }

  /********************приведение к общем данным сервисов patterns*******************************/
  generalViewOfServiceRules(index: number) {
    this.patternService[index] = this.phasesService.generalViewOfServiceRules(index, this.serviceSelected, this.serviceList);
  }

  /*************тип предложения********************/
  getWorkType() {
    this.$_typeWork = this.priority.getTypeMore();
  }

  /*************рубрика********************/
  getHeadings() {
    this.$_headings = this.priority.getHeadings();
  }

  /****************подрубрика***************************/
  setCatHeadings(idHeadings, indexForm) {
    this.disabled[indexForm] = false;
    this.$_catHeadings[indexForm] = this.priority.getWorkCategoryHeadings(idHeadings);
  }

  /***************вид занятости**************************************/
  getWorkSchedule() {
    this.$_workSchedule = this.priority.getScheduleMore();
  }

  /***************города***********************************************/
  getCity() {
    this.$_workCity = this.priority.getWorkCitiesMore();
  }

  /**********************игициализация ранее введенными данными первой формы*************************/
  predefinedData() {
    const dataPreDef = this.phasesService.phaseContainer;
    const serviceSelected = [];
    if (Array.isArray(dataPreDef)) {
      dataPreDef.forEach((element, index) => {
        this.serviceSelected[index] = this.phasesService.preDefinedService(element, serviceSelected, index);
        const formGroup = this.currentFormArray(index);
        if (typeof element === 'object') {
          this.phasesService.getTypeName().pipe(first()
            , takeUntil(this.destroyed$)
          ).subscribe((type) => {
            if (type) {
              const tDType = type[index];
              this.workType(tDType.type_id, index, tDType.typeName, false);
              if (element['query_work_headings']) {
                this.setCatHeadings(element['query_work_headings'], index);
              }
              Object.keys(element).forEach((field) => {
                const control = formGroup.get(field);
                try {
                  control.setValue(element[field]);
                } catch (e) {
                }
              });
            }
          });
        }
      });
    }
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      this.predefinedData();
    }, 0);
  }

}
