import {Validators} from '@angular/forms';

export class WorkingFormParams {

  static CONTROLSCONFIG = {
    query_price_min: [500],
    query_price_max: [null],
    query_currency: [null, 0],
    query_work_headings: [null, Validators.required],
    query_work_category_headings: [null],
    query_work_profLevel: [null],
    query_work_schedule: [null],
    query_city: [null],
    query_services_list: [],
    query_work_type: [null, Validators.required]
  };
}
