import {Component, OnDestroy, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {ContactComponent} from '../contact/contact.component';
import {BasetariffComponent} from '../tariff/basetariff.component';
import {WorkingFormComponent} from './working-form/working-form.component';
import {TariffworkComponent} from '../tariff/tariffwork/tariffwork.component';
import {Application_phases} from '../../../services/auth/application-phases.service';
import {ConstingService} from '../../../services/consting/consting.service';
import {DynamicInterface} from '../dynamicInterface';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {NgxSpinnerService} from 'ngx-spinner';

@Component({
  selector: 'app-working',
  templateUrl: './working.component.html',
  styleUrls: ['./working.component.scss']
})
export class WorkingComponent implements OnInit, OnDestroy, DynamicInterface {
  destroyed$ = new Subject<void>();
  @ViewChild('dynamic', {static: true, read: ViewContainerRef}) dynamic: ViewContainerRef;
  public count = 1;

  constructor(private phasesService: Application_phases, private consting: ConstingService, private spinnerService: NgxSpinnerService) {

  }

  ngOnInit() {

    this.consting.getTarrif()
      .pipe(takeUntil(this.destroyed$))
      .subscribe((tariff) => {
        if (tariff) {
          // tslint:disable-next-line:radix
          this.count = (Number.parseInt(tariff['isUnlimited']) === 1) ? 1 : 5;
        }
      });

    this.phasesService.get_current_phase()
      .pipe(takeUntil(this.destroyed$))
      .subscribe((currentPhase) => {
        if (currentPhase) {
          let goM = Object.keys(currentPhase).filter(it => currentPhase[it])[0];
          goM = `${goM}`;
          try {
            this[goM]();
          } catch (e) {
          }
        }

      });
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }

  phaseContact(): void {
    this.phasesService.customFactory(this.dynamic, ContactComponent);
  }

  phaseDataPay(): void {
    this.phasesService.customFactory(this.dynamic, BasetariffComponent);
  }

  phaseQuery(): void {
    this.phasesService.customFactory(this.dynamic, WorkingFormComponent, this.count);
  }

  phaseTariff(): void {
    this.phasesService.customFactory(this.dynamic, TariffworkComponent);
  }

}
