import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'FindLengthPipe'
})
export class FindLengthPipe implements PipeTransform {

  transform(value: any, args: any): any {
    if (Object.keys(value).length > 0) {
      if (value[args].length > 0) {
        return `+${value[args].length}`;
      }
    }
    return null;
  }

}
