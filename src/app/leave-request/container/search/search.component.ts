import {AfterViewInit, Component, ElementRef, EventEmitter, Input, OnDestroy, OnInit, Output, Renderer2, ViewChild} from '@angular/core';
import {TabsClass} from '../../../shared/tabs.class';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {HideshowtabsService} from '../../../services/leave-request/hideshowtabs.service';
import {TabService} from '../../../services/leave-request/tabsactive/tab.service';
import {CreateformdataService} from '../../../services/auth/createformdata.service';
import {TariffService} from '../../../services/tariff/tariff.service';
import {SpinnerService} from '../../../services/spinner.service';
import {AccountService} from '../../../services/accounts/accounts.service';
import {map, takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {DATASTEPS} from '../../../shared/leave-request/leave/data';
import {HintsService} from '../../../services/hints.service';
import {NgxSpinnerService} from 'ngx-spinner';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit, AfterViewInit, OnDestroy {
  destroyed$ = new Subject<void>();
  @Input() account = false;
  @Input() tariff = false;
  // tslint:disable-next-line:variable-name
  @ViewChild('search_tab', {static: true}) search_tab: ElementRef;
  @Output() onPushName = new EventEmitter<string>();
  // typeTariff: Typetariff[];
  hwstepsdata = DATASTEPS;

  public notRead: any = {};

  private fromHeroFormTitle = '';

  // initialize load step component


  private getStapsOnInitComponent() {

    let child;
    let firstElementChild;
    let index = 0;
    const url = this.router.url;

    this.changeRoute();
    this.router.events
      .pipe(takeUntil(this.destroyed$))
      .subscribe((val) => {
        if (val instanceof NavigationEnd) {
          this.changeRoute();
        }
      });


    let nameStaps;
    let steps_name;
    nameStaps = this.splitString(url, '/');


    steps_name = nameStaps.charAt(0).toUpperCase() + nameStaps.slice(1).toLowerCase();
    child = this.search_tab.nativeElement.children;
    const currentTabs = new TabsClass();

    for (const item of child) {
      firstElementChild = item; // 1, "string", false

      if (firstElementChild.getAttribute('data-groupName') === steps_name) {

        currentTabs.title = steps_name;
        currentTabs.active = true;
        this.tabService.updateTabs(currentTabs);

        // item.style.transform = 'translateY(-7px)';
        item.style.backgroundColor = '#ef9c00';
      }
      index++;
    }
  }

  private changeRoute() {
    const url = this.router.url;
    /*****скрываем табы если есть в них id сегмент (first-level-component.ts)****************/
    let cleanTabs: any = url.split('/');
    try {
      cleanTabs = cleanTabs[cleanTabs.length - 1];
      let currentTB: number = Number(cleanTabs);
      if (currentTB) {
        this.hide(false);
      } else {
        this.hide(true);
      }
    } catch (e) {
    }
  }

  focusFunction(event, i): void {

    let child;
    child = this.search_tab.nativeElement.children;

    const currentTabs = new TabsClass();
    for (const ind in child) {

      if (ind !== 'length' && ind !== 'item' && ind !== 'namedItem') {

        if (child[i] === child[ind]) {

          currentTabs.title = this.getAttr(event, 'data-groupName');

          currentTabs.active = true;

          this.tabService.updateTabs(currentTabs);
          // child[i].style.transform = 'translateY(-7px)';
          child[i].style.backgroundColor = '#ef9c00';
        } else {
          // child[ind].style.transform = 'translateY(1px)';
          child[ind].style.backgroundColor = '#ffffff';
        }
      }
    }
  }

  public getAttrNameStaps(element, currentNumber: number) {
    let child;
    child = this.search_tab.nativeElement.children;
    const currentTabs = new TabsClass();
    for (const ind in child) {
      if (ind !== 'length' && ind !== 'item' && ind !== 'namedItem') {

        if (child[currentNumber] === child[ind]) {
          currentTabs.title = child[currentNumber].textContent;
          currentTabs.active = true;

          this.tabService.updateTabs(currentTabs);
          // child[currentNumber].style.transform = 'translateY(-7px)';
          child[currentNumber].style.backgroundColor = '#ffffff';
          // child[i].querySelector('a').style.transform = 'translateY(-7px)';
        } else {
          // child[ind].style.transform = 'translateY(1px)';
          child[ind].style.backgroundColor = '#ef9c00';
        }
      }
    }

    const val = this.getAttr(element, 'data-groupName');
    this.hintsService.installRedirectToAccount(val);
    this.onPushName.emit(val);
  }

  private getAttr(element: any, attr_name: string) {
    const target = element.target || element.srcElement || element.currentTarget;
    const someattribute = target.getAttribute(attr_name);
    return someattribute;
  }

  constructor(private renderer: Renderer2,
              private hidesShow: HideshowtabsService,
              private tabService: TabService,
              private tariffService: TariffService,
              private router: Router,
              private accountService: AccountService,
              private spinnerService: NgxSpinnerService,
              private activatedRoute: ActivatedRoute,
              private hintsService: HintsService,
              private createFormDataService: CreateformdataService) {
  }

  public hide(bolean) {
    if (bolean === false) {
      this.renderer.setStyle(this.search_tab.nativeElement, 'display', 'none');
    } else {
      this.renderer.setStyle(this.search_tab.nativeElement, 'display', 'inline-flex');
    }
  }

  public splitString(stringToSplit, separator) {
    const arrayOfStrings = stringToSplit.split(separator);
    return arrayOfStrings[3];
  }

  ngAfterViewInit() {

    if (!this.account && !this.tariff) {

      this.getStapsOnInitComponent();
    }
    if (this.account) {
      this.accountService._obsViewNotifications.pipe(takeUntil(this.destroyed$), map((preRes) => {
        if (preRes) {
          return preRes.data;
        }
        return {};
      })).subscribe((result) => {
        if (Object.keys(result).length > 0) {
          this.notRead = result;
        }
      });
      /******************переход после оплаты с leave-request****/
      // this.search_tab.nativeElement.children[0].style.transform = 'translateY(-7px)';
      this.search_tab.nativeElement.children[0].style.backgroundColor = '#ef9c00';
      this.hintsService.__obsHRedirectToAccount.pipe(takeUntil(this.destroyed$)).subscribe((toAccount) => {

        if (toAccount) {
          let elementsByClassName = document.getElementsByClassName('tablinks');
          [].slice.call(elementsByClassName).forEach((item) => {
            if (item.getAttribute('data-groupname') === toAccount) {
              // item.style.transform = 'translateY(-7px)';
              item.style.backgroundColor = '#ef9c00';
              item.style.color = 'white';
            } else {
              // item.style.transform = 'translateY(1px)';
              item.style.color = 'black';
              item.style.backgroundColor = '#ffffff';
            }
          });
        } else {
          // this.search_tab.nativeElement.children[0].style.transform = 'translateY(-7px)';
          this.search_tab.nativeElement.children[0].style.backgroundColor = '#ef9c00';
        }
      });

    } else if (this.tariff) {

      this.search_tab.nativeElement.children[0].style.transform = 'translateY(-7px)';
    } else {
      // this.search_tab.nativeElement.children[0].style.transform = 'translateY(-7px)';
    }

    /************переход с главной страницы******************/
    this.tabService._obsFromHeroForm
      .pipe(takeUntil(this.destroyed$))
      .subscribe((value) => {
        let elementHTMLCollectionOf = document.getElementsByClassName('tablinks');
        if (value) {
          if (value === 'all') {
            [].slice.call(elementHTMLCollectionOf).forEach((el: HTMLElement, index) => {
              // let elementName: string = el.getAttribute('data-groupname');
              if (index === 0) {
                // el.style.transform = 'translateY(-7px)';
                el.style.backgroundColor = '#ef9c00';
              } else {
                // el.style.transform = 'translateY(1px)';
                el.style.backgroundColor = '#ffffff';
              }
              this.renderer.setStyle(el, 'display', 'block');
            });
            // this.router.navigate(['/leave/search/auto']);
          } else {
            [].slice.call(elementHTMLCollectionOf).map((el: HTMLElement) => {
              let elementName: string = el.getAttribute('data-groupname');
              if (elementName.toLowerCase() !== value) {
                this.renderer.setStyle(el, 'display', 'none');
              } else {
                this.fromHeroFormTitle = elementName;
              }
            });
          }
        }
      });
    /************(</)переход с главной страницы******************/
  }

  ngOnInit() {

    // data-attrIndex
    this.createFormDataService.cleanFormData();
    // window.scrollTo(160, 0);
    this.hidesShow.hide
      .pipe(takeUntil(this.destroyed$))
      .subscribe(increased => this.hide(increased));

  }

  ngOnDestroy(): void {
    this.account = null;
    this.destroyed$.next();
  }

}
