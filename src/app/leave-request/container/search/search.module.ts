import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SearchComponent} from './search.component';
import {FindLengthPipe} from './find-length.pipe';
import {RouterModule} from '@angular/router';

@NgModule({
    declarations: [SearchComponent, FindLengthPipe],
    exports: [
        SearchComponent
    ],
    imports: [
        CommonModule,
        RouterModule
    ]
})
export class SearchModule {
}
