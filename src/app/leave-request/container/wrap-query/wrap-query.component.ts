import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';
import {LoginService} from '../../../services/auth/login.service';
import { Observable } from 'rxjs/internal/Observable';
import {Application_phases} from '../../../services/auth/application-phases.service';

@Component({
  selector: 'app-wrap-query',
  templateUrl: './wrap-query.component.html',
  styleUrls: ['./wrap-query.component.scss']
})
export class WrapQueryComponent implements OnInit, OnChanges {
  public isHideStepper: Observable<boolean>;
  public Object = Object;
  @Input() wrapIndex = 0;
  @Input() count = 1;
  @Output() activeId = new EventEmitter();
  public objStickyActive = {};

  ngOnChanges(changes: any): void {
    if (this.count > 1) {
      if (changes['wrapIndex']) {
        const currentValue = changes['wrapIndex']['currentValue'];
        if (currentValue == 1){
          this.stickyActive(currentValue + 1);
        }else if (currentValue == 5){
          // this.stickyActive(currentValue + 1);
        } else {
          this.stickyActive(currentValue + 1);
        }

      }
    }

  }

  constructor(private login: LoginService, private phase: Application_phases) {
  }

  ngOnInit() {
    for (let index = 1; index <= this.count; index++) {
      this.objStickyActive[index] = (index === 1);
    }
    this.isHideStepper = this.phase.getHideStepper();
  }

  stickyActive(active) {
    this.login.setScroll();
    this.activeId.emit(active);
    Object.keys(this.objStickyActive).forEach((itemActive) => {
      this.objStickyActive[itemActive] = (itemActive == active);
    });
  }

}
