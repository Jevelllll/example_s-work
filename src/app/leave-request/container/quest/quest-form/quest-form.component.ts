import {AfterViewInit, Component, Input, OnDestroy, OnInit, Renderer2} from '@angular/core';
import {Subject} from 'rxjs/internal/Subject';
import {FormArray, FormBuilder, FormGroup} from '@angular/forms';
import {LoginService} from '../../../../services/auth/login.service';
import {addNewForm, Application_phases} from '../../../../services/auth/application-phases.service';
import {CommonService} from '../../../../services/common.service';
import {QueryFormParams} from './query-form-params';
import {AccountService} from '../../../../services/accounts/accounts.service';
import {QuestService} from '../../../../services/priority/quest.service';
import {QuestInfo} from '../../../../services/priority/resModel';
import {takeUntil} from 'rxjs/internal/operators/takeUntil';
import {DomSanitizer} from '@angular/platform-browser';
import {map} from 'rxjs/internal/operators/map';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-quest-form',
  templateUrl: './quest-form.component.html',
  styleUrls: ['./quest-form.component.scss']
})
export class QuestFormComponent implements OnInit, AfterViewInit, OnDestroy {
  destroyed$ = new Subject<void>();
  infoMore: QuestInfo;
  // questTaskMore: QuestTask;
  public isBlockedNext = false;
  public JSON = JSON;
  public serviceSelected = [];
  private CATEGORY_NAME = 'quest';
  public items: FormArray;
  cTitle = [];
  public disabled = [];
  public wrapIndex: any;
  public myForm: FormGroup;
  @Input() count = 1;
  public formOutputId = 0;
  public SANITIZER: any;

  // tslint:disable-next-line:variable-name
  constructor(private _fb: FormBuilder, private login: LoginService,
              private accoutService: AccountService,
              private phasesService: Application_phases,
              private commonService: CommonService,
              private renderer: Renderer2,
              private toast: ToastrService,
              private priotityQuest: QuestService,
              private sanitizer: DomSanitizer
  ) {
    this.SANITIZER = this.sanitizer;
  }

  public activeFromWrapQuery(data) {
    if (data) {
      this.formOutputId = data - 1;
    }
  }

  private formInitialize() {
    this.myForm = this._fb.group({title: 'quest', items: this._fb.array([])});
    for (let index = 1; index <= this.count; index++) {
      this.disabled[index - 1] = true;
      this.addItem();
    }
  }

  ngOnInit(): void {

    this.priotityQuest.getQuestInfoMore()
      .pipe(takeUntil(this.destroyed$),
        map((data) => {
          const currentUser = JSON.parse(localStorage.getItem('currentUser'));
          if (currentUser && data) {
            const infoId = data.id;
            this.priotityQuest.isWrite(currentUser.user_id, infoId).pipe(takeUntil(this.destroyed$))
              .subscribe((result) => {
                if (result) {
                  if (Number(result.id) === Number(infoId)) {
                    this.phasesService.buttonNextDisable(true);
                    this.phasesService.buttonBackDisable(true);
                    this.isBlockedNext = true;
                    this.toast.warning('Вы уже являетесь участником текущего квеста.', '', {
                      timeOut: 13000
                    });
                  }
                }
              });
          }
          return data;
        }))
      .subscribe((res) => this.infoMore = res, error => {
      });


    for (let index = 0; index < this.count; index++) {
      if (index === this.count - 1) {
        this.cTitle[index] = 'Подтвердить';
      } else {
        this.cTitle[index] = 'Следующая заявка';
      }
    }
    this.formInitialize();
  }

  private currentFormArray(index) {
    const arrayControls = this.myForm.get('items') as FormArray;
    return arrayControls.controls[index] as FormGroup;
  }

  addItem(): void {
    this.items = this.myForm.get('items') as FormArray;
    this.items.push(this.createItem());
  }

  /**********работа с экземпляром форм******************/
  // tslint:disable-next-line:variable-name
  addNewForm(indexForm: number, confirm, quest_id: number) {
    if (this.myForm.valid) {
      const preObj: any = {};
      const preForm: FormGroup = this.currentFormArray(indexForm);
      preForm.get('quest_id').setValue(quest_id);
      preForm.patchValue(preObj);
      this.serviceSelected[indexForm] = {11: true};
      addNewForm.apply(this, [indexForm]);
      if (indexForm === this.count - 1) {
        this.renderer.addClass(confirm, 'confirm_btn_success');
        this.renderer.removeClass(confirm, 'confirm_btn');
      } else {
        this.login.setScroll();
      }
    }
    return false;
  }

  private createItem() {
    return this._fb.group(QueryFormParams.CONTROLSCONFIG);
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }

  ngAfterViewInit(): void {
  }

}
