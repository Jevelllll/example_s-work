import {FormControl, ValidationErrors, Validators} from '@angular/forms';

export class QueryFormParams {

  static CONTROLSCONFIG = {
    share: [null, Validators.compose([shareValid])],
    query_services_list: [],
    quest_id: [],
  };
}

export function shareValid(control: FormControl): ValidationErrors {
  if (control.value) {
    return null;
  } else {
    return {isNull: true};
  }
}
