import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {Subject} from 'rxjs/internal/Subject';
import {Application_phases} from '../../../services/auth/application-phases.service';
import {ConstingService} from '../../../services/consting/consting.service';
import {takeUntil} from 'rxjs/internal/operators/takeUntil';
import {DynamicInterface} from '../dynamicInterface';
import {TariffquestComponent} from '../tariff/tarriffquest/tariffquest.component';
import {ContactComponent} from '../contact/contact.component';
import {QuestFormComponent} from './quest-form/quest-form.component';
import {distinctUntilChanged} from 'rxjs/internal/operators/distinctUntilChanged';
import {DataPayModel} from 'src/app/shared/tarifModel';
import * as moment from 'moment';
import {CreateformdataService} from '../../../services/auth/createformdata.service';
import {first} from 'rxjs/internal/operators/first';
import {AmountService} from '../../../services/pay/amount.service';
import {SpinnerService} from '../../../services/spinner.service';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';
import {LoginService} from '../../../services/auth/login.service';
import { take } from 'rxjs/internal/operators/take';

@Component({
  selector: 'app-quest',
  templateUrl: './quest.component.html',
  styleUrls: ['./quest.component.scss']
})
export class QuestComponent implements OnInit, AfterViewInit, OnDestroy, DynamicInterface {
  destroyed$ = new Subject<void>();
  @ViewChild('dynamic', {static: true, read: ViewContainerRef}) dynamic: ViewContainerRef;
  public count = 1;
  private foolOrderId;

  constructor(private phasesService: Application_phases,
              private toValidservice: CreateformdataService,
              private amountService: AmountService,
              private spinnerService: SpinnerService,
              private toastr: ToastrService,
              private router: Router,
              private loginService: LoginService,
              private consting: ConstingService) {
  }

  ngOnInit(): void {
    this.consting.getTarrif()
      .pipe(takeUntil(this.destroyed$))
      .subscribe((tariff) => {
        if (tariff) {
          // tslint:disable-next-line:radix
          this.count = (Number.parseInt(tariff['isUnlimited']) === 1) ? 1 : 5;
        }
      });

    this.phasesService.get_current_phase()
      .pipe(takeUntil(this.destroyed$))
      .subscribe((currentPhase) => {
        if (currentPhase) {
          let goM = Object.keys(currentPhase).filter(it => currentPhase[it])[0];
          goM = `${goM}`;
          try {
            this[goM]();
          } catch (e) {
          }
        }
      });
  }

  ngAfterViewInit(): void {
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }

  phaseContact(): void {
    this.phasesService.customFactory(this.dynamic, ContactComponent);
  }

  // Fri Jul 10 2020 03:00:00 GMT+0300 (Восточная Европа, летнее время)
  // basetariff.component.ts:193 Fri Jul 17 2020 00:00:00 GMT+0300 (Восточная Европа, летнее время)
  phaseDataPay(): void {
    // tslint:disable-next-line:variable-name
    const current_day = moment().set({hour: 3, minute: 0, second: 0, millisecond: 0}).format('YYYY-MM-DD HH:mm:ss');
    const endDay = moment().set({hour: 3, minute: 0, second: 0, millisecond: 0}).add('years', 1).format('YYYY-MM-DD HH:mm:ss');
    const date = new Date(current_day.replace(/-/g, '/'));
    const endDate = new Date(endDay.replace(/-/g, '/'));
    this.phasesService.get_phase_tariff()
      .pipe(distinctUntilChanged()
        , takeUntil(this.destroyed$)
      )
      .subscribe(objTariff => {
        const phasePay = new DataPayModel(objTariff.id, endDate, date);
        this.phasesService.add_phase_data_Pay(phasePay);
        this.phasesService.postQueryRequest();
      });

    this.toValidservice.getPayOrder_id()
      .pipe(distinctUntilChanged()
        , takeUntil(this.destroyed$)
      ).subscribe((result) => {
      if (result) {
        this.loginService.isLogin()
          .pipe(distinctUntilChanged(), take(1)
            , takeUntil(this.destroyed$)
          )
          .subscribe(isLogin => {
            if (isLogin) {
              this.newPayN(result);
            } else {
              this.phasesService.get_phase_contact()
                .pipe(distinctUntilChanged(), take(1)
                  , takeUntil(this.destroyed$)
                )
                .subscribe(user => {
                  const regExp = new RegExp(/[-()/\\]/g);
                  if (user) {
                    this.loginService.login(user['phone'].replace(regExp, ''), user['password'])
                      .pipe(take(1), distinctUntilChanged()
                        , takeUntil(this.destroyed$)
                      )
                      .subscribe((respIsLogin) => {
                        if (respIsLogin) {
                          this.newPayN(result);
                        }
                      });
                  }
                });
            }
          });
      }


      // if (result) {
      //   this.newPayN(result);
      // }
    });
  }

  private newPayN(result) {
    this.amountService.newWalletPay({order_id: result + '/quest'})
      .pipe(distinctUntilChanged(), first()
        , takeUntil(this.destroyed$)
      )
      // tslint:disable-next-line:no-shadowed-variable
      .subscribe(result => {
        if (result['msg']) {
          if (result['msg'] === 'success') {
            this.amountService.getCurrentBalance()
              .pipe(distinctUntilChanged(), first()
                , takeUntil(this.destroyed$)
              )
              .subscribe(() => {
                this.toastr.success('Квест активен');
                this.router.navigate(['/account']);
                this.spinnerService.hide();
              }, error1 => {
                this.toastr.error('Ошибка получения текущего баланса');
                this.spinnerService.hide();
              });
          }
        }
      }, error1 => {
        this.toastr.error('Платеж не прошел');
        this.spinnerService.hide();
      }, () => {
        this.spinnerService.hide();
      });
  }

  phaseQuery(): void {
    this.phasesService.customFactory(this.dynamic, QuestFormComponent, this.count);
  }

  phaseTariff(): void {
    this.phasesService.customFactory(this.dynamic, TariffquestComponent);
  }
}
