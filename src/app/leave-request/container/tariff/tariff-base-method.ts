import {CreateformdataService} from '../../../services/auth/createformdata.service';
import * as moment from 'moment';

export class TariffBaseMethod {
  // tslint:disable-next-line:variable-name
  private query_tariff_start: any;
  // tslint:disable-next-line:variable-name
  private query_tariff_end: any;
  private toValidservice: any;
  private diskount = 35;
  private tariffData: any;
  // tslint:disable-next-line:variable-name
  public sum_day_old_query_tariff = 0;
  // tslint:disable-next-line:variable-name
  public sum_day_query_tariff = 0;
  // tslint:disable-next-line:variable-name
  private count_discont_days = 30;
  private saving = 0;
  private error: any = [];

  constructor(tariffData?: any) {
    this.tariffData = tariffData;
    const params: any = '';

    this.toValidservice = new CreateformdataService(params);
  }

  // tslint:disable-next-line:variable-name
  public optionDate(must_field?: string, form?: any, query_tariff_start?: any, query_tariff_end?: any) {

    const myFormStatusChanges$ = form.statusChanges;
    this.query_tariff_start = moment.unix(query_tariff_start).format('YYYY-MM-DD');
    const dateFirst = new Date(this.query_tariff_start);
    this.query_tariff_start = moment(dateFirst).unix();

    this.query_tariff_end = moment.unix(query_tariff_end).format('YYYY-MM-DD');
    const dateSecond = new Date(this.query_tariff_end);
    this.query_tariff_end = moment(dateSecond).unix();

    // tslint:disable-next-line:variable-name
    let unix_date_start: any;
    // tslint:disable-next-line:variable-name
    let unix_date_end: any;
    unix_date_start = isNaN(this.query_tariff_start) ? Math.round(+new Date() / 1000) : this.query_tariff_start;
    unix_date_end = this.query_tariff_end;

    // tslint:disable-next-line:prefer-const variable-name
    let count_day: any;
    // tslint:disable-next-line:variable-name
    let current_unix: any;

    const dateF = moment().format('YYYY-MM-DD');
    const currentD = new Date(dateF);

    current_unix = moment(currentD).unix();

    if (unix_date_start < current_unix
      || isNaN(unix_date_start) || isNaN(unix_date_end)) {
      this.error = [];
      this.error.push('Не допустимое значение');
    } else if (unix_date_end < current_unix) {
      if (must_field === 'go') {
        this.error = [];
        this.error.push('Дата окончания не может быть меньше начальной');
      }
    } else if (unix_date_start > unix_date_end) {
      if (must_field === 'go') {
        this.error = [];
        this.error.push('Начальная дата не может быть больше конечной');
      }
    } else if (unix_date_start === unix_date_end) {
      this.error = [];
      this.error.push('Начальная дата не может быть равна конечной');
    } else {
      this.error = [];
      const convectorDay = (this.convectorDay(unix_date_end, unix_date_start));
      // tslint:disable-next-line:variable-name
      const before_sum = this.tariffData.sum_day * convectorDay * this.tariffData.discount;
      if (convectorDay >= this.count_discont_days) {
        this.saving = before_sum;
        this.sum_day_old_query_tariff = this.tariffData.sum_day * convectorDay;
        this.sum_day_query_tariff = (this.tariffData.sum_day * convectorDay) - before_sum;
      } else {
        this.saving = 0;
        this.sum_day_old_query_tariff = 0;
        this.sum_day_query_tariff = this.tariffData.sum_day * convectorDay;
      }
    }
  }

  private getUtcUnixTime(date) {
    return (+new Date(date)
      - new Date(date).getTimezoneOffset() * 60 * 1000)
      / 1000;
  }

  public getSum_day_query_tariff() {
    return this.sum_day_query_tariff;
  }

  public getSum_day_old_taridd() {
    return this.sum_day_old_query_tariff;
  }

  public getSaving() {
    return this.saving;
  }

  public getErrors() {
    return this.error;
  }

  // tslint:disable-next-line:variable-name
  private convectorDay(unix_date_end, unix_date_start) {
    const Tf = new Date(unix_date_end * 1000);
    const Tl = new Date(unix_date_start * 1000);
    Tf.setHours(4);
    Tl.setHours(4);
    // let day_first: any  = new Date(unix_date_end * 1000);
    // tslint:disable-next-line:variable-name
    const day_first: any = new Date(Tf.getTime() + Tf.getTimezoneOffset() * 60000);
    // tslint:disable-next-line:variable-name
    const day_last: any = new Date(Tl.getTime() + Tl.getTimezoneOffset() * 60000);
    // let day_last: any = new Date(unix_date_start * 1000);
    const seconds = (day_first - day_last) / 1000;
    const minutes = seconds / 60;
    const hours = minutes / 60;
    const days = hours / 24;
    return Math.ceil(days);
  }
}
