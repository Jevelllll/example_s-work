import {AfterViewInit, Component, ElementRef, OnDestroy, OnInit, Renderer2, ViewChild} from '@angular/core';
import {animate, style, transition, trigger} from '@angular/animations';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {TabService} from '../../../services/leave-request/tabsactive/tab.service';
import {ConstingService} from '../../../services/consting/consting.service';
import {CreateformdataService} from '../../../services/auth/createformdata.service';
import {TariffService} from '../../../services/tariff/tariff.service';
import {DomSanitizer} from '@angular/platform-browser';
import {ToastrService} from 'ngx-toastr';
import {HideshowtabsService} from '../../../services/leave-request/hideshowtabs.service';
import {Router} from '@angular/router';
import {LoginService} from '../../../services/auth/login.service';
import {Application_phases} from '../../../services/auth/application-phases.service';
import * as moment from 'moment';
import {Tariffinterface} from '../../../services/priority/resModel';
import {distinctUntilChanged, first, map, take, takeUntil, withLatestFrom} from 'rxjs/operators';
import {AmountService} from '../../../services/pay/amount.service';
import {Subject} from 'rxjs';
import {SpinnerService} from '../../../services/spinner.service';

@Component({
  selector: 'app-basetariff',
  templateUrl: './basetariff.component.html',
  styleUrls: ['./basetariff.component.scss'],
  animations: [
    trigger('dialog', [
      transition('void => *', [
        style({transform: 'translateX(-100%)'}),
        animate(350)
      ]),
      transition('* => void', [
        animate(350, style({transform: 'scale3d(.0, .0, .0)'}))
      ])
    ])
  ]
})
export class BasetariffComponent implements OnInit, AfterViewInit, OnDestroy {
  destroyed$ = new Subject<void>();
  /**************кнопка кошелек**********************/
  public isWalletPay = false;
  public foolOrderId = null;
  public balance;
  public SANITIZER: any;
  public isUnlimited = false;
  public date;
  // tslint:disable-next-line:variable-name
  public order_id: number;
  // tslint:disable-next-line:variable-name
  public pay_visible: boolean;
  // tslint:disable-next-line:variable-name
  public theHtmlString: any;
  // tslint:disable-next-line:variable-name
  public query_tariff_start;
  // tslint:disable-next-line:variable-name
  public query_tariff_end;
  // tslint:disable-next-line:variable-name
  public count_discont_days = 30;
  // tslint:disable-next-line:variable-name
  public flag_tariff: boolean;
  // tslint:disable-next-line:variable-name
  public title_query_to_liqupay: string;
  // tslint:disable-next-line:variable-name
  public sum_day: number;
  public saving = 0;
  // tslint:disable-next-line:variable-name
  public sum_day_old_query_tariff = 0;
  // tslint:disable-next-line:variable-name
  public sum_day_query_tariff = 0;
  public tariffData: any;
  // tslint:disable-next-line:variable-name
  public tariff_id = 0;
  public FormTariff: FormGroup;
  public diskount = 35;
  public mons = 30;
  public teste;
  private minNgAp = 1;
  public submited = false;
  public currentVal = 1;
  public currentSVal = 20;
  @ViewChild('countDay', {static: true}) countDay: ElementRef;
  @ViewChild('sumD', {static: true}) sumD: ElementRef;
  @ViewChild('inputFirst', {static: true}) inputFirst: ElementRef;
  @ViewChild('inputSecond', {static: true}) inputSecond: ElementRef;

  constructor(private tabService: TabService,
              private consting: ConstingService,
              private toValidservice: CreateformdataService,
              private tariffService: TariffService,
              private sanitizer: DomSanitizer,
              private toastr: ToastrService,
              private hideservice: HideshowtabsService,
              private renderer: Renderer2,
              private amountService: AmountService,
              private router: Router,
              private loginService: LoginService,
              private phaseService: Application_phases,
              private spinnerService: SpinnerService
  ) {
    this.SANITIZER = this.sanitizer.bypassSecurityTrustHtml;
  }

  minDate = new Date(moment().set({hour: 3, minute: 0, second: 0, millisecond: 0}).format('YYYY-MM-DD HH:mm:ss').replace(/-/g, '/'));
  maxDate = new Date(2020, 0, 1);

  selectedT(tariff: any) {
    if (tariff.isUnlimited == 1) {
      this.isUnlimited = true;
    } else {
      this.isUnlimited = false;
    }
    this.teste = tariff.title;
    this.currentSVal = tariff.priceDay;
  }

  save(model: Tariffinterface, isValid: boolean) {
    this.submited = true;
  }

  // tslint:disable-next-line:variable-name
  public optionDate(must_field?: string, form?: any) {
    const myFormStatusChanges$ = form.statusChanges;
    this.tariff_id = this.tariffData.id;

    const currentDate = moment().set({hour: 3, minute: 0, second: 0, millisecond: 0}).format('YYYY-MM-DD HH:mm:ss');
    // tslint:disable-next-line:variable-name
    const unix_date_start = isNaN(this.query_tariff_start) ? moment(currentDate).unix() : this.query_tariff_start;
    const unix_date_end = this.query_tariff_end;
    // tslint:disable-next-line:prefer-const
    let count_day: any;
    // tslint:disable-next-line:variable-name
    const current_unix = moment(currentDate).unix();
    if (unix_date_start < current_unix
      || isNaN(unix_date_start) || isNaN(unix_date_end)) {

      this.reset_view_price();
    } else if (unix_date_end < current_unix) {
      if (must_field === 'go') {
        this.toastr.warning('Дата окончания не может быть меньше начальной', '', {
          timeOut: 5000,
        });
        this.reset_view_price();
      }
      form.setErrors({'incorrect': true});
    } else if (unix_date_start > unix_date_end) {
      if (must_field === 'go') {
        this.toastr.warning('Начальная дата не может быть больше конечной', '' +
          '', {
          timeOut: 5000,
        });
        this.reset_view_price();
      }
      form.setErrors({'incorrect': true});
    } else if (unix_date_start === unix_date_end) {
      if (must_field === 'go') {
        this.toastr.warning('Начальная дата не может быть равна конечной', '' +
          '', {
          timeOut: 5000,
        });
        this.reset_view_price();
      }
      form.setErrors({'incorrect': true});
    } else {
      const convectorDay = (this.convectorDay(unix_date_end, unix_date_start));
      // tslint:disable-next-line:variable-name
      const before_sum = this.tariffData.sum_day * convectorDay * this.tariffData.discount;
      // if (convectorDay >= this.count_discont_days) {
      //   this.saving = before_sum;
      //   this.sum_day_old_query_tariff = this.tariffData.sum_day * convectorDay;
      //   this.sum_day_query_tariff = (this.tariffData.sum_day * convectorDay) - before_sum;
      //   form.setErrors({'incorrect': true});
      // } else {
      this.saving = 0;
      this.sum_day_old_query_tariff = 0;
      this.sum_day_query_tariff = this.tariffData.sum_day * convectorDay;
      myFormStatusChanges$
        .pipe(
          distinctUntilChanged(),
          map(data => data === 'VALID')
          , takeUntil(this.destroyed$)
        )
        .subscribe(isValid => {
          if (isValid) {
            this.phaseService.add_phase_data_Pay(form.value);
            this.phaseService.buttonNextDisable(false);
          } else {
            this.phaseService.buttonNextDisable(true);
          }
        });
      // }
    }
  }

  private reset_view_price() {
    this.sum_day_old_query_tariff = 0;
    this.sum_day_query_tariff = 0;
    this.saving = 0;
  }

  // tslint:disable-next-line:variable-name
  private convectorDay(unix_date_end, unix_date_start) {

    const start = moment.unix(unix_date_end).set({hour: 3, minute: 0, second: 0, millisecond: 0}).format('YYYY-MM-DD HH:mm:ss');
    const end = moment.unix(unix_date_start).set({hour: 3, minute: 0, second: 0, millisecond: 0}).format('YYYY-MM-DD HH:mm:ss');
    // tslint:disable-next-line:variable-name
    const day_first: any = new Date(start.replace(/-/g, '/'));
    // tslint:disable-next-line:variable-name
    const day_last: any = new Date(end.replace(/-/g, '/'));
    const seconds = (day_first - day_last) / 1000;
    const minutes = seconds / 60;
    const hours = minutes / 60;
    const days = hours / 24;
    return Math.ceil(days);
  }

  subcribeToFormChanges() {
    const myFormChanges$ = this.FormTariff.valueChanges;
    myFormChanges$
      .pipe(distinctUntilChanged()
        , takeUntil(this.destroyed$)
      )
      .subscribe(x => {
          this.hideservice.addHideTariffDisplay(true);
          this.theHtmlString = '';
          this.pay_visible = false;
          const Last = moment(x.query_tariff_end).set({hour: 3, minute: 0, second: 0, millisecond: 0}).format('YYYY-MM-DD HH:mm:ss');
          const First = moment(x.query_tariff_start).set({hour: 3, minute: 0, second: 0, millisecond: 0}).format('YYYY-MM-DD HH:mm:ss');
          const dateFirst = new Date(First.replace(/-/g, '/'));
          const dateLast = new Date(Last.replace(/-/g, '/'));

          this.query_tariff_end = moment(dateLast).unix();
          this.query_tariff_start = moment(dateFirst).unix();
          this.optionDate('go', this.FormTariff);
        }
      );

    this.toValidservice.getPayOrder_id()
      .pipe(distinctUntilChanged()
        , takeUntil(this.destroyed$)
      )
      // tslint:disable-next-line:variable-name
      .subscribe(order_id => {
        this.spinnerService.show();
        this.isWalletPay = false;
        this.order_id = order_id;
        this.foolOrderId = order_id + '/' + this.title_query_to_liqupay;
        if (this.isUnlimited) {
          this.loginService.isLogin()
            .pipe(distinctUntilChanged(), take(1)
              , takeUntil(this.destroyed$)
            )
            .subscribe(isLogin => {
              if (isLogin) {
                this.newWalletPay();
              } else {
                this.phaseService.get_phase_contact()
                  .pipe(distinctUntilChanged(), take(1)
                    , takeUntil(this.destroyed$)
                  )
                  .subscribe(user => {
                    const regExp = new RegExp(/[-()/\\]/g);
                    if (user) {
                      this.loginService.login(user['phone'].replace(regExp, ''), user['password'])
                        .pipe(take(1), distinctUntilChanged()
                          , takeUntil(this.destroyed$)
                        )
                        .subscribe((respIsLogin) => {
                          if (respIsLogin) {
                            this.newWalletPay();
                          }
                        });
                    }
                  });
              }
            });
        } else {
          if (this.balance) {
            if (this.balance >= this.sum_day_query_tariff) {
              this.isWalletPay = true;
              this.spinnerService.hide();
            } else {
              this.isWalletPay = false;
              this.setHtmlButtonLiqPay();
            }
          } else {
            this.setHtmlButtonLiqPay();
          }
        }
      });
  }

  private setHtmlButtonLiqPay() {
    setTimeout(() => {
      this.tariffService.postLiqPay(this.sum_day_query_tariff, this.order_id + '/' + this.title_query_to_liqupay)
        .pipe(distinctUntilChanged()
          , takeUntil(this.destroyed$)
        )
        .subscribe(lPay => {
            this.theHtmlString = lPay;
            this.spinnerService.hide();
          }, error => {
            this.toastr.error('Error');
            this.spinnerService.hide();
          },
          () => {
            this.spinnerService.hide();
          }
        );
    }, 3000);
  }

  /*******************оплатить через кошелек*************************************/
  public newWalletPay() {
    this.amountService.newWalletPay({order_id: this.foolOrderId})
      .pipe(distinctUntilChanged(), first()
        , takeUntil(this.destroyed$)
      )
      .subscribe(result => {
        if (result['msg']) {
          if (result['msg'] === 'success') {
            this.amountService.getCurrentBalance()
              .pipe(distinctUntilChanged(), first()
                , takeUntil(this.destroyed$)
              )
              .subscribe(() => {
                this.toastr.success('Заявка активирована');
                this.router.navigate(['/account']);
                this.spinnerService.hide();
              }, error1 => {
                this.toastr.error('Ошибка получения текущего баланса');
                this.spinnerService.hide();
              });
          }
        }
      }, error1 => {
        this.toastr.error('Платеж не прошел');
        this.spinnerService.hide();
      }, () => {
        this.spinnerService.hide();
      });
  }

  public getBalanceWalletPay() {
    this.amountService.balanceObs
      .pipe(distinctUntilChanged()
        , takeUntil(this.destroyed$)
      )
      .subscribe(result => {
        if (result !== null) {
          this.balance = result.balance;
        } else {
          try {
            this.amountService.getCurrentBalance()
              .pipe(distinctUntilChanged()
                , takeUntil(this.destroyed$)
              )
              .subscribe(value => {
                this.balance = value.balance;
              }, error => {
              });
          } catch (e) {
          }
        }
      }, error => {
      });
  }

  ngOnInit() {
    this.FormTariff = new FormGroup({
      id: new FormControl(0, [Validators.required as any]),
      query_tariff_start: new FormControl('', [Validators.required as any]),
      query_tariff_end: new FormControl('', [Validators.required as any]),
    });
    this.consting.obsResultsStore.pipe(withLatestFrom(this.consting.obsSkipResultsStore)).subscribe(([result, storeSk]) => {
      if (storeSk) {
        if (result) {
          this.phaseService.get_phase_tariff()
            .pipe(distinctUntilChanged()
              , takeUntil(this.destroyed$)
            ).subscribe(tariffId => {
            this.FormTariff = new FormGroup({
              id: new FormControl(tariffId.id, [Validators.required as any]),
              cust: new FormControl(true, []),
              query_tariff_start: new FormControl(0, [Validators.required as any]),
              query_tariff_end: new FormControl(0, [Validators.required as any]),
            });
            this.phaseService.add_phase_data_Pay(this.FormTariff.value);
            this.phaseService.postQueryRequest();
          });
        } else {
          this.phaseService.get_phase_tariff()
            .pipe(distinctUntilChanged()
              , takeUntil(this.destroyed$)
            ).subscribe(tariffId => {
            this.FormTariff = new FormGroup({
              id: new FormControl(tariffId.id, [Validators.required as any]),
              cust: new FormControl(true, []),
              query_tariff_start: new FormControl(0, [Validators.required as any]),
              query_tariff_end: new FormControl(0, [Validators.required as any]),
            });
            this.phaseService.add_phase_data_Pay(this.FormTariff.value);
            this.phaseService.postQueryRequest();
          });
          // customerQuestAddskip
          console.log('obsSkipResultsStore');
        }
      } else {
        this.subcribeToFormChanges();
        this.getBalanceWalletPay();
        // tslint:disable-next-line:variable-name
        const current_day = moment().set({hour: 3, minute: 0, second: 0, millisecond: 0}).format('YYYY-MM-DD HH:mm:ss');
        this.date = new Date(current_day.replace(/-/g, '/'));
        this.order_id = 0;
        this.pay_visible = false;
        this.toValidservice.getPayVisible()
          .pipe(distinctUntilChanged()
            , takeUntil(this.destroyed$)
          )
          .subscribe(visible => {
            this.pay_visible = visible;
          });
        this.tariffService.postLiqPay(this.sum_day_query_tariff, this.order_id)
          .pipe(distinctUntilChanged()
            , takeUntil(this.destroyed$)
          )
          .subscribe(lPay => {
            this.theHtmlString = lPay;
          });
        this.flag_tariff = false;

        this.phaseService.get_phase_tariff()
          .pipe(distinctUntilChanged()
            , takeUntil(this.destroyed$)
          )
          .subscribe(objTariff => {
            this.scrollToSelectStartEndDate();
            this.hideservice.addHideTariffDisplay(true);
            this.pay_visible = false;
            this.theHtmlString = '';
            this.flag_tariff = true;
            this.selectedT(objTariff);
            this.tariffData = objTariff;
            this.FormTariff.controls.id.patchValue(objTariff.id);
            this.optionDate('', this.FormTariff);
          });

        this.toValidservice.getPayOrderCategory()
          .pipe(distinctUntilChanged()
            , takeUntil(this.destroyed$)
          )
          .subscribe((title) => this.title_query_to_liqupay = title);
      }

    });


  }

  scrollToSelectStartEndDate() {
    const tariffDown = document.getElementsByClassName('confirmbtn')[0];
    tariffDown.scrollIntoView({
      block: 'end',
      behavior: 'smooth'
    });
  }

  ngAfterViewInit(): void {
    // window.scrollTo(0, 480);
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }
}
