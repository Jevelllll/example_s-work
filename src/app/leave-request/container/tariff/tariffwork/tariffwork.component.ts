import {Component, OnDestroy, OnInit} from '@angular/core';
import {TariffService} from '../../../../services/tariff/tariff.service';
import {addittional_style_tarriff_blocks} from '../tarrif_style_options';
import {take, takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {SpinnerService} from '../../../../services/spinner.service';

@Component({
  selector: 'app-tariffwork',
  templateUrl: './tariffwork.component.html',
  styleUrls: ['./tariffwork.component.scss']
})
export class TariffworkComponent implements OnInit, OnDestroy {
  destroyed$ = new Subject<void>();
  public tariffWork;
  // tslint:disable-next-line:variable-name
  private tarrif_type;
  // tslint:disable-next-line:variable-name
  public additional_style_property: any;

  constructor(private tariffService: TariffService, private spinnerService: SpinnerService) {
    this.additional_style_property = addittional_style_tarriff_blocks;
  }

  private setTypeTariff(typeT: any) {
    this.tarrif_type = typeT;
  }

  private getTypeTariff() {
    return this.tarrif_type[0].id;
  }

  ngOnInit() {
    this.spinnerService.show();
    this.tariffService.getTariffTypeListener
      .pipe(take(1), takeUntil(this.destroyed$))
      // tslint:disable-next-line:variable-name
      .subscribe(type_tariff => {
        this.setTypeTariff(type_tariff.tariff_type.filter(x => x.type_tariff === 'Work'));
        this.tariffService.getTariff(this.getTypeTariff())
          .pipe(takeUntil(this.destroyed$))
          .subscribe(x => {
            this.tariffWork = x.tariff;
          }, error => this.spinnerService.hide(), () => this.spinnerService.hide());
      }, error => this.spinnerService.hide(), () => this.spinnerService.hide());
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }

}
