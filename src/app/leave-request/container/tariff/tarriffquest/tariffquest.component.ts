import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Subject} from 'rxjs/internal/Subject';
import {TariffService} from '../../../../services/tariff/tariff.service';
import {SpinnerService} from '../../../../services/spinner.service';
import {addittional_style_tarriff_blocks} from '../tarrif_style_options';
import {take, takeUntil} from 'rxjs/operators';
import {TariffComponent} from '../../../../topbar/price-app/tariff/tariff.component';

@Component({
  selector: 'app-tariffquest',
  templateUrl: './tariffquest.component.html',
  styleUrls: ['./tariffquest.component.scss']
})
export class TariffquestComponent implements OnInit, OnDestroy, AfterViewInit {
  destroyed$ = new Subject<void>();
  @ViewChild(TariffComponent) appTariff: TariffComponent;

  public tariffQuest;
  // tslint:disable-next-line:variable-name
  private tarrif_type;
  // tslint:disable-next-line:variable-name
  public additional_style_property: any;

  constructor(private tariffService: TariffService, private spinnerService: SpinnerService) {
    this.additional_style_property = addittional_style_tarriff_blocks;
  }

  ngAfterViewInit(): void {
  }

  private setTypeTariff(typeT: any) {
    this.tarrif_type = typeT;
  }

  private getTypeTariff() {
    return this.tarrif_type[0].id;
  }

  ngOnInit(): void {
    this.spinnerService.show();
    this.tariffService.getTariffTypeListener
      .pipe(take(1), takeUntil(this.destroyed$))
      // tslint:disable-next-line:variable-name
      .subscribe(type_tariff => {
        this.setTypeTariff(type_tariff?.tariff_type.filter(x => x.type_tariff === 'Quest'));
        this.tariffService.getTariff(this.getTypeTariff())
          .pipe(take(1), takeUntil(this.destroyed$))
          .subscribe(x => {
            this.tariffQuest = x.tariff;
            //
            if (Array.isArray(this.tariffQuest)){
              this.appTariff.selectALSO(this.tariffQuest[0]);
            }
          //
          }, error => this.spinnerService.hide(), () => this.spinnerService.hide());
      }, error => this.spinnerService.hide(), () => this.spinnerService.hide());
  }

  ngOnDestroy(): void {
    if (this.destroyed$) {
      this.destroyed$.next();
    }
  }
}
