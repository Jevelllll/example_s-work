import {Component, OnDestroy, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {Application_phases, CurrentPhase} from '../../../services/auth/application-phases.service';
import {ConstingService} from '../../../services/consting/consting.service';
import {DynamicInterface} from '../dynamicInterface';
import {ContactComponent} from '../contact/contact.component';
import {BasetariffComponent} from '../tariff/basetariff.component';
import {RealPropertyFormComponent} from './real-property-form/real-property-form.component';
import {TariffpropertyComponent} from '../tariff/tariffproperty/tariffproperty.component';
import {take, takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';

@Component({
  selector: 'app-real-property',
  templateUrl: './real-property.component.html',
  styleUrls: ['./real-property.component.scss']
})
export class RealPropertyComponent implements OnInit, OnDestroy, DynamicInterface {
  destroyed$ = new Subject<void>();
  @ViewChild('dynamic', {static: true, read: ViewContainerRef}) dynamic: ViewContainerRef;
  public count = 1;

  constructor(private phasesService: Application_phases, private consting: ConstingService) {

  }

  ngOnInit() {

    this.consting.getTarrif()
      .pipe(takeUntil(this.destroyed$))
      .subscribe((tariff) => {
        if (tariff) {
          // tslint:disable-next-line:radix
          this.count = (Number.parseInt(tariff['isUnlimited']) === 1) ? 1 : 5;
        }
      });

    this.phasesService.get_current_phase()
      .pipe(takeUntil(this.destroyed$))
      .subscribe((currentPhase) => {
        if (currentPhase) {
          let goM = Object.keys(currentPhase).filter(it => currentPhase[it])[0];
          goM = `${goM}`;
          try {
            this[goM]();
          } catch (e) {
          }
        }

      });
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }

  phaseContact(): void {
    this.phasesService.customFactory(this.dynamic, ContactComponent);
  }

  phaseDataPay(): void {
    this.phasesService.customFactory(this.dynamic, BasetariffComponent);
  }

  phaseQuery(): void {
    this.phasesService.customFactory(this.dynamic, RealPropertyFormComponent, this.count);
  }

  phaseTariff(): void {
    setTimeout(() => {
      this.phasesService.customFactory(this.dynamic, TariffpropertyComponent);
    }, 0);

  }

}
