import {FormArray, FormGroup, Validators} from '@angular/forms';
import {first} from 'rxjs/operators';

export function getControlsConfig() {
  return {
    query_building_type: [null, Validators.compose([Validators.required])],
    query_property_options: [null, Validators.compose([Validators.required])],
    query_property_operations: [null, Validators.compose([Validators.required])],
    query_price_min: [1],
    query_price_max: [1000000],
    query_currency: [null],
    query_address: [null],
    query_region: [null],
    query_city: [null],
    query_price_list: [null],
    query_type_sentence: [null],
    query_property_districts: [0],
    query_auction: [0],
    query_price_for: [null],
    query_extra: this._fb.array([]),
    query_filing_period: [null],
    query_services_list: [null],
    query_extra_data: [null]
  };
}

/************выносим часть кода формирования динамических доп. полей********************/
export function additionalStarts(indexForm = null) {

  const currentForm: FormGroup = this.currentFormArray(indexForm);
  const dataParamsFields: any = {};
  const idTypeProperty = Number(currentForm.get('query_building_type').value);
  const idPropertyOptions = Number(currentForm.get('query_property_options').value);
  const idPropertyOperations = Number(currentForm.get('query_property_operations').value);
  this.additionalCam[indexForm] = [];
  if (idTypeProperty > 0 && idPropertyOperations) {
    this.priority.getPropertyExtra(idTypeProperty, idPropertyOptions, idPropertyOperations)
      // .pipe(takeUntil(componentDestroyed(this)))
      .subscribe(result => {
        const queryExtra = currentForm.controls.query_extra as FormArray;
        queryExtra.controls = [];
        if (Array.isArray(result)) {
          this.extraAdditionalFieldModel[indexForm] = [];
          result.forEach((valField, index) => {
            const values = valField.extraFields;
            if (values.type === 'text') {
              this.extraAdditionalFieldModel[indexForm].push(values);
              if (Boolean(values.is_from_to)) {
                dataParamsFields[values.tbl_name] = this._fb.group({
                  [values.valRia + '[from]']: '',
                  [values.valRia + '[to]']: ''
                });
                this.additionalCam[indexForm][values.tbl_name] = [
                  {label: values.name + ' от', val: [values.valRia + '[from]'], type: values.type},
                  {label: values.name + ' до', val: [values.valRia + '[to]'], type: values.type}
                ];
              }
            } else if (values.type === 'check' || values.type === 'check_one_select') {
              let valRiaName;
              const valR = JSON.parse(values.valRia);
              for (const vN in valR) {
                valRiaName = vN;
              }
              if (valRiaName !== '') {
                this.extraAdditionalFieldModel[indexForm].push(values);
                dataParamsFields[values.tbl_name] = this._fb.group({[valRiaName]: ''});
                this.additionalCam[indexForm][values.tbl_name] = [
                  {label: values.name, val: [valRiaName], type: values.type, data: valR[valRiaName]}
                ];
              }
            } else if (values.type === 'check_one') {
              this.extraAdditionalFieldModel[indexForm].push(values);
              dataParamsFields[values.tbl_name] = this._fb.group({[values.valRia]: ''});
              this.additionalCam[indexForm][values.tbl_name] = [{
                label: values.name, val: [values.valRia], type: values.type, data: {
                  id: values.valRia.replace(/[^0-9]/g, ''),
                  name_ru: values.name
                }
              }];
            }
          });
          queryExtra.push(this._fb.group(dataParamsFields));
        }

        const formGroup = this.currentFormArray(indexForm);
        setTimeout(() => {
          if (this.patternService[indexForm]) {
            this.phasesService.unlockedConditions(formGroup);
            this.phasesService.lockedFieldConditions(formGroup, this.patternService[indexForm][this.typeNameBlocked[indexForm]]);
          } else {
            this.phasesService.unlockedConditions(formGroup);
          }
        }, 0);
      });
  }
}

/**********************игициализация ранее введенными данными первой формы*************************/
export function predefinedData() {
  const dataPreDef = this.phasesService.phaseContainer;
  const serviceSelected = [];
  if (Array.isArray(dataPreDef)) {
    dataPreDef.forEach((element, index) => {
      this.serviceSelected[index] = this.phasesService.preDefinedService(element, serviceSelected, index);
      const formGroup = this.currentFormArray(index);
      if (typeof element === 'object') {
        this.phasesService.getTypeName().pipe(first()
          // , takeUntil(componentDestroyed(this))
        ).subscribe((type) => {
            if (type) {
              const tDType = type[index];
              this.getOptionsProperty(tDType.type_id, index, tDType.typeName, false);
              this.viewCurrentPriceFor(index);
              setTimeout(() => {
                this.getCurrency(index);
              }, 0);

              if (element.query_region) {
                this.getCity(element.query_region, index);
              }
              if (element.query_city) {
                this.getDistrictsProperty(element.query_city, index);
              }
              Object.keys(element).forEach((field) => {
                const control = formGroup.get(field);
                if (field === 'query_currency') {
                  this.disabled[index] = false;
                  control.setValue(element[field]);
                } else {
                  try {
                    control.setValue(element[field]);
                  } catch (e) {
                  }
                }
              });
              this.subscribeChangeFormByFormExtra(index);
              const queryExtra = formGroup.get('query_extra') as FormArray;
              queryExtra.valueChanges.pipe(first()
                // , takeUntil(componentDestroyed(this))
              ).subscribe(() => {
                if (element.query_extra) {
                  const elementElement = element.query_extra;
                  // tslint:disable-next-line:no-shadowed-variable
                  elementElement.forEach((value, index) => {
                    const abstractControl = queryExtra.at(index);
                    Object.keys(value).forEach((field) => {
                      const control = ((abstractControl as FormGroup).controls[field] as FormGroup).controls;
                      Object.keys(control).forEach((fieldTwo) => {
                        control[fieldTwo].patchValue(value[field][fieldTwo]);
                      });
                    });
                  });
                }
              });
            }

          }
        );
      }
    });
  }
}

/************к такому ввиду нужно привести доп поля*******************************************/
// [{"val":1,"valRia":"gearbox[1]","name":"Ручная"},{"val":3,"valRia":"gearbox[3]","name":"Типтроник"}]
// query_number_of_rooms: {characteristic[209][from]: 1, characteristic[209][to]: 2, name: "Количество комнат"}
export function additionalFormationValues(dataQueryExtra, index) {
  const countData = dataQueryExtra.length - 1;
  const preData = {};
  if (this.extraAdditionalFieldModel) {

    if (Array.isArray(dataQueryExtra) && dataQueryExtra.length > 0) {
      if (countData >= 0) {

        Object.entries(dataQueryExtra[countData]).forEach(
          ([keyFirst, valueFirst]) => {
            if (valueFirst) {
              const pSecondObj = [];

              if (typeof valueFirst === 'object') {
                Object.entries(valueFirst).forEach(([keyLast, valLast]) => {
                  if (valLast) {
                    let sufics = '';
                    if (keyLast.indexOf('from') !== -1) {
                      sufics = ' от';
                    } else if (keyLast.indexOf('to') !== -1) {
                      sufics = ' до';
                    }
                    let name: any = [];
                    try {
                      name = this.extraAdditionalFieldModel[index]
                        .filter(item => item.tbl_name === keyFirst)[0].name + sufics;
                    } catch (e) {
                      return false;
                    }
                    if (Array.isArray(valLast)) {
                      try {
                        valLast.forEach(item => {
                          pSecondObj.push({val: item.id, valRia: keyLast, name: item.name_ru});
                        });
                      } catch (e) {
                      }
                    } else {
                      pSecondObj.push({val: valLast, valRia: keyLast, name});
                    }
                  }
                });

                if (Object.keys(pSecondObj).length !== 0) {
                  preData[keyFirst] = JSON.stringify(pSecondObj);
                }
              }
            }
          });
      }
    }
  } else {
    return false;
  }
  if (Object.keys(preData).length !== 0) {

    return preData;
  }
}
