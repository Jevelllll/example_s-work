import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Input,
  OnDestroy,
  OnInit,
  QueryList,
  Renderer2,
  ViewChildren
} from '@angular/core';
import {animate, style, transition, trigger} from '@angular/animations';
import {ServiceListModel} from '../../../../services/auth/ServiceListModel';
import {FormArray, FormBuilder, FormGroup} from '@angular/forms';
import {Observable, Subject} from 'rxjs';
import {
  Application_phases,
  getServiceList,
  patternOfSelectList,
  serviceListSelect
} from '../../../../services/auth/application-phases.service';
import {CommonService} from '../../../../services/common.service';
import {ActivatedRoute} from '@angular/router';
import {LoginService} from '../../../../services/auth/login.service';
import {CreateformdataService} from '../../../../services/auth/createformdata.service';
import {additionalFormationValues, additionalStarts, getControlsConfig, predefinedData} from './real-property-form-params';
import {PriorityPropertyService} from '../../../../services/priority/property.service';
import {MatOption} from '@angular/material/core';
import {clearObject, eventPriceC, urlRusLat} from '../../../../shared/helper-container-leave';
import {Location} from '@angular/common';
import {map, takeUntil, tap} from 'rxjs/operators';
import {
  DistrictsProperty,
  OperationProperty,
  PriceForDomRiaField,
  PropertyFilingPData,
  PropertyResModel,
  TypeProperty
} from '../../../../services/priority/propertyResModel';
import {SpinnerService} from '../../../../services/spinner.service';
import {MatFormField} from '@angular/material/form-field';

@Component({
  selector: 'app-real-property-form',
  templateUrl: './real-property-form.component.html',
  styleUrls: ['./real-property-form.component.scss'],
  animations: [
    trigger('dialog', [
      transition('void => *', [
        style({transform: 'scale3d(.1, .1, .1)'}),
        animate(50)
      ])
    ])
  ]
})
export class RealPropertyFormComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChildren('setWidth') setWidth: QueryList<ElementRef>;
  dataUnsetPattern = {};
  destroyed$ = new Subject<void>();
  lagSelectType = true;
  translite = urlRusLat;
  flagSelectType = true;
  cTitle = [];
  static defaultValSelect = 0;
  public wrapIndex: any;
  public serviceSelected = [];
  public serviceList: ServiceListModel[] = [];
  private CATEGORY_NAME = 'query_property';

  public InvoiceButtonHide = true;
  public tittle = null;
  public typeId = null;

  public JSON = JSON;
  public Object = Object;
  public Number = Number;

  public hoverShadow = false;

  disabled = [];
  typeNameBlocked = [];
  public patternService = [];

  items: FormArray;
  additionalItems: FormArray;

  public typeProperty: Observable<TypeProperty[]> = new Observable();
  public optionsProperty: Observable<any[]> = new Observable();
  public operationsProperty: Observable<OperationProperty[]> = new Observable();
  public typeSentence: Observable<any> = new Observable<any>();
  public filingPObs: Observable<PropertyFilingPData> = new Observable<PropertyFilingPData>();

  // tslint:disable-next-line:variable-name
  public $_region: Observable<any> = new Observable();
  // tslint:disable-next-line:variable-name
  public $_city: Observable<any> = new Observable();
  // tslint:disable-next-line:variable-name
  public $_districtsProperty: Observable<DistrictsProperty[]> = new Observable<DistrictsProperty[]>();

  public currencyL: Observable<any[]> = new Observable();
  public priceList = null;

  public valListPriceFor: any[] = [];
  private currentPriceFor: PropertyResModel[];
  private priceFor: PropertyResModel[];
  public typeIdS;
  public extraAdditionalFieldModel: any[] = [];
  /******************для обратного преобразования доп.полей******************************/
  private skeletonAdditionalFields: any = {};
  additionalCam = [];
  dataParamsFields = [];
  public formOutputId = 0;
  @Input() count = 1;
  @ViewChildren('BtNVew') BtNVew;
  public myForm: FormGroup;

  // tslint:disable-next-line:variable-name
  constructor(private _fb: FormBuilder,
              private renderer: Renderer2,
              private phasesService: Application_phases,
              private commonService: CommonService,
              private ref: ChangeDetectorRef,
              private location: Location,
              private route: ActivatedRoute,
              private login: LoginService,
              private spinnerService: SpinnerService,
              private toValidservice: CreateformdataService, private priority: PriorityPropertyService) {
  }

  ngOnInit() {
    this.flagSelectType = true;
    for (let index = 0; index < this.count; index++) {
      if (index === this.count - 1) {
        this.cTitle[index] = 'Подтвердить';
      } else {
        this.cTitle[index] = 'Следующая заявка';
      }
    }
    this.formInitialize();
    /************список сервисов***************************/
    getServiceList.call(this);
    this.getTypeProperty();
    this.getOperationProperty();
    this.getTypeSentence();
    this.getFilingPeriod();
    this.$_region = this.commonService.getRegionMore();
    /********тип цены********/
    this.priority.repearPropertyPriceForObs
      .pipe(takeUntil(this.destroyed$))
      .subscribe(resultPrice => {
        if (resultPrice) {
          this.priceFor = resultPrice;
        } else {
          this.priority.getPropertyPriceFor()
            .pipe(takeUntil(this.destroyed$))
            .subscribe(price => {
              this.priceFor = price;
            });
        }
      });

  }

  private formInitialize() {
    this.myForm = this._fb.group({title: 'property', items: this._fb.array([])});
    for (let index = 1; index <= this.count; index++) {
      this.disabled[index - 1] = true;
      this.serviceSelected.push({});
      this.addItem();
      this.valListPriceFor[index - 1] = [];
      this.optionsProperty[index] = [];
      this.extraAdditionalFieldModel[index - 1] = [];
      this.additionalCam[index - 1] = [];

    }
  }

  addItem(): void {
    this.items = this.myForm.get('items') as FormArray;
    this.items.push(this.createItem());
  }

  private createItem() {
    return this._fb.group(getControlsConfig.call(this));
  }

  public activeFromWrapQuery(data) {
    if (data) {
      this.formOutputId = data - 1;
    }
  }

  /*****************Выбор Сервисов*******************************************/
  public serviceListSelect(serList, index) {
    serviceListSelect.apply(this, [serList, index]);
    // this.removeSkipUnsetFields(index);
    // переход из first-level-options

    const formGroup = this.currentFormArray(index);
    if (formGroup.get('query_region').value) {
      this.$_city[index] = new Observable();
      this.$_city[index] = this.patternsOfSelectList(index, 1, this.commonService.getCities(formGroup.get('query_region').value));
      if (formGroup.get('query_city').value) {
        this.getDistrictsProperty(formGroup.get('query_city').value, index);
      }
    }

    // if (formGroup.get('query_property_districts').value) {
    //   this.getDistrictsProperty(formGroup.get('query_property_districts').value, index);
    // }

    if (this.flagSelectType) {
      this.typeIdS = this.route.snapshot.paramMap.get('type_id');
      if (this.typeIdS) {
        this.typeId = this.typeIdS;

        setTimeout(() => {
          this.currentFormArray(0).get('query_building_type').patchValue(Number(this.typeIdS));
          this.getCurrency(0);
          const firstMatType: any = this.BtNVew;
          if (Array.isArray(firstMatType._results)) {
            firstMatType._results.forEach((val: MatOption) => {
              if (val.selected) {
                this.getOptionsProperty(Number(this.typeIdS), index, urlRusLat(val.selected['viewValue']));
              }
            });
          }
        }, 15);

        this.location.replaceState(this.location.path().replace(`/${this.typeIdS}`, ''));
        this.flagSelectType = false;
      }
    }

    // переход из first-level-options
  }

  private currentFormArray(index) {
    const arrayControls = this.myForm.get('items') as FormArray;
    return arrayControls.controls[index] as FormGroup;
  }

  /********************приведение к общем данным сервисов patterns*******************************/
  generalViewOfServiceRules(index: number) {
    this.patternService[index] = this.phasesService.generalViewOfServiceRules(index, this.serviceSelected, this.serviceList);
  }

  /******************************* / блокировка полей ввода**************************************/
  isDisabledInput(event) {
    this.hoverShadow = event;
  }

  getTypeProperty() {
    this.typeProperty = this.priority.getTypeOfProperty();
  }

  /***************тип недвижимости**************/
  // tslint:disable-next-line:variable-name
  getOptionsProperty(type_id: number, index, typeName, subjSend = true) {
    this.subscribeChangeFormByFormExtra(index);
    this.typeNameBlocked[index] = urlRusLat(typeName);
    if (subjSend) {
      this.phasesService.typeName[index] = {type_id, typeName: urlRusLat(typeName)};
    }

    const formGroup = this.currentFormArray(index);
    if (this.patternService[index]) {
      this.phasesService.lockedFieldConditions(formGroup, this.patternService[index][this.typeNameBlocked[index]]);
    } else {
      this.phasesService.unlockedConditions(formGroup);
    }
    formGroup.get('query_property_options').setValue(0);
    this.viewCurrentPriceFor(index);
    if (type_id === RealPropertyFormComponent.defaultValSelect) {
      this.optionsProperty[index] = new Observable<any[]>();
    } else {
      this.optionsProperty[index] = this.priority.getOptionsProperty(type_id);
    }
  }

  /*******тип операции продажа, аренда и др..****/
  getOperationProperty() {
    this.operationsProperty = this.priority.getOperationsPropertyMore();
  }

  /*****Проверка заполненных полей query_property_operations,query_property_options**формирование поля query_price_for**********/
  viewCurrentPriceFor(indexForm: number) {

    const formGroup = this.currentFormArray(indexForm);
    this.valListPriceFor[indexForm] = null;
    formGroup.controls.query_price_for.patchValue(0);
    const propertyOperationsId = formGroup.controls.query_property_operations.value;
    const propertyOptionsId = formGroup.controls.query_property_options.value;
    const validOperationsId = (propertyOperationsId !== 0);
    const validOptionsId = (propertyOptionsId !== 0);
    let currentPriceValues: PriceForDomRiaField;

    function getPriceFor() {
      return value => (Number(value.property_operations_id) === Number(propertyOperationsId)
        && Number(value.property_options_id) === Number(propertyOptionsId));
    }

    if (validOperationsId && validOptionsId) {
      this.currentPriceFor = this.priceFor.filter(getPriceFor());
      if (this.currentPriceFor) {
        if (this.currentPriceFor.length > 0) {
          currentPriceValues = this.currentPriceFor[0].priceForDomriaField;
          let prForVal;
          try {
            prForVal = JSON.parse(currentPriceValues.name);
          } catch (e) {
            prForVal = {};
          }
          if (prForVal) {
            this.valListPriceFor[indexForm] = [];
            if (Array.isArray(prForVal.options)) {
              prForVal.options.map(val => {
                this.valListPriceFor[indexForm].push({value: `${prForVal.val}=${val.characteristic_id}`, name: val.name});
              });
            }
          }
        }
      }
    }
  }

  /******************Получение валют*********************************/
  public getCurrency(indexForm: number) {
    if (typeof indexForm === 'number') {
      const formGroup = this.currentFormArray(indexForm);
      const propertyOperation = formGroup.get('query_property_operations').value;
      const queryBuildingType = formGroup.get('query_building_type').value;
      if (propertyOperation && queryBuildingType) {
        this.currencyL = this.priority.getCurrency(propertyOperation, queryBuildingType)
          .pipe(tap(item => {
            this.priceList = item.priceList[0];
            formGroup.get('query_price_list').setValue(this.priceList.id);
          }), map((item) => {
            let nClist = item.currencyList.filter(it => it.currency.currency_description_short !== 'EUR');
            return nClist;
            // return  item.currencyList;
          }));
      } else {
        this.priceList = null;
      }

    }
  }

  /************Тип предложения***************************/
  getTypeSentence() {
    this.typeSentence = this.priority.getTypeSentenceMore();
  }

  /**********************период подачи************************************/
  getFilingPeriod() {
    this.filingPObs = this.priority.filingPeriodMore();
  }

  /******************настройка параметров сумм********************************/
  eventPrice(sliderPriceMin = null, sliderPriceMax = null, i = null) {
    eventPriceC.call(this, sliderPriceMin, sliderPriceMax, this.currentFormArray(i));
  }

  // tslint:disable-next-line:variable-name
  getCity(region_id, index) {
    if (index !== null) {
      const cF = this.currentFormArray(index);
      cF.get('query_city').setValue(null, {emitEvent: false});
    }
    if (region_id) {
      this.$_city[index] = new Observable();
      this.$_city[index] = this.patternsOfSelectList(index, 1, this.commonService.getCities(region_id));
    }
  }

  // tslint:disable-next-line:variable-name
  getDistrictsProperty(id_city, index) {
    if (index !== null) {
      const cF = this.currentFormArray(index);
      cF.get('query_property_districts').setValue(0, {emitEvent: false});
    }
    if (id_city) {
      this.$_districtsProperty[index] = new Observable();
      this.$_districtsProperty[index] = this.patternsOfSelectList(index, 1, this.priority.getDistrictsProperty(id_city));
    }
  }

  currencyChange(index) {
    this.disabled[index] = false;
  }

  /**********работа с экземпляром форм******************/
  addNewForm(indexForm: number, confirm) {
    if (this.count === 1) {
      this.phasesService.nextPhaseClickM(true);
    }
    const formGroup = (<FormArray> this.myForm.get('items')).at(Number(indexForm));
    const isValid = formGroup.valid;
    formGroup.patchValue({query_services_list: this.JSON.stringify(this.serviceSelected[indexForm])});
    if (isValid) {
      // tslint:disable-next-line:variable-name
      const query_extra_data = additionalFormationValues.call(this, formGroup.get('query_extra').value, indexForm);
      if (query_extra_data) {
        if (Object.keys(query_extra_data)) {
          formGroup.get('query_extra_data').setValue(JSON.stringify(query_extra_data), {emitEvent: false});
          // formGroup.get('query_extra').setValue(JSON.stringify(query_extra_data), {emitEvent: false});
        }
      }
      this.phasesService.buttonNextDisable(false);
      const object = clearObject(formGroup.value);
      delete object.query_extra;
      object.title = this.myForm.get('title').value;
      this.phasesService.phaseContainer[indexForm] = object;
      if (this.count === indexForm + 1) {

      } else {
        this.activeFromWrapQuery(indexForm + 2);
      }

    }
    if (indexForm === this.count - 1) {
      this.renderer.addClass(confirm, 'confirm_btn_success');
      this.renderer.removeClass(confirm, 'confirm_btn');
    } else {
      this.login.setScroll();
    }
  }

  /***************паттерн на списки*****************/
  public patternsOfSelectList(indexForm, idSelect = 1, obSElement) {
    return patternOfSelectList.call(this, indexForm, idSelect, obSElement);
  }

  /******************работа с доп полями************************/
  subscribeChangeFormByFormExtra(indexForm) {
    additionalStarts.call(this, indexForm);
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }

  ngAfterViewInit(): void {
    this.setWidth.changes
      .pipe(takeUntil(this.destroyed$))
      .subscribe(datas => {
        if (datas instanceof QueryList && datas.length !== 0) {
          datas.toArray().forEach((resMat: MatFormField) => {
            const child: HTMLElement = resMat._elementRef.nativeElement.children[0];
            this.renderer.addClass(child, 'w-100');
          });
        }
        this.ref.detectChanges();
      });
    setTimeout(() => {
      predefinedData.call(this);
      this.phasesService.getDataPattenrn()
        .pipe(takeUntil(this.destroyed$))
        .subscribe(result => {
          this.dataUnsetPattern = result;
        });
    }, 0);
  }
}
