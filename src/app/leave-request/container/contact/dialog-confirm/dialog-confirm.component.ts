import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-dialog-confirm',
  templateUrl: './dialog-confirm.component.html',
  styleUrls: ['./dialog-confirm.component.scss']
})
export class DialogConfirmComponent implements OnInit {

  public controlPhone: any;
  constructor(@Inject(MAT_DIALOG_DATA) public data: any, public dialogRef: MatDialogRef<DialogConfirmComponent>) { }

  ngOnInit(): void {
    this.controlPhone = new FormControl('', {validators: Validators.compose([Validators.required, Validators.pattern(/^\d+$/)])});
  }
  onNoClick(): void {
    this.dialogRef.close(false);
  }
}
