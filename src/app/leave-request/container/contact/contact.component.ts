import {AfterViewInit, Component, OnDestroy, OnInit} from '@angular/core';
import {delay, distinctUntilChanged, first, map, takeUntil} from 'rxjs/operators';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {BoardRulesComponent} from '../../board-rules/board-rules.component';
import {HideshowtabsService} from '../../../services/leave-request/hideshowtabs.service';
import {Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {CreateformdataService} from '../../../services/auth/createformdata.service';
import {LoginService} from '../../../services/auth/login.service';
import {Application_phases, CurrentPhase} from '../../../services/auth/application-phases.service';
import {SpinnerService} from '../../../services/spinner.service';
import {MustMatch} from '../../../shared/other-help';
import {MatDialog} from '@angular/material/dialog';
import {Observable, Subject} from 'rxjs';
import {DialogConfirmComponent} from './dialog-confirm/dialog-confirm.component';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit, AfterViewInit, OnDestroy {
  isOneRegVisible: Observable<boolean>;
  destroyed$ = new Subject<void>();
  public flag = true;
  private phoneNumber: any;
  valid = false;
  public successMsg = '';
  public areaCodeMask = ['(', /[0-9]/, /\d/, /\d/, ')', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
  public regExpPhone = new RegExp(/^(\([0-9]{3}\)\s*|[0-9]{3}\-)[0-9]{3}-[0-9]{4}$/);
  public contactsForm: FormGroup;
  private preDataEqually = {};
  private sendFormPhone;
  public passwordView = true;
  public passwordConfirmView = true;
  public passwordGlStyle = {position: 'absolute', right: '52px', top: '2.7rem', 'font-size': '1.5rem'};

  public openBoard_rulesModal() {
    // tslint:disable-next-line:max-line-length
    const dialogRef = this.dialog.open(BoardRulesComponent, {
      // width: '600px',
      minHeight: '170px',
      panelClass: 'custom-modalbox-board-rules',
      disableClose: true
    });
  }

  constructor(private disabledService: HideshowtabsService,
              public dialog: MatDialog,
              // tslint:disable-next-line:variable-name
              private _fb: FormBuilder,
              private router: Router,
              private toster: ToastrService,
              private toValidservice: CreateformdataService,
              private loginService: LoginService,
              private spinnerService: SpinnerService,
              private phasesService: Application_phases) {
  }

  public mainForm() {

    this.contactsForm = this._fb.group({
      first_name: ['', [Validators.minLength(3), Validators.maxLength(144)]],
      last_name: ['', [Validators.minLength(3), Validators.maxLength(144)]],
      phone: ['', Validators.compose([
        Validators.required,
        Validators.pattern(this.regExpPhone),
        this.existUser.bind(this)
      ])
      ],
      email: ['', Validators.compose([Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,3}$')])],
      password: ['', Validators.compose([Validators.minLength(5),
        Validators.required,
        Validators.maxLength(144)])],
      confirmPassword: ['', Validators.compose([Validators.minLength(5), Validators.required,
        Validators.maxLength(144)])]
    }, {
      validator: MustMatch('password', 'confirmPassword')
    });

    this.contactsForm.statusChanges
      .pipe(map(data => data === 'VALID'), distinctUntilChanged(), takeUntil(this.destroyed$))
      .subscribe((isValid) => {
        if (isValid) {
          // this.contactsForm.get('phone').setValue(this.sendFormPhone, {emitEvent: false});
          this.phasesService.add_phase_contact(this.contactsForm.value);
          this.phasesService.buttonNextDisable(false);
        } else {
          this.phasesService.buttonNextDisable(true);
        }
      });

  }

  existUser(control: AbstractControl): any {
    let isExistNumber = false;
    const regExp = new RegExp(/[-_()/\\]/ig);
    const phoneValue = control.value.replace(regExp, '');
    const phoneLength = phoneValue.length;
    this.sendFormPhone = phoneValue;
    // if (this.phoneNumber !== phoneValue) {
    if (phoneLength === 10 && control.pristine === false) {
      this.phoneNumber = phoneValue;
      this.spinnerService.show();
      this.loginService.checkPhome(phoneValue, true)
        .pipe(
          first(),
          takeUntil(this.destroyed$),
          delay(2000))
        .subscribe((valResp): any => {
          if (valResp['status'] === 200 && valResp['message'] === 'User not exists') {
            // this.phasesService.buttonNextDisable(false);
            // this.contactsForm.controls.phone.setErrors(null);
          } else {
            this.contactsForm.controls.phone.setErrors({incorrect: true});
            this.phasesService.buttonNextDisable(true);
          }
          this.spinnerService.hide();
        }, error1 => {
          this.phasesService.buttonNextDisable(true);
          isExistNumber = true;
          this.toster.warning('Данный номер существует в базе данных');
          this.contactsForm.controls.phone.setErrors({incorrect: true});
          this.spinnerService.hide();
        }, () => {
          this.spinnerService.hide();
        });
    }
  }

  openConfirmPhoneDialog() {
    this.phasesService.obsShowConfirmPhoneDialog.pipe(takeUntil(this.destroyed$)).subscribe((isS) => {
      if (isS) {
        this.toValidservice.realPhoneNumber(this.phoneNumber).subscribe((result) => {
          this.toster.info('Введите код подстверждения из смс уведомления.', '', {timeOut: 10000});
        }, (error) => {
          this.toster.error('Ошибка сервера. Повторите попытку позже.');
        });
        this.contactsForm.controls.phone.setErrors({incorrectConfirm: true});
        const dialogRef = this.dialog.open(DialogConfirmComponent, {
          data: this.contactsForm.get('phone').value,
          autoFocus: false,
          disableClose: true
        });
        dialogRef.afterClosed().pipe(takeUntil(this.destroyed$)).subscribe(result => {
          if (result) {
            this.toValidservice.cRealPhoneNumber(Number(result))
              .pipe(takeUntil(this.destroyed$))
              .subscribe((dataResult) => {
                if (dataResult) {
                  const currentPhase = new CurrentPhase(false, false, false, true);
                  this.phasesService.add_current_phase(currentPhase);
                  this.phasesService.setShowConfirmPhoneDialog(false);
                  this.phasesService.setConfirmPhoneButton(false);
                  this.phasesService.buttonNextDisable(true);
                  this.contactsForm.controls.phone.setErrors(null);
                  this.toster.info('Номер успешно подтвержден', '', {timeOut: 10000});
                }
              }, error => {
                this.toster.error('Код не верный.');
                this.contactsForm.get('phone').patchValue(this.contactsForm.get('phone').value.slice(0, -1));
              });

          } else {
            this.contactsForm.get('phone').patchValue(this.contactsForm.get('phone').value.slice(0, -1));
          }
        });
        setTimeout(() => {
          document.querySelector('.cdk-global-scrollblock').classList.remove('cdk-global-scrollblock');
        }, 0);
      }

    });

  }

  ngOnInit() {

    this.isOneRegVisible = this.phasesService.obsOneRegistration;
    // this.contactsForm.controls.phone.setValue('');
    this.phasesService.buttonNextDisable(true);
    this.mainForm();

    /***************************инициализация ранее введенных данных контакт формы***************/
    this.phasesService.get_phase_contact()
      .pipe(takeUntil(this.destroyed$))
      .subscribe(repeatContactForm => {
        if (repeatContactForm) {
          // delete repeatContactForm.phone;
          Object.keys(repeatContactForm).forEach((field) => {
            if (field !== 'phone') {
              this.contactsForm.get(field).setValue(repeatContactForm[field], {emitEvent: false});
            }
          });
        }
      });
    this.openConfirmPhoneDialog();
  }

  ngAfterViewInit(): void {

    setTimeout(() => {
      this.openBoard_rulesModal();
    }, 0);
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }

  /*************finish first Form*****************/
  goNextM() {
    if (this.contactsForm.valid) {
      this.phasesService.setFinishContactForm(true);
    } else {
      this.phasesService.setFinishContactForm(true);
    }
  }
}
