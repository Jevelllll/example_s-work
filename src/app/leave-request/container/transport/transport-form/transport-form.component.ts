import {AfterViewInit, Component, Input, OnDestroy, OnInit, Renderer2, ViewChildren} from '@angular/core';
import {animate, style, transition, trigger} from '@angular/animations';
import {ServiceListModel} from '../../../../services/auth/ServiceListModel';
import {BehaviorSubject, Observable, Subject} from 'rxjs';
import {BodyType, FuelType, Gearbox, MarkAuto, ModelByMark, Typeauto, TypeOfDriveInterface} from '../../../../services/priority/resModel';
import {TransportFormParams} from './transport-form-params';
import {FormArray, FormBuilder, FormGroup} from '@angular/forms';
import {addNewForm, Application_phases, getServiceList, serviceListSelect, patternOfSelectList} from '../../../../services/auth/application-phases.service';
import {ActivatedRoute} from '@angular/router';
import {LoginService} from '../../../../services/auth/login.service';
import {CreateformdataService} from '../../../../services/auth/createformdata.service';
import {CommonService} from '../../../../services/common.service';
import {PriorityAutoService} from '../../../../services/priority/auto.service';
import {clearObject, eventPriceC, urlRusLat} from '../../../../shared/helper-container-leave';
import {first, map, takeUntil, withLatestFrom} from 'rxjs/operators';
import {MatOption} from '@angular/material/core';
import {Location} from '@angular/common';
import {SpinnerService} from '../../../../services/spinner.service';

@Component({
  selector: 'app-transport-form',
  templateUrl: './transport-form.component.html',
  styleUrls: ['./transport-form.component.scss'],
  animations: [
    trigger('dialog', [
      transition('void => *', [
        style({transform: 'scale3d(.1, .1, .1)'}),
        animate(50)
      ])
    ])
  ]
})
export class TransportFormComponent implements OnInit, AfterViewInit, OnDestroy {
  destroyed$ = new Subject<void>();
  flagSelectType = true;
  public wrapIndex: any;

  public cTitle = [];
  public serviceSelected = [];
  public serviceList: ServiceListModel[] = [];
  private CATEGORY_NAME = 'tbl_query_auto';
  public JSON = JSON;
  public Object = Object;
  public Number = Number;
  public InvoiceButtonHide = true;
  public tittle = null;
  public typeId = null;
  public typeAuto: Observable<Typeauto[]> = new Observable();
  public typeAutoFinal: Observable<Typeauto[]> = new Observable();
  public bodyTypeAuto: Observable<BodyType[]> = new Observable();
  public bodyTypeAutoFinal: Observable<BodyType[]> = new Observable();
  public typeOfDrive: Observable<TypeOfDriveInterface> = new Observable();
  public markAutoApi: Observable<MarkAuto[]> = new Observable();
  public markAutoApiFinal: Observable<MarkAuto[]> = new Observable();
  public modelAutoApi: Observable<ModelByMark[]> = new Observable();
  public modelFuelTypeApi: Observable<FuelType> = new Observable();
  public modelGearBoxMoreApi: Observable<Gearbox> = new Observable();
  public modelFillingObSMoreApi: Observable<any> = new Observable();
  public region$: Observable<any> = new Observable();
  public city$: Observable<any> = new Observable();
  public yearAfFool = [];
  public yearBeFool = [];
  public hoverShadow = false;
  disabled = [];
  typeNameBlocked = [];
  public patternService = [];
  transportFormParams = TransportFormParams;
  public myForm: FormGroup;
  items: FormArray;
  @Input() count = 1;
  @Input() typeIdS;
  @ViewChildren('BtNVew') BtNVew;
  public formOutputId = 0;

  // tslint:disable-next-line:variable-name
  constructor(private _fb: FormBuilder,
              private phasesService: Application_phases,
              private commonService: CommonService,
              private renderer: Renderer2,
              private location: Location,
              private route: ActivatedRoute,
              private login: LoginService,
              private toValidservice: CreateformdataService,
              private spinnerService: SpinnerService,
              public priority: PriorityAutoService) {
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      this.predefinedData();
    }, 0);

  }

  ngOnDestroy(): void {
    this.destroyed$.next();
    // this.__commonObsList.forEach((subs) => subs.unsubscribe());
  }

  ngOnInit(): void {

    this.flagSelectType = true;
    this.formInitialize();

    /************список сервисов***************************/
    getServiceList.call(this);
    // this.getTypeAuto();
    this.yearMore();
    this.modelFuelTypeApi = this.priority.getFuelTypeMore();
    this.modelGearBoxMoreApi = this.priority.getGearBoxMore();
    this.modelFillingObSMoreApi = this.priority.getFillingObS();
    this.region$ = this.commonService.getRegionMore();
  }

  // tslint:disable-next-line:variable-name
  getCity(region_id, index = null) {

    if (index !== null) {
      const cF = this.currentFormArray(index);
      cF.get('query_city').setValue(null, {emitEvent: false});
    }
    if (region_id) {
      this.city$ = new Observable();
      this.city$ = this.patternsOfSelectList(index, 1, this.commonService.getCities(region_id));
    }
  }

  /***************************set data ************************************/
  getTypeAuto(indexForm): void {
    this.priority.repeatAutoTypeObs
      .pipe(takeUntil(this.destroyed$))
      .subscribe((result) => {
        if (result) {
          // tslint:disable-next-line:variable-name
          const _typeAuto: BehaviorSubject<any> = new BehaviorSubject<any>(result);
          this.typeAuto[indexForm] = _typeAuto.asObservable();
          this.typeAutoFinal[indexForm] = this.typeAuto[indexForm];
          // this.__commonObsList.push(this.typeAuto[indexForm]);
        } else {
          this.typeAuto[indexForm] = this.priority.getTypeAuto();
          this.typeAutoFinal[indexForm] = this.typeAuto[indexForm];
          // this.__commonObsList.push(this.typeAuto[indexForm]);
        }
      });
  }

  /***************паттерн на списки*****************/
  public patternsOfSelectList(indexForm, idSelect = 1, obSElement) {
    return patternOfSelectList.call(this, indexForm, idSelect, obSElement);
  }

  /******************тип кузова**********/
  // tslint:disable-next-line:variable-name
  public getBodyType(auto_type_id, index, typeName, subjSend = true): void {
    // setTimeout(() => {
    //   this.bodyTypeAutoFinal[index] = this.patternsOfSelectList(index, auto_type_id, this.bodyTypeAuto[index]);
    // }, 0);


    this.typeNameBlocked[index] = urlRusLat(typeName);
    if (subjSend) {
      this.phasesService.typeName[index] = {auto_type_id, typeName: urlRusLat(typeName)};
    }
    this.generalViewOfServiceRules(index);
    const formGroup = this.currentFormArray(index);


    this.cleanAutoTypeSelect(index, formGroup);
    this.cleanBodyType(formGroup, index);
    this.cleanAutoMark(formGroup, index);

    this.phasesService.unlockedConditions(formGroup);
    if (this.patternService[index]) {
      this.phasesService.lockedFieldConditions(formGroup, this.patternService[index][this.typeNameBlocked[index]]);
    } else {
      this.phasesService.unlockedConditions(formGroup);
    }

    if (typeof Number(auto_type_id) === 'number') {
      const resetField = {query_body_type: null, query_drive_type: null, query_auto_mark: null, query_auto_model: null};
      this.modelAutoApi[index] = new Observable<[]>();
      this.currentFormArray(index).patchValue(resetField);
      this.bodyTypeAuto[index] = this.priority.getBody_type(auto_type_id);
      this.bodyTypeAutoFinal[index] = this.priority.getBody_type(auto_type_id);
    }
  }

  /*****************Выбор Сервисов*******************************************/
  public serviceListSelect(serList, index) {
    serviceListSelect.apply(this, [serList, index]);
    this.typeIdS = this.route.snapshot.paramMap.get('type_id');

    const formGroup = this.currentFormArray(index);

    this.cleanAutoTypeSelect(index, formGroup);
    this.cleanBodyType(formGroup, index);
    this.cleanAutoMark(formGroup, index);

    if (this.typeId) {
      if (formGroup.get('query_region').value) {
        formGroup.get('query_city').setValue(null, {emitEvent: false});
        this.city$ = this.patternsOfSelectList(index, 1, this.commonService.getCities(formGroup.get('query_region').value));
      }
    }
    // переход из first-level-options
    if (this.flagSelectType) {
      if (this.typeIdS) {
        this.typeId = this.typeIdS;
        setTimeout(() => {
          this.currentFormArray(0).get('query_auto_type').setValue(Number(this.typeIdS));
          this.getTypeOfDrive(Number(this.typeIdS), 0);
          this.getMark(Number(this.typeIdS), 0);
          const firstMatType: any = this.BtNVew;
          if (Array.isArray(firstMatType._results)) {
            firstMatType._results.forEach((val: MatOption) => {
              if (val.selected) {
                this.getBodyType(Number(this.typeIdS), 0, urlRusLat(val.viewValue));
              }
            });
          }
        }, 0);
        // @ts-ignore


        this.location.replaceState(this.location.path().replace(`/${this.typeIdS}`, ''));
        this.flagSelectType = false;
        // переход из first-level-options
      }
    }

  }

  private cleanAutoMark(formGroup: FormGroup, index) {
    /********если марка авто при выбранных сервисах не равен пред.. сбрасываем знач. формы*****/
    if (formGroup.get('query_auto_type').value) {
      setTimeout(() => {
        const preMarkAuto = this.patternsOfSelectList(index, 1, this.markAutoApi[index]);
        this.markAutoApi[index].pipe(withLatestFrom(preMarkAuto)
          // , takeUntil(componentDestroyed(this))
        ).pipe(takeUntil(this.destroyed$))
          .subscribe(([dataF, dataL]) => {
            const find = dataL.find((data) => data.value == formGroup.get('query_auto_mark').value);
            if (!find) {
              formGroup.get('query_auto_mark').reset();
              formGroup.get('query_auto_model').reset();
            }
            this.markAutoApiFinal[index] = new Observable((data) => data.next(dataL));
          });
      }, 0);
    }
  }

  private cleanBodyType(formGroup: FormGroup, index) {
    /********если тип кузова авто при выбранных сервисах не равен пред.. сбрасываем знач. формы*****/
    if (formGroup.get('query_auto_type').value) {
      setTimeout(() => {
        const preBodyType = this.patternsOfSelectList(index, formGroup.get('query_auto_type').value, this.bodyTypeAuto[index]);
        this.bodyTypeAuto[index].pipe(withLatestFrom(preBodyType)
          // , takeUntil(componentDestroyed(this))
        ).pipe(takeUntil(this.destroyed$))
          .subscribe(([dataF, dataL]) => {
            if (formGroup.get('query_body_type').value) {
              const find = dataL.find((data) => data.value == JSON.parse(formGroup.get('query_body_type').value).value || null);
              if (!find) {
                formGroup.get('query_body_type').reset();
              }
            }
            this.bodyTypeAutoFinal[index] = new Observable((data) => data.next(dataL));
          });
      }, 0);
    }
  }

  private cleanAutoTypeSelect(index, formGroup: FormGroup) {
    /********если тип авто при выбранных сервисах не равен пред.. сбрасываем знач. формы*****/
    const preToeAuto = this.patternsOfSelectList(index, 1, this.typeAuto[index]);
    this.typeAuto[index].pipe(withLatestFrom(preToeAuto)
      // , takeUntil(componentDestroyed(this))
    )
      .pipe(takeUntil(this.destroyed$))
      .subscribe(([dataF, dataL]) => {
        const find = dataL.find((data) => data.id == formGroup.get('query_auto_type').value);
        if (!find) {
          formGroup.get('query_auto_type').reset();
          formGroup.get('query_body_type').reset();
        }
        this.typeAutoFinal[index] = new Observable((data) => data.next(dataL));
      });
  }

  /*****************Тип привода по типу транспорта*******************************************/
  public getTypeOfDrive(idTypeAuto: number, index): void {
    if (typeof Number(idTypeAuto) === 'number') {
      this.typeOfDrive[index] = this.priority.getTypeOfDrive(idTypeAuto).pipe(map(typeDr => typeDr.data));
    }

  }

  /*****************По марке транспорта*******************************************/
  getMark(id: number = null, index): void {

    if (typeof Number(id) === 'number') {
      this.markAutoApi[index] = this.priority.getAllMarkAutoAutoTypeId(id);
      this.markAutoApiFinal[index] = this.markAutoApi[index];
    }
  }

  /*****************По модели транспорта*******************************************/
  getModel(id: number, index) {
    if (typeof Number(id) === 'number' && typeof Number(this.typeId) === 'number') {
      this.currentFormArray(index).patchValue({query_auto_model: null});
      this.modelAutoApi[index] = this.priority.getModelAuto(id, this.typeId);
    }
  }

  /*****************Год выпуска*******************************************/
  yearMore() {
    this.priority.yearMore()
      .pipe(takeUntil(this.destroyed$))
      .subscribe((data) => {
        for (let index = 0; index < this.count; index++) {
          this.yearBeFool[index] = data['s_yers'];
          this.yearAfFool[index] = data['po_yers'];

          if (index === this.count - 1) {
            this.cTitle[index] = 'Подтвердить';
          } else {
            this.cTitle[index] = 'Следующая заявка';
          }
          this.getTypeAuto(index);
        }
      });
  }

  /**********************игициализация ранее введенными данными первой формы*************************/
  predefinedData() {
    // let conditionControl = ['query_auto_mark', 'query_auto_model', 'query_price_min', 'query_price_max'];
    const dataPreDef = this.phasesService.phaseContainer;
    const serviceSelected = [];

    if (Array.isArray(dataPreDef)) {
      dataPreDef.forEach((element, index) => {
        this.serviceSelected[index] = this.phasesService.preDefinedService(element, serviceSelected, index);
        const formGroup = this.currentFormArray(index);
        if (typeof element === 'object') {
          this.phasesService.getTypeName().pipe(first()
            , takeUntil(this.destroyed$)
          ).subscribe((type) => {
            if (type) {
              const tDType = type[index];
              this.getBodyType(tDType.auto_type_id, index, tDType.typeName, false);
              this.getTypeOfDrive(tDType.auto_type_id, index);
              this.getMark(tDType.auto_type_id, index);
              this.typeId = tDType.auto_type_id;
              this.currencyChange(index);
              if (element.query_auto_mark) {
                this.getModel(element.query_auto_mark, index);
              }
              if (element.query_region) {
                this.getCity(element.query_region);
              }
              Object.keys(element).forEach((field) => {
                const control = formGroup.get(field);
                try {
                  control.setValue(element[field]);
                } catch (e) {
                }
              });
            }
          });
        }
      });
    }
  }

  private currentFormArray(index) {
    const arrayControls = this.myForm.get('items') as FormArray;
    return arrayControls.controls[index] as FormGroup;
  }

  private createItem() {
    return this._fb.group(TransportFormParams.CONTROLSCONFIG);
  }

  addItem(): void {
    this.items = this.myForm.get('items') as FormArray;
    this.items.push(this.createItem());
  }

  private formInitialize() {
    this.myForm = this._fb.group({title: 'auto', items: this._fb.array([])});
    for (let index = 1; index <= this.count; index++) {
      this.disabled[index - 1] = true;
      this.serviceSelected.push({});
      this.addItem();
    }
  }

  /**********работа с экземпляром форм******************/
  addNewForm(indexForm: number, confirm) {
    // this.wrapIndex = indexForm;
    addNewForm.apply(this, [indexForm]);
    if (indexForm === this.count - 1) {

      this.renderer.addClass(confirm, 'confirm_btn_success');
      this.renderer.removeClass(confirm, 'confirm_btn');
    } else {
      this.login.setScroll();
    }
  }

  /******************************* / блокировка полей ввода**************************************/
  isDisabledInput(event) {
    this.hoverShadow = event;
  }

  /******************настройка параметров сумм********************************/
  eventPrice(sliderPriceMin = null, sliderPriceMax = null, i = null) {
    eventPriceC.call(this, sliderPriceMin, sliderPriceMax, this.currentFormArray(i));
  }

  currencyChange(index) {
    this.disabled[index] = false;
  }

  /********************приведение к общем данным сервисов patterns*******************************/
  generalViewOfServiceRules(index: number) {
    this.patternService[index] = this.phasesService.generalViewOfServiceRules(index, this.serviceSelected, this.serviceList);
  }

  public activeFromWrapQuery(data) {
    if (data) {
      this.formOutputId = data - 1;
    }
  }

}
