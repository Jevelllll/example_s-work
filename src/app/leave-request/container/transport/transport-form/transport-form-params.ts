import {FormGroup, Validators} from '@angular/forms';

export class TransportFormParams {

  static CONTROLSCONFIG = {
    query_auto_type: [null, Validators.compose([Validators.required])],
    query_auto_mark: [null],
    query_auto_model: [null],
    query_price_min: [1],
    query_price_max: [100000],
    query_currency: [null, Validators.compose([Validators.required])],
    query_address: [null],
    query_region: [null],
    query_city: [null],
    query_body_type: [null],
    query_auction: [null],
    query_s_yers: [null],
    query_po_yers: [null],
    query_drive_type: [null],
    query_gearbox: [null],
    query_fuel: [],
    query_filing_period: [0],
    query_race: [],
    raceFrom: [null, Validators.maxLength(4)],
    raceTo: [null, Validators.maxLength(4)],
    query_services_list: []
  };

  /**********дата выпуска от и до****************************/
  static fixYears(myForm: FormGroup) {
    const yearMin = myForm.controls.query_s_yers.value,
      yearMax = myForm.controls.query_po_yers.value;
    let modelYear: any = {query_s_yers: yearMin, query_po_yers: yearMax};
    if (yearMin && yearMax) {
      if (yearMin > yearMax) {
        modelYear.query_po_yers = yearMin;
        modelYear.query_s_yers = yearMax;
        myForm.patchValue(modelYear);
      }
    }
  }


}
