import {Component, OnDestroy, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {Application_phases, CurrentPhase} from '../../../services/auth/application-phases.service';
import {ConstingService} from '../../../services/consting/consting.service';
import {DynamicInterface} from '../dynamicInterface';
import {ContactComponent} from '../contact/contact.component';
import {TransportFormComponent} from './transport-form/transport-form.component';
import {TariffautoComponent} from '../tariff/tariffauto/tariffauto.component';
import {BasetariffComponent} from '../tariff/basetariff.component';
import {Subject} from 'rxjs';
import {take, takeUntil} from 'rxjs/operators';

@Component({
  selector: 'app-transport',
  templateUrl: './transport.component.html',
  styleUrls: ['./transport.component.scss']
})
export class TransportComponent implements OnInit, DynamicInterface, OnDestroy {
  destroyed$ = new Subject<void>();
  @ViewChild('dynamic', {read: ViewContainerRef}) dynamic: ViewContainerRef;
  public count = 1;
  public typeId;

  constructor(private phasesService: Application_phases, private consting: ConstingService) {

  }

  ngOnInit(): void {

    this.consting.getTarrif()
      .pipe(takeUntil(this.destroyed$))
      .subscribe((tariff) => {
        if (tariff) {
          // tslint:disable-next-line:radix
          this.count = (Number.parseInt(tariff['isUnlimited']) === 1) ? 1 : 5;
        }
      });
    this.phasesService.get_current_phase()
      .pipe(takeUntil(this.destroyed$))
      .subscribe((currentPhase) => {
        if (currentPhase) {
          let goM = Object.keys(currentPhase).filter(it => currentPhase[it])[0];
          goM = `${goM}`;
          try {
            this[goM]();
          } catch (e) {
          }
        }

      });
  }

  phaseContact() {
    this.phasesService.customFactory(this.dynamic, ContactComponent);
  }

  phaseDataPay() {
    this.phasesService.customFactory(this.dynamic, BasetariffComponent);
  }

  phaseQuery() {
    setTimeout(() => {
    this.phasesService.customFactory(this.dynamic, TransportFormComponent, this.count);
    });
  }

  phaseTariff() {
    setTimeout(() => {
    this.phasesService.customFactory(this.dynamic, TariffautoComponent);
    }, 0);
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }

}
