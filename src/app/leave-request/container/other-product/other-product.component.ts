import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {DynamicInterface} from '../dynamicInterface';
import {Application_phases, CurrentPhase} from '../../../services/auth/application-phases.service';
import {ConstingService} from '../../../services/consting/consting.service';
import {ContactComponent} from '../contact/contact.component';
import {BasetariffComponent} from '../tariff/basetariff.component';
import {OtherProductFormComponent} from './other-product-form/other-product-form.component';
import {TariffotherComponent} from '../tariff/tariffother/tariffother.component';
import {take, takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';

@Component({
  selector: 'app-other-product',
  templateUrl: './other-product.component.html',
  styleUrls: ['./other-product.component.scss']
})
export class OtherProductComponent implements OnInit, AfterViewInit, OnDestroy, DynamicInterface {
  destroyed$ = new Subject<void>();
  @ViewChild('dynamic', {static: true, read: ViewContainerRef}) dynamic: ViewContainerRef;
  public count = 1;

  constructor(private phasesService: Application_phases, private consting: ConstingService) {
  }

  ngOnInit() {

    this.consting.getTarrif()
      .pipe(takeUntil(this.destroyed$))
      .subscribe((tariff) => {
        if (tariff) {
          // tslint:disable-next-line:radix
          this.count = (Number.parseInt(tariff['isUnlimited']) === 1) ? 1 : 5;
        }
      });

    this.phasesService.get_current_phase()
      .pipe(takeUntil(this.destroyed$))
      .subscribe((currentPhase) => {
        if (currentPhase) {
          let goM = Object.keys(currentPhase).filter(it => currentPhase[it])[0];
          goM = `${goM}`;
          try {
            this[goM]();
          } catch (e) {
          }
        }

      });
  }

  ngAfterViewInit(): void {
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }

  phaseContact(): void {
    this.phasesService.customFactory(this.dynamic, ContactComponent);
  }

  phaseDataPay(): void {
    this.phasesService.customFactory(this.dynamic, BasetariffComponent);
  }

  phaseQuery() {
    this.phasesService.customFactory(this.dynamic, OtherProductFormComponent, this.count);
  }

  phaseTariff() {
    this.phasesService.customFactory(this.dynamic, TariffotherComponent);
  }

}
