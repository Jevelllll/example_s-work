import {Directive, ElementRef, HostListener, Renderer2} from '@angular/core';

@Directive({
  selector: '[appSkipFilter]',
})
export class SkipFilterDirective {

  private currentElement: HTMLElement;

  constructor(private el: ElementRef, private renderer: Renderer2) {
    this.currentElement = el.nativeElement;
  }

  @HostListener('focus')
  setInputFocus(): void {
    const parentElement = this.currentElement.parentElement;
    this.renderer.setStyle(parentElement, 'border-top', '0px');
  }

  @HostListener('blur')
  setInputFocusOut(): void {
    const parentElement = this.currentElement.parentElement;
    this.renderer.setStyle(parentElement, 'border-top', 'gray 1px solid');
  }
}
