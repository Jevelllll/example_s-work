import {Validators} from '@angular/forms';

export class OtherProductFormParams {

  static CONTROLSCONFIG = {
    query_key_words: [null, Validators.compose([Validators.min(2), Validators.required])],
    query_skip_words: [],
    preSlipWorlds: [],
    query_price_min: [''],
    query_price_max: [''],
    query_region: [''],
    query_city: [''],
    query_currency: [null],
    query_services_list: []
  };
}

/**********************игициализация ранее введенными данными первой формы*************************/
export function predefinedData() {
  const dataPreDef = this.phasesService.phaseContainer;
  const serviceSelected = [];
  if (Array.isArray(dataPreDef)) {
    dataPreDef.forEach((element, index) => {

      this.serviceSelected[index] = this.phasesService.preDefinedService(element, serviceSelected, index);
      const formGroup = this.currentFormArray(index);
      if (typeof element === 'object') {

        if (element['query_region']) {
          this.getCity(element['query_region'], index);
        }
        Object.keys(element).forEach((field) => {
          const control = formGroup.get(field);
          if (field === 'query_currency') {
            this.disabled[index] = false;
            control.setValue(element[field]);
          } else if (field === 'query_skip_words') {
            let parseQuerySkipWords = [];
            try {
              parseQuerySkipWords = JSON.parse(element[field]);
            } catch (e) {
            }
            if (parseQuerySkipWords.length > 0) {
              parseQuerySkipWords.forEach((val) => {
                this.skipFilterWords[index].add(val);
              });
            }
            control.preSlipWorlds.patchValue('');
          } else {
            try {
              control.setValue(element[field]);
            } catch (e) {
            }
          }
        });
      }
    });
  }
}
