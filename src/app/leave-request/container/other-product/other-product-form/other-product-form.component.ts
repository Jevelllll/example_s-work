import {AfterViewInit, Component, Input, OnDestroy, OnInit, Renderer2} from '@angular/core';
import {FormArray, FormBuilder, FormGroup} from '@angular/forms';
import {eventPriceC} from '../../../../shared/helper-container-leave';
import {OtherProductFormParams, predefinedData} from './other-product-form-params';
import {addNewForm, Application_phases, getServiceList, serviceListSelect} from '../../../../services/auth/application-phases.service';
import {LoginService} from '../../../../services/auth/login.service';
import {CommonService} from '../../../../services/common.service';
import {Observable, Subject} from 'rxjs';
import {CreateformdataService} from '../../../../services/auth/createformdata.service';
import {PriorityAutoService} from '../../../../services/priority/auto.service';
import {ServiceListModel} from '../../../../services/auth/ServiceListModel';
import {animate, style, transition, trigger} from '@angular/animations';
import {SpinnerService} from '../../../../services/spinner.service';

// import {clearObject} from '../../../../shared/helper-container-leave'

@Component({
  selector: 'app-other-product-form',
  templateUrl: './other-product-form.component.html',
  styleUrls: ['./other-product-form.component.scss'],
  animations: [
    trigger('dialog', [
      transition('void => *', [
        style({transform: 'scale3d(.1, .1, .1)'}),
        animate(50)
      ])
    ])
  ]
})
export class OtherProductFormComponent implements OnInit, AfterViewInit, OnDestroy {

  destroyed$ = new Subject<void>();
  cTitle = [];
  public wrapIndex: any;
  public tittle = null;
  public serviceSelected = [];
  public serviceList: ServiceListModel[] = [];
  private CATEGORY_NAME = 'other';
  public JSON = JSON;
  public Object = Object;
  public Number = Number;
  public myForm: FormGroup;
  public items: FormArray;
  @Input() count = 1;
  public formOutputId = 0;
  public disabled = [];
  public typeNameBlocked = [];
  public InvoiceButtonHide = true;
  skipFilterWords: any = [];
  Array: any = Array;
  public patternService = [];
  public hoverShadow = false;
  public region$: Observable<any> = new Observable();
  public city$: Observable<any> = new Observable();
  public currency$: Observable<any> = new Observable();
  public priceMin;
  public priceMax;
  public labelStyle = {position: 'relative', left: '20px'};

  // tslint:disable-next-line:variable-name
  constructor(private _fb: FormBuilder,
              private login: LoginService,
              private phasesService: Application_phases,
              private commonService: CommonService, private renderer: Renderer2,
              private spinnerService: SpinnerService,
              private toValidservice: CreateformdataService, public priority: PriorityAutoService) {
    this.priceMin = 1;
    this.priceMax = 500000;
  }

  ngOnInit() {
    for (let index = 0; index < this.count; index++) {
      if (index === this.count - 1) {
        this.cTitle[index] = 'Подтвердить';
      } else {
        this.cTitle[index] = 'Следующая заявка';
      }
    }
    this.formInitialize();
    /************список сервисов***************************/
    getServiceList.call(this);
    this.region$ = this.commonService.getRegionMore();
    this.getCurrencies();
  }

  getPreSlipWorldsLength(value) {
    return !!value;
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      this.predefinedData();
    }, 0);
  }

  private createItem() {
    return this._fb.group(OtherProductFormParams.CONTROLSCONFIG);
  }

  addItem(): void {
    this.items = this.myForm.get('items') as FormArray;
    this.items.push(this.createItem());
  }

  private formInitialize() {
    this.myForm = this._fb.group({title: 'other', items: this._fb.array([])});
    for (let index = 1; index <= this.count; index++) {
      this.disabled[index - 1] = true;
      this.serviceSelected.push({});
      this.addItem();
      // const skipFilterWo: any = new Set();
      this.skipFilterWords[index - 1] = new Set();
    }
  }

  public activeFromWrapQuery(data) {
    if (data) {
      this.formOutputId = data - 1;
    }
  }

  /**********работа с экземпляром форм******************/
  addNewForm(indexForm: number, confirm) {

    const preObj: any = {};
    const preForm: FormGroup = this.currentFormArray(indexForm);
    preObj.query_skip_words = JSON.stringify(Array.from(this.skipFilterWords[indexForm]));
    preForm.patchValue(preObj);

    addNewForm.apply(this, [indexForm]);
    if (indexForm === this.count - 1) {
      this.renderer.addClass(confirm, 'confirm_btn_success');
      this.renderer.removeClass(confirm, 'confirm_btn');
    } else {
      this.login.setScroll();
    }
  }

  /*****************Выбор Сервисов*******************************************/
  public serviceListSelect(serList, index) {
    serviceListSelect.apply(this, [serList, index]);
  }

  currencyChange(index) {
    this.disabled[index] = false;
  }

  /******************************* / блокировка полей ввода**************************************/
  isDisabledInput(event) {
    this.hoverShadow = event;
  }

  /****************************валюта**********************************************/
  getCurrencies() {
    this.currency$ = this.priority.getCurrency();
  }

  /******************настройка параметров сумм********************************/
  eventPrice(sliderPriceMin = null, sliderPriceMax = null, i = null) {

    eventPriceC.call(this, sliderPriceMin, sliderPriceMax, this.currentFormArray(i));
  }

  /**********************игициализация ранее введенными данными первой формы*************************/
  predefinedData() {
    predefinedData.apply(this);
  }

  /********************приведение к общем данным сервисов patterns*******************************/
  generalViewOfServiceRules(index: number) {
    this.patternService[index] = this.phasesService.generalViewOfServiceRules(index, this.serviceSelected, this.serviceList);
  }

  // tslint:disable-next-line:variable-name
  getCity(region_id, indexForm) {
    if (region_id === '') {
      this.city$[indexForm] = new Observable();
    }
    if (region_id) {
      this.city$[indexForm] = this.commonService.getCities(region_id);
    }
    this.currentFormArray(indexForm).get('query_city').setValue('', {emitEvent: false});
  }

  addWorld(str, index) {
    const myForm = this.currentFormArray(index);
    if (typeof str === 'string') {
      if (str.length > 0) {
        this.skipFilterWords[index].add(str);
        myForm.controls.preSlipWorlds.patchValue('');
      }
    }
  }

  removeWorlds(val, index) {
    const myForm = this.currentFormArray(index);
    this.skipFilterWords[index].delete(val);
    myForm.controls.query_skip_words.patchValue(JSON.stringify(Array.from(this.skipFilterWords[index])));
  }

  private currentFormArray(index) {
    const arrayControls = this.myForm.get('items') as FormArray;
    return arrayControls.controls[index] as FormGroup;
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }

}
