import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'skipp'
})
export class SkipparamsPipe implements PipeTransform {

  transform(array: any, skip: any): number {
    try {
      return array.filter(item => item.name !== skip);
    } catch (e) {

    }


  }
}
