import {Component, ElementRef, Inject, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {Application_phases, CurrentPhase} from '../../../services/auth/application-phases.service';
import {DOCUMENT} from '@angular/common';
import {ActivatedRoute, Router} from '@angular/router';
import {ConstingService} from '../../../services/consting/consting.service';
import {LoginService} from '../../../services/auth/login.service';
import {takeUntil, withLatestFrom} from 'rxjs/operators';
import {MatDialog} from '@angular/material/dialog';
import {DialogConfirmComponent} from '../contact/dialog-confirm/dialog-confirm.component';
import {CreateformdataService} from '../../../services/auth/createformdata.service';

@Component({
  selector: 'app-new-base-common-m',
  templateUrl: './new-base-common-m.component.html',
  styleUrls: ['./new-base-common-m.component.scss']
})
export class NewBaseCommonMComponent implements OnInit, OnDestroy {
  destroyed$ = new Subject<void>();
  flagTf = false;
  @ViewChild('goNext') goNext: ElementRef;
  @ViewChild('goBack') goBack: ElementRef;
  @ViewChild('aSW') aSW: ElementRef;
  @ViewChild('next') next: ElementRef;
  public currentPhase;
  public titleNext = 'Далее';
  public titleBack = 'Назад';
  public buttonNextDisable: Observable<boolean>;
  public buttonBackDisable: Observable<boolean>;
  public buttonShowConfirmButton: Observable<boolean>;
  public nextPhase = new CurrentPhase();
  private prevPhase = new CurrentPhase();
  private isLogin;

  constructor(
    @Inject(DOCUMENT) private document: Document,
    private route: ActivatedRoute,
    private toValidservice: CreateformdataService,
    private router: Router,
    private phasesService: Application_phases,
    private consting: ConstingService,
    private loginService: LoginService,
    public dialog: MatDialog
  ) {

  }

  public goBackM() {
    this.loginService.setScroll();
    if (this.currentPhase.phaseTariff) {
    } else if (this.currentPhase.phaseDataPay) {
      this.phasesService.add_current_phase(
        (this.isLogin)
          ? new CurrentPhase(false, true, false, false)
          : new CurrentPhase(false, false, true, false)
      );
    } else {
      this.phasesService.add_current_phase(this.prevPhase);
    }
  }

  public goNextM() {
    this.loginService.setScroll();
    if (this.currentPhase.phaseDataPay) {
      this.phasesService.postQueryRequest();
    } else if (this.currentPhase.phaseQuery) {
      this.phasesService.add_current_phase(
        (this.isLogin)
          ? new CurrentPhase(false, false, false, true)
          : new CurrentPhase(false, false, true, false)
      );
      this.phasesService.buttonNextDisable(true);
    } else {
      this.phasesService.buttonNextDisable(true);
      this.phasesService.add_current_phase(this.nextPhase);
    }
  }

  ngOnInit(): void {

    this.phasesService.getNextPhaseClick().pipe(takeUntil(this.destroyed$)).subscribe((isNextClick) => {
      setTimeout(() => {
        this.next.nativeElement.click();
      }, 0);
    });

    this.flagTf = false;
    this.phasesService.phaseContainer = [];
    this.phasesService.buttonNextDisable(true);
    // this.phasesService.add_current_phase(new CurrentPhase());

    this.buttonNextDisable = this.phasesService._obsButtonNextD;
    this.buttonBackDisable = this.phasesService._obsButtonBAck;
    this.buttonShowConfirmButton = this.phasesService.obsShowConformButton;

    this.consting.getTarrif()
      .pipe(takeUntil(this.destroyed$))
      .subscribe((tariff) => {
        if (tariff) {
          this.goNextM();
          this.phasesService.add_phase_tariff(tariff);
          this.phasesService.buttonNextDisable(false);
        }
      });

    this.phasesService.get_current_phase()
      .pipe(
        takeUntil(this.destroyed$),
        withLatestFrom(this.loginService.isLogin()))
      .subscribe(([currentPhase, isLogin]) => {
        this.isLogin = isLogin;
        this.currentPhase = currentPhase;
        if (isLogin) {
          if (currentPhase.phaseDataPay) {
            this.phasesService.add_phase_query(this.phasesService.phaseContainer.filter((it) => it));
          }
        } else {
          if (currentPhase.phaseContact) {
            console.log(currentPhase);
            this.phasesService.setConfirmPhoneButton(true);
            this.phasesService.add_phase_query(this.phasesService.phaseContainer.filter((it) => it));
          }
        }
        if (currentPhase.phaseDataPay) {
          this.titleNext = 'Создать';
        } else {
          this.titleNext = 'Далее';
        }

        if (currentPhase.phaseTariff) {
          this.phasesService.phaseContainer = [];
          this.phasesService.buttonBackDisable(true);
        } else if (currentPhase.phaseQuery) {
          /*******очищаем контэйнер заявок*******/
          if (this.phasesService.phaseContainer.length > 0) {
            this.phasesService.buttonBackDisable(false);
          } else {
            this.phasesService.buttonBackDisable(true);
          }

        } else if (currentPhase.phaseContact) {
          this.phasesService.buttonBackDisable(false);
        } else if (currentPhase.phaseDataPay) {

          this.phasesService.buttonBackDisable(false);
        } else {
          this.phasesService.buttonBackDisable(false);
        }


        if (currentPhase.phaseDataPay || currentPhase.phaseTariff) {
          this.phasesService.buttonNextDisable(true);
        } else if (currentPhase.phaseQuery) {
          if (this.phasesService.phaseContainer.length > 0) {
            this.phasesService.buttonBackDisable(false);
          } else {
            this.phasesService.buttonBackDisable(true);
          }
        } else {
          this.phasesService.buttonNextDisable(false);
        }
        // if (!currentPhase.final) {
        this.nextPhase = currentPhase.next();
        // }
        this.prevPhase = currentPhase.prev();

        if (currentPhase.phaseQuery || currentPhase.phaseContact || currentPhase.phaseDataPay) {
          this.phasesService.buttonBackDisable(false);
        }
      });
  }

  public openPhoneConfirmDialog() {
    this.phasesService.setShowConfirmPhoneDialog(true);
  }
  ngOnDestroy(): void {
    this.phasesService.add_current_phase(new CurrentPhase());
    this.destroyed$.next();
  }
}
