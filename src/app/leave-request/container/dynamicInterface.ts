export interface DynamicInterface {

  phaseTariff(): void;

  phaseQuery(): void;

  phaseContact(): void;

  phaseDataPay(): void;
}
