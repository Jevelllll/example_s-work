import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {FirstLevelOptionsComponent} from './first-level-options.component';


const routes: Routes = [{path: '', component: FirstLevelOptionsComponent}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FirstLevelOptionsRoutingModule { }
