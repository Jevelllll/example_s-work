import {AfterViewInit, Component, OnDestroy, OnInit} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {CharacteristicList} from '../shared/character';
import {ActivatedRoute, NavigationEnd, Router, ResolveEnd} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {PriorityAutoService} from '../services/priority/auto.service';
import {PriorityPropertyService} from '../services/priority/property.service';
import {PriorityWorkService} from '../services/priority/work.service';
import {filter, map, first} from 'rxjs/operators';

@Component({
  selector: 'app-first-level-options',
  templateUrl: './first-level-options.component.html',
  styleUrls: ['./first-level-options.component.scss']
})
export class FirstLevelOptionsComponent implements OnInit, AfterViewInit, OnDestroy {
  destroyed$ = new Subject<void>();
  public dataList: Observable<any>;
  public title = '';
  public mainPhotoStyle;
  private currentSegment;
  private characteristicList = new CharacteristicList();

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private toster: ToastrService,
    private priorityAutoService: PriorityAutoService,
    private priorityPropertyService: PriorityPropertyService,
    private priorityWork: PriorityWorkService,
  ) {

  }

  ngOnInit(): void {
    try {
      const characteristic = this.route.snapshot.paramMap.get('characteristic');
      if (this.characteristicList.validate(characteristic)) {
        if (characteristic === 'other' || characteristic === 'autoproducts') {
          const url = `/leave/search/${characteristic}`;
          this.router.navigate([url]);
        } else {
          this.selectCurrentService(characteristic);
        }

      } else {
        this.router.navigate(['/']);
      }
    } catch (e) {
      this.toster.error('Ошибка сервера!');
    }
  }

  redirectC(typeId) {
    const url = `/leave/search/${this.currentSegment}/${typeId}`;
    this.router.navigate([url]);
  }

  // public imagesMappingAuto(id: number) {
  //   return mapingAuto.call(this, id);
  // }

  selectCurrentService(serviceName: string) {
    this.currentSegment = serviceName;
    switch (serviceName) {
      case 'auto':
        this.dataList = this.priorityAutoService.getTypeAuto();
        this.title += 'Тип Транспорта';

        this.mainPhotoStyle = {
          'background-image': 'url(../../assets/images/landing/auto/auto-3339373_1280.jpg)',
          'background-position': 'center'
        };

        break;
      case  'property':
        this.dataList = this.priorityPropertyService.getTypeProperty();
        this.title += 'Вид Недвижимости';

        this.mainPhotoStyle = {'background-image': 'url(../../assets/images/landing/mainproperty.png)', 'background-position': 'center'};

        break;
      case  'work' :
        this.dataList = this.priorityWork.getType().pipe(map(value => {
          return value.map((vOne) => {
            const newFormObject = {};
            Object.keys(vOne).forEach((field => {
              if (field === 'name') {
                newFormObject['type'] = vOne[field];
              } else {
                newFormObject[field] = vOne[field];
              }
            }));
            return newFormObject;
          });
        }));
        this.mainPhotoStyle = {'background-color': 'rgb(40, 167, 69)'};
        this.title += 'Работа';
        break;
    }

    this.dataList = this.dataList.pipe(map(result => {
      const prePushResult = [];
      result.forEach(val => {
        const anies: any = Object.values(val);
        /***************[0] => id  [1] => type*********************************************/
        if (anies) {
          const anNewObj = {id: anies[0], type: anies[1]};
          if (anies.length > 2) {
            anNewObj['img'] = anies[2];
          }
          prePushResult.push(anNewObj);
        }
      });
      return prePushResult;
    }));
  }

  ngAfterViewInit(): void {
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }

}
