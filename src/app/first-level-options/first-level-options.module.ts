import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {FirstLevelOptionsRoutingModule} from './first-level-options-routing.module';
import {FirstLevelOptionsComponent} from './first-level-options.component';
import {PriorityModule} from '../services/priority/priority.module';
import {WebGameModule} from '../web-game/web-game.module';


@NgModule({
  declarations: [FirstLevelOptionsComponent],
  imports: [
    CommonModule,
    PriorityModule,
    FirstLevelOptionsRoutingModule,
    WebGameModule
  ]
})
export class FirstLevelOptionsModule {
}
