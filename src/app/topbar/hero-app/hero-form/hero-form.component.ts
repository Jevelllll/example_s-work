import {AfterViewInit, Component, ElementRef, OnDestroy, OnInit, Renderer2, ViewChild} from '@angular/core';
import {SELECTEDHERO} from '../../../shared/templData';
import {Router} from '@angular/router';
import {TabService} from '../../../services/leave-request/tabsactive/tab.service';
declare let $: any;
@Component({
  selector: 'app-hero-form',
  templateUrl: './hero-form.component.html',
  styleUrls: ['./hero-form.component.scss']
})

export class HeroFormComponent implements OnInit, AfterViewInit, OnDestroy {

  @ViewChild('button1', {static: true}) button1: ElementRef;
  selectedval = SELECTEDHERO;
  // tslint:disable-next-line:variable-name
  category_name;
  private customPosition: any = 'customBtnPosition__marginBtn';

  focusFunction() {
    this.renderer.addClass(this.button1.nativeElement, this.customPosition);
  }

  focusOutFunction() {
    this.renderer.removeClass(this.button1.nativeElement, this.customPosition);
  }
  constructor(private router: Router, private tabService: TabService, private renderer: Renderer2) { }

  public listenerClick(event) {



    if (event === undefined) {
    } else {
      if (event === 'other') {
        this.tabService.transitionHeroFrom(event);
        this.router.navigate(['/leave/search/other']);
      } else if (event === 'autoproducts') {
        this.tabService.transitionHeroFrom(event);
        this.router.navigate(['/leave/search/autoproducts']);
      } else if (event === 'kraft') {
        this.tabService.transitionHeroFrom(event);
        this.router.navigate(['/special/craft/main']);
      } else if (event === 'quest') {
        this.tabService.transitionHeroFrom(event);
        this.router.navigate(['/leave/search/quest']);
      } else if (event === 'legal') {
        this.tabService.transitionHeroFrom(event);
        this.router.navigate(['/leave/search/legal']);
      } else if (event === 'sto') {
        this.router.navigate([`/${event}`]);
      }
        // else if(event === 'work'){
        // this.tabService.transitionHeroFrom(event);
        // this.router.navigate(['/leave/search/work']);
      // }
      else {
        this.tabService.transitionHeroFrom(event);
        this.router.navigate(['/type/' + event]);
      }

    }
    // if (this.category_name === undefined) {} else {
    //   this.router.navigate(['/leave/search/' + this.category_name]);
    // }

  }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    $('body,html').animate({
      scrollTop: 67
    }, 800);
    // window.scrollTo(0, 67);
    // window.scrollTo(0, 67);
  }

  ngOnDestroy(): void {
  }

}
