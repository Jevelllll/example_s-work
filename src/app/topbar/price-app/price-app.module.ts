import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {PriceAppRoutingModule} from './price-app-routing.module';
import {PriceAppComponent} from './price-app.component';
import {TariffComponent} from './tariff/tariff.component';
import {CustQuestFormComponent} from './tariff/cust-quest-form/cust-quest-form.component';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [PriceAppComponent, TariffComponent, CustQuestFormComponent],
  exports: [
    TariffComponent,
    CustQuestFormComponent
  ],
  imports: [
    FormsModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatInputModule,
    MatButtonModule,
    CommonModule,
    PriceAppRoutingModule
  ]
})
export class PriceAppModule {
}
