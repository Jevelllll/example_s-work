import {Component, OnDestroy, OnInit} from '@angular/core';
import {TariffService} from '../../services/tariff/tariff.service';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {SpinnerService} from '../../services/spinner.service';

@Component({
  selector: 'app-price-app',
  templateUrl: './price-app.component.html',
  styleUrls: ['./price-app.component.scss']
})
export class PriceAppComponent implements OnInit, OnDestroy {
  destroyed$ = new Subject<void>();
  public popularTariff: any;

  constructor(private tariff: TariffService, private spinnerService: SpinnerService) {
  }

  ngOnInit() {
    this.spinnerService.show();
    this.tariff.setAllPopularsTarifPlain();
    this.tariff.getAllPopularsTarifPlain()
      .pipe(takeUntil(this.destroyed$))
      .subscribe(value => {
          this.popularTariff = value;
          // this.spinnerService.hide();
          window.scrollTo(0, 67);
        }, error => setTimeout(() => this.spinnerService.hide(), 15),
        () => {
          setTimeout(() => this.spinnerService.hide(), 150);
        });
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }

}
