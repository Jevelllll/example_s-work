import {Component, ElementRef, Input, OnDestroy, OnInit, QueryList, Renderer2, ViewChild, ViewChildren} from '@angular/core';
import {CarouselComponent} from 'angular-bootstrap-md';
import {ConstingService} from '../../../services/consting/consting.service';
import {Router} from '@angular/router';
import {TariffModel} from '../../../shared/tarifModel';
import {default_addittional_style_tarriff_blocks} from '../../../leave-request/container/tariff/tarrif_style_options';
import {NgxSpinnerService} from 'ngx-spinner';
import {Subject} from 'rxjs/internal/Subject';
import {MatDialog} from '@angular/material/dialog';
import {CustQuestFormComponent} from './cust-quest-form/cust-quest-form.component';
import {takeUntil} from 'rxjs/internal/operators/takeUntil';

@Component({
  selector: 'app-tariff',
  templateUrl: './tariff.component.html',
  styleUrls: ['./tariff.component.scss']
})
export class TariffComponent implements OnInit, OnDestroy {
  destroyed$ = new Subject<void>();
  allowPoll = false;
  allowEmptyUponReg = true;
  public blockName = {
    true: 'Выбрано',
    false: 'Выбрать'
  };
  @ViewChild('carouselRef', {static: true}) carouselRef: CarouselComponent;
  @Input() isPrice = false;
  // tslint:disable-next-line:variable-name
  @Input() one_active_field: boolean;
  @Input() dataTarif;
  // tslint:disable-next-line:variable-name
  @Input() data_one_active: any;
  // tslint:disable-next-line:variable-name
  @Input() additional_style_property: any;
  // tslint:disable-next-line:variable-name
  public default_addittional_style_tarriff_blocks;
  // tariffComp = PRICETARIFF;
  // tslint:disable-next-line:variable-name
  public data_one_active_btn;
  // tslint:disable-next-line:variable-name
  public styles_item: any;
  element = '';
  btnParamssW = [];
  public Boolean: any = Boolean;
  @ViewChild('param1', {static: true}) param1: ElementRef;
  @ViewChildren('btnParams') btnParams: QueryList<TariffComponent>;
  @ViewChildren('sTrf') sTrf: ElementRef;

  constructor(private renderer: Renderer2,
              private constingService: ConstingService,
              private router: Router,
              public dialog: MatDialog,
              private spinnerService: NgxSpinnerService,
  ) {
    this.default_addittional_style_tarriff_blocks = default_addittional_style_tarriff_blocks;
  }

  public selectALSO(tariff) {
    this.constingService.addTariff(tariff);
  }

  private openUponInquirer(tariff): void {
    this.constingService.skipResultStore(true);
    if (this.allowEmptyUponReg) {
      this.constingService.obsResultsStore.pipe(takeUntil(this.destroyed$))
        .subscribe((store) => {
          if (store) {
            this.allowPoll = true;
          } else {
            this.allowPoll = false;
          }
        });
      if (this.allowPoll) {
        this.selectALSO(tariff);
      } else {
        const dialogRef = this.dialog.open(CustQuestFormComponent, {
          minHeight: '170px',
          autoFocus: false,
          data: tariff,
          disableClose: true
        });
        dialogRef.afterClosed().pipe(takeUntil(this.destroyed$)).subscribe(result => {
          this.selectALSO(tariff);
        });
        setTimeout(() => {
          document.querySelector('.cdk-global-scrollblock').classList.remove('cdk-global-scrollblock');
        }, 0);
      }
    } else {
      this.selectALSO(tariff);
    }

  }

  selectUponTariff(tariff: TariffModel): void {
    this.openUponInquirer(tariff);
  }

  selectTariff(tariff: TariffModel) {
    if (this.router.url !== undefined) {

      const url = this.router.url;
      if (url === '/price') {
        setTimeout(function() {
          this.router.navigate(['/leave/search/' + tariff.typeTitle.toLowerCase()]);
        }.bind(this), 2000);
      }
    }
    this.cleanData();
    tariff.select = true;
    this.constingService.addTariff(tariff);
  }

  focusFunction(i) {
    this.btnParamssW = this.btnParams.toArray();
    this.element = this.btnParamssW[i].nativeElement;
    this.renderer.addClass(this.element, 'customBtnPosition__marginBtn');
  }

  focusOutFunction() {
    this.renderer.removeClass(this.element, 'customBtnPosition__marginBtn');
  }

  cleanData() {
    for (let i = 0; i < this.dataTarif.length; i++) {
      this.dataTarif[i].select = false;
    }
  }

  public splitString(stringToSplit, separator) {
    const arrayOfStrings = stringToSplit.split(separator);
    return arrayOfStrings[3];
  }


  goLeaveRequest() {
    this.router.navigate(['/leave']);
  }

  popular(): void {
    // const text = this.renderer.createText('Самое популярное');
    // this.renderer.appendChild(this.param1.nativeElement, text);
  }

  ngOnInit() {
    this.constingService.skipResultStore(false);
    this.constingService.obsFormFields.pipe(takeUntil(this.destroyed$)).subscribe((fieldList) => {
      if (fieldList) {
      } else {
        this.constingService.getFormUponFields().pipe(takeUntil(this.destroyed$)).subscribe(() => {
        }, error => {
          this.allowEmptyUponReg = false;
        });
      }
    }, error => this.allowEmptyUponReg = false);
    // this.constingService.onUpon(false);
    try {
      // this.carouselRef.isControls = false;
    } catch (e) {

    }


    if (this.additional_style_property === undefined) {
      this.styles_item = this.default_addittional_style_tarriff_blocks;
    } else {
      this.styles_item = this.additional_style_property;
    }

    this.data_one_active_btn = true;
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }

}
