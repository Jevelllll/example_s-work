import {Component, OnInit, Inject, OnDestroy} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {TariffModel} from '../../../../shared/tarifModel';
import {ConstingService} from '../../../../services/consting/consting.service';
import {Subject} from 'rxjs/internal/Subject';
import {takeUntil} from 'rxjs/internal/operators/takeUntil';
import {FormGroup, FormControl} from '@angular/forms';
import {Observable} from 'rxjs/internal/Observable';

@Component({
  selector: 'app-cust-quest-form',
  templateUrl: './cust-quest-form.component.html',
  styleUrls: ['./cust-quest-form.component.scss']
})
export class CustQuestFormComponent implements OnInit, OnDestroy {
  destroyed$ = new Subject<void>();
  public myCForm: FormGroup = new FormGroup({});
  formTemplate: any = [];

  waitForm: Observable<any> = new Observable<any>();

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: TariffModel,
    public dialogRef: MatDialogRef<CustQuestFormComponent>,
    private constinS: ConstingService,
    // tslint:disable-next-line:variable-name
  ) {
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }

  onSubmit() {
    this.constinS.addResultsStore(JSON.stringify(this.myCForm.value));
  }

  ngOnInit(): void {
    this.constinS.obsFormFields.pipe(takeUntil(this.destroyed$)).subscribe((fieldList) => {
      if (fieldList) {
        this.formTemplate.push({label: 'tariff_id', isShow: false});
        this.myCForm.addControl('tariff_id', new FormControl(this.data.id));
        for (const field of fieldList) {
          this.formTemplate.push({label: field.label, isShow: true});
          this.myCForm.addControl(field.label, new FormControl(null));
        }
      }
    });
  }


  onNoClick(): void {
    this.dialogRef.close(false);

  }

}
