import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {PriceAppComponent} from './price-app.component';


const routes: Routes = [{path: '', component: PriceAppComponent}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PriceAppRoutingModule {
}
