import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {TopbarHomesRoutingModule} from './topbar-homes-routing.module';
import {HeroAppComponent} from './hero-app/hero-app.component';
import {HeroFormComponent} from './hero-app/hero-form/hero-form.component';
import {TabServiceModule} from '../services/leave-request/tabsactive/tab-service.module';

@NgModule({
  imports: [
    CommonModule,
    TopbarHomesRoutingModule,
    TabServiceModule,
  ],
  exports: [
    HeroAppComponent
  ],
  declarations: [
    HeroAppComponent, HeroFormComponent
  ]
})
export class TopbarHomesModule {
}
