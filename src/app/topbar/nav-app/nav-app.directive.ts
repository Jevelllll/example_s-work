import {Directive, ElementRef, HostListener, Input, Renderer2, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {NavbarService} from 'angular-bootstrap-md';

declare let $: any;

@Directive({
  selector: '[appNavClose]'
})
export class NavAppDirective {
  @Input() public menu: any;
  // tslint:disable-next-line:variable-name
  @Input() public login_scroll: boolean;
  @ViewChild('divClick', {static: true}) divClick: ElementRef;
  private el: HTMLElement;

  constructor(private router: Router,
              // tslint:disable-next-line:variable-name
              private element: ElementRef, private renderer: Renderer2, private _navbarService: NavbarService) {
    this.el = element.nativeElement;

  }

  @HostListener('click', ['$event']) onClick(btn) {

    if (this.login_scroll) {

      const element = document.getElementById('read_nav');
      element.scrollIntoView({behavior: 'smooth', block: 'start'});
    }

    // tslint:disable-next-line:variable-name
    const time_count = 1000;
    if (this.menu === undefined || this.menu === 'undefined') {
    } else {
      this.scroll_to_custom(time_count, btn);
    }
    this._navbarService.setNavbarLinkClicks();

  }

  // tslint:disable-next-line:variable-name
  private scroll_to_custom(time_count: number, btn) {
    // time_count = (this.checkTabs() === undefined) ? 300 : 1000;
    // this.router.navigate(['/']);
    // // tslint:disable-next-line:only-arrow-functions
    // setTimeout(function() {
    //   // tslint:disable-next-line:variable-name
    //   let element_id = btn.srcElement.getAttribute('dhref');
    //   element_id = element_id.replace('#', '');
    //   const element = document.getElementById(element_id);
    //   element.scrollIntoView({behavior: 'smooth', block: 'start'});
    // }, time_count);
  }

  private checkTabs() {

    const url = this.router.url;
    let nameStaps;
    nameStaps = this.splitString(url, '/');
    return nameStaps;
  }

  private splitString(stringToSplit, separator) {

    const arrayOfStrings = stringToSplit.split(separator);
    const vPattern = ['help', 'rules', 'privacy-policy', 'contact_us', 'about', 'leave', 'news', 'auto', 'electronics', 'property'];
    // tslint:disable-next-line:ban-types
    let finalStr: String;
    vPattern.forEach(pattern => {
      const filter = arrayOfStrings.filter(data => (data === pattern));
      if (filter.length !== 0) {
        finalStr = filter[0];
      }
    });
    return finalStr;
  }
}
