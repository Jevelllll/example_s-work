import {Component, ElementRef, HostListener, Inject, OnDestroy, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {animate, style, transition, trigger} from '@angular/animations';
import {MatToolbar} from '@angular/material/toolbar';
import {Router} from '@angular/router';
import {DOCUMENT} from '@angular/common';
import {LoginService} from '../../services/auth/login.service';
import {MatDialog} from '@angular/material/dialog';
import {MatDrawer} from '@angular/material/sidenav/drawer';
import {WebGameService} from '../../services/web-game.service';
import {Subject} from 'rxjs/internal/Subject';
import { takeUntil } from 'rxjs/internal/operators/takeUntil';

declare let $: any;

@Component({
  selector: 'app-nav-app',
  templateUrl: './nav-app.component.html',
  styleUrls: ['./nav-app.component.scss'],
  animations: [
    trigger('dialog', [
      transition('void => *', [
        style({transform: 'scale3d(.3, .3, .3)'}),
        animate(350)
      ]),
      transition('* => void', [
        animate(350, style({transform: 'scale3d(.0, .0, .0)'}))
      ])
    ])
  ]
})
export class NavAppComponent implements OnInit, OnDestroy {
  @ViewChild('naCvbar', {static: true}) naCvbar: ElementRef;
  @ViewChild('sidenav', {static: true}) sidenav: ElementRef;
  @ViewChild('matToolbar', {static: true}) matToolbar: MatToolbar;
  position = 'unset';
  top = '-100%';
  height = '100%';
  public newDrawerClass = 'drawerHIle';
  // tslint:disable-next-line:variable-name
  public sign_out_c: any = 'black';
  // tslint:disable-next-line:variable-name
  public sign_out_back: any = 'green !important';
  public showDialog = false;
  public btnLoging;
  // public nav_class: any;
  // public modalRef: BsModalRef;
  destroyed$ = new Subject<void>();

  constructor(
    private router: Router,
    @Inject(DOCUMENT) private document: Document,
    private loginService: LoginService,
    private dialog: MatDialog,
    private webGameService: WebGameService
  ) {
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }

  public setLoginForm() {
    this.loginService.setIsLoginForm('0');
  }

  customSideNavClose(sidenav: MatDrawer): void {
    if (sidenav.opened) {
      sidenav.toggle();
    }
  }

  seeSidenav() {
    this.newDrawerClass = (this.sidenav['_opened']) ? 'drawerShow' : 'drawerHIle';
    this.height = (this.sidenav['_opened']) ? '80vh' : '100%';
  }

  public upTop() {
    $('body,html').animate({
      scrollTop: 0
    }, 800);
  }

  public close() {
    this.loginService.logout();
    this.router.navigate(['']);
  }

  openLk() {
    this.router.navigate(['account']);
  }

  public openModal(template: TemplateRef<any>) {
    this.dialog.open(template, {panelClass: 'custom-modalbox-exit'});
    // this.modalRef = this.modalService.show(template, {class: 'modal-sm'}); // {3}
  }

  public onCancel(): void {
    // this.modalRef.hide();
  }

  public onConfirm(): void {
    this.close();
    // this.modalRef.hide();
  }

  @HostListener('window:scroll', ['$event']) getScrollHeight(event) {
    this.changePositionNavBar(window.pageYOffset, this.matToolbar._elementRef.nativeElement.offsetHeight);
  }

  changePositionNavBar(top, offsetHeight): void {
    if (this.router.url.indexOf('leave') !== -1) {
    } else {
      this.position = (top > offsetHeight + 50) ? 'sticky' : 'unset';
      this.top = (top > offsetHeight + 50) ? '0' : '-100%';
    }
  }

  closeDialog(event) {
    this.setLoginForm();
    this.showDialog = event;
  }

  ngOnInit(): void {
    if (localStorage.getItem('currentUser')) {
      this.btnLoging = true;
    } else {
      this.btnLoging = false;
    }
    this.loginService.isLogin()
      // .pipe(takeUntil(componentDestroyed(this)))
      .subscribe(isLogin => {
        this.btnLoging = isLogin;
      });



  }

}
