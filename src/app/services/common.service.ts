import {Injectable, OnDestroy} from '@angular/core';
import {BehaviorSubject, Observable, Subject} from 'rxjs';
import {catchError, map, takeUntil} from 'rxjs/operators';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../environments/environment';
import * as xml2js from 'xml2js';

@Injectable({
  providedIn: 'root'
})
export class CommonService implements OnDestroy{
  destroyed$ = new Subject<void>();
  public dataResult = [];
  private url = environment.cities_url;
  // tslint:disable-next-line:variable-name
  private api_url = `${environment.api_url}`;

  private repeatCities: Subject<any> = new Subject<any>();
  public repeatCitiesObs: Observable<any> = new Observable();
  private repeatRegions: BehaviorSubject<any>;
  public repeatRegionsObs: Observable<any> = new Observable();

  constructor(private http: HttpClient) {
    this.repeatCitiesObs = this.repeatCities.asObservable();
    // this.repeatRegionsObs = this.repeatRegions.asObservable();

    this.repeatRegions = new BehaviorSubject<any>(null);
    this.repeatRegionsObs = this.repeatRegions.asObservable();
  }

  // getCities(): Observable<any[]> {
  //     return this.http.get(this.url)
  //         .map(this.extractData)
  //         .catch(this.handleError);
  // }
  // tslint:disable-next-line:variable-name
  getCities(region_id: number): Observable<any[]> {

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Client-Service': 'frontend-client',
        'Auth-Key': '???'
      })
    };
    return this.http.get<any>(`${this.api_url}regcity/city/${region_id}`, httpOptions)
      .pipe(
        map(value => {
          this.repeatCities.next(value);
          return value;
        }),
        catchError(this.handleError)
      );
  }

  getRegions(): Observable<any[]> {

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Client-Service': 'frontend-client',
        'Auth-Key': '???'
      })
    };
    return this.http.get<any>(this.api_url + '' + 'regcity/region',
      httpOptions)
      .pipe(
        map(value => {
          this.repeatRegions.next(value);
          return value;
        }),
        catchError(this.handleError));
  }

  getRegionMore() {
    return new Observable((obs) => {
      this.repeatRegionsObs
        .pipe(takeUntil(this.destroyed$))
        .subscribe(regions => {
          if (regions) {
            obs.next(regions);
          } else {
            this.getRegions()
              .pipe(takeUntil(this.destroyed$))
              .subscribe(region => {
                obs.next(region);
              });
          }
        }, error1 => obs.error(error1));
    });

  }

  private extractJsData(res: any) {
    const body = res.json();
    return body || [];
  }

  private extractData(res: Response) {
    const parseString = new xml2js.Parser({strict: false, trim: true}).parseString;
    let body = res.text();
    // tslint:disable-next-line:only-arrow-functions
    parseString(res.text(), (err, result) => {
      body = result.regions.region;
    });
    return body || [];
  }

  private handleError(error: any) {
    const errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }
}
