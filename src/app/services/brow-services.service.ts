import { Injectable } from '@angular/core';
import {Observable, Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BrowServices {

  private subjectBrow =  new Subject<any>();
  private subjectF =  new Subject<any>();
  private featuresIndex =  new Subject<any>();
  constructor() { }
  addfeatures(brs: any) {
    this.featuresIndex.next(brs);
  }
  getfeatures(): Observable<any> {
    return this.featuresIndex.asObservable();
  }
  addData(brs: any) {
    this.subjectBrow.next(brs);
  }
  getData(): Observable<any> {
    return this.subjectBrow.asObservable();
  }
  addDataFeatures(brs: any) {
    this.subjectF.next(brs);
  }
  getDataFeatures(): Observable<any> {
    return this.subjectF.asObservable();
  }
}
