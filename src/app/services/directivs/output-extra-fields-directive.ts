import {Directive, ElementRef, EventEmitter, HostListener, Output} from '@angular/core';

@Directive({
  selector: '[appExtraFieldOutput]'
})
export class OutputExtraFieldsDirective {
  @Output() isDisabledExtra: EventEmitter<boolean> = new EventEmitter<boolean>();
  private readonly el: any;
  constructor(private elementRef: ElementRef) {
    this.el = this.elementRef.nativeElement;
  }

  @HostListener('click', ['$event'])
  clickEvent(event) {
    this.isDisabledExtra.emit(true);
  }
}
