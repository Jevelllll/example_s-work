import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PointerDirective} from './pointer-directive';
import { HeightServiceDirective } from './height-service.directive';
import { HiddenDisableElementDirective } from './hidden-disable-element.directive';

@NgModule({
    imports: [
        CommonModule,
    ],
    declarations: [
        PointerDirective,
        HeightServiceDirective,
        HiddenDisableElementDirective
    ],
  exports: [
    PointerDirective,
    HeightServiceDirective,
    HiddenDisableElementDirective
  ],
    schemas: [
        CUSTOM_ELEMENTS_SCHEMA ],
    providers: [
    ]
})
export class PointerModule {
}
