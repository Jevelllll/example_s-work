import {Directive, ElementRef, Renderer2} from '@angular/core';

@Directive({
  selector: '[appHeightService]',
  host: {
    '(click)': 'onClick(elementRef)'
  }
})
export class HeightServiceDirective {
  private readonly el: any;

  constructor(private elementRef: ElementRef, private renderer: Renderer2) {
    this.el = this.elementRef.nativeElement;
  }

  onClick(e) {
    let isActive: any = [].slice.call(e.nativeElement.classList);
    if (!isActive.includes('services-active')) {
      this.renderer.addClass(this.el, 'p-0');
      this.renderer.setStyle(this.el, 'height', 'max-content');
    } else {
      this.renderer.removeClass(this.el, 'p-0');
    }
  }
}
