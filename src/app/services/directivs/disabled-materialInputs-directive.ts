import {AfterViewInit, Directive, ElementRef, HostListener, Input} from '@angular/core';

declare var $: any;

@Directive({
  selector: '[appDisableMInput]'
})
// tslint:disable-next-line:class-name
export class disabledMaterialInputsDirective implements AfterViewInit {
  private el: any;
  @Input() appDisableMInput: string;

  constructor(private elementRef: ElementRef) {
    this.el = this.elementRef;
  }

  @HostListener('window:click', ['$event.target'])
  onClick(targetElement: any) {
    if (targetElement.classList.contains('mat-form-field-infix')) {
      const call = [].slice.call(targetElement.parentElement.getElementsByClassName('mat-form-field-suffix'));
      if (Array.isArray(call)) {
        call.forEach((item) => {
          item.querySelector('button').click();
        });
      }
    }
  }

  ngAfterViewInit(): void {
    const parentElement = this.appDisableMInput;
    // tslint:disable-next-line:only-arrow-functions
    $(document).ready(function() {
      const matInputElement = $(parentElement).find('input.mat-input-element');
      // tslint:disable-next-line:only-arrow-functions
      matInputElement.each(function(index, item) {
        const parent1 = $(item).parent().parent();
        parent1[0].children[1].style.fontSize = '2rem';
        $(item).css('color', 'black').prop('disabled', 'disabled');
      });
    });
  }
}
