import {Directive, ElementRef, Renderer2} from '@angular/core';

@Directive({
  selector: '[appMatStepStyle]',
})
export class MatStepDirective {
  private readonly el: any;

  constructor(private elementRef: ElementRef, private renderer: Renderer2) {
    this.el = this.elementRef.nativeElement;
  }
}
