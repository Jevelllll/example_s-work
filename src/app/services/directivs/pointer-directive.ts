import {Directive, ElementRef, Input} from '@angular/core';

declare var $: any;
@Directive({
    selector: '[appPointdir]'
})
export class PointerDirective {
    private el: any;

    constructor(private elementRef: ElementRef) {
        this.el = this.elementRef;
    }
}
