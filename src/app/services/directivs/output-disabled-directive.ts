import {Directive, ElementRef, EventEmitter, HostListener, Output} from '@angular/core';

@Directive({
  selector: '[appOutputDisabled]'
})
export class OutputDisabledDirective {
  @Output() isDisabled: EventEmitter<boolean> = new EventEmitter<boolean>();
  private readonly el: any;
  constructor(private elementRef: ElementRef) {
    this.el = this.elementRef.nativeElement;

  }

  @HostListener('mouseenter') onMouseEnter() {
    const htmlInputElement = this.el.querySelector('input');
    if (htmlInputElement.classList.contains('disableInp')){
      this.isDisabled.emit(true);
    }
  }
  @HostListener('mouseleave') onMouseLeave(){
    this.isDisabled.emit(false);
  }
}
