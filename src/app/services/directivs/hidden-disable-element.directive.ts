import {AfterViewInit, Directive, ElementRef, Input, OnChanges, Renderer2, SimpleChanges} from '@angular/core';
import {Application_phases} from '../auth/application-phases.service';

declare let $: any;

@Directive({
  selector: '[appHiddenDisableElement]'
})
export class HiddenDisableElementDirective implements OnChanges {

  element: ElementRef;
  @Input() data: any;
  @Input() index: any;
  @Input() changeD: any;

  constructor(private elem: ElementRef, private renderer: Renderer2, private phaseService: Application_phases) {
    this.element = this.elem;
  }

  ngOnChanges(changes: SimpleChanges) {
  }

}
