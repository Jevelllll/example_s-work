export class QuerySPoYers {
  data: Sp;
}

export interface Tariffinterface {
  id: number;
  query_tariff_start: number;
  query_tariff_end: number;
}

export class PostQueryPayModel {
  // tslint:disable-next-line:variable-name
  id_query: any;
}

export class Automodel {
  currentModel: number;
  titleaditional: string;
}

export interface AutoForm {
  query_auto_type: number;
  query_auto_mark: number;
  query_auto_model: number;
  query_price_min: number;
  query_price_max: number;
  query_currency: number;
}

export interface Sp {
  s_yers: number;
  po_yers: number;
}

export interface Typeauto {
  id: number;
  type_auto: string;
}

export interface MarkAuto {
  id: any;
  mark_auto: any;
}

export class BodyType {

  constructor(public name: string, public value: number) {
  }
}

export interface ModelByMark {
  id: any;
  tbl_mark_auto_id: any;
  model_auto: string;
  tbl_type_auto_id: any;
}

export class TypeOfDrive {
  status: number;
  data: [TypeOfDriveInterface];
}

export interface TypeOfDriveInterface {
  name: string;
  val: number;
  valRia: string;
}

export class Gearbox {
  status: number;
  data: [GearBoxInterface];
}

export class LegalCity {
  // tslint:disable-next-line:variable-name
  id_city: number;
  // tslint:disable-next-line:variable-name
  city_name: string;
}

export class LegalRegion {
  // tslint:disable-next-line:variable-name
  id_region: number;
  // tslint:disable-next-line:variable-name
  name_region: string;
}

export class LegalCategory {
  id: string;
  name: string;
}

export class GearBoxInterface {
  val: number;
  valRia: string;
  name: string;
  check: boolean;
}

export class FuelType {
  status: number;
  data: [FuelTypeInterface];
}

export interface FuelTypeInterface {
  val: number;
  valRia: string;
  name: string;
}

export class CraftCategory {
  data: any;

  // tslint:disable-next-line:variable-name
  constructor(public id: number = 0, craft_category_name: string = '', craft_img: string = '') {
  }
}

export class CraftInfo {
  public id: number;
  public desc: string;
}

export class Promo {
  id: number;
  name: string;
  promo_text: string;
  // tslint:disable-next-line:variable-name
  created_at: string;
}

export class CraftInfos {
  data: CraftInfo;
}

export class CraftItem {
  constructor(
    // tslint:disable-next-line:variable-name
    public id: number = null, public craft_cat_id: number = null, public desc: string = '',
    public images: CraftItemImages[] = [new CraftItemImages()]) {
  }
}

export class CrGetData {
  public data: any;
}

export class CraftItemImages {
  // tslint:disable-next-line:variable-name
  constructor(public id: number = null, public craft_item_id: number = 0, public craft_img: string = '') {
  }
}

export class QuestTask {
  constructor(public id = 0, public title = null,
              public desc = null, public start = null, public end = null) {
  }
}

export class QuestInfo {
  constructor(public id = 0, public title = null, public desc = null) {
  }
}
