import {Injectable, OnDestroy} from '@angular/core';
import {environment} from '../../../environments/environment';
import {BehaviorSubject, Observable, of, Subject} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {
  OperationProperty,
  OptionsProperty,
  PropertyFilingPData,
  PropertyFilingPesriod,
  PropertyResModel,
  TypeProperty
} from './propertyResModel';
import {catchError, map, switchMap, takeUntil, shareReplay} from 'rxjs/operators';
import {ExtraAdditionalFieldModel} from '../aditionals/electronicsmodel';
import {httpOptions} from './baseOptions';

@Injectable()
export class PriorityPropertyService implements OnDestroy {
  destroyed$ = new Subject<void>();
  private API_KEY = environment.RIA_API_KEY;
  private url = environment.api_url;

  private repeatFilingPeriod: BehaviorSubject<any>;
  public repeatFilingPeriodObs: Observable<any> = new Observable();

  private repeatTypeSentence: BehaviorSubject<any>;
  public repeatTypeSentenceObs: Observable<any> = new Observable();

  private repearPropertyPriceFor: BehaviorSubject<any>;
  public repearPropertyPriceForObs: Observable<any> = new Observable();

  private repeatOperationsProperty: BehaviorSubject<any>;
  public repeatOperationsPropertyObs: Observable<any> = new Observable();

  private repeatTypeProperty: BehaviorSubject<any>;
  public repeatTypePropertyObs: Observable<any> = new Observable();

  private repeatCurrency: BehaviorSubject<any>;
  public repeatCurrencyObs: Observable<any> = new Observable();

  constructor(private http: HttpClient) {

    this.repeatTypeSentence = new BehaviorSubject<any>(null);
    this.repeatTypeSentenceObs = this.repeatTypeSentence.asObservable();

    this.repeatTypeProperty = new BehaviorSubject<any>(null);
    this.repeatTypePropertyObs = this.repeatTypeProperty.asObservable();

    this.repeatOperationsProperty = new BehaviorSubject<any>(null);
    this.repeatOperationsPropertyObs = this.repeatOperationsProperty.asObservable();

    this.repearPropertyPriceFor = new BehaviorSubject<any>(null);
    this.repearPropertyPriceForObs = this.repearPropertyPriceFor.asObservable();

    this.repeatCurrency = new BehaviorSubject<any>(null);
    this.repeatCurrencyObs = this.repeatCurrency.asObservable();

    this.repeatFilingPeriod = new BehaviorSubject<any>(null);
    this.repeatFilingPeriodObs = this.repeatFilingPeriod.asObservable();
  }

  /***************тип недвижимости***************/
  getTypeProperty(): Observable<TypeProperty[]> {
    return this.http.get<any>(this.url + 'type/property')
      .pipe(
        map(value => {
          this.repeatTypeProperty.next(value);
          return value;
        }),
        catchError(this.handleError));
  }

  getTypeOfProperty(): Observable<TypeProperty[]> {
    return new Observable((observer) => {
      this.repeatTypePropertyObs
        .pipe(takeUntil(this.destroyed$))
        .subscribe(typeProp => {
          if (typeProp) {
            observer.next(typeProp);
          } else {
            this.getTypeProperty()
              .pipe(takeUntil(this.destroyed$))
              .subscribe(
                type => {
                },
                error => observer.error(error)
              );
          }
        });
    });
  }


  // tslint:disable-next-line:variable-name
  getOptionsProperty(by_typeId: number): Observable<OptionsProperty[]> {
    return this.http.get<any>(this.url + 'type/property/options/bytypeid/' + by_typeId)
      .pipe(
        map(value => value),
        catchError(this.handleError));
  }

// tslint:disable-next-line:variable-name
  getDistrictsProperty(id_city: number): Observable<any[]> {

    // const URL_DOMRIA = 'https://developers.ria.com/dom/cities_districts/'
    //   + id_city + '?api_key=' + this.API_KEY;
    const URL_DOMRIA = `${this.url}regcity/districts/${id_city}`;

    return this.http.get<any>(URL_DOMRIA)
      .pipe(
        map(value => value),
        shareReplay ({bufferSize: 1, refCount: true}),
        catchError(this.handleError));
  }

  /**************тип предложения***************/
  getTypeSentence(): Observable<any[]> {
    return this.http.get<any>(`${this.url}type/property/sentence`, {responseType: 'json'})
      .pipe(catchError(this.handleError), switchMap(value => {
        const sentenceFilter = value.filter(it => it.id !== 2);
        this.repeatTypeSentence.next(sentenceFilter);
        return of(sentenceFilter);
      }));
  }

  getTypeSentenceMore() {
    return new Observable((observer) => {
      this.repeatTypeSentenceObs
        .pipe(takeUntil(this.destroyed$))
        .subscribe((data) => {
          if (data) {
            observer.next(data);
          } else {
            this.getTypeSentence().subscribe((result) => {
            });
          }
        }, error => observer.error(error));

    });
  }

  /**********тип операции пр: аоенда, проодажа***********************/
  getOperationsProperty(): Observable<any[]> {
    const skip = [1, 4];
    return this.http.get<any>(this.url + 'type/property/operations')
      .pipe(
        switchMap(swVal => {
          const data = swVal.filter(iFS => iFS.id_property_operations != 3);
          return of(data);
        }),
        map(value => {
          this.repeatOperationsProperty.next(value);
          return value;
        }),
        catchError(this.handleError));
  }

  getOperationsPropertyMore(): Observable<any[]> {
    /*******тип операции продажа, аренда и др..****/
    return new Observable((observer) => {
      this.repeatOperationsPropertyObs.pipe(takeUntil(this.destroyed$)).subscribe(rOperationPr => {
        if (rOperationPr) {
          const operationsProperty = [];
          rOperationPr.forEach(item => {
            operationsProperty.push(new OperationProperty(item.id_property_operations, item.property_operation));
          });
          observer.next(operationsProperty);
        } else {
          this.getOperationsProperty()
            .pipe(
              // takeUntil(componentDestroyed(this)),
              switchMap(valRes => {
                return of(valRes);
              }))
            .subscribe(value => {
              const operationsProperty = [];
              value.forEach(item => {
                operationsProperty.push(new OperationProperty(item.id_property_operations, item.property_operation));
              });
              observer.next(operationsProperty);
            });
        }
      });
    });
  }


  // getCurrency(): Observable<Electronicsmodel[]> {
  //   return of(this.Currency);
  // }

  cleanCurrency(): void {
    this.repeatCurrency.next(null);
  }

  getCurrency(optionsId, idTypeProperty): Observable<any> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Client-Service': 'frontend-client',
      'Auth-Key': 'sishik_rest_api'
    });
    return this.http.get<any>(`${this.url}type/property/currency/${optionsId}/${idTypeProperty}`, {headers, responseType: 'json'})
      .pipe(map(value => {
          this.repeatCurrency.next(value);
          return value;
        }
      ), catchError(this.handleError));
  }

  /*******************новые доп поля (extra)***********************************************/
  // tslint:disable-next-line:variable-name
  getPropertyExtra(id_type_property: number, id_property_options: number, id_property_operations: number): Observable<ExtraAdditionalFieldModel[]> {

    if (id_type_property || id_property_options || id_property_operations) {
      const headers = new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        'Client-Service': 'frontend-client',
        'Auth-Key': 'sishik_rest_api'
      });
      return this.http.post<ExtraAdditionalFieldModel[]>(`${this.url}type/property/extra-fields`, {
        body: {id_type_property, id_property_options, id_property_operations}
      }, {headers, responseType: 'json'}).pipe(map(value => {
        return value;
      }));
    } else {
      return Observable.throw(false);
    }

  }

  /*****************Период подачи***********************************************/
  getFilingPeriod(): Observable<PropertyFilingPData> {
    const headers = httpOptions();
    return this.http.get<PropertyFilingPesriod>(`${this.url}property/filing-period`, {headers, responseType: 'json'})
      .pipe(takeUntil(this.destroyed$), switchMap(period => {
        this.repeatFilingPeriod.next(period.data);
        return of(period.data);
      }));
  }

  filingPeriodMore(): Observable<PropertyFilingPData> {
    return new Observable((observer) => {
      this.repeatFilingPeriodObs
        .pipe(takeUntil(this.destroyed$))
        .subscribe(period => {
          if (period) {
            observer.next(period);
          } else {
            this.getFilingPeriod()
              .pipe(takeUntil(this.destroyed$))
              .subscribe(() => {
              });
          }
        });
    });
  }

  /********************цена за и тип цены**************************************/
  getPropertyPriceFor(): Observable<PropertyResModel[]> {

    const headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Client-Service': 'frontend-client',
      'Auth-Key': 'sishik_rest_api'
    });

    return this.http.get<PropertyResModel[]>(
      `${this.url}type/property/price-for-property`,
      {
        headers,
        responseType: 'json'
      }).pipe(takeUntil(this.destroyed$), switchMap(data => {
        this.repearPropertyPriceFor.next(data);
        return of(data);
      }),
      catchError(this.handleError));
  }


  private extractData(res: Response) {
    const body = res.json();
    return body || [];
  }

  private handleError(error: any) {
    const errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }


}
