import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {PriorityPropertyService} from './property.service';
import {PriorityWorkService} from './work.service';
import {PriorityAutoService} from './auto.service';
import {PriorityLegalService} from './legal.service';



@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [
    PriorityPropertyService,
    PriorityWorkService,
    PriorityAutoService,
    PriorityLegalService
  ]
})
export class PriorityModule { }
