import {OnDestroy} from '@angular/core';
import {Subject} from 'rxjs/internal/Subject';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {LegalCategory, LegalCity, LegalRegion} from './resModel';
import {BehaviorSubject} from 'rxjs/internal/BehaviorSubject';
import {Observable} from 'rxjs/internal/Observable';
import {distinctUntilChanged} from 'rxjs/internal/operators/distinctUntilChanged';
import {takeUntil} from 'rxjs/internal/operators/takeUntil';
import {catchError} from 'rxjs/internal/operators/catchError';
import {map} from 'rxjs/internal/operators/map';

function Headers() {
  return new HttpHeaders({
    'Content-Type': 'application/x-www-form-urlencoded',
    'Client-Service': 'frontend-client',
    'Auth-Key': 'sishik_rest_api'
  });
}

export class PriorityLegalService implements OnDestroy {
  destroyed$ = new Subject<void>();
  public url = environment.api_url;
  private repeatLegalCity: BehaviorSubject<LegalCity>;
  public repeatLegalCityObs: Observable<LegalCity> = new Observable();

  private repeatLegalRegion: BehaviorSubject<LegalRegion>;
  public repeatLegalRegionObs: Observable<LegalRegion> = new Observable();

  private repeatLegalCategory: BehaviorSubject<LegalCategory>;
  public repeatLegalCategoryObs: Observable<LegalCategory> = new Observable();

  constructor(private http: HttpClient) {
    this.repeatLegalCity = new BehaviorSubject<LegalCity>(null);
    this.repeatLegalCityObs = this.repeatLegalCity.asObservable();
    this.repeatLegalRegion = new BehaviorSubject<LegalRegion>(null);
    this.repeatLegalRegionObs = this.repeatLegalRegion.asObservable();
    this.repeatLegalCategory = new BehaviorSubject<LegalCategory>(null);
    this.repeatLegalCategoryObs = this.repeatLegalCategory.asObservable();
  }

  getRepeatLegalCityMore(): Observable<LegalCity> {
    return new Observable((observer) => {
      this.repeatLegalCityObs.pipe(distinctUntilChanged(), takeUntil(this.destroyed$))
        .subscribe((city) => {
          if (city) {
            observer.next(city);
          } else {
            this.getLegalCity()
              .pipe(distinctUntilChanged()
                , takeUntil(this.destroyed$)
              )
              .subscribe((data) => {
              }, error => observer.error(error));
          }
        });
    });
  }

  getLegalCity(): Observable<any> {
    const headers = Headers();
    return this.http.get<LegalCity>(`${this.url}legal/city`,
      {headers, responseType: 'json'}).pipe(map((data) => this.repeatLegalCity.next(data)), catchError(this.handleError));
  }


  getRepeatLegalRegionMore(): Observable<LegalRegion> {
    return new Observable((observer) => {
      this.repeatLegalRegionObs.pipe(distinctUntilChanged(), takeUntil(this.destroyed$))
        .subscribe((region) => {
          if (region) {
            observer.next(region);
          } else {
            this.getLegalRegion()
              .pipe(distinctUntilChanged()
                , takeUntil(this.destroyed$)
              )
              .subscribe((data) => {
              }, error => observer.error(error));
          }
        });
    });
  }

  getLegalRegion(): Observable<any> {
    const headers = Headers();
    return this.http.get<LegalRegion>(`${this.url}legal/region`,
      {headers, responseType: 'json'}).pipe(map((data) => this.repeatLegalRegion.next(data)), catchError(this.handleError));
  }

  getRepeatLegalCategoryMore(): Observable<LegalCategory> {
    return new Observable((observer) => {
      this.repeatLegalCategoryObs.pipe(distinctUntilChanged(), takeUntil(this.destroyed$))
        .subscribe((category) => {
          if (category) {
            observer.next(category);
          } else {
            this.getLegalCategory()
              .pipe(distinctUntilChanged()
                , takeUntil(this.destroyed$)).subscribe((data) => {
            }, error => observer.error(error));
          }
        });

    });
  }

  getLegalCategory(): Observable<any> {
    const headers = Headers();
    return this.http.get<LegalCategory>(`${this.url}legal/category`,
      {headers, responseType: 'json'}).pipe(map((data) => this.repeatLegalCategory.next(data)), catchError(this.handleError));
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }

  private handleError(error: any) {
    const errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg);
    return Observable.throw(errMsg);
  }
}
