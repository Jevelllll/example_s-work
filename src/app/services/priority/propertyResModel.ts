export class PropertyResModel {
  id: number;
  // tslint:disable-next-line:variable-name
  price_for_domria_field: string;
  priceForDomriaField: PriceForDomRiaField;
  // tslint:disable-next-line:variable-name
  price_for_name: string;
  // tslint:disable-next-line:variable-name
  property_operations_id: number;
  // tslint:disable-next-line:variable-name
  property_options_id: number;
}
export class DistrictsProperty {
// tslint:disable-next-line:variable-name
  constructor(public area_id: number,
              // tslint:disable-next-line:variable-name
              public city_id: number,
              public name: string,
              public type: number,
              public value: number) {
  }

}
export interface PropertyForm {
  query_building_type: number;
  query_property_options: number;
  query_price_min: number;
  query_price_max: number;
  query_currency: number;
  query_address: string;
  query_city: number;
}
export interface PriceForDomRiaField {
    id: number;
    name: string;
}

export interface PricebeforeJsParse {
  value: string;
  name: string;
}

export interface PropertyFilingPesriod {
  status: number;
  data: PropertyFilingPData;
}
export interface PropertyFilingPData {
  id: number;
  valRia: string;
  name: string;
}
export interface TypeProperty {
  id: number;
  type: string;
}

export interface OptionsProperty {
  property_options_id: number;
  property_options_type: string;
}

export class OperationProperty {
// tslint:disable-next-line:variable-name
  constructor(public operation_property_id: number, public operation_property: string) {
  }

}
