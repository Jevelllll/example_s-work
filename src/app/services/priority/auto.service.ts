import {Injectable, OnDestroy} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {BehaviorSubject, Observable, of, Subject, throwError} from 'rxjs';
import {environment} from '../../../environments/environment';
import {catchError, distinctUntilChanged, filter, map, takeUntil, shareReplay} from 'rxjs/operators';
import {Automodel, BodyType, FuelType, Gearbox, MarkAuto, ModelByMark, QuerySPoYers, Typeauto, TypeOfDrive} from './resModel';

function Headers() {
  return new HttpHeaders({
    'Content-Type': 'application/x-www-form-urlencoded',
    'Client-Service': 'frontend-client',
    'Auth-Key': '???'
  });
}

@Injectable()
export class PriorityAutoService implements OnDestroy {
  destroyed$ = new Subject<void>();
  public repeatAutoType: BehaviorSubject<any>;
  public repeatAutoTypeObs: Observable<any> = new Observable();
  public repeatFilingPeriod: BehaviorSubject<any>;
  public repeatFilingPeriodObs: Observable<any> = new Observable();
  public repeatFuel: BehaviorSubject<any>;
  public repeatFuelObs: Observable<any> = new Observable();
  public gearBoxB: BehaviorSubject<any>;
  public gearBoxObs: Observable<any> = new Observable();
  public vehicleYearBeh: BehaviorSubject<any>;
  public vehicleYearObs: Observable<any> = new Observable();
  public API_KEY = environment.RIA_API_KEY;
  public url = environment.api_url;
  Markauto: Automodel[] = [
    {
      currentModel: 0,
      titleaditional: 'Reno',
    },
    {
      currentModel: 1,
      titleaditional: '1222',
    }
  ];
  Modelauto: Automodel[] = [
    {
      currentModel: 0,
      titleaditional: 'Reno',
    },
    {
      currentModel: 1,
      titleaditional: '1222',
    }
  ];

  Currency: Automodel[] = [
    {
      currentModel: 1,
      titleaditional: 'USD',
    },
    {
      currentModel: 2,
      titleaditional: 'EUR',
    },
    {
      currentModel: 3,
      titleaditional: 'UAH',
    }
  ];

  constructor(private http: HttpClient) {
    this.repeatAutoType = new BehaviorSubject<any>(null);
    this.repeatAutoTypeObs = this.repeatAutoType.asObservable();
    this.repeatFilingPeriod = new BehaviorSubject<any>(null);
    this.repeatFilingPeriodObs = this.repeatFilingPeriod.asObservable();
    this.repeatFuel = new BehaviorSubject<any>(null);
    this.repeatFuelObs = this.repeatFuel.asObservable();
    this.gearBoxB = new BehaviorSubject<any>(null);
    this.gearBoxObs = this.gearBoxB.asObservable();
    this.vehicleYearBeh = new BehaviorSubject<any>(null);
    this.vehicleYearObs = this.vehicleYearBeh.asObservable();
  }

  /**********дата от и до****************************/
  getVehicleYear(): Observable<QuerySPoYers> {
    return this.http.get<any>(this.url + 'vehicleyear')
      .pipe(
        map(value => {
          this.vehicleYearBeh.next(value);
          return value;
        }),
        catchError(this.handleError));
  }

  yearMore() {
    return new Observable((observer) => {
      this.vehicleYearObs
        .pipe(takeUntil(this.destroyed$))
        .subscribe(vehicleY => {
          if (vehicleY) {
            const prePushPoYears = [];
            const preYear = {s_yers: [], po_yers: []};
            for (let index = vehicleY.data.s_yers; index <= vehicleY.data.po_yers; index++) {
              preYear.s_yers.push(index);
              prePushPoYears.push(index);
            }
            preYear.po_yers = prePushPoYears.reverse();
            observer.next(preYear);
          } else {
            this.getVehicleYear()
              .pipe(takeUntil(this.destroyed$))
              .subscribe(respData => {
              });
          }
        }, error1 => observer.error(error1));
    });

  }


  getAllMarkAuto(): Observable<MarkAuto[]> {
    return this.http.get<any>(this.url + 'mark/auto')
      .pipe(
        map(value => value),
        catchError(this.handleError));
  }

  // tslint:disable-next-line:variable-name
  getAllMarkAutoAutoTypeId(type_id: number): Observable<MarkAuto[]> {
    return this.http.get<any>(this.url + 'mark/auto/type/' + type_id)
      .pipe(
        map(value => value),
        shareReplay ({bufferSize: 1, refCount: true}),
        catchError(this.handleError));
  }

  // tslint:disable-next-line:variable-name
  getModelAuto(idmark: number, type_id: number): Observable<ModelByMark[]> {
    return this.http.get<any>(this.url + 'model/auto/mark/' + idmark + '/type/' + type_id)
      .pipe(
        map(value => value),
        catchError(this.handleError));
  }

  getTypeAuto(): Observable<Typeauto[]> {
    return this.http.get<any>(this.url + 'type/auto')
      .pipe(
        map(value => {
          this.repeatAutoType.next(value);
          return value;
        }),
        catchError(this.handleError));
  }


  // tslint:disable-next-line:variable-name
  getBody_type(transport_type_id: number, services = ''): Observable<BodyType[]> {
    let URL_AUTORIA = '???' + transport_type_id;

    if (services.length > 1) {
      URL_AUTORIA = '???' + transport_type_id + '/' + services;
    } else {
      URL_AUTORIA = '???' + transport_type_id;
    }

    return this.http.get<any>(URL_AUTORIA)
      .pipe(
        map(value => {
          return value;
        }),
        shareReplay ({bufferSize: 1, refCount: true}),
        catchError(this.handleError));

  }

  /*****************Тип привода по типу транспорта*******************************************/
  getTypeOfDrive(idTypeAuto: number): Observable<TypeOfDrive> {
    const headers = Headers();
    return this.http.get<TypeOfDrive>(`${this.url}type/drive/${idTypeAuto}`,
      {headers, responseType: 'json'}).pipe(catchError(this.handleError));
  }

  /*****************Коробка передач*******************************************/
  getGearBox(): Observable<Gearbox> {
    const headers = Headers();
    return this.http.get<Gearbox>(`${this.url}type/gearbox`,
      {headers, responseType: 'json'}).pipe(map(value => {
      this.gearBoxB.next(value);
      return value;
    }), catchError(this.handleError));
  }

  getGearBoxMore(): Observable<Gearbox> {
    let dataNext: Observable<any> = new Observable<any>();
    this.gearBoxObs
      .pipe(takeUntil(this.destroyed$))
      .subscribe(valGearBox => {
        if (valGearBox) {
          const gear: BehaviorSubject<any> = new BehaviorSubject(valGearBox);
          dataNext = gear.asObservable().pipe(map(gbS => gbS.data));
        } else {
          dataNext = this.getGearBox().pipe(map(gearBox => gearBox.data));
        }
      }, error1 => {
      });
    return dataNext;
  }

  /*****************Тип топлива******************************************/
  getFuelType(): Observable<FuelType> {
    const headers = Headers();
    return this.http.get<FuelType>(`${this.url}type/fuel`,
      {headers, responseType: 'json'}).pipe(map(fuel => {
      this.repeatFuel.next(fuel);
      return fuel;
    }), catchError(this.handleError));
  }

  getFuelTypeMore(): Observable<FuelType> {
    let dataNext: Observable<any> = new Observable<any>();
    this.repeatFuelObs
      .pipe(takeUntil(this.destroyed$))
      .subscribe(fuelS => {
        if (fuelS) {
          const bsFuel: BehaviorSubject<any> = new BehaviorSubject(fuelS);
          dataNext = bsFuel.asObservable().pipe(map(bsF => bsF.data));
        } else {
          dataNext = this.getFuelType().pipe(map(fuel => fuel.data));
        }
      }, error1 => {
      });
    return dataNext;
  }

  /*****************Период подачи***********************************************/
  getFilingPeriod(): Observable<FuelType> {
    const headers = Headers();
    return this.http.get<FuelType>(`${this.url}type/filing-period`,
      {headers, responseType: 'json'}).pipe(map(value => {
      this.repeatFilingPeriod.next(value);
      return value;
    }), catchError(this.handleError));
  }

  getCurrency(): Observable<Automodel[]> {
    return of(this.Currency);
  }

  getFillingObS() {
    return new Observable((observer) => {
      this.repeatFilingPeriodObs
        .pipe(distinctUntilChanged(), takeUntil(this.destroyed$))
        .subscribe((filingObs) => {
          if (filingObs) {
            observer.next(filingObs.data);
          } else {
            this.getFilingPeriod().pipe(map(filing => filing.data))
              .pipe(distinctUntilChanged()
                , takeUntil(this.destroyed$)
              )
              .subscribe((data) => {
              }, error => observer.error(error));
          }
        }, error => observer.error(error));
    });

  }

  private extractData(res: Response) {
    const body = res.json();
    return body || [];
  }

  private handleError(error: any) {
    const errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg);
    return throwError(errMsg);
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }
}
