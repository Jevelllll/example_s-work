import {Injectable, OnDestroy} from '@angular/core';
import {environment} from '../../../environments/environment';
import {BehaviorSubject, Observable, Subject} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {catchError, map, takeUntil} from 'rxjs/operators';

@Injectable()
export class PriorityWorkService implements OnDestroy{
  destroyed$ = new Subject<void>();
  private url = `${environment.api_url}type/work/`;

  private repeatHeadings: BehaviorSubject<any[]>;
  public repeatHeadingsObs: Observable<any[]> = new Observable();

  private repeatWorkCategoryHeadings: BehaviorSubject<any[]>;
  public repeatWorkCategoryHeadingsObs: Observable<any[]> = new Observable();

  private repeatWorkCities: BehaviorSubject<any[]>;
  public repeatWorkCitiesObs: Observable<any[]> = new Observable();

  private repeatWorkProfLevel: BehaviorSubject<any[]>;
  public repeatWorkProfLevelObs: Observable<any[]> = new Observable();

  private repeatWorkSchedule: BehaviorSubject<any>;
  public repeatWorkScheduleObs: Observable<any> = new Observable();

  private repeatWorkType: BehaviorSubject<any>;
  public repeatWorkTypeObs: Observable<any> = new Observable();

  constructor(private http: HttpClient) {

    this.repeatHeadings = new BehaviorSubject<any>(null);
    this.repeatHeadingsObs = this.repeatHeadings.asObservable();

    this.repeatWorkCategoryHeadings = new BehaviorSubject<any>(null);
    this.repeatWorkCategoryHeadingsObs = this.repeatWorkCategoryHeadings.asObservable();

    this.repeatWorkCities = new BehaviorSubject<any>(null);
    this.repeatWorkCitiesObs = this.repeatWorkCities.asObservable();

    this.repeatWorkProfLevel = new BehaviorSubject<any>(null);
    this.repeatWorkProfLevelObs = this.repeatWorkProfLevel.asObservable();

    this.repeatWorkSchedule = new BehaviorSubject<any>(null);
    this.repeatWorkScheduleObs = this.repeatWorkSchedule.asObservable();

    this.repeatWorkType = new BehaviorSubject<any>(null);
    this.repeatWorkTypeObs = this.repeatWorkType.asObservable();
  }


  /********************рубрика**********************/
  getHeadings(): Observable<any[]> {
    return this.http.get<any>(this.url + 'headings')
      .pipe(
        takeUntil(this.destroyed$),
        map(value => {
          const data = value.data.map((item) => {
            return {
              id: item.id,
              name: JSON.parse(item.heading_name).ru,
              sendId: item.heading_id
            };
          });
          this.repeatHeadings.next(data);
          return data;
        }),
        catchError(this.handleError));
  }

  /********************предложение тип работы *****************/
  getType(): Observable<any[]> {
    return this.http.get<any>(this.url + 'type')
      .pipe(
        takeUntil(this.destroyed$),
        map(value => {
          this.repeatWorkType.next(value);
          return value;
        }),
        catchError(this.handleError));
  }

  getTypeMore(): Observable<any> {
    return new Observable((observer) => {
      this.repeatWorkTypeObs
        .pipe(takeUntil(this.destroyed$))
        .subscribe((type) => {
          if (type) {
            observer.next(type);
          } else {
            this.getType()
              .pipe(takeUntil(this.destroyed$))
              .subscribe(() => {
            }, error => observer.next(error));
          }

        }, error => observer.next(error));
    });
  }

  /*************подрубрика*****************************/
  getWorkCategoryHeadings(idHeadings): Observable<any[]> {
    return this.http.get<any>(this.url + `category-headings/${idHeadings}`)
      .pipe(
        takeUntil(this.destroyed$),
        map(value => {
          this.repeatWorkCategoryHeadings.next(value.data);
          return value.data;
        }),
        catchError(this.handleError));
  }

  /************становка городов************************/
  getWorkCities(): Observable<any[]> {
    return this.http.get<any>(this.url + `cities`)
      .pipe(
        takeUntil(this.destroyed$),
        map(value => {
          const data = value.data.map((item) => {
            if (item.rabota_centerId === item.rabota_id) {
              return {
                id: item.id,
                name: JSON.parse(item.rabota_name).ru
              };
            }

          }).filter((it) => it);
          this.repeatWorkCities.next(data);
          return data;
        }),
        catchError(this.handleError));
  }

  getWorkCitiesMore(): Observable<any[]> {
    return new Observable((observer) => {
      this.repeatWorkCitiesObs
        .pipe(takeUntil(this.destroyed$))
        .subscribe((city) => {
        if (city) {
          observer.next(city);
        } else {
          this.getWorkCities()
            .pipe(takeUntil(this.destroyed$))
            .subscribe(() => {
          }, error => observer.next(error));
        }
      }, error => observer.next(error));
    });
  }

  getProfLevel(): Observable<any[]> {
    return this.http.get<any>(this.url + `prof-level`)
      .pipe(
        takeUntil(this.destroyed$),
        map(value => {
          this.repeatWorkProfLevel.next(value.data);
          return value.data;
        }),
        catchError(this.handleError));
  }

  /***************занятость*********************/
  getSchedule(): Observable<any> {
    return this.http.get<any>(this.url + `schedule`)
      .pipe(
        takeUntil(this.destroyed$),
        map(value => {
          this.repeatWorkSchedule.next(value.data);
          return value.data;
        }),
        catchError(this.handleError));
  }

  getScheduleMore(): Observable<any> {
    return new Observable((observer) => {
      this.repeatWorkScheduleObs
        .pipe(takeUntil(this.destroyed$))
        .subscribe((schedule) => {
        if (schedule) {
          observer.next(schedule);
        } else {
          this.getSchedule()
            .pipe(takeUntil(this.destroyed$))
            .subscribe(() => {
          }, error => observer.error(error));
        }
      }, error => observer.error(error));
    });
  }

  /**********************\\модификации на товары***************/
  private handleError(error: any) {
    const errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }
}
