import {Injectable, OnDestroy} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {BehaviorSubject, Observable, Subject} from 'rxjs';
import {environment} from '../../../environments/environment';
import {QuestInfo, QuestTask} from './resModel';
import {catchError} from 'rxjs/internal/operators/catchError';
import {takeUntil} from 'rxjs/internal/operators/takeUntil';
import {map} from 'rxjs/internal/operators/map';

const headers = () => {
  return new HttpHeaders({
    'Client-Service': 'frontend-client',
    'Auth-Key': 'sishik_rest_api'
  });
};

@Injectable({
  providedIn: 'root'
})
export class QuestService implements OnDestroy {
  destroyed$ = new Subject<void>();
  private url = `${environment.api_url}type/quest/`;

  private repeatQuestTask: BehaviorSubject<QuestTask>;
  public repeatQuestTaskObs: Observable<QuestTask> = new Observable();

  private repeatQuestInfo: BehaviorSubject<QuestInfo>;
  public repeatQuestInfoObs: Observable<QuestInfo> = new Observable();

  constructor(private http: HttpClient) {
    this.repeatQuestTask = new BehaviorSubject<QuestTask>(new QuestTask());
    this.repeatQuestTaskObs = this.repeatQuestTask.asObservable();

    this.repeatQuestInfo = new BehaviorSubject<QuestInfo>(new QuestInfo());
    this.repeatQuestInfoObs = this.repeatQuestInfo.asObservable();
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }

  private getQuestTask(): Observable<QuestTask> {
    return this.http.get<QuestTask>(`${this.url}task`, {headers: headers(), responseType: 'json'})
      .pipe(map((data) => {
        this.repeatQuestTask.next(data);
        return data;
      }), catchError(this.handleError));
  }

  public isWrite(userId: number, infoId: number) {
    const bodyHttp = 'userId=' + userId + '&infoId=' + infoId;
    const preHeaders = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Client-Service': 'frontend-client',
      'Auth-Key': 'sishik_rest_api'
    });
    return this.http.post<any>(this.url + 'is-write',
      bodyHttp, {headers: preHeaders}).pipe(
      map(res => res),
      catchError(this.handleError));
  }

  public getQuestTaskMore(): Observable<QuestTask> {
    let dataQTask: Observable<QuestTask> = new Observable<QuestTask>();
    this.repeatQuestTaskObs.pipe(takeUntil(this.destroyed$)).subscribe((result) => {
      if (result.id !== 0) {
        const bDQT: BehaviorSubject<QuestTask> = new BehaviorSubject<QuestTask>(result);
        dataQTask = bDQT.asObservable();
      } else {
        dataQTask = this.getQuestTask();
      }
    });
    return dataQTask;
  }

  private getQuestInfo(): Observable<QuestInfo> {
    return this.http.get<QuestInfo>(`${this.url}info`, {headers: headers(), responseType: 'json'})
      .pipe(map((data) => {
        this.repeatQuestInfo.next(data);
        return data;
      }, catchError(this.handleError)));
  }

  public getQuestInfoMore(): Observable<QuestInfo> {
    let dataQuestInfo: Observable<QuestInfo> = new Observable<QuestInfo>();
    this.repeatQuestInfo.pipe(takeUntil(this.destroyed$)).subscribe((result) => {
      if (result.desc !== null || result.title !== null) {
        const bDQT: BehaviorSubject<QuestInfo> = new BehaviorSubject<QuestInfo>(result);
        dataQuestInfo = bDQT.asObservable();
      } else {
        dataQuestInfo = this.getQuestInfo();
      }
    }, error => {
    });
    return dataQuestInfo;
  }

  private handleError(error: any) {
    const errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    return Observable.throw(errMsg);
  }
}
