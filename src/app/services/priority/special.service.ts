import {Injectable, OnDestroy} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {BehaviorSubject, Observable, Subject} from 'rxjs';
import {CraftCategory, CraftInfos, CraftItem, CrGetData} from './resModel';
import {map} from 'rxjs/internal/operators/map';
import {catchError} from 'rxjs/internal/operators/catchError';
import {shareReplay, takeUntil, tap} from 'rxjs/operators';

function Headers() {
  return new HttpHeaders({
    'Content-Type': 'application/x-www-form-urlencoded',
    'Client-Service': 'frontend-client',
    'Auth-Key': 'sishik_rest_api'
  });
}

@Injectable()
export class SpecialService implements OnDestroy {
  destroyed$ = new Subject<void>();
  public url = environment.api_url;

  private repeatCraftCategory: BehaviorSubject<CraftCategory[]>;
  public repeatCraftCategoryObs: Observable<CraftCategory[]> = new Observable();
  private repeatCraftItem: BehaviorSubject<CraftItem[]>;
  public repeatCraftItemObs: Observable<CraftItem[]> = new Observable();

  private repeatCraftIInfo: BehaviorSubject<CraftInfos>;
  public repeatCraftInfoObs: Observable<CraftInfos> = new Observable();

  public sendCraftItem: BehaviorSubject<CraftItem[]> = new BehaviorSubject([new CraftItem()]);

  constructor(private http: HttpClient) {
    this.repeatCraftCategory = new BehaviorSubject<CraftCategory[]>([new CraftCategory()]);
    this.repeatCraftCategoryObs = this.repeatCraftCategory.asObservable();

    this.repeatCraftItem = new BehaviorSubject<CraftItem[]>([new CraftItem()]);
    this.repeatCraftItemObs = this.repeatCraftItem.asObservable();

    this.repeatCraftIInfo = new BehaviorSubject<CraftInfos>(null);
    this.repeatCraftInfoObs = this.repeatCraftIInfo.asObservable();
  }

  private craftCategories(): Observable<CraftCategory[]> {
    return this.http.get<CrGetData>(`${this.url}craft/category`, {headers: Headers(), responseType: 'json'})
      .pipe(map((data) => {
        return data.data;
      }), catchError(this.handleError), shareReplay({bufferSize: 1, refCount: true}));
  }

  public getCraftInfos(): Observable<CraftInfos> {
    return this.http.get<CraftInfos>(`${this.url}craft/info`, {headers: Headers(), responseType: 'json'})
      .pipe(
        tap((data) => this.repeatCraftIInfo.next(data),
          catchError(this.handleError)), shareReplay({bufferSize: 1, refCount: true})
      );
  }

  public getCraftCategoriesMore(): Observable<CraftCategory[]> {
    // @ts-ignore
    let dataCraftCat: Observable<CraftCategory[]> = new Observable<CraftCategory>();
    this.repeatCraftCategoryObs.pipe(takeUntil(this.destroyed$)).subscribe((category) => {
      if (category[0].id !== 0) {
        const behDCraft: BehaviorSubject<CraftCategory[]> = new BehaviorSubject([new CraftCategory()]);
        dataCraftCat = behDCraft.asObservable();
      } else {
        dataCraftCat = this.craftCategories();
      }
    });
    return dataCraftCat;
  }

  public getCraftItemsMore(): Observable<CraftItem[]> {
    // @ts-ignore
    let dataCraftItem: Observable<CraftItem[]> = new Observable<CraftItem>();
    this.repeatCraftItemObs.pipe(takeUntil(this.destroyed$)).subscribe((item) => {
      if (item[0].id && Array.isArray(item)) {
        const behDItem: BehaviorSubject<CraftItem[]> = new BehaviorSubject([new CraftItem()]);
        dataCraftItem = behDItem.asObservable();
      } else {
        dataCraftItem = this.craftItem();
      }
    });
    return dataCraftItem;
  }

  public getItemByCategoryId($craftCategoryId = null) {
    return this.getCraftItemsMore().pipe(map((result) => {
      if ($craftCategoryId) {
        const newResult: CraftItem[] = result.filter((data) => Number(data.craft_cat_id) === Number($craftCategoryId));
        this.sendCraftItem.next(newResult);
        return newResult;
      }
      this.sendCraftItem.next(result);
      return result;
    }));
  }

  public getCraftItem(): Observable<CraftItem[]> {
    return this.sendCraftItem.asObservable();
  }

  public craftItem(): Observable<CraftItem[]> {
    return this.http.get<CrGetData>(`${this.url}craft/subcategory`, {headers: Headers(), responseType: 'json'})
      .pipe(map((data) => {
        return data.data;
      }), catchError(this.handleError), shareReplay({bufferSize: 1, refCount: true}),);
  }

  private handleError(error: any) {
    const errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    return Observable.throw(errMsg);
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }
}
