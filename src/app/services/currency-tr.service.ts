import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {BehaviorSubject, Observable, of} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {httpOptions} from './priority/baseOptions';
import {first, switchMap} from 'rxjs/operators';

@Injectable()
export class CurrencyTrService {

  private url: any = environment.api_url;
  private currencyBS: BehaviorSubject<any>;
  public currencyObs: Observable<any> = new Observable<any>();

  constructor(private http: HttpClient) {

    this.currencyBS = new BehaviorSubject<any>(null);
    this.currencyObs = this.currencyBS.asObservable();
  }

  /*************список всез видов валют***************************/
  getCurrencyList(): Observable<any> {
    const headers = httpOptions();
    return this.http.get(`${this.url}allquerylist/currency-translator`, {headers, responseType: 'json'})
      .pipe(first(), switchMap(valC => {
        this.currencyBS.next(valC);
        return of(valC);
      }));
  }
}
