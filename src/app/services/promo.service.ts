import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Promo} from './priority/resModel';
import {BehaviorSubject} from 'rxjs/internal/BehaviorSubject';
import {Observable} from 'rxjs/internal/Observable';
import {catchError} from 'rxjs/internal/operators/catchError';
import {tap} from 'rxjs/internal/operators/tap';
import {httpOptions} from './priority/baseOptions';

function Headers() {
  return new HttpHeaders({
    'Content-Type': 'application/x-www-form-urlencoded',
    'Client-Service': 'frontend-client',
    'Auth-Key': 'sishik_rest_api'
  });
}

@Injectable({
  providedIn: 'root'
})
export class PromoService {
  public url = environment.api_url;
  private repeatPromo: BehaviorSubject<Promo[]>;
  public repeatPromoObs: Observable<Promo[]> = new Observable();
  private httpOptions = httpOptions;

  constructor(private http: HttpClient) {
    this.repeatPromo = new BehaviorSubject(null);
    this.repeatPromoObs = this.repeatPromo.asObservable();
  }

  getPromo(): Observable<Promo[]> {
    let user;
    try {
      user = JSON.parse(localStorage.getItem('currentUser'));
    } catch (e) {
    }
    if (user) {
      const headers = this.httpOptions(null, user.user_id, user.token);
      return this.http.get<Promo[]>(`${this.url}promo`, {headers, responseType: 'json'})
        .pipe(tap((data) => this.repeatPromo.next(data)), catchError(this.handleError));
    } else {
      return Observable.throw('error');
    }
  }

  private handleError(error: any) {
    const errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    return Observable.throw(errMsg);
  }
}
