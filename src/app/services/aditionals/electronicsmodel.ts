export class Electronicsmodel {
  currentModel: number;
  titleaditional: string;
}

export interface MarkElectronics {
  id: any;
  mark_electronics: any;
}

export interface ModelElectronics {
  id: any;
  model_electronics: any;
}

export interface TypeElectronics {
  id: number;
  type_electronics: string;
}

export interface CategoryElectronics {
  id: number;
  tbl_type_elecronics_id: number;
  category_electronics: string;
}

export interface BrandElectronics {
  id: number;
  tbl_category_elecronics: number;
  brend_electronics: string;
}

export interface ModellElectronics {
  id: number;
  tbl_brand_electronics_id: number;
  model_electronics: string;
}

export interface ModelByMarkElectronics {
  id: any;
  tbl_mark_electronics_id: any;
  model_electronics: string;
  tbl_type_auto_id: any;
}

export interface ElectronicsForm {
  query_type_electronics: number;
  query_category_electronics: number;
  query_electronics_brand: number;
  query_electronics_model: number;
  query_price_min: number;
  query_price_max: number;
  query_currency: number;
  query_address: string;
  query_city: number;
}

export class ElectronicsFormModel {
  constructor(
    // tslint:disable-next-line:variable-name
    public query_electronics_mark: number,
    // tslint:disable-next-line:variable-name
    public query_electronics_model: number,
    // tslint:disable-next-line:variable-name
    public query_price_min: number,
    // tslint:disable-next-line:variable-name
    public query_price_max: number,
    // tslint:disable-next-line:variable-name
    public query_currency: number,
  ) {
  }
}

export class ExtraAdditionalFieldModel {

  extraFields: extraFields;
  id: number;
  // tslint:disable-next-line:variable-name
  id_property_extra_fields: number;
  // tslint:disable-next-line:variable-name
  id_property_options: number;
  // tslint:disable-next-line:variable-name
  id_type_property: number;
}

// tslint:disable-next-line:class-name
export interface extraFields {
  id: number;
  is_from_to: number;
  name: string;
  type: string;
  valRia: string;
  tbl_name: string;
}


