import {Injectable, OnDestroy} from '@angular/core';
import {NavigationEnd, Router} from '@angular/router';
import {LoginService} from '../auth/login.service';
import {WebGameService} from '../web-game.service';
import {takeUntil} from 'rxjs/internal/operators/takeUntil';
import {Subject} from 'rxjs/internal/Subject';
import {filter} from 'rxjs/internal/operators/filter';
import {combineLatest} from 'rxjs/internal/operators/combineLatest';
import {timer} from 'rxjs/internal/observable/timer';
import { first } from 'rxjs/internal/operators/first';
// tslint:disable-next-line:ban-types
declare var ga: Function;

@Injectable()
export class GoogleAnalyticsService implements OnDestroy {
  destroyed$ = new Subject<void>();

  constructor(public router: Router, private loginService: LoginService, private webGameService: WebGameService) {
    this.router.events.subscribe(event => {

      try {
        if (typeof ga === 'function') {
          if (event instanceof NavigationEnd) {
            ga('set', 'page', event.urlAfterRedirects);
            ga('send', 'pageview');
          }
        }
      } catch (e) {
      }
    });


    /****** web game *****/
    this.router.events.pipe(takeUntil(this.destroyed$), filter(event => event instanceof NavigationEnd),
      combineLatest(this.loginService.isLogin(), this.webGameService.obsGameMap))
      .subscribe(([route, isLogin, gameMap]) => {
        const isset = this.detectGame(gameMap);
        let flagIsStart;
        if (isLogin) {
          if (isset) {
            flagIsStart = true;
            this.webGameService.getGame(isset)
              .pipe(takeUntil(this.destroyed$), first())
              .subscribe((d) => {
                if (d['status'] === 401 || d['message'] === 'empty') {
                  this.webGameService.timerIsStart(false);
                } else {
                  // this.webGameService.timerIsStart(true);
                  this.webGameService.addDataGame(d);
                }
              });
          } else {
            flagIsStart = false;
          }
        } else {
          flagIsStart = false;
        }
        this.webGameService.timerIsStart(flagIsStart);
      }, error => {
        this.webGameService.timerIsStart(false);
      });


    this.webGameService.obsGameMap.pipe(takeUntil(this.destroyed$)).pipe(takeUntil(this.destroyed$))
      .subscribe((gameMap) => {
        if (gameMap) {
        } else {
          this.webGameService.gameMap().pipe(takeUntil(this.destroyed$)).pipe(takeUntil(this.destroyed$))
            .subscribe(() => {
            }, error => {
            });
        }
      }, error => {
      });

  }


  private startTimer() {
    // @ts-ignore
    // this.timer = timer(0, 2000);
    return timer(this.timerResume);
  }

  private detectGame(gameMap) {
    if (gameMap) {
      // console.log(gameMap);
      const fullLinkHost = `${window.location.href}/`;
      const fullLinkHref = window.location.href;
      const isset = [fullLinkHost, fullLinkHref].map((item) => {
        if (gameMap.indexOf(item) !== -1) {
          return item;
        }
      }).filter(it => it).shift();
      return isset;
    }
    return false;
  }

  /**
   * Emit google analytics event
   * Fire event example:
   * this.emitEvent("testCategory", "testAction", "testLabel", 10);
   */
  public emitEvent(eventCategory: string,
                   eventAction: string,
                   eventLabel: string = null,
                   eventValue: number = null) {
    if (typeof ga === 'function') {
      ga('send', 'event', {
        eventCategory,
        eventLabel,
        eventAction,
        eventValue
      });
    }
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }
}
