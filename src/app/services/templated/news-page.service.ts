import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {Observable, of, Subject} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {catchError, map} from 'rxjs/operators';
import {httpOptions} from './baseOptions';
@Injectable()
export class NewsPageService {

  private httpOptions = httpOptions;
  private url = environment.api_url;
  private dataExtendData;
  private dataInnerdData;

  private subject = new Subject<any>();
  private subjectBack = new Subject<boolean>();

  sendDetail(data: any): void {
    this.subject.next(data);
  }

  sendBack(data: boolean): void {
    this.clearMessage();
    this.subjectBack.next(data);
  }
  getBack(): Observable<boolean> {
    return this.subjectBack.asObservable();
  }

  clearMessage(): void {
    this.subject.next();
  }

  getDetail(): Observable<any> {
    return this.subject.asObservable();
  }
  sendExtendData(data: any) {
    this.dataExtendData = [];
    this.dataExtendData = data;
  }

  sendDataInner(data: any) {
    this.dataInnerdData = [];
    this.dataInnerdData = data;
  }

  getDataInner(): Observable<any> {
    return of(this.dataInnerdData);
  }
  getDataExtend(): Observable<any> {
    return of(this.dataExtendData);
  }
  constructor(private http: HttpClient) {
  }

  getNews() {

    // const headers: any = new Headers();
    // headers.append('Client-Service', 'frontend-client');
    // headers.append('Auth-Key', 'sishik_rest_api');
    // tslint:disable-next-line:no-shadowed-variable
    const httpOptions = {
      headers: new HttpHeaders(this.httpOptions())
    };
    return this.http.get<any>(this.url + 'news', httpOptions)
      .pipe(
        map(value => value),
        catchError(this.handleError));
  }

  private extractData(res: Response) {
    const body = res.json();
    return body || [];
  }

  private handleError(error: any) {
    const errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg);
    return Observable.throw(errMsg);
  }
}
