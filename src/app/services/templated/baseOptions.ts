// tslint:disable-next-line:no-unused-expression
export function httpOptions(contentType = null , UserId = null, Authorization = null) {

  const httpHeaders = {
    'Content-Type': contentType,
    'User-Id': UserId,
    'Authorization': Authorization,
    'Client-Service': 'frontend-client',
    'Auth-Key': 'sishik_rest_api'
  }
  if (contentType === null) {
    delete httpHeaders['Content-Type'];
  }
  if (UserId === null) {
    delete httpHeaders['User-Id'];
  }
  if (UserId === null) {
    delete httpHeaders['User-Id'];
  }
  if (Authorization === null) {
    delete httpHeaders['Authorization'];
  }
  return httpHeaders;
}
