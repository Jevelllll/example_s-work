import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {httpOptions} from './baseOptions';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs';
import {catchError, map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
@Injectable()
export class InformationDataService {
  private url: any = environment.api_url;
  private httpOptions = httpOptions;
  constructor(private http: HttpClient) {
  }

  // tslint:disable-next-line:variable-name
  public get_i(inform_name): Observable<InfoAbRules[]> {
    // const headers: any = new Headers();
    // headers.append('Client-Service', 'frontend-client');
    // headers.append('Auth-Key', 'sishik_rest_api');
    // tslint:disable-next-line:no-shadowed-variable
    const httpOptions = {
      headers: new HttpHeaders(this.httpOptions())
    };
    return this.http.get<any>(this.url + 'inform/' + inform_name, httpOptions)
      .pipe(
        map(value => value),
        catchError(this.handleError));
  }

  private extractData(res: Response) {
    const body = res.json();
    return body || [];
  }

  private handleError(error: any) {
    const errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg);
    return Observable.throw(errMsg);
  }
}

export class InfoHelpM {
  // tslint:disable-next-line:variable-name
  constructor(public inform_id: number,
              // tslint:disable-next-line:variable-name
              public inform_name: string,
              // tslint:disable-next-line:variable-name
              public inform_title: string,
              // tslint:disable-next-line:variable-name
              public inform_description: string) {
  }
}

export class InfoAbRules {
  // tslint:disable-next-line:variable-name
  public inform_id: number;
  // tslint:disable-next-line:variable-name
  public inform_name: string;
  // tslint:disable-next-line:variable-name
  public inform_title: string;
  // tslint:disable-next-line:variable-name
  public inform_description: string;

}
