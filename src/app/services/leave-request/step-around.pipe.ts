import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'stepAround'
})
export class StepAroundPipe implements PipeTransform {

  transform(value: number, isRegistration: boolean): number {
    // tslint:disable-next-line:variable-name
    let current_namber = 0;
    switch (isRegistration)  {
        case true :
            if ( value === 0) {
                current_namber = 1;
            } else {
                current_namber = 2;
            }
            break;
        case false :
            if ( value === 0) {
                current_namber = 1;
            } else if (value === 1) {
                current_namber = 2;
            } else if (value === 2) {
                current_namber = 3;
            }else if (value === 3) {
              current_namber = 4;
            }
            break;
    }
    return current_namber;
  }

}
