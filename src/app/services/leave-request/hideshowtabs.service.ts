import {EventEmitter, Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {TariffModel} from '../../shared/tarifModel';

@Injectable()
export class HideshowtabsService {

    hide = new EventEmitter<boolean>();
    disabled = new EventEmitter<boolean>();

    private subject = new Subject<TariffModel>();
    constructor() { }
    addHideTariffDisplay(flagT: any) {
        this.subject.next(flagT);
    }
    getHideTariffDisplay(): Observable<any> {
        return this.subject.asObservable();
    }
    change(increased: any) {
        this.hide.emit(increased);
    }
    changeDisabled(increased: any) {
        this.disabled.emit(increased);
    }
}
