import {EventEmitter, Injectable} from '@angular/core';
import {Stepsmodel} from '../../shared/leave-request/leave/stepsmodel';


@Injectable()
export class StepsService {

    nextSteps = new EventEmitter<Stepsmodel>();
    DATASTEPS: Stepsmodel[] = [
        {
            title: 'Шаг',
            text: 'Выбрать категорию',
            active: false,
        },
        {
            title: 'Шаг',
            text: 'Оставить контактные данные',
            active: false,
        },
        {
            title: 'Шаг',
            text: 'Оплата заявки',
            active: false,
        }
    ];
    setEventS(numb) {
      // tslint:disable-next-line:prefer-for-of
        for (let i = 0; i < this.getWorks().length; i++) {
            this.getWorks()[i].active = false;
        }
        this.getWorks()[numb].active = true;
    }
    getWorks() {
        return this.DATASTEPS;
    }
    constructor() { }

}
