import {NgModule} from '@angular/core';
import {DescriptionComponent} from '../../../leave-request/container/quest/description/description.component';
import {CommonModule} from '@angular/common';

@NgModule({
  declarations: [DescriptionComponent],
  exports: [DescriptionComponent],
  imports: [CommonModule]
})
export class QuestDescModule {
}
