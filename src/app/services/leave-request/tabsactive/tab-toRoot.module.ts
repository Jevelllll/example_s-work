import {ModuleWithProviders, NgModule} from '@angular/core';
import {TabService} from './tab.service';
import {HideshowtabsService} from '../hideshowtabs.service';

@NgModule({})
export class TabToRootModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: TabToRootModule,
      providers: [TabService, HideshowtabsService]
    }
  }
}
