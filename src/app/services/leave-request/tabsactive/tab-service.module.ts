import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TabService} from './tab.service';
import {HideshowtabsService} from '../hideshowtabs.service';


@NgModule({
  imports: [CommonModule],
  providers: [TabService, HideshowtabsService]
})
export class TabServiceModule {

}
