import {EventEmitter, Injectable} from '@angular/core';
import {BehaviorSubject, Observable, of, Subject} from 'rxjs';
import {TabsClass} from '../../../shared/tabs.class';

@Injectable()
export class TabService {

  // tslint:disable-next-line:variable-name
  private _bFromHeroForm: BehaviorSubject<any>;
  // tslint:disable-next-line:variable-name
  public _obsFromHeroForm: Observable<any> = new Observable<any>();

  nextTabs = new EventEmitter<TabsClass>();

  dataTestList = [];

  private tabHide: Subject<boolean> = new Subject<boolean>();
  TABS: TabsClass[] = [{
    title: 'Auto',
    active: true,
  }];

  tCL(): Observable<any> {
    return of(this.dataTestList);
  }

  setCloseTab() {
    this.dataTestList.push(false);
    this.tabHide.next(true);
  }

  getCloseTab(): Observable<any> {
    return this.tabHide.asObservable();
  }

  updateTabs(tabs: TabsClass) {
    this.TABS = [];
    this.TABS.push(tabs);
  }

  getTabs(): Observable<TabsClass[]> {
    return of(this.TABS);
  }

  public transitionHeroFrom(event): void {
    this._bFromHeroForm.next(event);
  }

  constructor() {
    this._bFromHeroForm = new BehaviorSubject<any>(null);
    this._obsFromHeroForm = this._bFromHeroForm.asObservable();
  }

}
