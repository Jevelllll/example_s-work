import {EventEmitter, Injectable, Output, Renderer2} from '@angular/core';

@Injectable()
export class PutdisabledService {

    hide = new EventEmitter<boolean>();
    disabled = new EventEmitter<boolean>();
    constructor() {}
    change(increased: any) {
        this.hide.emit(increased);
    }
    changeDisabled(increased: any) {
        this.disabled.emit(increased);
    }

}
