import {Injectable, OnDestroy} from '@angular/core';
import {environment} from '../../../environments/environment';
import {BehaviorSubject, Observable, Subject} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {catchError, first, map, takeUntil, tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ChatRoomService implements OnDestroy {
  destroyed$ = new Subject<void>();
  private url = environment.cities_url;
  // tslint:disable-next-line:variable-name
  private api_url = `${environment.api_url}`;

  // tslint:disable-next-line:variable-name
  private __bhChatUpdate: BehaviorSubject<any>;
  // tslint:disable-next-line:variable-name
  public _obsChatUpdate: Observable<any> = new Observable<any>();

  private chatRoomState: BehaviorSubject<boolean>;

  constructor(private http: HttpClient) {
    this.chatRoomState = new BehaviorSubject<boolean>(false);

    this.__bhChatUpdate = new BehaviorSubject<any>(null);
    this._obsChatUpdate = this.__bhChatUpdate.asObservable();
  }

  public setChatRoomState(state) {
    this.chatRoomState.next(state);
  }

  public getChatRoomState() {
    return this.chatRoomState.asObservable();
  }

  sendMsh($modelMsg) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        'Client-Service': 'frontend-client',
        'Auth-Key': '???'
      })
    };
    return this.http.post<any>(`${this.api_url}chat/upload`, $modelMsg).pipe(
      catchError(this.handleError));

  }


  /*******************отобразить кол-во непросмотренных оповещений**********************************************/
  viewNotRead(key): Observable<any> {
    return Observable.create((observer) => {
      this._obsChatUpdate
        .pipe(
          takeUntil(this.destroyed$),
          first())
        .subscribe(result => {
          if (result) {
            observer.next(result);
          } else {
            this.updateViewNotRead(observer, key);
          }
        }, err => observer.error(false));
    });

  }

  updateViewNotRead(observer, key) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Client-Service': 'frontend-client',
      'Auth-Key': '???'
    });
    return this.http.post(`${this.api_url}chat/get-chat`, 'key=' + key, {headers, responseType: 'json'}).pipe(
      first(),
      takeUntil(this.destroyed$),
      tap(val => {
        this.__bhChatUpdate.next(val);
        return val;
      })).subscribe(result => observer.next(result), err => observer.error(false));
  }

  timeUpdate(key): Observable<any> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Client-Service': 'frontend-client',
      'Auth-Key': '???'
    });
    // tslint:disable-next-line:no-shadowed-variable
    return Observable.create((observable) => {
      this.http.post(`${this.api_url}chat/get-chat`, 'key=' + key, {headers, responseType: 'json'})
        .pipe(takeUntil(this.destroyed$), map(val => {
          this.__bhChatUpdate.next(val);
          return val;
        }), first()).subscribe(result => observable.next(result),
        error => observable.error(false));
    });

  }


  private handleError(error: any) {
    const errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }
}
