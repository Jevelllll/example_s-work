import {ModuleWithProviders, NgModule} from '@angular/core';
import {HintsService} from './hints.service';

@NgModule()
export class HintsModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: HintsModule,
      providers: [HintsService]
    }
  }
}
