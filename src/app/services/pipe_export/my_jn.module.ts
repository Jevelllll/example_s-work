import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CarouselModule} from 'angular-bootstrap-md';


@NgModule({
  imports: [
    CommonModule,
    CarouselModule
  ],
    // declarations: [
    //     TariffComponent
    // ],
    // exports: [
    //     TariffComponent
    // ],
    // entryComponents: [
    //     TariffComponent,
    // ],
})

export class MyJnModule {}
