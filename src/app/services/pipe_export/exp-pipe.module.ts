import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MapToKeysPipe} from './map-to-keys.pipe';
import {NgMathPipesModule} from 'ngx-pipes';
import {StepAroundPipe} from '../leave-request/step-around.pipe';
import {GroupbypipePipe} from '../directivs/groupbypipe';
import {NosanitizePipe} from '../tariff/nosanitize.pipe';


@NgModule({
    imports: [
        CommonModule,
        NgMathPipesModule
    ],
  exports: [
    NosanitizePipe,
    GroupbypipePipe
  ],
    declarations: [
        NosanitizePipe,
        StepAroundPipe,
        GroupbypipePipe,
        MapToKeysPipe
    ]
})

export class ExpPipeModule {
}
