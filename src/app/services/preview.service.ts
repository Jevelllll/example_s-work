import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/internal/Observable';
import {environment} from '../../environments/environment';
import {catchError} from 'rxjs/internal/operators/catchError';
import {shareReplay} from 'rxjs/internal/operators/shareReplay';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PreviewService {

  private url: any = environment.api_url;

  constructor(private http: HttpClient) {
  }

  getPreview(typeName: string): Observable<string> {
    return this.http.get<string>(`${this.url}${typeName}/preview`).pipe(
      shareReplay({bufferSize: 1, refCount: true}),
      catchError(this.handleError));
  }

  private handleError(error: HttpErrorResponse) {
    // TODO: seems we cannot use messageService from here...
    const errMsg = (error.message) ? error.message : 'Server error';
    if (error.status === 401) {
      window.location.href = '/';
    }
    return Observable.throw(errMsg);
  }
}
