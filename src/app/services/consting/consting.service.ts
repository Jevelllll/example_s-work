import {Injectable} from '@angular/core';
import {TariffModel} from '../../shared/tarifModel';
import {Observable, Subject, BehaviorSubject, throwError} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {LegalCategory} from '../priority/resModel';
import {environment} from '../../../environments/environment';
import {catchError} from 'rxjs/internal/operators/catchError';
import {map} from 'rxjs/internal/operators/map';
import {UserEditInterface} from '../auth/loginInterface';

function Headers() {
  return new HttpHeaders({
    'Content-Type': 'application/x-www-form-urlencoded',
    'Client-Service': 'frontend-client',
    'Auth-Key': '???'
  });
}

@Injectable({
  providedIn: 'root'
})
export class ConstingService {
  private url: any = environment.api_url;
  private behUpon: BehaviorSubject<boolean>;
  public obsUpon: Observable<boolean> = new Observable<boolean>();

  private behUponInquirer: BehaviorSubject<boolean>;
  public obsUponInquirer: Observable<boolean> = new Observable<boolean>();

  private behFormFields: BehaviorSubject<any>;
  public obsFormFields: Observable<any> = new Observable<any>();

  private behResultsStore: BehaviorSubject<any>;
  public obsResultsStore: Observable<any> = new Observable<any>();

  private behSkipResultsStore: BehaviorSubject<boolean>;
  public obsSkipResultsStore: Observable<boolean> = new Observable<boolean>();

  private subject = new Subject<TariffModel>();

  constructor(private http: HttpClient) {
    this.behUpon = new BehaviorSubject<boolean>(false);
    this.obsUpon = this.behUpon.asObservable();
    this.behUponInquirer = new BehaviorSubject<boolean>(false);
    this.obsUponInquirer = this.behUponInquirer.asObservable();

    this.behFormFields = new BehaviorSubject<any>(null);
    this.obsFormFields = this.behFormFields.asObservable();

    this.behResultsStore = new BehaviorSubject<any>(null);
    this.obsResultsStore = this.behResultsStore.asObservable();

    this.behSkipResultsStore = new BehaviorSubject<any>(false);
    this.obsSkipResultsStore = this.behSkipResultsStore.asObservable();
  }

  addResultsStore(data: any): void {
    this.behResultsStore.next(data);
  }
  skipResultStore(data: boolean): void {
    this.behSkipResultsStore.next(data);
  }
  /************c add skip**********************/
  // addskip
  // tslint:disable-next-line:variable-name
  public customerQuestAddskip(user_id: number, tariff_id: number, query_id: number): Observable<any> {
    const bodyHttp = `user_id=${user_id}&tariff_id=${tariff_id}&query_id=${query_id}`;
    const preHeaders = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Client-Service': 'frontend-client',
      'Auth-Key': '???'
    });
    return this.http.post<any>(`${this.url}customer-quest/addskip`, bodyHttp, {headers: preHeaders, responseType: 'json'})
      .pipe(catchError(this.handleError));
  }
  /************customer-questadd***************/
  // tslint:disable-next-line:variable-name
  public customerQuestAdd(user_id: number, data: string, tariff_id: number, query_id: number): Observable<any> {
    const bodyHttp = `user_id=${user_id}&data=${data}&tariff_id=${tariff_id}&query_id=${query_id}`;
    const preHeaders = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Client-Service': 'frontend-client',
      'Auth-Key': '???'
    });
    return this.http.post<any>(`${this.url}customer-quest/add`, bodyHttp, {headers: preHeaders, responseType: 'json'})
      .pipe(catchError(this.handleError));
  }

  getFormUponFields(): Observable<any> {
    const preHeaders = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Client-Service': 'frontend-client',
      'Auth-Key': '???'
    });
    let link = `${this.url}customer-quest`;
    let user;
    try {
      user = JSON.parse(localStorage.getItem('currentUser'));
      link = `${link}/${user.user_id}`;
    } catch (e) {
    }
    return this.http.get<any>(link,
      {headers: preHeaders, responseType: 'json'}).pipe(map(data => {
      this.behFormFields.next(data);
      return data;
    }), catchError(this.handleError));
  }

  onUpon(data: boolean): void {
    this.behUpon.next(data);
  }

  onUponInquirer(data: boolean): void {
    this.behUpon.next(data);
  }

  addTariff(tariffObj: TariffModel) {
    this.subject.next(tariffObj);
  }

  getTarrif(): Observable<TariffModel> {
    return this.subject.asObservable();
  }

  private handleError(error: any) {
    const errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    return throwError(errMsg);
  }
}
