import {Pipe, PipeTransform} from '@angular/core';
import {urlRusLat} from '../shared/helper-container-leave';
import {Subject} from 'rxjs/internal/Subject';

@Pipe({
  name: 'leaveUnsetSkipServices'
})
export class LeaveUnsetSkipServicesPipe implements PipeTransform {

  destroyed$ = new Subject<void>();

  transform(value: any, type: string, dataUnsetPattern, item): any {
    const preD = [];
    if (item) {
      if (dataUnsetPattern) {
        if (Array.isArray(value)) {
          value.forEach((val) => {
            const skField: any = urlRusLat(val[type]);

            try {
              if (dataUnsetPattern[skField]) {
              } else {
                preD.push(val);
              }
            } catch (e) {
              preD.push(val);
            }
          });
          const filter = preD.filter(it => it.id === item.value);
          if (filter.length === 0) {
            item.reset();
          }
        }
        return preD;
      }
    }
    return value;
  }
}
