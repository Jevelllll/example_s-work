import {EventEmitter, Injectable, OnDestroy} from '@angular/core';
import {environment} from '../../../environments/environment';
import {TariffModel} from '../../shared/tarifModel';
import {BehaviorSubject, Observable, of, Subject, throwError} from 'rxjs';
import {catchError, map, shareReplay, take, takeUntil} from 'rxjs/operators';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {ToastrService} from 'ngx-toastr';
import {isPrimitive} from 'util';

@Injectable({
  providedIn: 'root'
})
export class TariffService implements OnDestroy {
  destroyed$ = new Subject<void>();
  private url = environment.api_url;
  select = new EventEmitter<boolean>();
  private popular: any;
  POPULAR: TariffModel[] = [];
  POPULARNEW: TariffModel[] = [];

  private tariffTypeListenerBehSubj: BehaviorSubject<any>;
  public getTariffTypeListener: Observable<any> = new Observable<any>();

  setPopulars() {
    // tslint:disable-next-line:variable-name
    this.getTypeTariff()
      .pipe(takeUntil(this.destroyed$))
      // tslint:disable-next-line:variable-name
      .subscribe(type_tariff => {

        for (let i = 0; i < type_tariff.tariff_type.length; i++) {


          this.getTariff(type_tariff.tariff_type[i].id).pipe(takeUntil(this.destroyed$)).subscribe(x => {
            // tslint:disable-next-line:variable-name
            let obj_select;
            if (i === 1) {

              obj_select = Object.assign({}, x.tariff[0], {select: true});
            } else {
              obj_select = x.tariff[i];
            }
            this.POPULAR.push(Object.assign({}, obj_select,
              {typeTitle: type_tariff.tariff_type[i].type_tariff}));
            // this.POPULAR.push(Object.assign(new Object(), x.tariff[0],
            //     {'typeTitle': type_tariff.tariff_type[i].type_tariff}));
          });
        }
      }, error1 => {
        this.toster.error('ошибка запроса', '', {timeOut: 100000});
      });
  }

  setAllPopularsTarifPlain() {
    this.getTypeTariff()
      .pipe(takeUntil(this.destroyed$))
      .subscribe(typeTariff => {
        if (Array.isArray(typeTariff['tariff_type'])) {
          typeTariff['tariff_type'].map(resultType => {
            this.getTariff(resultType.id)
              .pipe(takeUntil(this.destroyed$))
              .subscribe(resGetT => {
                if (resGetT.status === 200) {
                  let tariff = resGetT.tariff;
                  if (Array.isArray(tariff)) {
                    let index = 0;
                    tariff = tariff.map(tMap => {
                      let obj = {};
                      if (index === 1) {
                        obj = Object.assign({}, tMap, {select: true});
                      } else {
                        obj = tMap;
                      }
                      index++;
                      return obj;
                    });
                    this.POPULARNEW.push(Object.assign(tariff, {typeTitle: resultType.type_tariff}));
                  }
                }
              });
          });

        }
      });
  }

  getAllPopularsTarifPlain(): Observable<any> {
    return of(this.POPULARNEW);
  }


  getPopulars(): Observable<TariffModel[]> {
    return of(this.POPULAR);

  }

  // tslint:disable-next-line:variable-name
  getTariff(type_id: number): Observable<any> {
    let link = `${this.url}tariff/${type_id}`;
    let user;
    try {
      user = JSON.parse(localStorage.getItem('currentUser'));
      link = `${link}/${user.user_id}`;
    } catch (e) {
    }
    return this.http.get(link)
      .pipe(
        shareReplay({bufferSize: 1, refCount: true}),
        catchError(this.handleError));
  }

  // tariff_type: Array(3)
  // 0: {id: "1", type_tariff: "Auto"}
  // 1: {id: "2", type_tariff: "Electronics"}
  // 2: {id: "3", type_tariff: "Property"}
  getTypeTariff(): Observable<any> {
    return this.http.get(this.url + 'tariff/tarifftype')
      .pipe(
        take(1),
        shareReplay({bufferSize: 1, refCount: true}),
        takeUntil(this.destroyed$),
        map(value => {
          const newVal = {tariff_type: []};
          if (Array.isArray(value['tariff_type'])) {
            value['tariff_type'].map(item => {
              if (item['type_tariff'] !== 'Electronics') {
                newVal.tariff_type.push(item);
              }
            });
          }
          this.tariffTypeListenerBehSubj.next(newVal);
          return newVal;
        }),
        catchError(this.handleError));
  }

  constructor(private http: HttpClient, private toster: ToastrService) {
    this.tariffTypeListenerBehSubj = new BehaviorSubject<any>(null);
    this.getTariffTypeListener = this.tariffTypeListenerBehSubj.asObservable();
    // this.setPopulars();
    // this.setAllPopularsTarifPlain();
    // this.setPopulars();
  }

  // tslint:disable-next-line:variable-name
  // tslint:disable-next-line:variable-name
  postLiqPay(sum_day_query_tariff: number, order_id: any) {

    const preHeaders = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      Accept: 'application/json, text/plain, */*',
    });

    return this.http.post<string>(this.url + 'user/liqpay',
      'sum_day_query_tariff=' + sum_day_query_tariff +
      '&order_id=' + order_id, {headers: preHeaders, responseType: 'text' as 'json'})
      .pipe(
        map(res => res),
        catchError(this.handleError));
  }

  // tslint:disable-next-line:variable-name
  postActivateLiqPay(sum_day_query_tariff: number, order_id: any, query_start_tariff: any, query_end_tariff) {

    const preBody = 'sum_day_query_tariff='
      + sum_day_query_tariff
      + '&order_id=' + order_id + '&query_tariff_start='
      + this.getUtcUnixTime(query_start_tariff)
      + '&query_tariff_end='
      + this.getUtcUnixTime(query_end_tariff)
      + '&activated=activated';

    const preHeaders = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      Accept: 'application/json, text/plain, */*',
    });
    return this.http.post<any>(this.url + 'user/liqpay',
      preBody, {headers: preHeaders, responseType: 'text' as 'json'})
      .pipe(
        map(res => res),
        catchError(this.handleError));
  }

  private extractData(res: any) {
    const body = res.json();
    return body || [];
  }

  private objectToParams(object) {
    return Object.keys(object).map((value) => {
      const objectValue = isPrimitive(object[value]) ? object[value] : JSON.stringify(object[value]);
      return `${value}=${objectValue}`;
    }).join('&');
  }

  public getUtcUnixTime(date) {
    return (+new Date(date)
      - new Date(date).getTimezoneOffset() * 60 * 1000)
      / 1000;
  }

  /*
*
* @param error
*/
  // tslint:disable-next-line:adjacent-overload-signatures
  // private handleError(error: HttpErrorResponse) {
  //   // TODO: seems we cannot use messageService from here...
  //   const errMsg = (error.message) ? error.message : 'Server error';
  //   if (error.status === 401) {
  //     window.location.href = '/';
  //   }
  //   return Observable.throw(errMsg);
  // }
  /*
   *
   * @param error
   */
  private handleError(error: HttpErrorResponse | any) {
    // TODO: seems we cannot use messageService from here...
    const errMsg = (error.message) ? error.message : 'Server error';
    if (error.status === 401) {
      window.location.href = '/';
    }
    return throwError(errMsg);
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }
}
