export class TariffModel {
    // title: string;
    // countSmsDay: number;
    // priceDay: number;
    // priceMonth: number;
    // select: boolean;
    // // discount: number;
    id: number;
  // tslint:disable-next-line:variable-name
    count_sms: number;
    discount: number;
  // tslint:disable-next-line:variable-name
    sum_day: number;
  // tslint:disable-next-line:variable-name
    sum_month: number;
    select: boolean;
  // tslint:disable-next-line:variable-name
    tariff_name: string;
    typeTitle: string;
    // discount: number;
}
