import {Injectable, OnDestroy} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {catchError} from 'rxjs/internal/operators/catchError';
import {BehaviorSubject, Observable, Subject, throwError} from 'rxjs';
import {tap} from 'rxjs/internal/operators/tap';
import {delay} from 'rxjs/internal/operators/delay';
import {ToastrService} from 'ngx-toastr';
import {takeUntil} from 'rxjs/internal/operators/takeUntil';
import {LegalCategory} from './priority/resModel';
import {map} from 'rxjs/internal/operators/map';
import {clearObject} from '../shared/helper-container-leave';

function Headers() {
  return new HttpHeaders({
    'Content-Type': 'application/x-www-form-urlencoded',
    'Client-Service': 'frontend-client',
    'Auth-Key': '???'
  });
}

@Injectable({
  providedIn: 'root'
})
export class WebGameService implements OnDestroy {
  destroyed$ = new Subject<void>();
  private url: any = environment.api_url;
  private user: any;

  public obsGameMap: Observable<any> = new Observable<any>();
  private behGameMap: BehaviorSubject<any>;

  public obsDataGame: Observable<any> = new Observable<any>();
  private behDataGame: BehaviorSubject<any>;

  public obsIsStartGame: Observable<boolean> = new Observable<boolean>();
  private behIsStartGame: BehaviorSubject<boolean>;

  public obsWebPoint: Observable<WebPoint> = new Observable<WebPoint>();
  private behWebPoint: BehaviorSubject<WebPoint>;

  public obsWbProcess: Observable<LegalCategory[]> = new Observable<LegalCategory[]>();
  private behWbProcess: BehaviorSubject<LegalCategory[]>;

  constructor(private http: HttpClient, private toaster: ToastrService) {
    this.behGameMap = new BehaviorSubject<any>(false);
    this.obsGameMap = this.behGameMap.asObservable();

    this.behDataGame = new BehaviorSubject<any>(false);
    this.obsDataGame = this.behDataGame.asObservable();

    this.behIsStartGame = new BehaviorSubject<boolean>(false);
    this.obsIsStartGame = this.behIsStartGame.asObservable();

    this.behWebPoint = new BehaviorSubject<WebPoint>(null);
    this.obsWebPoint = this.behWebPoint.asObservable();

    const emptyWpProcess = new LegalCategory();
    emptyWpProcess.id = null;
    emptyWpProcess.name = '';
    this.behWbProcess = new BehaviorSubject<LegalCategory[]>([emptyWpProcess]);
    this.obsWbProcess = this.behWbProcess.asObservable();

    try {
      this.user = JSON.parse(localStorage.getItem('currentUser'));
    } catch ($e) {

    }
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }

  addDataGame(data: any): void {
    this.behDataGame.next(data);
  }

  timerIsStart(data: boolean): void {
    this.behIsStartGame.next(data);
  }

  getGame(link) {
    return new Observable((obs) => {
      try {
        this.user = JSON.parse(localStorage.getItem('currentUser'));
      } catch ($e) {
      }
      if (this.user) {
        const preHeaders = new HttpHeaders({
          'Content-Type': 'application/x-www-form-urlencoded',
          'User-Id': this.user.user_id,
          Authorization: this.user.token,
          'Client-Service': 'frontend-client',
          'Auth-Key': '???',
          Link: link
        });
        const us = 'users_id=' + this.user.user_id;
        return this.http.post<any>(`${this.url}web-game/game`, us,
          {
            headers: preHeaders
          }).subscribe((data) => obs.next(data), error => (data) => obs.next(null));
      } else {
        obs.next(null);
      }
    });
  }

  wgProcess(): Observable<LegalCategory[]> {
    const headers = Headers();
    return this.http.get<LegalCategory[]>(`${this.url}wg-process`,
      {headers, responseType: 'json'}).pipe(map(data => {
      this.behWbProcess.next(data);
      return data;
    }), catchError(this.handleError));
  }

  getGProcess(): Observable<LegalCategory[]> {
    let dataNext: Observable<any> = new Observable<any>();
    this.obsWbProcess
      .pipe(takeUntil(this.destroyed$))
      .subscribe(valGearBox => {
        if (valGearBox[0].id) {
          const process: BehaviorSubject<any> = new BehaviorSubject(valGearBox);
          dataNext = process.asObservable();
        } else {
          dataNext = this.wgProcess();
        }
      }, error1 => {
      });
    return dataNext;
  }

  webWithdrawal(data): Observable<any> {
    if (this.user) {
      const datas: any = clearObject(data);
      const preHeaders = new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        Accept: 'application/json, text/plain, */*',
        'User-Id': this.user.user_id,
        Authorization: this.user.token,
        'Client-Service': 'frontend-client',
        'Auth-Key': '???'
      });
      const searchParams = Object.keys(datas).map((key) => {
        return encodeURIComponent(key) + '=' + encodeURIComponent(datas[key]);
      }).join('&');
      return this.http.post<any>(`${this.url}web-withdrawal`, searchParams,
        {
          headers: preHeaders,
          responseType: 'json'
          // tslint:disable-next-line:no-shadowed-variable
        }).pipe(tap((dataSEnd) => this.behWebPoint.next(dataSEnd)), catchError(this.handleError));
    } else {
      return throwError('error');
    }
  }

  notifyGame($linkId) {
    if (this.user) {
      const preHeaders = new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        Accept: 'application/json, text/plain, */*',
        'User-Id': this.user.user_id,
        Authorization: this.user.token,
        'Link-Id': $linkId.toString(),
        'Client-Service': 'frontend-client',
        'Auth-Key': '???'
      });
      const us = 'users_id=' + this.user.user_id;
      return this.http.post<string>(`${this.url}web-game/notify`, us,
        {
          headers: preHeaders,
        }).pipe(tap((item) => {
        if (item['status'] !== 200) {
          if (item['link']['count_game'] !== 0) {
            this.toaster.info(`+${item['link']['amount_one_game']} балла`, 'Игра', {timeOut: 800});
          } else {
            this.toaster.info(`+${item['link']['amount_one_game']} балла`, 'Игра', {timeOut: 800});
            this.toaster.success(`На сегодня всё, встретимся завтра`, 'Игра', {timeOut: 17000});
          }
        }
      }), delay(1800), catchError(this.handleError));
    } else {
      return throwError('error');
    }

  }

  gameMap(): Observable<any> {
    if (this.user) {
      const preHeaders = new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        'Client-Service': 'frontend-client',
        'Auth-Key': '???'
      });
      const us = '';
      return this.http.post<any>(`${this.url}web-game/game-map`, us,
        {
          headers: preHeaders,
        }).pipe(tap(data => this.behGameMap.next(data)), catchError(this.handleError));
    } else {
      return throwError('error');
    }
  }

  gamePoint(): Observable<WebPoint> {
    if (this.user) {
      const preHeaders = new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        Accept: 'application/json, text/plain, */*',
        'User-Id': this.user.user_id,
        Authorization: this.user.token,
        'Client-Service': 'frontend-client',
        'Auth-Key': '???'
      });
      return this.http.get<WebPoint>(`${this.url}web-point`, {headers: preHeaders, responseType: 'json'}).pipe(tap((data) => {
        this.behWebPoint.next(data);
      }));
    } else {
      return throwError('error');
    }
  }

  private handleError(error: any) {
    const errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    return throwError(errMsg);
  }
}

export interface WebPoint {
  game_points: string;
}
