export interface AmountIntervace {
  data: string;
}

export interface Balance {
  id: number;
  user_id: number;
  date_create: string;
  date_update: string;
  balance: number;

}

export interface History {
  service: string;
  date_create: string;
  status: string;
  id: number;
  currency: string;
  amount: number;
}

export interface Transactions {
  id: number;
  date_create: string;
  amount: number;
  order: string;
}
