import {Injectable, OnDestroy} from '@angular/core';
import {BehaviorSubject, Observable, Subject, throwError} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {httpOptions} from '../priority/baseOptions';
import {catchError, delay, first, map, takeUntil, tap} from 'rxjs/operators';
import {Balance, Transactions} from './amount-intervace';

@Injectable({
  providedIn: 'root'
})
export class AmountService implements OnDestroy{
  destroyed$ = new Subject<void>();
  private balanceBh: BehaviorSubject<Balance>;
  public balanceObs: Observable<Balance> = new Observable<Balance>();

  private historyPayBh: BehaviorSubject<any>;
  public historyPayObs: Observable<any> = new Observable<any>();

  private historyTransactionsPayBh: BehaviorSubject<any>;
  public historyTransactionsPayObs: Observable<any> = new Observable<any>();

  constructor(private http: HttpClient) {

    this.historyPayBh = new BehaviorSubject<any>(null);
    this.historyPayObs = this.historyPayBh.asObservable();

    this.historyTransactionsPayBh = new BehaviorSubject<any>(null);
    this.historyTransactionsPayObs = this.historyTransactionsPayBh.asObservable();

    this.servicesListBH = new BehaviorSubject(null);
    this.serviceListObs = this.servicesListBH.asObservable();

    this.balanceBh = new BehaviorSubject<Balance>(null);
    this.balanceObs = this.balanceBh.asObservable();


  }

  private user: any = {user_id: '', token: ''};
  private url: any = environment.api_url;
  private headers: any;

  private servicesListBH: BehaviorSubject<any>;
  public serviceListObs: Observable<any> = new Observable<any>();


  private static handleError(error: any) {
    const errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    return throwError(errMsg);
  }

  /******************список доступных сервисов пример LiqPay*******************************/
  public getServiceList(): Observable<any> {
    this.extracted();
    return this.http
      .get(`${this.url}wallet/services`, {headers: httpOptions(null, this.user.user_id, this.user.token), responseType: 'json'})
      .pipe(tap(data => {
        this.servicesListBH.next(data);
      }), first(), catchError(AmountService.handleError));
  }

  /******************получить контент для оплаты*******************************************/
  public newPayContent(data: any): Observable<string> {
    const searchParams = Object.keys(data).map((key) => {
      return encodeURIComponent(key) + '=' + encodeURIComponent(data[key]);
    }).join('&');

    const preHeaders = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      Accept: 'application/json, text/plain, */*',
      'User-Id': this.user.user_id,
      Authorization: this.user.token,
      'Client-Service': 'frontend-client',
      'Auth-Key': '???'
    });

    return this.http.post<string>(`${this.url}wallet/new-pay`, searchParams,
      {
        headers: preHeaders,
      }).pipe(delay(400), map(value => value), catchError(AmountService.handleError));

  }

  /***************получить текущий баланс*******************************************************/
  public getCurrentBalance(): Observable<Balance> {
    this.extracted();
    if (this.user) {
      const headers = new HttpHeaders({
        'User-Id': this.user.user_id,
        Authorization: this.user.token,
        'Client-Service': 'frontend-client',
        'Auth-Key': '???'
      });
      if (this.user.user_id) {
        return this.http.get<Balance>(`${this.url}wallet/view-balance`, {headers})
          .pipe(
            catchError(AmountService.handleError),
            map(val => {
              this.balanceBh.next(val);
              return val;
            }));
      }
    } else {
      return Observable.create(false);
    }
  }

  private extracted() {
    try {
      this.user = JSON.parse(localStorage.getItem('currentUser'));
    } catch (e) {
    }

  }

  /*******************оплатить через кошелек*************************************/
  public newWalletPay(orderId) {

    this.extracted();


    if (this.user.user_id) {
      const searchParams = Object.keys(orderId).map((key) => {
        return encodeURIComponent(key) + '=' + encodeURIComponent(orderId[key]);
      }).join('&');

      const preHeaders = new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        Accept: 'application/json, text/plain, */*',
        'User-Id': `${this.user.user_id.toString()}.`,
        Authorization: this.user.token,
        'Client-Service': 'frontend-client',
        'Auth-Key': '???'
      });
      return this.http.post<string>(`${this.url}wallet/pay-now`, searchParams,
        {
          headers: preHeaders,
        }).pipe(delay(400), map(value => {
        this.getHistoryPay()
          .pipe(takeUntil(this.destroyed$))
          .subscribe(() => {
          }, error1 => {
            // console.log(error1);
          });
        this.getHistoryTransactionsPay()
          .pipe(takeUntil(this.destroyed$))
          .subscribe(() => {
          }, error1 => {
            // console.log(error1);
          });
        return value;
      }), catchError(AmountService.handleError));
    } else {
      return Observable.create((observable) => observable.next(false));
    }
    // wallet/pay-now
  }

  /****************история платажей************************************************************/
  public getHistoryPay(): Observable<any> {
    this.extracted();
    try {
      const headers = new HttpHeaders({
        'User-Id': this.user.user_id,
        Authorization: this.user.token,
        'Client-Service': 'frontend-client',
        'Auth-Key': '???'
      });
      return this.http.get<Balance>(`${this.url}wallet/history`, {headers})
        .pipe(
          catchError(AmountService.handleError),
          map(val => {
            this.historyPayBh.next(val);
            return val;
          }));
    } catch (e) {

    }
  }

  /****************история транзакций************************************************************/
  public getHistoryTransactionsPay(): Observable<any> {
    this.extracted();
    try {
      const headers = new HttpHeaders({
        'User-Id': this.user.user_id,
        Authorization: this.user.token,
        'Client-Service': 'frontend-client',
        'Auth-Key': '???'
      });
      return this.http.get<Transactions>(`${this.url}wallet/transactions`, {headers})
        .pipe(
          catchError(AmountService.handleError),
          map(val => {
            this.historyTransactionsPayBh.next(val);
            return val;
          }));
    } catch (e) {

    }
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }
}
