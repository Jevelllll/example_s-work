import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable, Subject} from 'rxjs';
import {environment} from '../../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {httpOptions} from '../priority/baseOptions';
import {catchError, map} from 'rxjs/operators';

@Injectable()
export class ModalDataService {

  private sDataModel: any = {};

  modelChange: Subject<any> = new Subject();

  /***********новый метод модалки ()*******************/
  private matModalClose: Subject<any> = new Subject<any>();

  private httpOptions = httpOptions;
  private url: any = environment.api_url;
  private subject = new Subject<any>();
  private dataActive;
  private dataExtendData;

  public confirmState: Subject<boolean> = new Subject<boolean>();

  constructor(private http: HttpClient) {
  }

  setmodelChange(data: any): void {
    this.modelChange.next(data);
  }

  getMmodelChange(): Observable<any> {
    return this.modelChange.asObservable();
  }

  setConfirmState(state) {
    this.confirmState.next(state);
  }

  getConfirmState(): Observable<boolean> {
    return this.confirmState.asObservable();
  }

  setMatModalClose() {
    this.matModalClose.next(true);
  }

  getMatModalClose(): Observable<any> {
    return this.matModalClose.asObservable();
  }

  sendData(data: any) {
    this.subject.next(data);
  }

  getData(): Observable<any> {

    return this.subject.asObservable();
    // return  of(this.sDataModel);
  }

  sendActiveData(data: any) {
    this.dataActive = [];
    this.dataActive = data;
  }

  sendExtendData(data: any) {
    this.dataExtendData = [];
    this.dataExtendData = data;
  }

  // tslint:disable-next-line:variable-name
  clearData(user_id, token, model: any) {
    // tslint:disable-next-line:no-shadowed-variable
    const httpOptions = {
      headers: new HttpHeaders(this.httpOptions(null, user_id, token))
    };
    if (model === 'error') {
      return new BehaviorSubject(false);
    } else {
      return this.http.delete(this.url + model.titleQuery.toLowerCase()
        + '/'
        + model.query_id, httpOptions)
        .pipe(
          map(value => value),
          catchError(this.handleError));
    }
  }


  getDataActive() {
    return this.dataActive;
  }

  getDataExtend() {
    return this.dataExtendData;
  }

  private extractData(res: Response) {
    const body = res.json();
    return body || [];
  }

  private handleError(error: any) {
    const errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg);
    return Observable.throw(errMsg);
  }
}
