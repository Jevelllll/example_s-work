import {Injectable, OnDestroy} from '@angular/core';
import {httpOptions} from '../priority/baseOptions';
import {environment} from '../../../environments/environment';
import {BehaviorSubject, Observable, Subject, throwError} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {catchError, first, map, takeUntil, tap} from 'rxjs/operators';
import {UserEditInterface} from '../auth/loginInterface';
import {AccountUsers, GetAllQueryList, IShowFoundHelp, QuerElectronics} from './accountIntarface';

@Injectable({
  providedIn: 'root'
})
export class AccountService implements OnDestroy{
  destroyed$ = new Subject<void>();
  private httpOptions = httpOptions;
  private url: any = environment.api_url;
  private userInfoBehSubject: BehaviorSubject<any>;
  /**************/

  // tslint:disable-next-line:variable-name
  private __bSubject: BehaviorSubject<any>;
  // tslint:disable-next-line:variable-name
  public __obsResizeWidth: Observable<any> = new Observable();

  public obsUserInfo: Observable<any> = new Observable();

  private tokenDataUsId: BehaviorSubject<any>;
  public tokenDataUsIdObserable: Observable<any> = new Observable();

  /*********передача из таблиц на hover-helper************************/
  public bHSDataCountNotif: BehaviorSubject<any> = new BehaviorSubject<any>(null);
  // tslint:disable-next-line:variable-name
  private _bhViewNotifications: BehaviorSubject<any>;
  // tslint:disable-next-line:variable-name
  public _obsViewNotifications: Observable<any> = new Observable<any>();

  constructor(private http: HttpClient) {

    this.userInfoBehSubject = new BehaviorSubject<any>(null);
    this.obsUserInfo = this.userInfoBehSubject.asObservable();

    this._bhViewNotifications = new BehaviorSubject<any>(null);
    this._obsViewNotifications = this._bhViewNotifications.asObservable();

    this.__bSubject = new BehaviorSubject<any>(window.innerWidth);
    this.__obsResizeWidth = this.__bSubject.asObservable();

    try {
      this.tokenDataUsId = new BehaviorSubject(JSON.parse(localStorage.getItem('currentUser')));
      this.tokenDataUsIdObserable = this.tokenDataUsId.asObservable();
    } catch (e) {
    }

  }

  setSubjectWidth(event) {
    this.__bSubject.next(event);
  }

  // tslint:disable-next-line:variable-name
  public userEditData(user_id, token, data: any): Observable<UserEditInterface[]> {
    const paramsData = Object.keys(data).map((key) => {
      return encodeURIComponent(key) + '=' + encodeURIComponent(data[key]);
    }).join('&');
    // tslint:disable-next-line:no-shadowed-variable
    const httpOptions = {
      headers: new HttpHeaders(this.httpOptions('application/x-www-form-urlencoded', user_id, token))
    };
    return this.http.put<any>(this.url + 'user/edit', paramsData,
      httpOptions)
      .pipe(
        map(value => value),
        catchError(this.handleError));
  }

  // tslint:disable-next-line:variable-name
  public userEditUpdate(user_id, token, data): Observable<any> {
    const subject: Subject<any> = new Subject();

    this.userEditData(user_id, token, data)
      .pipe(takeUntil(this.destroyed$))
      .subscribe(response => {
        if (response['message'] === 'Data has been updated' && response['status'] === 200) {
          this.getPersonalInfo(user_id, token)
            .pipe(takeUntil(this.destroyed$), first())
            .subscribe(valResp => {
              subject.next(valResp);
            });
        }
      });
    return subject.asObservable();
  }

// tslint:disable-next-line:variable-name
  getQueryEl(user_id, token): Observable<QuerElectronics[]> {
    // tslint:disable-next-line:no-shadowed-variable
    const httpOptions = {
      headers: new HttpHeaders(this.httpOptions('application/x-www-form-urlencoded', user_id, token))
    };
    return this.http.get<any>(this.url + 'electronics', httpOptions)
      .pipe(
        map(value => value),
        catchError(this.handleError));
  }

// tslint:disable-next-line:variable-name
  getAllQueryList(user_id, token): Observable<GetAllQueryList[]> {

    const httpOptions = {
      headers: new HttpHeaders(this.httpOptions('application/x-www-form-urlencoded', user_id, token))
    };
    return this.http.get<any>(this.url + 'allquerylist', httpOptions)
      .pipe(
        map(value => {
          return value.map(item => {
            const preObj = {};
            for (const mItem in item) {
              if (mItem === 'query_name') {
                preObj[mItem] = item.query_name.replace(item.query_category, '').trim();
              } else {
                preObj[mItem] = item[mItem];
              }
            }
            return preObj;
          });
        }),
        catchError(this.handleError));
  }

// tslint:disable-next-line:variable-name
  getDatasShowFoundHelp(user_id, token, query_id, category_tblName): Observable<IShowFoundHelp[]> {
    // tslint:disable-next-line:no-shadowed-variable
    const httpOptions = {
      headers: new HttpHeaders(this.httpOptions('application/x-www-form-urlencoded', user_id, token))
    };
    return this.http.get<any>(this.url + 'notification/' + query_id + '/' + category_tblName,
      httpOptions)
      .pipe(
        map(value => value),
        catchError(this.handleError));
  }

// tslint:disable-next-line:variable-name
  getPersonalInfo(user_id, token): Observable<AccountUsers[]> {
    // tslint:disable-next-line:no-shadowed-variable
    const httpOptions = {
      headers: new HttpHeaders(this.httpOptions('application/x-www-form-urlencoded', user_id, token))
    };
    return this.http.get<any>(this.url + 'user/info', httpOptions)
      .pipe(
        map(value => {
          this.userInfoBehSubject.next(value);
          return value;
        }),
        catchError(this.handleError));
  }

  private extractData(res: Response) {
    const body: any = res.json();
    return body || [];
  }

  private handleError(error: any) {
    if (error.error.name === 'Unauthorized') {
      return throwError({auth: false});
    }
    const errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    return Observable.throw(errMsg);
  }

  /************обновление нотификации******************************/
  public updateReadNotification(id, userId, token): Observable<any> {
    const updateStr = `id=${id}`;
    // tslint:disable-next-line:no-shadowed-variable
    const httpOptions = {
      headers: new HttpHeaders(this.httpOptions('application/x-www-form-urlencoded', userId, token))
    };
    return this.http.put<any>(this.url + 'notification', updateStr,
      httpOptions)
      .pipe(
        map(value => value),
        catchError(this.handleError));
  }

  /*******************отобразить кол-во непросмотренных оповещений**********************************************/
  viewNotRead(userId, token): Observable<any> {
    return Observable.create((observer) => {
      this._obsViewNotifications
        .pipe(
          takeUntil(this.destroyed$),
          first())
        .subscribe(result => {
          if (result) {
            observer.next(result);
          } else {
            this.updateViewNotRead(observer, userId, token);
          }
        }, err => observer.error(false));
    });

  }

  updateViewNotRead(observer, userId, token) {
    const headers = httpOptions(null, userId, token);
    return this.http.get(`${this.url}notification/not-read`, {headers, responseType: 'json'}).pipe(
      first(),
      takeUntil(this.destroyed$),
      tap(val => {
        this._bhViewNotifications.next(val);
        return val;
      })).subscribe(result => observer.next(result), err => observer.error(false));
  }

  timeUpdate(userId, token): Observable<any> {
    const headers = httpOptions(null, userId, token);
    // tslint:disable-next-line:no-shadowed-variable
    return Observable.create((observable) => {
      this.http.get(`${this.url}notification/not-read`, {headers, responseType: 'json'})
        .pipe(map(val => {
          this._bhViewNotifications.next(val);
          return val;
        }), first(), takeUntil(this.destroyed$)).subscribe(result => observable.next(result),
        error => observable.error(false));
    });

  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }
}
