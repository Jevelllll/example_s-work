import { Injectable } from '@angular/core';
import {Observable, Subject} from 'rxjs';

@Injectable()
export class GroupingService {

  private subject = new Subject<any>();
  // tslint:disable-next-line:variable-name
  private visible_see = new Subject<any>();
  private subjectSort = new Subject<any>();
  // tslint:disable-next-line:variable-name
  send_visible_see(visible_item: string) {
    this.visible_see.next(visible_item);
  }
  get_visible_see(): Observable<string> {
    return this.visible_see.asObservable();
  }
  sendSort(dataSort: string) {
    this.subjectSort.next(dataSort);
  }
  sendGroup(dataGr: string) {
    this.subject.next(dataGr);
  }
  clearGroup() {
    this.subject.next();
  }
  clearSort() {
    this.subjectSort.next();
  }
  getGroup(): Observable<string> {
    return this.subject.asObservable();
  }
  getSort(): Observable<string> {
    return this.subjectSort.asObservable();
  }
  constructor() { }
}
