import {Injectable, OnDestroy} from '@angular/core';
import {environment} from '../../../environments/environment';
import {BehaviorSubject, Observable, Subject} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {httpOptions} from '../priority/baseOptions';
import {first, map, takeUntil} from 'rxjs/operators';

@Injectable()
export class StatisticsService implements OnDestroy {
  destroyed$ = new Subject<void>();
  private url: any = environment.api_url;

  private behStatistics: BehaviorSubject<any>;
  public obsStatistics: Observable<any> = new Observable<any>();

  constructor(private httpClient: HttpClient) {
    this.behStatistics = new BehaviorSubject<any>(null);
    this.obsStatistics = this.behStatistics.asObservable();
  }

  getStatistics(): Observable<any> {
    return Observable.create((observer) => {
      this.obsStatistics
        .pipe(takeUntil(this.destroyed$), first())
        .subscribe(resp => {
          if (resp) {
            observer.next(resp);
          } else {
            this.updateStatistic(observer);
          }
        });
    });
  }

  public updateStatistic(observer = null) {
    let usData: any;
    try {
      usData = JSON.parse(localStorage.getItem('currentUser'));
      const headers = httpOptions(null, usData.user_id, usData.token);
      this.httpClient.get(`${this.url}statistics`, {headers, responseType: 'json'}).pipe(
        first(),
        takeUntil(this.destroyed$),
        map(val => {
          this.behStatistics.next(val);
          return val;
        })).subscribe(result => {
        if (observer) {
          observer.next(result);
        } else {
          return this.obsStatistics;
        }
      });
    } catch (e) {
    }
  }

  // public updateInComponent() {
  //   const headers = httpOptions(null, userId, token);
  //   // tslint:disable-next-line:no-shadowed-variable
  //   return Observable.create((observable) => {
  //     this.http.get(`${this.url}notification/not-read`, {headers, responseType: 'json'})
  //       .pipe(map(val => {
  //         this._bhViewNotifications.next(val);
  //         return val;
  //       }), first()).subscribe(result => observable.next(result),
  //       error => observable.error(false));
  //   });
  // }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }
}
