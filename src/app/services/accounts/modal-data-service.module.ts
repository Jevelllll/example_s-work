import {ModuleWithProviders, NgModule} from '@angular/core';
import {ModalDataService} from './modal-data.service';

@NgModule()
export class ModalDataServiceModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: ModalDataServiceModule,
      providers: [ModalDataService]
    };
  }
}
