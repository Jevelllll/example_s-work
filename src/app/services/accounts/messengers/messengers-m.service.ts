import {Injectable} from '@angular/core';
import {environment} from '../../../../environments/environment';
import {BehaviorSubject, Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {catchError, tap} from 'rxjs/operators';

@Injectable()
export class MessengersMService {

  private url: any = environment.api_url;

  // tslint:disable-next-line:variable-name
  public __obsMessengers: Observable<any> = new Observable<any>();
  // tslint:disable-next-line:variable-name
  private __bSubjMessengers: BehaviorSubject<any>;

  constructor(private http: HttpClient) {
    this.__bSubjMessengers = new BehaviorSubject<any>(null);
    this.__obsMessengers = this.__bSubjMessengers.asObservable();
  }

  public messengersList(userId): Observable<any> {
    return this.http.get(`${this.url}messages/${userId}`, {responseType: 'json'}).pipe(
      tap(val => {
        this.__bSubjMessengers.next(val);
        return val;
      }), catchError(this.handleError));
  }

  private handleError(error: any) {
    const errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    return Observable.throw(errMsg);
  }
}
