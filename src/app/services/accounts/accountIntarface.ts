export class AccountIntarface {
}

export interface QuerElectronics {
  query_electronics_model: any;
  query_category_electronics: any;
  query_tariff: any;
}

// export interface AccountUsers {
//     phone: number;
//     email: string;
//     first_name: string;
//     last_name: string;
//     query_count: number;
// }
export class AccountUsers {
  constructor(public phone: number, public email: string,
              // tslint:disable-next-line:variable-name
              public first_name: string, public last_name: string,
              // tslint:disable-next-line:variable-name
              public query_count: number) {
  }
}

export interface GetAllQueryList {
  query_activated: string;
  paid_for: string;
  query_name: string;
  query_category: string;
  titleQuery: string;
  query_id: number;
  // query_user_id: number;
  query_confirm_time: string;
  query_tariff_end: string;
  query_tariff_start: string;
  tariff: [{
    id: number;
    tariff_type_id: number;
    sum_day: number;
    sum_month: number
  }];
  query_tariff_type: [{
    type_tariff: string;
  }];
}

export interface IShowFoundHelp {
  query_id: number;
  category_name: string;
  href: string;
  name: string;
  notice: number;
  find_time: string;
  find_data: string;
  coast: string;
  notif_phone: string;
  query_currency: any;
  currency_description_short: string;
  isRead: any;
  id_notification: number;
  service: string;
}

