import {ComponentFactoryResolver, Inject, Injectable, OnDestroy, OnInit, ViewContainerRef} from '@angular/core';
import {environment} from '../../../environments/environment';
import {BehaviorSubject, Observable, of, Subject, timer} from 'rxjs';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {DOCUMENT} from '@angular/common';
import {LoginService} from './login.service';
import {clearObject} from '../../shared/helper-container-leave';
import {FormArray, FormGroup} from '@angular/forms';
import {catchError, map, takeUntil, withLatestFrom, distinctUntilChanged, take} from 'rxjs/operators';
import {ServiceListModel} from './ServiceListModel';
import {CreateformdataService} from './createformdata.service';
import {SpinnerService} from '../spinner.service';
import {HintsService} from '../hints.service';
import {NavigationEnd, Router} from '@angular/router';
import {ConstingService} from '../consting/consting.service';
import {ToastrService} from 'ngx-toastr';

export const interpreter = {
  phaseTariff: 'Выбрать тариф',
  phaseQuery: 'Подать заявку',
  phaseContact: 'Оставьте контактные данные',
  phaseDataPay: 'Оплатить',
  final: 'boolean',
  stop: 'boolean'
};
declare let $;

@Injectable()
// tslint:disable-next-line:class-name
export class Application_phases implements OnInit, OnDestroy {
  destroyed$ = new Subject<void>();
  private url = environment.api_url;
  public typeName = [];

  /***********показать кнопки одиночной регистрации******************/
  private behOneRegistration: BehaviorSubject<boolean>;
  public obsOneRegistration: Observable<boolean> = new Observable<boolean>();
  /************finish submit contact form*****************************/
  private behFinishContactForm: BehaviorSubject<boolean>;
  public obsFinishContactForm: Observable<boolean> = new Observable<boolean>();

  private behUnsetPattern: BehaviorSubject<any>;
  public obsUnsetPattern: Observable<any> = new Observable<any>();
  public dataUnsetPattern = [];
  // /************список передающийся в дериктивы полей для их скрытия*******/
  // private _hideList: BehaviorSubject<any>;
  // public $_hideList: Observable<any> = new Observable<any>();
  /***после дотвреждения формы переходим кслед шагу*****************/
  private nextPhaseClick: Subject<boolean> = new Subject<boolean>();

  /*******контейнер фаз******/
  public phaseContainer = [];
  /*******контейнер фаз******/
  public phaseContainerContact = [];
  // обновление кнопок блокировки 29.01.2020
  private behSButtonNextD: BehaviorSubject<boolean>;
  // tslint:disable-next-line:variable-name
  public _obsButtonNextD: Observable<boolean> = new Observable();
  private behSButtonBAck: BehaviorSubject<boolean>;
  // tslint:disable-next-line:variable-name
  public _obsButtonBAck: Observable<boolean> = new Observable();
  // tslint:disable-next-line:variable-name
  private _bCurrentPhase: BehaviorSubject<CurrentPhase> = new BehaviorSubject<CurrentPhase>(new CurrentPhase(true));
  // tslint:disable-next-line:variable-name
  private _obsCurrentPhase: Observable<CurrentPhase> = new Observable<CurrentPhase>();
  //  29.01.2020
  // tslint:disable-next-line:variable-name
  private phase_tariff: BehaviorSubject<any> = new BehaviorSubject<any>(null);
  // tslint:disable-next-line:variable-name
  private phase_query: BehaviorSubject<any> = new BehaviorSubject<any>(null);
  // tslint:disable-next-line:variable-name
  private phase_contact: BehaviorSubject<any> = new BehaviorSubject<any>(null);
  // tslint:disable-next-line:variable-name
  private phase_data_pay: BehaviorSubject<any> = new BehaviorSubject<any>(null);

  private commonObs: Observable<any> = new Observable<any>();

  private hideStepper: BehaviorSubject<boolean>;

  private behShowConformButton: BehaviorSubject<boolean>;
  public obsShowConformButton: Observable<boolean> = new Observable<boolean>();
  private behShowConfirmPhoneDialog: BehaviorSubject<boolean>;
  public obsShowConfirmPhoneDialog: Observable<boolean> = new Observable<boolean>();

  // tslint:disable-next-line:variable-name
  constructor(private _http: HttpClient = null, @Inject(DOCUMENT) document,
              private loginService: LoginService,
              private hint: HintsService,
              private router: Router,
              private toastr: ToastrService,
              private constingService: ConstingService,
              private toValidservice: CreateformdataService, private spinnerService: SpinnerService,
              private componentFactoryResolver: ComponentFactoryResolver) {
    this._obsCurrentPhase = this._bCurrentPhase.asObservable();

    this.behShowConformButton = new BehaviorSubject<boolean>(false);
    this.obsShowConformButton = this.behShowConformButton.asObservable();
    this.behShowConfirmPhoneDialog = new BehaviorSubject<boolean>(false);
    this.obsShowConfirmPhoneDialog = this.behShowConfirmPhoneDialog.asObservable();

    this.behUnsetPattern = new BehaviorSubject<any>({});
    this.obsUnsetPattern = this.behUnsetPattern.asObservable();
    // обновление кнопок блокировки 29.01.2020
    this.behSButtonNextD = new BehaviorSubject<boolean>(true);
    this._obsButtonNextD = this.behSButtonNextD.asObservable();
    this.behSButtonBAck = new BehaviorSubject<boolean>(true);
    this._obsButtonBAck = this.behSButtonBAck.asObservable();

    this.behOneRegistration = new BehaviorSubject<boolean>(false);
    this.obsOneRegistration = this.behOneRegistration.asObservable();

    this.behFinishContactForm = new BehaviorSubject<boolean>(false);
    this.obsFinishContactForm = this.behFinishContactForm.asObservable();

    this.hideStepper = new BehaviorSubject<boolean>(this.router.url.indexOf('quest') !== -1);
    this.router.events.subscribe(event => {
      try {
        if (event instanceof NavigationEnd) {
          if (event.url.indexOf('quest') !== -1) {
            this.setHideStepper(true);
          } else {
            this.setHideStepper(false);
          }
        }
      } catch (e) {
      }
    });
  }

  public setConfirmPhoneButton(val: boolean): void {
    this.behShowConformButton.next(val);
  }

  public setShowConfirmPhoneDialog(val: boolean): void {
    this.behShowConfirmPhoneDialog.next(val);
  }

  public setOneRegistration(isOnce: boolean): void {
    this.behOneRegistration.next(isOnce);
  }

  public setFinishContactForm(isFCF: boolean): void {
    this.behFinishContactForm.next(isFCF);
  }

  private setHideStepper(hide = false): void {
    this.hideStepper.next(hide);
  }

  public getHideStepper(): Observable<boolean> {
    return this.hideStepper.asObservable();
  }

  public nextPhaseClickM(next = true) {
    this.nextPhaseClick.next(next);
  }

  public getNextPhaseClick(): Observable<boolean> {
    return this.nextPhaseClick.asObservable();
  }

  /********************получить в транcлите название типа заявки******************/
  getTypeName() {
    return of(this.typeName);
  }

  add_current_phase(currentPhase: CurrentPhase): void {
    this._bCurrentPhase.next(currentPhase);
  }

  get_current_phase(): Observable<CurrentPhase> {
    return this._obsCurrentPhase;
  }

  // создание фаз
  // tslint:disable-next-line:variable-name
  public add_phase_tariff(phase_tariff): void {
    this.phase_tariff.next(phase_tariff);
  }

// tslint:disable-next-line:variable-name
  public add_phase_query(phase_query: any): void {
    if (Array.isArray(phase_query)) {
      try {
        this.hint.installRedirectToAccount(phase_query[0].title.charAt(0).toUpperCase() + phase_query[0].title.substr(1).toLowerCase());
      } catch (e) {
      }
    }
    this.phase_query.next(phase_query);
  }

  public cleanPhases() {
    // this.phase_contact.unsubscribe();
    // this.phase_tariff.next(null);
    // this.phase_query.next(null);
    // this.phase_data_pay.next(null);
  }

// tslint:disable-next-line:variable-name
  public add_phase_contact(phase_contact): void {
    this.phase_contact.next(phase_contact);
  }

// tslint:disable-next-line:variable-name
  public add_phase_data_Pay(phase_data_pay): void {
    this.phase_data_pay.next(phase_data_pay);
  }

  // получение фаз
  public get_phase_tariff(): Observable<any> {
    return this.phase_tariff.asObservable();
  }

  public get_phase_query(): Observable<any> {
    return this.phase_query.asObservable();
  }

  public get_phase_contact(): Observable<any> {
    return this.phase_contact.asObservable();
  }

  public get_phase_data_pay(): Observable<any> {
    return this.phase_data_pay.asObservable();
  }

  public customFactory(containerRef: ViewContainerRef, dynamic: any, count = null, typeId = null) {

    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(dynamic);
    containerRef.clear();
    const rel = containerRef.createComponent(componentFactory);
    if (count) {
      rel.instance['count'] = count;
    }
    if (typeId) {
      rel.instance['typeIdS'] = typeId;
    }
    window.scrollTo(0, 67);
    // $('body,html').animate({
    //   scrollTop: 67
    // }, 800);
  }

  // обновление кнопок блокировки 29.01.2020
  public buttonNextDisable(isDisable = false) {
    this.behSButtonNextD.next(isDisable);
  }

  /****************************установка раннее выбранных сервисов*******/
  public preDefinedService(element, serviceSelected: any[], index: number) {
    const serviceDef: string = element.query_services_list;
    let serviceObj: ServiceListModel = element.query_services_list;
    try {
      serviceObj = JSON.parse(serviceDef);
    } catch (e) {
    }
    return serviceSelected[index] = serviceObj;
  }

  /**************Общий вид правил обслуживания сервисов и совмещение данных*******************************/
  public generalViewOfServiceRules(indexForm, serviceSelected, serviceList) {
    const selectServiceList = clearObject(serviceSelected[indexForm]);
    let patternService = {};
    const allParsePattern = [];
    // tslint:disable-next-line:prefer-for-of

    for (let index = 0; index < Object.keys(selectServiceList).length; index++) {
      const serviceId = Object.keys(selectServiceList)[index];
      const serviceListModel: ServiceListModel = serviceList.find((element) => element.id == Number(serviceId));
      if (serviceListModel.skip_pattern) {
        const skip_pattern = serviceListModel.skip_pattern;
        try {
          patternService = JSON.parse(skip_pattern);
        } catch (e) {
        }
        allParsePattern.push(patternService);
      }
    }
    if (allParsePattern.length > 0) {

      const title = {};
      allParsePattern.forEach((element, index) => {
        Object.keys(element).forEach((item) => {
          title[item] = true;
        });
      });

      const newTObj = {};
      Object.keys(title).forEach((field) => {
        // let preList = new Set();
        const preList = {};
        allParsePattern.forEach((element, index) => {
          if (element[field]) {
            element[field].forEach((pNP) => {
              preList[pNP] = true;
              // preList.add(pNP);
            });
          }
        });
        // if (Array.from(preList).length > 0) {
        //   newTObj[field] = Array.from(preList);
        // }
        if (Object.keys(preList).length > 0) {
          newTObj[field] = preList;
        }
      });

      this.addUnsetPattern(null, indexForm);
      try {
        if (newTObj['unset']) {
          this.addUnsetPattern(newTObj['unset'], indexForm);
        }
      } catch (e) {
      }
      return newTObj;
    } else {
      this.addUnsetPattern(null, indexForm);
    }
  }

  addUnsetPattern(data, index) {
    this.dataUnsetPattern[index] = data;
    this.behUnsetPattern.next(this.dataUnsetPattern);
  }

  getDataPattenrn() {
    return this.obsUnsetPattern;
  }

  /*******************условия формирования заблокированных FormControls***********************/
  lockedFieldConditions(form: FormGroup, patternObject) {
    if (patternObject && form) {
      let extraForm: FormGroup;
      if (form.controls.hasOwnProperty('query_extra')) {
        extraForm = (form.get('query_extra') as FormArray).at(0) as FormGroup;
      }
      Object.keys(patternObject).forEach((controlName) => {

        if (controlName === 'query_race') {
          form.get('raceFrom').disable({emitEvent: false});
          form.get('raceFrom').reset(null, {emitEvent: false});
          form.get('raceTo').disable({emitEvent: false});
          form.get('raceTo').reset(null, {emitEvent: false});
          form.get(controlName).reset(null, {emitEvent: false});
        } else if (controlName === 'query_yers') {
          form.get('query_s_yers').disable();
          form.get('query_s_yers').reset(null, {emitEvent: false});
          form.get('query_po_yers').disable();
          form.get('query_po_yers').reset(null, {emitEvent: false});
        } else {
          try {
            form.get(controlName).disable({emitEvent: false});
            form.get(controlName).reset(null, {emitEvent: false});
          } catch (e) {
          }
        }
        if (extraForm) {
          try {
            const abstractControls = (extraForm.get(controlName) as FormGroup).controls;
            Object.keys(abstractControls).forEach((field) => {
              abstractControls[field].reset();
            });
            extraForm.get(controlName).disable({emitEvent: false});
          } catch (e) {
          }
        }
      });
    }
  }

  unlockedConditions(form: FormGroup) {
    /****************исключения**************/
    const condition = ['query_price_max', 'query_price_min'];
    Object.keys(form.controls).forEach((controls) => {
      if (!condition.includes(controls)) {
        form.get(controls).enable({emitEvent: false});
      }
    });
  }

  public buttonBackDisable(isDisable = false) {
    this.behSButtonBAck.next(isDisable);
  }

  // tslint:disable-next-line:jsdoc-format
  /**отправить заявку**/
  postQueryRequest() {

    this.spinnerService.show();
    const timer1$ = timer(1);
    timer1$.pipe(takeUntil(this.destroyed$)
      , withLatestFrom(this.loginService.isLogin(), this.get_phase_contact(), this.get_phase_query(), this.get_phase_data_pay()))
      // tslint:disable-next-line:variable-name
      .subscribe(([common, isLogin, contact, query, data_pay]) => {
        const title = query[0].title;

        if (title) {
          if (isLogin) {

            let user: any = this.loginService.user;
            const currentUser = JSON.parse(localStorage.getItem('currentUser'));
            if (data_pay.cust) {
              console.log(data_pay);
              this.toValidservice.postQueryNew(
                query, title, currentUser.user_id, data_pay.id, data_pay.query_tariff_start, data_pay.query_tariff_end, true)
                .pipe(takeUntil(this.destroyed$))
                .subscribe(
                  // tslint:disable-next-line:no-shadowed-variable
                  (datas) => {
                    this.constingService.obsResultsStore.pipe(takeUntil(this.destroyed$))
                      .subscribe((custField) => {
                        if (custField) {
                          this.constingService
                            .customerQuestAdd(currentUser.user_id, custField, data_pay.id, datas.id_query)
                            .pipe(takeUntil(this.destroyed$)).subscribe((resultRT) => {
                            this.toastr.success('Заявка активирована');
                            this.router.navigate(['/account']);
                          });

                        } else {
                           this.constingService
                             .customerQuestAddskip(currentUser.user_id, data_pay.id, datas.id_query)
                             .pipe(takeUntil(this.destroyed$)).subscribe((resultRT) => {
                             this.toastr.success('Заявка активирована');
                             this.router.navigate(['/account']);
                           });
                        }
                      }, error => {});
                  }, error => {});
            } else {
              this.toValidservice
                .postQueryNew(query, title, currentUser.user_id, data_pay.id, data_pay.query_tariff_start, data_pay.query_tariff_end)
                .pipe(takeUntil(this.destroyed$))
                .subscribe((data) => {
                    this.toValidservice.setPayOrderCategory(title);
                    this.toValidservice.setPayOrder_id(data.id_query);
                    // this.spinnerService.hide();
                  }, error => this.spinnerService.hide(),
                  () => {
                    this.toValidservice.setPayVisible(true);
                    // this.spinnerService.hide();
                  });
            }
          } else {
            if (data_pay.cust) {
              this.toValidservice.postContact([contact], false)
                .pipe(takeUntil(this.destroyed$)).subscribe((data) => {

                this.toValidservice.postQueryNew(
                  query, title, data.user_id, data_pay.id, data_pay.query_tariff_start, data_pay.query_tariff_end, true)
                  .pipe(takeUntil(this.destroyed$))
                  .subscribe(
                    // tslint:disable-next-line:no-shadowed-variable
                    (datas) => {

                      this.constingService.obsResultsStore.pipe(takeUntil(this.destroyed$))
                        .subscribe((custField) => {
                          this.constingService
                            .customerQuestAdd(data.user_id, custField, data_pay.id, datas.id_query)
                            .pipe(takeUntil(this.destroyed$)).subscribe((resultRT) => {
                            const regExp = new RegExp(/[-()/\\]/g);
                            if (data) {
                              this.loginService.login(contact['phone'].replace(regExp, ''), contact['password'])
                                .pipe(take(1), distinctUntilChanged()
                                  , takeUntil(this.destroyed$)
                                )
                                .subscribe((respIsLogin) => {
                                  if (respIsLogin) {
                                    this.toastr.success('Заявка активирована');
                                    this.router.navigate(['/account']);
                                  }
                                });
                            }
                          }, error => this.spinnerService.hide());
                        }, error => this.spinnerService.hide());
                    },
                    error => this.spinnerService.hide(),
                    () => {
                      this.toValidservice.setPayVisible(true);
                      this.spinnerService.hide();
                    }
                  );
              });
            } else {
              this.toValidservice.postContact([contact], true)
                .pipe(takeUntil(this.destroyed$)
                ).subscribe((data) => {

                this.toValidservice.postQueryNew(
                  query, title, data.user_id, data_pay.id, data_pay.query_tariff_start, data_pay.query_tariff_end)
                  .pipe(takeUntil(this.destroyed$))
                  .subscribe(
                    // tslint:disable-next-line:no-shadowed-variable
                    (datas) => {
                      this.toValidservice.setPayOrderCategory(title);
                      this.toValidservice.setPayOrder_id(datas.id_query);
                      // this.spinnerService.hide();
                    },
                    error => this.spinnerService.hide(),
                    () => {
                      this.toValidservice.setPayVisible(true);
                      // this.spinnerService.hide();
                    }
                  );
              });
            }

          }
        }
      });
  }


  private handleError(error: HttpErrorResponse) {
    // TODO: seems we cannot use messageService from here...
    const errMsg = (error.message) ? error.message : 'Server error';
    if (error.status === 401) {
      window.location.href = '/';
    }
    return Observable.throw(errMsg);
  }

  // tslint:disable-next-line:contextual-lifecycle
  ngOnInit(): void {

  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }
}

export class CurrentPhase {

  phaseTariff: boolean;
  phaseQuery: boolean;
  phaseContact: boolean;
  phaseDataPay: boolean;
  // final: boolean;
  // stop: boolean;

  // tslint:disable-next-line:variable-name
  constructor(phase_tariff = true, phase_query = false, phase_contact = false, phase_data_pay = false) {
    this.phaseTariff = phase_tariff;
    this.phaseQuery = phase_query;
    this.phaseContact = phase_contact;
    this.phaseDataPay = phase_data_pay;
  }

  next() {
    let nextTrue: any = {};
    const toArray = Object.keys(this);
    const pre = Object.keys(this);
    const cPS = new CurrentPhase();
    toArray.forEach((field, index) => {
      if (this[field]) {
        if (field === pre.reverse()[0]) {
          return false;
        } else {
          if (toArray[index + 1]) {
            nextTrue = {[toArray[index + 1]]: true};
            toArray.forEach((val) => {
              cPS[val] = false;
            });
            if (Object.keys(nextTrue)[0]) {
              cPS[Object.keys(nextTrue)[0]] = true;
            }

          }
        }
      }
    });
    return cPS;
  };

  prev() {

    let backTrue: any = {};
    const toArray = Object.keys(this);
    const pre = Object.keys(this);
    const cPS = new CurrentPhase();
    toArray.forEach((field, index) => {
      if (this[field]) {
        if (field === pre.reverse()[0]) {
          return false;
        } else {
          if (toArray[index - 1]) {
            backTrue = {[toArray[index - 1]]: true};
            toArray.forEach((val) => {
              cPS[val] = false;
            });
            if (Object.keys(backTrue)[0]) {
              cPS[Object.keys(backTrue)[0]] = true;
            }

          }
        }
      }
    });
    return cPS;
  }
}

/*****************Выбор Сервисов*******************************************/
export function serviceListSelect(serList, index) {

  this.serviceSelected[index][serList.id] = !this.serviceSelected[index][serList.id];
  this.serviceSelected[index] = clearObject(this.serviceSelected[index]);
  this.generalViewOfServiceRules(index);
  if (this.typeNameBlocked) {
    const formGroup = this.currentFormArray(index);
    if (this.patternService[index]) {
      this.phasesService.unlockedConditions(formGroup);
      this.phasesService.lockedFieldConditions(formGroup, this.patternService[index][this.typeNameBlocked[index]]);
    } else {
      this.phasesService.unlockedConditions(formGroup);
    }
  }
}

/**********работа с экземпляром форм******************/
export function addNewForm(indexForm) {
  if (this.count === 1) {
    this.phasesService.nextPhaseClickM(true);
  }
  const formGroup = (<FormArray> this.myForm.get('items')).at(Number(indexForm));
  const isValid = formGroup.valid;
  formGroup.patchValue({query_services_list: this.JSON.stringify(this.serviceSelected[indexForm])});
  if (isValid) {
    this.phasesService.buttonNextDisable(false);
    const object = clearObject(formGroup.value);
    object.title = this.myForm.get('title').value;
    this.phasesService.phaseContainer[indexForm] = object;

    if (this.count === 1) {

    } else {
      if (this.count === indexForm + 1) {
      } else {
        this.activeFromWrapQuery(indexForm + 2);
      }
    }

  }
}

/************список сервисов***************************/
export function getServiceList() {
  this.spinnerService.show();
  this.phasesService.buttonNextDisable(true);
  this.toValidservice.serviceListObs
    .pipe(takeUntil(this.destroyed$))
    .subscribe((serviceList) => {
      if (serviceList) {
        try {
          // this.serviceList = serviceList.filter(item => item.category === this.CATEGORY_NAME && item.name !== 'RST');
          this.serviceList = serviceList.filter(item => item.category === this.CATEGORY_NAME);
        } catch (e) {
        }
        this.spinnerService.hide();
      } else {
        this.toValidservice.initServiceList()
          .pipe(takeUntil(this.destroyed$))
          .subscribe(resultService => {
            try {
              // this.serviceList = resultService.filter(item => item.category === this.CATEGORY_NAME && item.name !== 'RST');
              this.serviceList = resultService.filter(item => item.category === this.CATEGORY_NAME);
            } catch (e) {
            }
            this.spinnerService.hide();
          });
      }
    }, error1 => {
      this.spinnerService.hide();
    });
}

export function patternOfSelectList(indexForm, idSelect: number, obSElement) {
  const selectServiceList = clearObject(this.serviceSelected[indexForm]);
  if (selectServiceList) {
    if (Object.keys(selectServiceList).length > 0 && idSelect > 0) {
      obSElement = obSElement.pipe(map((dataVal) => {
        const objServiceName = {};
        const clearData = [];

        // tslint:disable-next-line:prefer-for-of
        for (let index = 0; index < Object.keys(selectServiceList).length; index++) {
          const serviceId = Object.keys(selectServiceList)[index];
          const serviceListModel: any = this.serviceList.find((element) => element.id == Number(serviceId)).name;
          objServiceName[serviceListModel] = 1;
        }

        if (Array.isArray(dataVal)) {
          dataVal.forEach((data) => {
            if (typeof data === 'object') {
              const preField = [];
              Object.keys(objServiceName).forEach((serviceField) => {
                if (data[serviceField] == 0) {
                  preField.push(data);
                }
              });
              if (preField.length === 0) {
                clearData.push(data);
              }
            }
          });
        }

        return clearData;
      }), catchError((error): any => {
      }));

    }
  }

  return obSElement;
}
