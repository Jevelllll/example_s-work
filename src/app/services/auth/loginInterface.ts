export class LoginInterface {
       user_phone: string;
       user_password: string;
       remember_me: string;
}
export interface UserEditInterface {
    status: number;
    message: string;
}