import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {BehaviorSubject, Observable, throwError} from 'rxjs';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {catchError, map} from 'rxjs/operators';

@Injectable()
export class LoginService {

  public token: string;
  public user: any;

  private url = environment.api_url;
  private subject: BehaviorSubject<any>;

  private isLoginForm: BehaviorSubject<any>;
  // tslint:disable-next-line:variable-name
  public __isLoginForm: Observable<any> = new Observable<any>();

  /*****************скрол под навбар***********/
  private isScrolDownNav: BehaviorSubject<any>;
  // tslint:disable-next-line:variable-name
  public __isScrolDownNav: Observable<any> = new Observable<any>();

  constructor(private http: HttpClient) {
    // set token if saved in local storage

    this.isScrolDownNav = new BehaviorSubject<any>(null);
    this.__isScrolDownNav = this.isScrolDownNav.asObservable();

    this.isLoginForm = new BehaviorSubject<any>('0');
    this.__isLoginForm = this.isLoginForm.asObservable();

    const currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.token = currentUser && currentUser.token;


    if (this.token) {
      this.subject = new BehaviorSubject<any>(true);
      this.user = currentUser;
    } else {
      this.subject = new BehaviorSubject<any>(false);
    }
  }


  public setScroll() {
    this.isScrolDownNav.next(true);
  }

  public setIsLoginForm(isLogin: string) {
    this.isLoginForm.next(isLogin);
  }

  public getIsLoginForm() {
    return this.__isLoginForm;
  }

  public setIsLogin(login: boolean) {
    this.subject.next(login);
  }

  public isLogin(): Observable<boolean> {
    return this.subject.asObservable();
  }

  checkEsputnicPhone(phone: number) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        'Client-Service': 'frontend-client',
        'Auth-Key': '???'
      })
    };
    return this.http.post(this.url + 'esputnic/check-phone',
      `phone=${phone}`, httpOptions).pipe(map((res) => res), catchError(this.handleError));
  }

  esputnicResetKey(phone: number) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        'Client-Service': 'frontend-client',
        'Auth-Key': '???'
      })
    };
    return this.http.post(this.url + 'esputnic/get-key',
      `phone=${phone}`, httpOptions).pipe(map((res) => res), catchError(this.handleError));
  }

  // tslint:disable-next-line:variable-name
  esputnicValidkey(phone: number, secret_key: any): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        'Client-Service': 'frontend-client',
        'Auth-Key': '???'
      })
    };
    return this.http.post<any>(this.url + 'esputnic/valid-secret-key',
      `phone=${phone}&secret_key=${secret_key}`, httpOptions).pipe(map((res) => res), catchError(this.handleError));
  }

  // tslint:disable-next-line:variable-name
  esputnicUpdatePassword(phone: number, secret_key: any, password: any): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        'Client-Service': 'frontend-client',
        'Auth-Key': '???'
      })
    };
    return this.http.put<any>(this.url + 'esputnic/update-user',
      `phone=${phone}&secret_key=${secret_key}&password=${password}`, httpOptions)
      .pipe(map((res) => res), catchError(this.handleError));

  }


  /*Проверка на на существование сегоднешнего токена */
  public checkdatepassword(phone: number) {

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        'Client-Service': 'frontend-client',
        'Auth-Key': '???'
      })
    };
    return this.http.post(this.url + 'user/checkdatepas',
      `phone=${phone}`, httpOptions).pipe(map((res) => res), catchError(this.handleError));
  }

  // tslint:disable-next-line:variable-name
  public checkPhome(phone: number, is_sms?: boolean) {
    // tslint:disable-next-line:variable-name
    const is_Sms = (is_sms === undefined) ? false : is_sms;
    const preBody = 'phone=' + phone + '&is_sms=' + is_Sms;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        'Client-Service': 'frontend-client',
        'Auth-Key': '???'
      })
    };
    return this.http.post(this.url + 'user/check',
      preBody, httpOptions)
      .pipe(map(value => {
        return value;
      }), catchError(this.handleError));
  }

  // tslint:disable-next-line:variable-name
  public login(user_phone: string, user_password: string): Observable<any> {

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        'Client-Service': 'frontend-client',
        'Auth-Key': '???'
      })
    };
    // {headers: preHeaders, responseType: 'text' as 'json'}
    const preBody = `user_phone=${user_phone}&user_password=${user_password}`;
    return this.http.post<any>(this.url + 'auth/login', preBody,
      {headers: httpOptions.headers, responseType: 'json'}).pipe(map((response): any => {
      const token = response && response.token;
      // tslint:disable-next-line:variable-name
      const user_id = response && response.id;
      if (token) {
        // set token property
        this.token = token;

        // store username and jwt token in local storage to keep user logged in between page refreshes
        localStorage.setItem('currentUser', JSON.stringify({user_id, token}));
        this.setIsLogin(true);
        // return true to indicate successful login
        return true;
      } else {
        // return false to indicate failed login
        return false;
      }
    }), catchError(this.handleError));
  }

  // tslint:disable-next-line:variable-name
  public checksession(authorization: any = null, user_id = null) {

    // tslint:disable-next-line:one-variable-per-declaration
    let auth = null, userId = null;
    if (user_id === null || authorization === null) {
      try {
        const userAuth: any = JSON.parse(localStorage.getItem('currentUser'));
        auth = userAuth.token;
        userId = userAuth.user_id;
      } catch (e) {
        return throwError(false);
      }
    } else {
      auth = authorization;
      userId = user_id;
    }
    if (auth !== null || userId !== null) {
      const httpOptions = {
        headers: new HttpHeaders({
          Authorization: auth,
          'Client-Service': 'frontend-client',
          'Auth-Key': '???',
          'User-Id': userId
        })
      };

      return this.http.get(this.url + 'user/checksession', httpOptions)
        .pipe(
          map((value) => {
            if (value['status'] === 200 && value['message'] === 'Authorized.') {
              return value;
            } else {
              return false;
            }
          }));
    }
    return throwError(false);
  }

  public logout(): void {
    this.setIsLogin(false);
    // clear token remove user from local storage to log user out
    this.token = null;
    localStorage.removeItem('currentUser');
  }

  private extractData(res: any) {
    const body = res.json();
    return body || [];
  }

  /*
    *
    * @param error
    */
  private handleError(error: HttpErrorResponse | any) {
    // TODO: seems we cannot use messageService from here...
    const errMsg = (error.message) ? error.message : 'Server error';
    if (error.status === 401) {
      window.location.href = '/';
    }
    return Observable.throw(errMsg);
  }
}
