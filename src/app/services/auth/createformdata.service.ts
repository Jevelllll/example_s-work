import {EventEmitter, Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {BehaviorSubject, Observable, of, Subject, throwError} from 'rxjs';
import {ElectronicsForm} from '../aditionals/electronicsmodel';
import {ServiceListModel} from './ServiceListModel';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {catchError, first, map, shareReplay, tap} from 'rxjs/operators';
import {PropertyForm} from '../priority/propertyResModel';
import {AutoForm, PostQueryPayModel, Tariffinterface} from '../priority/resModel';
import {UserEditInterface} from './loginInterface';

@Injectable({
  providedIn: 'root'
})
export class CreateformdataService {

  private url = environment.api_url;
  private payVisible = new Subject<boolean>();
  private additGoBtnConfirm = new Subject<boolean>();
  // order_id = query_id
  private strAdditional: any = '';
  public additFields: any[] = [];
  private payOrderId = new Subject<number>();
  private payCategoryName = new Subject<string>();
  public autoModel: AutoForm[] = [];
  public otherM: any[] = [];
  public workM: any[] = [];
  public autoproductsM: any[] = [];
  public electronicsModel: ElectronicsForm[] = [];
  public propertyModel: PropertyForm[] = [];
  public contactForm: any = [];
  public tariffForm: Tariffinterface[] = [];
  private subject = new Subject<any>();
  private subjectTariff = new Subject<any>();
  private sendCommonForm = new Subject<any>();

  private newUserId: BehaviorSubject<any>;
  // tslint:disable-next-line:variable-name
  public _obsNewUserId: Observable<any> = new Observable();

  private serviceListSubject: BehaviorSubject<ServiceListModel[]>;
  public serviceListObs: Observable<ServiceListModel[]> = new Observable<ServiceListModel[]>();

  public extraDataRepeat: any;


  setExtraDataRepeat(data) {
    this.extraDataRepeat = data;
  }

  getExtraDataRepeat() {
    return of(this.extraDataRepeat);
  }

  isBackButton: EventEmitter<any> = new EventEmitter<any>();

  cleanFormData() {
    this.autoModel = [];
    this.otherM = [];
    this.workM = [];
    this.autoproductsM = [];
    this.electronicsModel = [];
    this.propertyModel = [];
    this.contactForm = [];
  }

  // tslint:disable-next-line:variable-name
  constructor(private _http: HttpClient = null) {
    this.serviceListSubject = new BehaviorSubject<any>(null);
    this.serviceListObs = this.serviceListSubject.asObservable();

    this.newUserId = new BehaviorSubject<any>(null);
    this._obsNewUserId = this.newUserId.asObservable();
  }

  /*********************список сервисов*********************************************/
  public initServiceList(): Observable<ServiceListModel[]> {

    const headers = new HttpHeaders({
      'Client-Service': 'frontend-client',
      'Auth-Key': '???'
    });
    return this._http.get<ServiceListModel[]>(`${this.url}services-list`, {headers, responseType: 'json'}).pipe(
      first(),
      tap((data) => {
        this.serviceListSubject.next(data);
      }),
      catchError(this.handleError)
    );
  }

  // tslint:disable-next-line:variable-name
  setPayOrder_id(query_id: number) {
    this.payOrderId.next(query_id);
  }

  getPayOrder_id() {
    return this.payOrderId;
  }

  setPayOrderCategory(categoryName: string) {
    this.payCategoryName.next(categoryName);
  }

  getPayOrderCategory() {
    return this.payCategoryName;
  }

  setPayVisible(visible: boolean) {
    this.payVisible.next(visible);
  }

  getPayVisible(): Observable<boolean> {
    return this.payVisible.asObservable();
  }

  setCommonForm(commonForm: any) {
    this.sendCommonForm.next(commonForm);
  }

  getCommonForm(): Observable<any> {
    return this.sendCommonForm.asObservable();
  }

  getDataFirstForm(): Observable<AutoForm[]> {
    return of(this.autoModel);
  }

  getDataOtherFirstForm(): Observable<any[]> {
    return of(this.otherM);
  }

  getDataWorkFirstForm(): Observable<any[]> {
    return of(this.workM);
  }

  getDataAutoProductsMForm(): Observable<any[]> {
    return of(this.autoproductsM);
  }

  getDataElectronicsFirstForm(): Observable<ElectronicsForm[]> {
    return of(this.electronicsModel);
  }

  getDataPropertyFirstForm(): Observable<PropertyForm[]> {
    return of(this.propertyModel);
  }

  getDataContactForm(): Observable<any[]> {
    return of(this.contactForm);
  }

  getDataTariffForm(): Observable<Tariffinterface[]> {
    return of(this.tariffForm);
  }

  addElectronicsModel(elMD: ElectronicsForm) {
    this.electronicsModel = [];
    this.electronicsModel.push(elMD);
  }

  additionalFieldGoBtn(additConfirm: boolean) {
    this.additGoBtnConfirm.next(additConfirm);
  }

  getAdditionalFieldGo(): Observable<boolean> {
    return this.additGoBtnConfirm.asObservable();
  }

  private unique(arr) {
    const obj = {};
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < arr.length; i++) {
      const str = arr[i];
      obj[str] = true;
    }
    return Object.keys(obj);
  }

  // addAdditionalForm(additfields: any) {
  //   this.additFields = [];
  //   const data: any = [];
  //   // tslint:disable-next-line:variable-name
  //   let ful_id: any;
  //   // tslint:disable-next-line:only-arrow-functions
  //   const out = additfields.map(function(el) {
  //     return el.afields_id;
  //   });
  //   ful_id = this.unique(out);
  //   // tslint:disable-next-line:prefer-for-of
  //   for (let i = 0; i < additfields.length; i++) {
  //     if (data[additfields[i].afields_id] === undefined) {
  //       data[additfields[i].afields_id] = '\[\"' + additfields[i].afields_value.trim() + '\",';
  //     } else {
  //       data[additfields[i].afields_id] += '\"' + additfields[i].afields_value.trim() + '\",';
  //     }
  //   }
  //   // tslint:disable-next-line:variable-name
  //   let item_index: number;
  //   item_index = 0;
  //   for (const item of ful_id) {
  //     this.additFields.push(new AdditFields('ainfo_id_additional_fields\[' + item + '\]', data[item].slice(0, -1) + '\]'.trim()));
  //     item_index++;
  //   }
  // }

  getDataAdditionalForm(): Observable<any> {
    const transformRequest = this.transformRequestAdditional(this.additFields);
    return of(transformRequest);
  }

  cleanAdditionalForm() {
    this.additFields = [];
  }

  addAutoModel(autMd: AutoForm) {
    this.autoModel = [];
    this.autoModel.push(autMd);
  }

  addPropertyModel(autMd: any) {
    this.propertyModel = [];
    this.propertyModel.push(autMd);
  }

  addOtherModel(othN: any) {
    this.otherM = [];
    this.otherM.push(othN);
  }

  addWorkModel(othN: any) {
    this.workM = [];
    this.workM.push(othN);
  }

  addAutoProductsModel(othN: any) {
    this.autoproductsM = [];
    this.autoproductsM.push(othN);
  }

  addContactModel(autMd: any) {
    let prePush = {};
    prePush = autMd;
    // @ts-ignore
    prePush['date_year'] = moment.unix();
    this.contactForm = [];
    this.contactForm.push(prePush);
  }

  addTariffModel(autMd: Tariffinterface) {
    this.tariffForm = [];
    this.tariffForm.push(autMd);
  }

  paramsSubm(tariffObj: any) {
    this.subject.next(tariffObj);
  }

  paramsSubJectTariff(tariffObj: any) {
    this.subjectTariff.next(tariffObj);
  }

  undisbledSubm(tariffObj: any) {
    this.subject.next(tariffObj);
  }

  getParams(): Observable<any> {
    return this.subject.asObservable();
  }

  getParamsTariif(): Observable<any> {
    return this.subjectTariff.asObservable();
  }

  // tslint:disable-next-line:variable-name
  postQuery(query: any, title: string, user_id: any, tariff_id: any, query_tariff_start: any, query_tariff_end: any) {
    const body = this.transformRequest(query[0]);
    const bodyHttp = body + '&query_tariff=' + tariff_id + '&query_tariff_start=' +
      +Math.round(this.getUtcUnixTime(query_tariff_start))
      + '&query_tariff_end=' + Math.round(this.getUtcUnixTime(query_tariff_end));

    const preHeaders = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'User-Id': user_id.toString(),
      'Client-Service': 'frontend-client',
      'Auth-Key': '???'
    });

    // const preHeaders = new HttpHeaders({
    //   'Content-Type': 'application/x-www-form-urlencoded',
    //   Accept: 'application/json, text/plain, */*',
    // });
    return this._http.post<PostQueryPayModel>(this.url + title,
      bodyHttp, {headers: preHeaders}).pipe(
      map(res => res),
      catchError(this.handleError));
  }

  /********Post Query New********/
  // tslint:disable-next-line:variable-name
  postQueryNew(query: any, title: string, user_id: any, tariff_id: any, query_tariff_start: any, query_tariff_end: any, isCust = null) {

    const newQuery = {data: ''};
    try {
      newQuery.data = JSON.stringify(query);
    } catch (e) {
    }
    const body = this.transformRequest(newQuery);
    const bodyHttp = body + '&query_tariff=' + tariff_id + '&query_tariff_start=' +
      +Math.round(this.getUtcUnixTime(query_tariff_start))
      + '&query_tariff_end=' + Math.round(this.getUtcUnixTime(query_tariff_end));

    const newHVar: any = {
      'Content-Type': 'application/x-www-form-urlencoded',
      'User-Id': user_id.toString(),
      'Client-Service': 'frontend-client',
      'Auth-Key': '???'
    };
    if (isCust) {
      newHVar['isCust'] = '1';
    }
    const preHeaders = new HttpHeaders(newHVar);
    return this._http.post<PostQueryPayModel>(this.url + title + '/new-create',
      bodyHttp, {headers: preHeaders}).pipe(shareReplay({bufferSize: 1, refCount: true}),
      map(res => res),
      catchError(this.handleError));

  }

  /************real phone number (POST DATA)***************/
  public realPhoneNumber(phone: number): Observable<UserEditInterface> {
    const bodyHttp = `phone=${phone}`;
    const preHeaders = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Client-Service': 'frontend-client',
      'Auth-Key': '???'
    });
    return this._http.post<UserEditInterface>(`${this.url}esputnic/cph`, bodyHttp, {headers: preHeaders, responseType: 'json'})
      .pipe(catchError(this.handleError));
  }

  /************real phone number (POST DATA)***************/
  public cRealPhoneNumber(code: number): Observable<UserEditInterface> {
    const bodyHttp = `code=${code}`;
    const preHeaders = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Client-Service': 'frontend-client',
      'Auth-Key': '???'
    });
    return this._http.post<UserEditInterface>(`${this.url}esputnic/sendcph`, bodyHttp, {headers: preHeaders, responseType: 'json'})
      .pipe(catchError(this.handleError));
  }

  postTariff(dataTariff: any) {
    const body = this.transformRequest(dataTariff[0]);
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
      })
    };

    return this._http.post(this.url + 'tariff',
      body, httpOptions)
      .pipe(map(res => res));
  }

  postContact(dataContact: any, toUserHistory: boolean = false) {

    const body = (toUserHistory) ?
      this.transformRequest(Object.assign({}, {toUserHistory}, dataContact[0])) :
      this.transformRequest(dataContact[0]);
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
      })
    };
    return this._http.post<any>(this.url + 'user',
      body, httpOptions)
      .pipe(tap((res) => this.newUserId.next(res)), map(res => {
        return res;
      }), catchError(this.handleError));
  }

  /********************проверяем что была нажата кнопка назад***********************************/
  setIsClickBackButton() {
    this.isBackButton.emit(true);
  }

  getIsClickBackButton(): Observable<any> {
    return this.isBackButton;
  }

  transformRequestAdditional(obj) {
    const str = [];
    let i;
    i = 0;
    for (const item of obj) {
      str.push(item.afields_name + '=' + item.afields_value);
    }
    return str.join('&');
  }

  transformRequest(obj) {
    const str = [];
    // tslint:disable-next-line:forin
    for (const p in obj) {
      str.push(p + '=' + obj[p]);
    }
    return str.join('&');
  }

  public getUtcUnixTime(date) {
    return (+new Date(date)
      - new Date(date).getTimezoneOffset() * 60 * 1000)
      / 1000;
  }

  private handleError(error: HttpErrorResponse) {
    // TODO: seems we cannot use messageService from here...
    const errMsg = (error.message) ? error.message : 'Server error';
    if (error.status === 401) {
      window.location.href = '/';
    }
    return throwError(errMsg);
  }
}
