import {ModuleWithProviders, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LoginService} from './login.service';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
  ],
  providers: [
    LoginService
  ]
})
export class SharedModuleLogin {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModuleLogin,
      providers: [LoginService]
    };
  }
}
