export class ServiceListModel {
  id: number;
  name: string;
  description: string;
  img: string;
  category: string;
  // tslint:disable-next-line:variable-name
  skip_pattern: string;
}
