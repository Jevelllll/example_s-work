export interface Tariffinterface {
    id: number;
    query_tariff_start: number;
    query_tariff_end: number;
}
