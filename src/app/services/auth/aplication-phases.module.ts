import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {Application_phases} from './application-phases.service';



@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [
    Application_phases
  ]
})
export class AplicationPhasesModule { }
