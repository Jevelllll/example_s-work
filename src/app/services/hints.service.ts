import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';

@Injectable()
export class HintsService {

  // tslint:disable-next-line:variable-name
  private __bhsHomeState: BehaviorSubject<any>;
  // tslint:disable-next-line:variable-name
  public __obsHomeState: Observable<any> = new Observable<any>();
  // tslint:disable-next-line:variable-name
  private __bhsLeaveRequestState: BehaviorSubject<any>;
  // tslint:disable-next-line:variable-name
  public __obsLeaveRequestState: Observable<any> = new Observable<any>();

  private localSaveHomeHint;
  private localSaveLeaveRequestHint;
  private localRedirectToAccount;
  // tslint:disable-next-line:variable-name
  private __bhsRedirectToAccount: BehaviorSubject<string>;
  // tslint:disable-next-line:variable-name
  public __obsHRedirectToAccount: Observable<string> = new Observable<any>();

  constructor() {

    /********подсказки главной*********************/
    let isIssetHomeHint = null;
    try {
      isIssetHomeHint = JSON.parse(localStorage.getItem('hint-home'));
    } catch (e) {
    }
    if (isIssetHomeHint) {
      this.localSaveHomeHint = isIssetHomeHint;
    }
    this.__bhsHomeState = new BehaviorSubject<any>(isIssetHomeHint);
    this.__obsHomeState = this.__bhsHomeState.asObservable();

    /********подсказки формирования заявок***********/
    let isIssetLeaveRequestHint = null;
    try {
      isIssetLeaveRequestHint = JSON.parse(localStorage.getItem('hint-leave-request'));
    } catch (e) {
    }
    if (isIssetLeaveRequestHint) {
      this.localSaveLeaveRequestHint = isIssetLeaveRequestHint;
    }
    this.__bhsLeaveRequestState = new BehaviorSubject<any>(isIssetLeaveRequestHint);
    this.__obsLeaveRequestState = this.__bhsLeaveRequestState.asObservable();

    let toAccount = null;
    try {
      toAccount = JSON.parse(localStorage.getItem('to-acc')).toAcc;
    } catch (e) {
    }
    this.__bhsRedirectToAccount = new BehaviorSubject(toAccount);
    this.__obsHRedirectToAccount = this.__bhsRedirectToAccount.asObservable();

  }

  /*********редирект после оплаты заявки************/
  installRedirectToAccount(fromCategoryName: string) {
    this.__bhsRedirectToAccount.next(fromCategoryName);
    localStorage.setItem('to-acc', JSON.stringify({toAcc: fromCategoryName}));
  }

  /********подсказки главной*********************/
  installHomeState() {
    this.__bhsHomeState.next({hint: true});
    localStorage.setItem('hint-home', JSON.stringify({hint: true}));
  }

  deleteHomeState() {
    this.__bhsHomeState.next(null);
    localStorage.removeItem('hint-home');
  }


  /********подсказки формирования заявок***********/
  installLeaveRequesState() {
    this.__bhsLeaveRequestState.next({hint: true});
    localStorage.setItem('hint-leave-request', JSON.stringify({'hint': true}));
  }

  deleteLeaveRequesState() {
    this.__bhsLeaveRequestState.next(null);
    localStorage.removeItem('hint-leave-request');
  }
}
