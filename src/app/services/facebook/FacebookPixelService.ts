import {Injectable} from '@angular/core';
import {NavigationEnd, Router} from '@angular/router';
declare const fbq: any;
@Injectable()
export class FacebookPixelService {
  constructor(public router: Router) {
    this.load();
    this.router.events.subscribe(event => {
      try {
        if (event instanceof NavigationEnd) {
          (window as any).fbq('track', 'PageView');
        }
         }catch (e) {

      }
    });
  }
  load() {
    // tslint:disable-next-line:only-arrow-functions
    (function(f: any, b, e, v, n, t, s) {
      // tslint:disable-next-line:only-arrow-functions
      if (f.fbq) { return; } n = f.fbq = function() {
        n.callMethod ?
          n.callMethod.apply(n, arguments) : n.queue.push(arguments);
        // tslint:disable-next-line:align
      }; if (!f._fbq) { f._fbq = n; }
      n.push = n; n.loaded = !0; n.version = '2.0'; n.queue = []; t = b.createElement(e); t.async = !0;
      t.src = v; s = b.getElementsByTagName(e)[0]; s.parentNode.insertBefore(t, s);
    })(window, document, 'script', 'https://connect.facebook.net/en_US/fbevents.js');
    (window as any).fbq.disablePushState = true; // not recommended, but can be done
    (window as any).fbq('init', '932295857142604');
    (window as any).fbq('track', 'PageView');
  }
}
