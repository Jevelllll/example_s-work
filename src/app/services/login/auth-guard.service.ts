import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {CanActivate, Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  private subject = new Subject<boolean>();
  private closeLoginForm = new Subject<boolean>();

  constructor(private router: Router) {
  }

  closeLogForm(close: boolean) {
    this.closeLoginForm.next(close);
  }

  getCloseLoginForm(): Observable<boolean> {
    return this.closeLoginForm.asObservable();
  }

  canActivate() {
    if (localStorage.getItem('currentUser')) {
      // logged in so return true
      return true;
    }

    // not logged in so redirect to login page
    this.router.navigate(['']);
    return false;
  }

  checkLogin() {
    if (localStorage.getItem('currentUser')) {
      // logged in so return true
      return true;
    }
    return false;
  }

}
