import {AfterViewInit, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Subject} from 'rxjs/internal/Subject';
import {StoUnService} from '../services/sto/sto-un.service';
import {Sto, StoCity} from '../services/sto/sto-un-models';
import {Observable} from 'rxjs/internal/Observable';
import {distinctUntilChanged, flatMap} from 'rxjs/operators';

@Component({
  selector: 'app-sto',
  templateUrl: './sto.component.html',
  styleUrls: ['./sto.component.scss']
})
export class StoComponent implements OnInit, AfterViewInit {
  p1 = 1;
  itemsPerPage = 4;
  destroyed$ = new Subject<void>();
  public cityList: Observable<StoCity[]>;
  public stoList: Observable<Sto[]> = new Observable();
  public cityForm: FormGroup;

  constructor(
    // tslint:disable-next-line:variable-name
    private stoUnService: StoUnService, private _fb: FormBuilder, private cdref: ChangeDetectorRef) {
  }

  ngAfterViewInit(): void {
    this.cityForm.setValue({cityControl: '0'});
    this.cdref.detectChanges();
  }

  ngOnInit(): void {
    this.cityForm = this._fb.group({
      cityControl: this._fb.control(['0'])
    });
    this.cityList = this.stoUnService.getStoCities().pipe(distinctUntilChanged());
    this.stoList = this.cityForm.get('cityControl').valueChanges.pipe(flatMap((data => {
      this.p1 = 1;
      return this.stoUnService.getAllSto((Number(data) > 0) ? data : 0);
    })));
  }
}
