import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {StoComponent} from './sto.component';

const routes: Routes = [
  {
    path: '', component: StoComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StoRoutingModule { }
