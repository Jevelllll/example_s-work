import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoRoutingModule } from './sto-routing.module';
import { StoComponent } from './sto.component';
import {StoUnService} from '../services/sto/sto-un.service';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import {MatCardModule} from '@angular/material/card';
import {FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
@NgModule({
  declarations: [StoComponent],
  imports: [
    CommonModule,
    StoRoutingModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    ReactiveFormsModule,
    FormsModule,
    MatCardModule,
    NgxPaginationModule
  ],
  providers: [
    StoUnService
  ]
})
export class StoModule { }
