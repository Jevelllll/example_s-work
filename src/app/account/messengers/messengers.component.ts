import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-messengers',
  templateUrl: './messengers.component.html',
  styleUrls: ['./messengers.component.scss']
})
export class MessengersComponent {
  constructor(@Inject(MAT_DIALOG_DATA) public dataMessengers: any) {
  }
}
