import {Component, ElementRef, OnDestroy, OnInit, Renderer2, ViewChild} from '@angular/core';
import {AccountUsers} from '../../../services/accounts/accountIntarface';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AccountService} from '../../../services/accounts/accounts.service';
import {Router} from '@angular/router';
import {LoginService} from '../../../services/auth/login.service';
import {SpinnerService} from '../../../services/spinner.service';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {NgxSpinner} from 'ngx-spinner/lib/ngx-spinner.enum';
import {NgxSpinnerService} from 'ngx-spinner';

@Component({
  selector: 'app-personal-information',
  templateUrl: './personal-information.component.html',
  styleUrls: ['./personal-information.component.scss']
})
export class PersonalInformationComponent implements OnInit, OnDestroy {
  destroyed$ = new Subject<void>();
  public user: any;
  private regExpEmail = new RegExp(/^\w+([\\.-]?\w+)*@\w+([\\.-]?\w+)*(\.\w{2,3})+$/);
  public areaCodeMask = ['(', /[0-9]/, /\d/, /\d/, ')', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
  private dataTouched: any;
  private dataSygnal: any = [];
  @ViewChild('formSignal', { static: true }) formSignal: ElementRef;
  public accountUsers: AccountUsers = new AccountUsers(0, '', '', '', 0);
  private errorMessage: any = '';
  public FormPersonalInfo: FormGroup;
  constructor(private accountService: AccountService,
              private spinnerService: SpinnerService,
              private renderer: Renderer2,
              private router: Router, private loginService: LoginService) {
  }

  /**********инициализация начальными данными************************/
  private formDataInstance(query) {
    Object.keys(this.FormPersonalInfo.controls).forEach(fields => {
      try {
        this.FormPersonalInfo.controls[fields].patchValue(query[fields]);
      } catch (e) {
      }
    });
  }
  getPersonalInfo() {
    this.spinnerService.show();
    this.user = this.read<string>('currentUser');
    try {
      this.accountService.obsUserInfo.subscribe(valResp => {
        if (valResp) {
          this.accountUsers = valResp;
          this.formDataInstance(valResp);
          this.spinnerService.hide();
        } else {
          setTimeout(function() {
            this.accountService.getPersonalInfo(this.user.user_id, this.user.token)
              .pipe(takeUntil(this.destroyed$))
              .subscribe(query => {
                  this.accountUsers = query;
                  this.formDataInstance(valResp);
                },
                error => {
                  try {
                    if (error.auth === false) {
                      // this.loginService.logout();
                      // this.router.navigate(['/']);
                    }
                  } catch (e) {
                  }
                },
                () => {
                  this.spinnerService.hide();
                }
              );
          }.bind(this), 700);
        }
      });
    } catch (e) {
    }
  }

  save(value: any) {
    this.subscribeToUserType();

    this.accountService.userEditUpdate(this.user.user_id, this.user.token, value)
      .pipe(takeUntil(this.destroyed$))
      .subscribe(response => {});

    this.FormPersonalInfo.markAsUntouched();
    this.dataTouched.forEach(x => this.dataTouched.splice(x));
  }


  private subscribeToUserType(): void {
    this.dataTouched = [];
    let children: any;
    children = this.formSignal.nativeElement.children;
    const controls = this.FormPersonalInfo.controls;
    Object.keys(controls)
      .forEach(controlName => {
        const validToched = this.FormPersonalInfo.controls[controlName].touched;
        if (validToched) {
          this.dataTouched.push(controlName);
        }
      });
    for (const child of children) {
      for (let i = 0; i < this.dataTouched.length; i++) {
        if (child.querySelector('span') == null) {
        } else {
          if (child.querySelector('span').getAttribute('data-fieldName') === this.dataTouched[i]) {
            this.renderer.setStyle(child.querySelector('span'), 'display', 'block');
            setTimeout(function() {
              this.renderer.addClass(child.querySelector('span'), 'transition-s');
            }.bind(this), 1);
            setTimeout(function() {
              this.FormPersonalInfo.markAsUntouched();
              this.resetCustom();
            }.bind(this), 3000);
          }
        }
      }
    }
  }

  private resetCustom() {
    const children = this.formSignal.nativeElement.children;
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < children.length; i++) {
      if (children[i].querySelector('span') == null) {
      } else {
        this.renderer.removeClass(children[i].querySelector('span'), 'transition-s');
        setTimeout(function() {
          this.renderer.setStyle(children[i].querySelector('span'), 'display', 'none');
        }.bind(this), 1500);
      }
    }
  }

  private formPersonalInfoRun() {
    this.FormPersonalInfo = new FormGroup({
      email: new FormControl({value: '', disabled: true},
        [Validators.pattern(/^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i)]),
      first_name: new FormControl({value: '', disabled: true}, [Validators.minLength(2), Validators.maxLength(20)]),
      last_name: new FormControl({value: '', disabled: true}, [Validators.minLength(2), Validators.maxLength(20)]),
      phone: new FormControl({value: '', disabled: true}),
      date_year: new FormControl({value: '', disabled: true}),
    });
  }

  ngOnInit() {
    this.formPersonalInfoRun();
    this.getPersonalInfo();
  }

  read<T>(key: string): T {
    const value: string = localStorage.getItem(key);

    // tslint:disable-next-line:triple-equals
    if (value && value != 'undefined' && value != 'null') {
      return JSON.parse(value) as T;
    }

    return null;
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }
}
