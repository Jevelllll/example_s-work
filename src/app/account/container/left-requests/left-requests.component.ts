import {Component, ComponentFactoryResolver, OnDestroy, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {OF_GROUP_TABLES} from './helperTables';
import {GroupingService} from '../../../services/accounts/grouping.service';
import {GroupCategoryComponent} from './grouping/group-category/group-category.component';

@Component({
  selector: 'app-left-requests',
  templateUrl: './left-requests.component.html',
  styleUrls: ['./left-requests.component.scss']
})
export class LeftRequestsComponent implements OnInit, OnDestroy {
  destroyed$ = new Subject<void>();
  @ViewChild('dynamicLeftRequest', {static: true, read: ViewContainerRef}) dynamicLeftRequest: ViewContainerRef;

  constructor(private componentFactoryResolver: ComponentFactoryResolver,
              private groupService: GroupingService) {
  }

  private customFactory(dynamic: any) {
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(dynamic);
    this.dynamicLeftRequest.clear();
    this.dynamicLeftRequest.createComponent(componentFactory);
  }

  // private basicTable(): void {
  //   this.customFactory(BasicTableComponent);
  // }

  private groupCategory(): void {
    this.customFactory(GroupCategoryComponent);
  }

  // private groupTariff(): void {
  //   this.customFactory(GroupTariffComponent);
  // }

  private currentComponent(nameSortGroup: string): void {
    setTimeout(function() {
      if (nameSortGroup == 'category') {
        this.groupCategory();
      } else if (nameSortGroup == 'tariff') {
        this.groupTariff();
      } else {
        this.basicTable();
      }
    }.bind(this), 700);
  }

  ngOnInit() {

    if (OF_GROUP_TABLES) {
      this.groupCategory();
    } else {
      // this.basicTable();
      /**********************отвечает за групировку таблиц **10 07 19********/
      this.groupService.getGroup()
        .pipe(takeUntil(this.destroyed$))
        .subscribe(name => {
          this.currentComponent(name);
        });
      /*************************************************************/
    }

  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }

}
