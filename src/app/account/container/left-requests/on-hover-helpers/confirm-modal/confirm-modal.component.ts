import { Component, OnInit } from '@angular/core';
import {Subject} from 'rxjs';
import {ModalDataService} from '../../../../../services/accounts/modal-data.service';
import { trigger, transition, style, animate } from '@angular/animations';

@Component({
  selector: 'app-confirm-modal',
  templateUrl: './confirm-modal.component.html',
  styleUrls: ['./confirm-modal.component.scss'],
  animations: [
    trigger('dialog', [
      transition('void => *', [
        style({transform: 'translateX(-100%)'}),
        animate(350)
      ]),
      transition('* => void', [
        animate(350, style({transform: 'scale3d(.0, .0, .0)'}))
      ])
    ])
  ]
})
export class ConfirmModalComponent implements OnInit {


  public onClose: Subject<boolean>;

  constructor(private modalDataService: ModalDataService) {
  }

  public ngOnInit(): void {
    this.onClose = new Subject();
  }

  public onConfirm(): void {
    this.onClose.next(true);
    this.modalDataService.setConfirmState(true);
    this.modalDataService.setMatModalClose();
  }

  public onCancel(): void {
    this.onClose.next(false);
    this.modalDataService.setConfirmState(false);
    this.modalDataService.setMatModalClose();
  }

}
