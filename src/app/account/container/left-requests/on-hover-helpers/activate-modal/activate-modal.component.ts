import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subject} from 'rxjs';
import {takeUntil, first} from 'rxjs/operators';
import {Tariffinterface} from '../../../../../services/priority/resModel';
import {ModalDataService} from '../../../../../services/accounts/modal-data.service';
import {ToastrService} from 'ngx-toastr';
import {TariffService} from '../../../../../services/tariff/tariff.service';
import {AmountService} from '../../../../../services/pay/amount.service';
import {Router} from '@angular/router';
import {SpinnerService} from '../../../../../services/spinner.service';
import {TariffBaseMethod} from '../../../../../leave-request/container/tariff/tariff-base-method';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {getBalanceWalletPay, newWalletPay} from '../common-wallet-pay-helper';
import {NgxSpinnerService} from 'ngx-spinner';
import { trigger, transition, animate, style } from '@angular/animations';

@Component({
  selector: 'app-activate-modal',
  templateUrl: './activate-modal.component.html',
  styleUrls: ['./activate-modal.component.scss'],
  animations: [
    trigger('dialog', [
      transition('void => *', [
        style({transform: 'translateX(-100%)'}),
        animate(350)
      ]),
      transition('* => void', [
        animate(350, style({transform: 'scale3d(.0, .0, .0)'}))
      ])
    ])
  ]
})
export class ActivateModalComponent implements OnInit, OnDestroy {
  destroyed$ = new Subject<void>();
  // tslint:disable-next-line:variable-name
  public t_activate = true;
  public t_st: any;
  // tslint:disable-next-line:variable-name
  public t_end: any;
  /**************кнопка кошелек**********************/
  public isWalletPay = false;
  public foolOrderId = null;
  public balance = null;
  public balanceVisible = true;

  public theHtmlString: any;
  public errorb: any = [];
  private tariffVaseObj: any;
  public model: any;
  public onClose: Subject<boolean>;
  public FormActivated: FormGroup;
  // tslint:disable-next-line:variable-name
  public sum_day_old_query_tariff = 0;
  // tslint:disable-next-line:variable-name variable-name
  public sum_day_query_tariff = 0;
  // tslint:disable-next-line:variable-name
  public sum_day: number;
  public saving = 0;
  // tslint:disable-next-line:variable-name
  public query_tariff_start;
  // tslint:disable-next-line:variable-name
  public query_tariff_end;
  private submited = false;
  public minDate = new Date();
  // tslint:disable-next-line:variable-name
  public pay_visible: boolean;

  constructor(private sendDatamodal: ModalDataService,
              // tslint:disable-next-line:variable-name
              // private _bsModalRef: MDBModalRef,
              private toastr: ToastrService,
              private tariffService: TariffService,
              private amountService: AmountService,
              private router: Router,
              private spinnerService: SpinnerService) {
  }

  ngOnInit() {
    this.pay_visible = false;
    this.onClose = new Subject();
    this.model = this.sendDatamodal.getDataActive();
    this.query_tariff_start = new Date(this.model.query_tariff_start * 1000);
    this.query_tariff_end = new Date(this.model.query_tariff_end * 1000);
    this.tariffVaseObj = new TariffBaseMethod(this.model.tariff[0]);
    if (this.tariffVaseObj.tariffData.sum_day == 0) {
      this.balanceVisible = false;
    }
    this.FormActivated = new FormGroup({
      id: new FormControl(0, [<any> Validators.required]),
      query_tariff_start: new FormControl(this.query_tariff_start, [<any> Validators.required]),
      query_tariff_end: new FormControl(this.query_tariff_end, [<any> Validators.required]),
    });
    this.tariffVaseObj.optionDate('go', this.FormActivated, this.model.query_tariff_start, this.model.query_tariff_end);
    this.baseDataInit();
    if (this.errorb.length > 0) {
      setTimeout(() => this.toastr.info(this.errorb[0]));

    }
    this.subcribeToFormChanges();

    this.getBalanceWalletPay();
    this.foolOrderId = this.model.query_id + '/' + this.model.titleQuery.toLowerCase();
  }

  public onConfirm(): void {
    this.onClose.next(true);
    this.sendDatamodal.setMatModalClose();
  }

  changeDataFieldWithError() {
    if (this.errorb.length > 0) {
      this.pay_visible = false;
      this.toastr.info(this.errorb[0]);
    }
  }

  private baseDataInit() {
    this.sum_day_query_tariff = this.tariffVaseObj.getSum_day_query_tariff();
    this.sum_day_old_query_tariff = this.tariffVaseObj.getSum_day_old_taridd();
    this.saving = this.tariffVaseObj.getSaving();
    this.errorb = [];
    this.errorb = this.tariffVaseObj.getErrors();
  }

  subcribeToFormChanges() {
    const myFormChanges$ = this.FormActivated.valueChanges;
    myFormChanges$
      .pipe(takeUntil(this.destroyed$))
      // tslint:disable-next-line:variable-name
      .subscribe(query_tariff => {
        this.tariffVaseObj.optionDate('go', this.FormActivated, query_tariff.query_tariff_start / 1000,
          query_tariff.query_tariff_end / 1000);
        this.baseDataInit();
        this.isWalletPay = false;
        this.pay_visible = false;
      });
    this.FormActivated.controls['id'].patchValue(this.model.tariff[0].id);
  }

  public onCancel(): void {
    this.onClose.next(false);
    this.sendDatamodal.setMatModalClose();
    // this._bsModalRef.hide();
  }

  save(model: Tariffinterface, isValid: boolean) {


    if (this.balance && this.balance >= this.sum_day_query_tariff) {
      this.isWalletPay = true;
      this.t_st = Math.round(model.query_tariff_start);
      this.t_end = Math.round(model.query_tariff_end);
    } else {
      this.isWalletPay = false;
      this.spinnerService.show();
      setTimeout(() => {
        this.tariffService.postActivateLiqPay(this.sum_day_query_tariff,
          this.model.query_id + '/' + this.model.titleQuery.toLowerCase(),
          Math.round(model.query_tariff_start), Math.round(model.query_tariff_end))
          .pipe(first(), takeUntil(this.destroyed$))
          .subscribe(
            lPay => {
              this.theHtmlString = lPay.trim();
              this.spinnerService.hide();
            }, error => {
              this.toastr.error('Повторите попытку', 'Ошибка сервера');
            },
            () => {
              this.errorb.push(['hide']);
              this.pay_visible = true;
              this.spinnerService.hide();
            }
          );
      }, 30);
    }
    this.submited = true;
  }

  /*******************оплатить через кошелек*************************************/
  public newWalletPay() {
    newWalletPay.call(this);
  }

  public getBalanceWalletPay() {
    getBalanceWalletPay.call(this);
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }

}
