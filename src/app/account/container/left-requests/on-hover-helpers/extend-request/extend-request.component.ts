import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subject} from 'rxjs';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ModalDataService} from '../../../../../services/accounts/modal-data.service';
import {ToastrService} from 'ngx-toastr';
import {AmountService} from '../../../../../services/pay/amount.service';
import {TariffService} from '../../../../../services/tariff/tariff.service';
import {SpinnerService} from '../../../../../services/spinner.service';
import {TariffBaseMethod} from '../../../../../leave-request/container/tariff/tariff-base-method';
import {takeUntil, first} from 'rxjs/operators';
import {Tariffinterface} from '../../../../../services/priority/resModel';
import {getBalanceWalletPay, newWalletPay} from '../common-wallet-pay-helper';
import {NgxSpinnerService} from 'ngx-spinner';

@Component({
  selector: 'app-extend-request',
  templateUrl: './extend-request.component.html',
  styleUrls: ['./extend-request.component.scss']
})
export class ExtendRequestComponent implements OnInit, OnDestroy {
  destroyed$ = new Subject<void>();
  // tslint:disable-next-line:variable-name
  t_activate = false;
  // tslint:disable-next-line:variable-name
  public t_st: any;
  // tslint:disable-next-line:variable-name
  public t_end: any;
  /**************кнопка кошелек**********************/
  public isWalletPay = false;
  public foolOrderId = null;
  public balance = null;

  /**********************промежуточный страрт тарифа*****/
  private preQueryTariffStart = 0;
  public balanceVisible = true;
  public theHtmlString: any;
  public errorb: any = [];
  private tariffVaseObj: any;
  public model: any;
  public onClose: Subject<boolean>;
  public FormActivated: FormGroup;
  // tslint:disable-next-line:variable-name
  public sum_day_old_query_tariff = 0;
  // tslint:disable-next-line:variable-name
  public sum_day_query_tariff = 0;
  // tslint:disable-next-line:variable-name
  public sum_day: number;
  public saving = 0;
  // tslint:disable-next-line:variable-name
  public query_tariff_start;
  // tslint:disable-next-line:variable-name
  public query_tariff_end;
  // tslint:disable-next-line:variable-name
  public query_tariff_extend;
  public det_data;
  private submited = false;
  public minDate = new Date();
  // tslint:disable-next-line:variable-name
  public pay_visible: boolean;

  // tslint:disable-next-line:variable-name
  private query_tariff_startOLDSAVE;

  constructor(private sendDatamodal: ModalDataService,
              private toastr: ToastrService,
              private amountService: AmountService,
              private tariffService: TariffService,
              private spinnerService: SpinnerService) {
  }

  ngOnInit() {
    this.pay_visible = false;
    this.onClose = new Subject();
    this.model = this.sendDatamodal.getDataExtend();

    /********нужно для заполнение поля действителен до*************/
    this.query_tariff_startOLDSAVE = new Date(this.model.query_tariff_start * 1000);

    this.query_tariff_start = new Date(this.model.query_tariff_start * 1000);
    this.query_tariff_extend = new Date(this.model.query_tariff_end * 1000);

    this.tariffVaseObj = new TariffBaseMethod(this.model.tariff[0]);

    if (this.tariffVaseObj.tariffData.sum_day == 0) {
      this.balanceVisible = false;
    }
    this.FormActivated = new FormGroup({
      id: new FormControl(0, [<any> Validators.required]),
      query_tariff_start: new FormControl(this.query_tariff_extend, [<any> Validators.required]),
      query_tariff_end: new FormControl('', [<any> Validators.required]),
    });


    this.tariffVaseObj.optionDate('go', this.FormActivated, this.query_tariff_extend, this.model.query_tariff_end / 1000);

    this.baseDataInit();

    this.subcribeToFormChanges();
    this.getBalanceWalletPay();
    this.foolOrderId = this.model.query_id + '/' + this.model.titleQuery.toLowerCase();
  }

  public onConfirm(): void {
    this.onClose.next(true);
    this.sendDatamodal.setMatModalClose();
  }

  changeDataFieldWithError() {
    if (this.errorb.length > 0) {
      this.pay_visible = false;
      this.toastr.info(this.errorb[0]);
    }
  }

  private baseDataInit() {
    this.sum_day_query_tariff = this.tariffVaseObj.getSum_day_query_tariff();
    this.sum_day_old_query_tariff = this.tariffVaseObj.getSum_day_old_taridd();
    this.saving = this.tariffVaseObj.getSaving();
    this.errorb = [];
    this.errorb = this.tariffVaseObj.getErrors();
  }

  subcribeToFormChanges() {
    const myFormChanges$ = this.FormActivated.valueChanges;
    // tslint:disable-next-line:variable-name
    myFormChanges$
      .pipe(takeUntil(this.destroyed$))
      // tslint:disable-next-line:variable-name
      .subscribe(query_tariff => {
        this.tariffVaseObj.optionDate('go', this.FormActivated, query_tariff.query_tariff_start / 1000,
          query_tariff.query_tariff_end / 1000);
        this.baseDataInit();
        this.isWalletPay = false;
        this.pay_visible = false;
      });
    this.FormActivated.controls['id'].patchValue(this.model.tariff[0].id);
  }

  public onCancel(): void {
    this.onClose.next(false);
    this.sendDatamodal.setMatModalClose();
  }

  save(model: Tariffinterface, isValid: boolean) {

    if (this.balance >= this.sum_day_query_tariff) {
      this.isWalletPay = true;
      this.t_st = model.query_tariff_start;
      this.t_end = model.query_tariff_end;
    } else {
      this.spinnerService.show();
      setTimeout(() => {
        this.tariffService.postActivateLiqPay(this.sum_day_query_tariff,
          this.model.query_id + '/' + this.model.titleQuery.toLowerCase(), model.query_tariff_start, model.query_tariff_end)
          .pipe(first(), takeUntil(this.destroyed$))
          .subscribe(
            lPay => {
              this.theHtmlString = lPay;
              this.spinnerService.hide();
            }, error => {
              this.toastr.error('Повторите попытку', 'Ошибка сервера');
              this.spinnerService.hide();
            },
            () => {
              this.errorb.push(['hide']);
              this.pay_visible = true;
              this.spinnerService.hide();
            }
          );
      }, 10);
    }

    this.submited = true;
  }

  /*******************оплатить через кошелек*************************************/
  public newWalletPay() {
    newWalletPay.call(this);
  }

  public getBalanceWalletPay() {
    getBalanceWalletPay.call(this);
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }

}
