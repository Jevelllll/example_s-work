import {takeUntil} from 'rxjs/operators';

export function newWalletPay() {
  // query_tariff_end

  // tslint:disable-next-line:variable-name
  const query_tariff_extend = getUtcUnixTime(this.query_tariff_extend);
  // tslint:disable-next-line:variable-name
  const query_tariff_start = getUtcUnixTime(this.query_tariff_start);
  // tslint:disable-next-line:variable-name
  const query_tariff_end = getUtcUnixTime(this.t_end);
  // tslint:disable-next-line:variable-name
  const payModel = {
    order_id: this.foolOrderId, activate: true,
    query_tariff_start, query_tariff_end,
    query_tariff_extend,
    sum_day_query_tariff: this.sum_day_query_tariff
  };
  if (!query_tariff_extend) {
    delete payModel.query_tariff_extend;
  }

  this.amountService.newWalletPay(payModel)
    .pipe(takeUntil(this.destroyed$))
    .subscribe(result => {
      let successMsg = 'Заявка продлена';
      if (result['msg']) {
        if (result['msg'] === 'success') {
          let newModel = this.model;

          if (!query_tariff_extend) {
            newModel.query_tariff_end = getUtcUnixTime(this.t_end);
            newModel.query_tariff_start = getUtcUnixTime(this.t_st);
          } else {
            newModel.query_tariff_end = getUtcUnixTime(this.t_end);
            newModel.query_tariff_start = getUtcUnixTime(this.query_tariff_startOLDSAVE);
          }

          if (this.t_activate) {
            successMsg = 'Заявка активирована';
            newModel.query_activated = 1;
            newModel.paid_for = 1;
          }
          this.sendDatamodal.setmodelChange(newModel);
          this.onCancel();
          this.toastr.success(successMsg);
          this.amountService.getCurrentBalance()
            .pipe(takeUntil(this.destroyed$))
            .subscribe(() => {
            });
        }
      }
    }, error1 => {

      this.toastr.error('Платеж не прошел');
    }, () => {

    });
}

export function getBalanceWalletPay() {
  this.amountService.balanceObs
    .pipe(takeUntil(this.destroyed$))
    .subscribe(result => {
      if (result !== null) {
        this.balance = result.balance;
      } else {
        try {
          this.amountService.getCurrentBalance()
            .pipe(takeUntil(this.destroyed$))
            .subscribe(value => {
              this.balance = value.balance;
            }, error => {
            });
        } catch (e) {
        }
      }
    }, error => {
    });
}

function getUtcUnixTime(date) {
  return (+new Date(date)
    - new Date(date).getTimezoneOffset() * 60 * 1000)
    / 1000;
}
