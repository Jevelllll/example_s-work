import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {errorAssocc} from '../../../../../shared/fieldErrortranslation';
import {clearObject} from '../../../../../shared/helper-container-leave';
import {Subject} from 'rxjs';
import {ModalDataService} from '../../../../../services/accounts/modal-data.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-info-help',
  templateUrl: './info-help.component.html',
  styleUrls: ['./info-help.component.scss']
})
export class InfoHelpComponent implements OnInit {

  @Input() model;

  @Output() modalHide: any = new EventEmitter();
  public moreData = [];
  public queryExtraData = {
    value: null,
    name: null
  };
  public SANITIZER: any;
  public onClose: Subject<boolean>;

  closeDialog(event) {
    this.modalHide.emit(event);
  }

  public onCancel(): void {
    this.onClose.next(false);
    this.sendDatamodal.setMatModalClose();
    // this._bsModalRef.hide();
  }

  // tslint:disable-next-line:variable-name
  public count_day: number = 0;

  constructor(private sendDatamodal: ModalDataService, private sanitizer: DomSanitizer
              // private _bsModalRef: MDBModalRef,
  ) {
    this.SANITIZER = this.sanitizer;
  }

  ngOnInit() {
    this.onClose = new Subject();
    this.model = this.sendDatamodal.getDataExtend();
    this.count_day = this.getDayDelta(this.model.query_tariff_start * 1000, this.model.query_tariff_end * 1000);
    this.addMoreInfo();
  }

  /***********расширяем информацию*****************/
  private addMoreInfo() {
    if (this.model.more_details) {
      const cleanObj = clearObject(this.model.more_details);
      this.moreData = Object.keys(cleanObj).map(value => {

        if (value === 'query_extra_data') {
          if (typeof this.model.more_details[value] === 'object') {
            const preExtraList = Object.keys(this.model.more_details[value]).map(strExtra => {
              let preArrayFields = [];
              try {
                preArrayFields = JSON.parse(this.model.more_details[value][strExtra]);
              } catch (e) {
                preArrayFields = [];
              }
              if (Array.isArray(preArrayFields) && preArrayFields.length > 0) {
                preArrayFields = preArrayFields.map(valueExtra => {
                    if (this.isKyr(strExtra)) {
                      return {
                        name: valueExtra.name.replace(strExtra, '').trim(),
                        value: valueExtra.val || valueExtra.data, isViewVal: false
                      };
                    } else {
                      const strAssoc = errorAssocc[strExtra];
                      if (strAssoc.hasOwnProperty('isViewVal')) {
                        return {name: valueExtra.name.replace(strAssoc.ru, '').trim(), value: valueExtra.val, isViewVal: strAssoc.isViewVal};
                      }
                    }

                  }
                );
                return {name: (this.isKyr(strExtra)) ? strExtra : errorAssocc[strExtra].ru, value: preArrayFields};
              }
              return false;
            }).filter(v => v);
            this.queryExtraData = {name: 'extra', value: preExtraList};
          }
        } else if (value === 'query_skip_words') {
          return {name: errorAssocc[value].ru, value: this.model.more_details[value]};
        } else {
          if (this.model.more_details[value]) {
            return {name: errorAssocc[value].ru, value: this.model.more_details[value]};
          }
        }
      }).filter(val => val);
    }

  }

  isKyr(str) {
    return /[а-я]/i.test(str);
  }

  private getDayDelta(dataStart, dataEnd) {
    let date1;
    let date2;
    let timeDiff;
    let diffDays;
    date1 = new Date(dataStart);
    date2 = new Date(dataEnd);
    timeDiff = Math.abs(date2.getTime() - date1.getTime());
    diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
    return diffDays;
  }

}
