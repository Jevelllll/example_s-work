import {AfterViewInit, Component, ElementRef, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import * as moment from 'moment';
import {BehaviorSubject, Observable, Subject} from 'rxjs';
import {AccountService} from '../../../../services/accounts/accounts.service';
import {MatDialog} from '@angular/material/dialog';
import {ModalDataService} from '../../../../services/accounts/modal-data.service';
import {first, takeUntil} from 'rxjs/operators';
import {StatisticsService} from '../../../../services/accounts/statistics.service';
import {ShowFoundHelpComponent} from './show-found-help/show-found-help.component';
import {InfoHelpComponent} from './info-help/info-help.component';
import {ExtendRequestComponent} from './extend-request/extend-request.component';
import {ActivateModalComponent} from './activate-modal/activate-modal.component';
import {ConfirmModalComponent} from './confirm-modal/confirm-modal.component';
import {animate, style, transition, trigger} from '@angular/animations';

@Component({
  selector: 'app-on-hover-helpers',
  templateUrl: './on-hover-helpers.component.html',
  styleUrls: ['./on-hover-helpers.component.scss'],
  animations: [
    trigger('dialog', [
      transition('void => *', [
        style({transform: 'scale3d(.3, .3, .3)'}),
        animate(350)
      ]),
      transition('* => void', [
        animate(350, style({transform: 'scale3d(.0, .0, .0)'}))
      ])
    ])
  ]
})
export class OnHoverHelpersComponent implements OnInit, OnDestroy, AfterViewInit {
  destroyed$ = new Subject<void>();
  public moment: any = moment;
  @Output() del: EventEmitter<any> = new EventEmitter<any>(true);
  @Input() model;
  @ViewChild('howhelp', {static: true}) howhelp: ElementRef;
  // tslint:disable-next-line:variable-name
  private _modelBSubject: BehaviorSubject<any>;
  // tslint:disable-next-line:variable-name
  public __obsWidth: Observable<any> = new Observable();
  // tslint:disable-next-line:variable-name
  private count_notif: any;

  animate = false;

  constructor(
    private statistics: StatisticsService,
    private accountService: AccountService,
    public dialog: MatDialog,
    private sendDatamodal: ModalDataService) {
  } // {2}

  // tslint:disable-next-line:variable-name
  public openShowFoundDialog(notif_count: any) {
    const observable = this._modelBSubject.asObservable();
    observable
      .pipe(takeUntil(this.destroyed$), first())
      // tslint:disable-next-line:no-shadowed-variable variable-name
      .subscribe(notif_count => {

        notif_count = Number(notif_count.notification_count);
        if (notif_count === 0) {
        } else {
          this.sendDatamodal.sendExtendData(this.model);
          this.dialog.open(ShowFoundHelpComponent, {
            width: '600px',
            minHeight: '170px',
            panelClass: 'custom-modalbox'
          });
        }
      }, error => {
      });

  }

  @Input()
  set clickListTr(data) {
    if (data) {
      this.openShowFoundDialog('');
    }
  }

  public openModal() {
    this.dialog.open(InfoHelpComponent, {
      width: '600px',
      minHeight: '170px',
      panelClass: 'custom-modalbox'
    });
    this.sendDatamodal.sendExtendData(this.model);
  }

  public openExtendDialog() {
    this.sendDatamodal.sendExtendData(this.model);
    this.dialog.open(ExtendRequestComponent, {
      // width: '600px',
      // height: '375px',
      panelClass: 'custom-modalbox'
    });
  }

  public openActivateDialog() {
    this.sendDatamodal.sendActiveData(this.model);
    this.dialog.open(ActivateModalComponent, {
      // width: '600px',
      // height: '375px',
      panelClass: 'custom-modalbox'
    });
  }

  openConfirmDialog() {

    this.dialog.open(ConfirmModalComponent, {
      width: '600px',
      panelClass: 'custom-modalbox'
    });
    this.sendDatamodal.getConfirmState()
      .pipe(takeUntil(this.destroyed$), first())
      .subscribe(result => {

        if (result === true) {
          this.statistics.updateStatistic();
          this.sendDatamodal.sendData(this.model);

        } else {
          this.sendDatamodal.sendData('error');
        }
      }, error => {
      });

  }


  ngOnInit() {

    this.__obsWidth = this.accountService.__obsResizeWidth;
    this._modelBSubject = new BehaviorSubject<any>(this.model);
    const observable = this.accountService.bHSDataCountNotif.asObservable();
    observable
      .pipe(takeUntil(this.destroyed$))
      .subscribe(value => {

        if (Array.isArray(value)) {

          const newVount = value.filter(
            result => result.query_id === this.model.query_id && result.titleQuery === this.model.titleQuery)[0];
          this.model = Object.assign({}, this.model, newVount);

          if (Number(this.count_notif) > 0) {
            this.count_notif = newVount.notification_count;
            try {
              this.model.notification_count = this.count_notif;
            } catch (e) {

            }
          } else {
            try {
              this.model.notification_count = newVount.notification_count;
            } catch (e) {

            }

          }
          this._modelBSubject.next(this.model);
        }

      }, error => {
      });
  }

  ngAfterViewInit() {

    this.sendDatamodal.getMatModalClose().pipe(takeUntil(this.destroyed$)).subscribe((dataRes) => {
      this.dialog.closeAll();
    });
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }

}
