import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'showCoast'
})
export class ShowCoastPipe implements PipeTransform {

  transform(value: any, service = null, currency = null): any {
    if (value) {
      const serviceCurrencyOnlyUSD = ['AUTO_RIA', 'DOM_RIA'];
      let str: any = 'Не указана';
      const skipNull = ['от', 'до', '-'];
      if (!serviceCurrencyOnlyUSD.includes(service)) {
        if (currency || currency != 'null') {
          if (value) {
            str = value.replace(currency, '').replace('null', '');
            str = (str.length > 4) ? str : 'Не указана';
          }
        }
      } else {
        str = value;
      }
      return str;
    }
  }

}
