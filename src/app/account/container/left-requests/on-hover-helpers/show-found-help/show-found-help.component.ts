import {AfterViewInit, Component, ElementRef, EventEmitter, Input, OnDestroy, OnInit, Output, QueryList, ViewChildren} from '@angular/core';
import {IShowFoundHelp} from '../../../../../services/accounts/accountIntarface';
import {Subject} from 'rxjs';
import {ModalDataService} from '../../../../../services/accounts/modal-data.service';
import {AccountService} from '../../../../../services/accounts/accounts.service';
import {ToastrService} from 'ngx-toastr';
import {first, takeUntil} from 'rxjs/operators';
import {translitCurrency} from '../../../../../shared/helper-container-leave';

@Component({
  selector: 'app-show-found-help',
  templateUrl: './show-found-help.component.html',
  styleUrls: ['./show-found-help.component.scss']
})
export class ShowFoundHelpComponent implements OnInit, AfterViewInit, OnDestroy {
  destroyed$ = new Subject<void>();
  @Input() model;
  @Output() modalHide: any = new EventEmitter();
  @ViewChildren('elementGroup') elementGroup: QueryList<ElementRef>;
  @ViewChildren('linkSelector') matExpansionPanelContent: any;
  indexArrows = 0;
  // tslint:disable-next-line:variable-name
  public show_found: IShowFoundHelp[] = [];
  hideArrows = false;
  public showFoundGroupsByDate: any[] = [];
  public showFoundGroupsPreList: any[] = [];
  private user: any;
  public onClose: Subject<boolean>;
  currentGroup: any;
  foundSQ: any[];
  public active = {};
  panelOpenState = false;
  step = 0;

  // tslint:disable-next-line:variable-name
  constructor(
    private sendDatamodal: ModalDataService,
    private toster: ToastrService,
    private accountService: AccountService) {
  }

  setStep(index: number) {
    this.step = index;
  }

  closeDialog(event) {
    this.modalHide.emit(event);
  }

  visibleList(startStep = 0, nextStep = 2, isDef = false) {

    this.showFoundGroupsPreList = [];
    if (isDef) {
      for (let ind = 0; ind < this.showFoundGroupsByDate.length; ind++) {
        if (this.showFoundGroupsByDate[ind]) {
          if (this.showFoundGroupsByDate[ind].dataList) {
            this.showFoundGroupsPreList.push(this.showFoundGroupsByDate[ind]);
          }
        }
      }
    } else {
      for (let ind = startStep; ind < nextStep; ind++) {

        this.showFoundGroupsPreList.push(this.showFoundGroupsByDate[ind]);
      }

    }

  }

  nexArrow(): void {
    this.foundSQ = [];
    this.showFoundGroupsPreList = [];
    const countGroup = this.showFoundGroupsByDate.length;
    this.indexArrows = this.indexArrows + 2;
    if (this.indexArrows >= countGroup) {
      this.indexArrows = 0;
    }
    this.visibleList(this.indexArrows, this.indexArrows + 2);
    this.cleanActive();
  }

  cleanActive() {
    const elementNodeListOf = document.getElementsByClassName('nav-link active');
    [].slice.call(elementNodeListOf).map((el) => {
      el.classList.remove('active');
    });

  }

  prevArrow(): void {
    this.foundSQ = [];
    const countGroup = this.showFoundGroupsByDate.length;
    this.indexArrows = this.indexArrows - 2;
    if (this.indexArrows <= countGroup && this.indexArrows < 0) {
      this.indexArrows = countGroup - 2;
    }
    this.visibleList(this.indexArrows, this.indexArrows + 2);
    this.cleanActive();
  }

  private static generateTblName(model: any) {
    // tslint:disable-next-line:variable-name
    let name_db = '';
    // tslint:disable-next-line:variable-name
    const model_name = model.titleQuery;
    switch (model_name) {
      case 'Electronics':
        name_db = 'query_electronics';
        break;
      case 'Auto':
        name_db = 'tbl_query_auto';
        break;
      case 'Property':
        name_db = 'query_property';
        break;
      case 'Other':
        name_db = 'query_other';
        break;
      case  'Autoproducts':
        name_db = 'query_autoproducts';
        break;
      case  'Work':
        name_db = 'query_work';
        break;
      case  'Legal':
        name_db = 'query_legal';
        break;
      case 'Quest':
        name_db = 'query_quest';
        break;
    }
    return name_db;
  }

  /****************обновляем просмотр нового оповещения*******************************************/
  updateNotification(fDataList: IShowFoundHelp) {
    if (fDataList.isRead) {
      try {
        this.accountService.updateReadNotification(fDataList.id_notification,
          this.user.user_id, this.user.token).subscribe(result => {
          if (result.status === 200 && result.msg === 'data_update') {
            fDataList.isRead = false;
            return fDataList;
          }
        }, error => {
          if (error.status) {
            if (error.status === 303) {
            } else {
              this.toster.error('Ошибка Сервера!');
            }
          }
        });
      } catch (e) {
        this.toster.error('Ошибка Сервера!');
      }
    }
    return fDataList;

  }


  ngOnInit() {

    this.onClose = new Subject();
    this.user = this.read<string>('currentUser');
    this.model = this.sendDatamodal.getDataExtend();
    const query = this.model;

    this.accountService
      .getDatasShowFoundHelp(this.user.user_id, this.user.token, query.query_id, ShowFoundHelpComponent.generateTblName(query))
      .pipe(takeUntil(this.destroyed$))
      .subscribe(value => {
        const groupByName = {};
        value.forEach((a) => {
          const serviceCurrencyOnlyUSD = ['AUTO_RIA', 'DOM_RIA'];
          groupByName[a.find_data] = groupByName[a.find_data] || [];
          groupByName[a.find_data].push({
            query_id: a.query_id, name: a.name,
            href: a.href, find_time: a.find_time,
            service: a.service,
            coast: `${a.coast}${(!serviceCurrencyOnlyUSD.includes(a.service)) ? ' ' + a.currency_description_short : ' USD'}`,
            notif_phone: a.notif_phone,
            isRead: (a.isRead === '0' || a.isRead === 0) ? true : false,
            id_notification: a.id_notification,
            query_currency: translitCurrency(a.query_currency) || a.currency_description_short
          });
          return groupByName;
        });

        // tslint:disable-next-line:forin
        for (const group in groupByName) {
          this.showFoundGroupsByDate.push({group: [group], dataList: groupByName[group]});
        }
        this.showFoundGroupsByDate = this.showFoundGroupsByDate.sort((a, b) => {
          return +new Date(b.group[0]) - +new Date(a.group[0]);
        });

        try {
          const isEvenNUM = this.showFoundGroupsByDate.length % 2;
          if (isEvenNUM > 0) {
            this.showFoundGroupsByDate.push({group: [null], dataList: groupByName['00-00-00']});
          }
        } catch (e) {
        }
        this.hideArrowOneGroup();

        // this.visibleList();
        this.visibleList(0, 0, true);
        if (value === undefined) {
        } else {
          value.map(item => {
            this.show_found.push(item);
          });
        }
      });
  }

  private hideArrowOneGroup() {
    /******************если одно значение****************************/
    const countGroup = this.showFoundGroupsByDate.filter(group => group.group[0] !== null).length;
    setTimeout(() => {
      try {
        this.elementGroup.first.nativeElement.querySelector('a').click();
      } catch (e) {
      }
    }, 1);
  }

  public onConfirm(): void {
    this.onClose.next(true);
    this.sendDatamodal.setMatModalClose();
  }

  public onCancel(): void {
    this.onClose.next(false);
    this.sendDatamodal.setMatModalClose();
  }

  public read<T>(key: string): T {
    const value: string = localStorage.getItem(key);
    if (value && value != 'undefined' && value != 'null') {
      return <T> JSON.parse(value);
    }

    return null;
  }

  ngAfterViewInit(): void {
    const matExpansionPanelContent1 = this.matExpansionPanelContent;
    matExpansionPanelContent1.changes
      .pipe(first(), takeUntil(this.destroyed$))
      .subscribe(matPanel => {
        setTimeout(() => {
          matExpansionPanelContent1.first.open();
        }, 0);
      });
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }

}
