import {AfterViewInit, Component, ElementRef, Inject, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {notificationCount, OF_GROUP_TABLES, removeQuery} from '../../helperTables';
import {AccountService} from '../../../../../services/accounts/accounts.service';
import {ToastrService} from 'ngx-toastr';
import {GroupingService} from '../../../../../services/accounts/grouping.service';
import {SpinnerService} from '../../../../../services/spinner.service';
import {GetAllQueryList} from '../../../../../services/accounts/accountIntarface';
import {Subject} from 'rxjs';
import {countPagination, styleTdUnHoverHelper} from '../../../../../shared/common-options';
import {map, takeUntil} from 'rxjs/operators';
import * as moment from 'moment';
import {ModalDataService} from '../../../../../services/accounts/modal-data.service';
import {HintsService} from '../../../../../services/hints.service';
import {animate, style, transition, trigger} from '@angular/animations';
import {DOCUMENT} from '@angular/common';
import {OrderPipe} from 'ngx-order-pipe';

@Component({
  selector: 'app-group-category',
  templateUrl: './group-category.component.html',
  styleUrls: ['./group-category.component.scss'],
  animations: [
    trigger('fadeInOut', [
      transition(':enter', [   // :enter is alias to 'void => *'
        style({opacity: 0}),
        animate(500, style({opacity: 1}))
      ]),
      transition(':leave', [   // :leave is alias to '* => void'
        animate(500, style({opacity: 0}))
      ])
    ])
  ]
})
export class GroupCategoryComponent implements OnInit, OnDestroy, AfterViewInit {
  destroyed$ = new Subject<void>();
  @ViewChild('elementSum') elementSum: ElementRef;
  public OF_GROUP_TABLES = OF_GROUP_TABLES;
  public moment: any = moment;

  clickListTr = false;

  public groupList: any = {};

  hideme: any = {};
  public countw: 10;
  result: any;
  electr: any[] = [];
  account = true;
  public groUpName = 'Auto';
  n = 0;
  p1 = 1;
  p2 = 1;
  p: number[] = [];
  key = '';
  reverse = true;
  auto: any[] = [];
  property: any[] = [];
  dataTitle: any[] = [];
  private user: any;
  public notificationCountList: any;
  // tslint:disable-next-line:variable-name
  public visible_seed: string;
  countPagination: number;
  public queryAllListNext: GetAllQueryList[] = [];
  public queryAllList: GetAllQueryList[] = [];
  private errorMessage: any = '';
  styleTdUnHoverHelper = styleTdUnHoverHelper;

  getAllQueryList() {
    this.spinnerService.show();
    this.user = this.read<string>('currentUser');
    setTimeout(() => {
      this.accountService.getAllQueryList(this.user.user_id, this.user.token)
        .pipe(takeUntil(this.destroyed$), map((item) => {
          return this.orderPipe.transform(item, 'query_tariff_start', true);
        }))
        .subscribe(
          query => {
            this.queryAllList = query;
            query.forEach(groupsN => {
              this.groupList[groupsN.titleQuery] = true;
            });
            this.queryAllListNext = this.queryAllList;
            this.spinnerService.hide();
          },
          error => {
            this.errorMessage = error as any;
            this.spinnerService.hide();
          },
          () => {
            this.spinnerService.hide();
          }
        );
    }, 500);
  }

  addDays(date: any, days: number) {
    const t = date.split(/[- :]/);
    const d = new Date(Date.UTC(t[0], t[1] - 1, t[2], t[3], t[4], t[5]));
    const sumDays = new Date();
    sumDays.setDate(days);
    d.setDate(d.getDate() + sumDays.getDate());
    return {date: d};
  }

  onPushName(event) {
    this.groUpName = event;
  }

  constructor(private accountService: AccountService,
              @Inject(DOCUMENT) private document: Document,
              private sendDatamodal: ModalDataService,
              private orderPipe: OrderPipe,
              private spinnerService: SpinnerService, private toastService: ToastrService,
              private hintsService: HintsService,
              private groupService: GroupingService) {
    this.countPagination = countPagination;
  }

  read<T>(key: string): T {
    const value: string = localStorage.getItem(key);

    if (value && value != 'undefined' && value != 'null') {
      return JSON.parse(value) as T;
    }

    return null;
  }

  sort(key) {
    if (this.key === key) {
      this.reverse = !this.reverse;
    }
    this.key = key;
  }

  mousOut() {
    this.getPChange('');
  }

  getPChange(field: any) {
    Object.keys(this.hideme).forEach(h => {
      this.hideme[h] = false;
    });
  }

  over(element: any) {
    Object.keys(this.hideme).forEach(h => {
      this.hideme[h] = false;
    });
    this.hideme[element] = true;
  }

  ngOnInit() {

    /******************переход после оплаты с leave-request****/
    this.hintsService.__obsHRedirectToAccount.pipe(takeUntil(this.destroyed$)).subscribe((toAccount) => {
      if (toAccount) {
        this.onPushName(toAccount);
      }


    });
    /********************изменение контента после изменений модального окна активации***********************/
    this.sendDatamodal.getMmodelChange()
      .pipe(takeUntil(this.destroyed$))
      .subscribe(result => {
        let preList = [];
        this.queryAllListNext.forEach(item => {
          if (result.titleQuery === item.titleQuery && result.query_id === item.query_id) {
            preList.push(result);
          } else {
            preList.push(item);
          }
        });
        this.queryAllList = preList;
        this.queryAllListNext = preList;

      });

    notificationCount.call(this);
    this.visible_seed = '1';
    // tslint:disable-next-line:variable-name
    this.groupService.get_visible_see()
      .pipe(takeUntil(this.destroyed$))
      // tslint:disable-next-line:variable-name
      .subscribe(see_visibled => {
        // tslint:disable-next-line:only-arrow-functions
        this.queryAllListNext = this.queryAllList.filter(function(el) {
          if (see_visibled === 'all') {
            return (el);
          } else {
            return (el.query_activated === see_visibled);
          }
        });
      });

    // this.sendDatamodal.getData()
    //   .pipe(takeUntil(componentDestroyed(this)))
    //   .subscribe(dataModel => {
    //   this.splice(this.queryAllList, dataModel);
    // });
    this.getAllQueryList();
    removeQuery.call(this);
    // this.document.querySelector('body').scrollIntoView();
  }

  private splice(arr, val) {
    for (let i = arr.length; i--;) {
      if (arr[i] === val) {
        arr.splice(i, 1);
      }
    }
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }

  ngAfterViewInit(): void {
    // window.scrollTo(0, 67);
  }

}
