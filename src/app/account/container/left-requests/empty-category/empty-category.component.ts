import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-empty-category',
  templateUrl: './empty-category.component.html',
  styleUrls: ['./empty-category.component.scss']
})
export class EmptyCategoryComponent implements OnInit {

  public categoryName = 'auto';
  public urlTo = '/leave/search';

  constructor(private router: Router) {
  }

  public emptyCat = false;
  public styleCat = {
    display: 'block',
    position: 'relative',
    right: '0px',
    width: '100%',
    top: '-26px'
  };

  ngOnInit() {
  }

  @Input()
  set catName($event) {

    if (typeof $event === 'string') {
      if ($event.toLowerCase() === 'other') {
        this.categoryName = `/leave/search/${$event.toLowerCase()}`;
      } else {
        this.categoryName = `/type/${$event.toLowerCase()}`;
      }
    }

  }

  @Input()
  set isEmpty($event: boolean) {
    this.emptyCat = $event;
  }

}
