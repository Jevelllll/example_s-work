import {takeUntil} from 'rxjs/operators';

export const OF_GROUP_TABLES = true;

export function notificationCount() {
  this.accountService._obsViewNotifications
    .pipe(takeUntil(this.destroyed$))
    .subscribe(resultNotification => {
      this.dataCountNotif = [];
      if (this.queryAllListNext.length > 0) {
        if (resultNotification.status === 200) {
          this.notificationCountList = resultNotification.data;
          if (Array.isArray(this.queryAllListNext)) {
            this.queryAllListNext.forEach(value => {
              if (this.notificationCountList[value.titleQuery.toLowerCase()]) {
                const notificationCountListElement = this.notificationCountList[value.titleQuery.toLowerCase()];
                if (Array.isArray(notificationCountListElement)) {
                  const lengthNotificator = notificationCountListElement.filter(el => el.query_Id === value.query_id).length;
                  this.dataCountNotif.push(
                    {
                      query_id: value.query_id,
                      titleQuery: value.titleQuery,
                      countNotificator: lengthNotificator,
                      notification_count: String(Number(value.notification_count) + lengthNotificator)
                    });
                }
              }
            });
          }
          this.accountService.bHSDataCountNotif.next(this.dataCountNotif);
        }

      }
    }, err => console.log(err));
}

/**********************удаление заявок в таблицах ************************/
export function removeQuery() {
  this.sendDatamodal.getData()
    .pipe(takeUntil(this.destroyed$))
    .subscribe(dataModel => {
      this.sendDatamodal.clearData(this.user.user_id, this.user.token, dataModel)
        .pipe(takeUntil(this.destroyed$))
        // tslint:disable-next-line:variable-name
        .subscribe(delete_request => {
          try {
            if (delete_request['status'] === 200) {
              this.queryAllList = this.queryAllList.filter(resultList => resultList.query_id !== dataModel.query_id);
              this.queryAllListNext = this.queryAllListNext.filter(resultList => resultList.query_id !== dataModel.query_id);
              this.toastService.info('Заявка#' + dataModel.query_id + '/' + dataModel.titleQuery + ' успешно удалена', '', {
                timeOut: 3000,
              });

            } else {

            }
          } catch (e) {

          }

        }, error => {
        }, () => {
        });
    });
}
