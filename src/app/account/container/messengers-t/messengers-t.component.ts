import {Component, OnDestroy, OnInit} from '@angular/core';
import {ToastrService} from 'ngx-toastr';
import {ActivatedRoute, Router} from '@angular/router';
import {MessengersMService} from '../../../services/accounts/messengers/messengers-m.service';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

@Component({
  selector: 'app-messengers-t',
  templateUrl: './messengers-t.component.html',
  styleUrls: ['./messengers-t.component.scss']
})
export class MessengersTComponent implements OnInit, OnDestroy {
  destroyed$ = new Subject<void>();
  private user: any;
  public dataMessengers: any;

  constructor(private toster: ToastrService, private route: ActivatedRoute, private router: Router,
              private messengersMService: MessengersMService) {
  }

  onSelect(web_h_redirect) {
    window.open(`${web_h_redirect}${this.user.token}`);
    setTimeout(() => {
      document.location.reload();
    }, 0);
  }

  ngOnInit() {
    this.user = this.read<string>('currentUser');
    this.getMsgList();
  }

  private getMsgList() {
    this.messengersMService.messengersList(this.user.user_id).pipe(takeUntil(this.destroyed$))
      .subscribe((data) => {
        this.dataMessengers = data;
      }, error => {
      });
  }

  public read<T>(key: string): T {
    const value: string = localStorage.getItem(key);
    if (value && value != 'undefined' && value != 'null') {
      return <T> JSON.parse(value);
    }
    return null;
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }

}
