import {AfterViewInit, Component, OnDestroy, OnInit, Renderer2, ViewChildren} from '@angular/core';
import {Subject} from 'rxjs';
import {StatisticsService} from '../../../services/accounts/statistics.service';
import {map, takeUntil} from 'rxjs/operators';
import {SpinnerService} from '../../../services/spinner.service';

declare let $: any;

@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.scss']
})
export class StatisticsComponent implements OnInit, AfterViewInit, OnDestroy {

  destroyed$ = new Subject<void>();
  public noWrapSlides = false;
  public activeSlideIndex: any = 0;
  public dataChart = [];
  public statisticsView = true;
  @ViewChildren('carouselRef') carouselRef: any;
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = false;
  showXAxisLabel = true;
  xAxisLabel = '';
  showYAxisLabel = true;
  yAxisLabel = '';

  colorScheme = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  };

  onSelect(event) {

  }

  activeSlideChange(event: any) {
  }


  constructor(private statisticService: StatisticsService, private renderer: Renderer2, private spinnerService: SpinnerService) {
  }

  ngOnInit() {
    this.spinnerService.show();
    this.statisticService.getStatistics()
      .pipe(takeUntil(this.destroyed$), map((preVal) => {

        return preVal.map((data) => {
          return {
            date: data.date,
            datas: data.datas.map((datasList) => {
              return {
                name: trnslitName(datasList.name),
                value: datasList.value
              };
            })
          };
        });

      }))
      .subscribe(result => {
        this.dataChart = result;
        this.spinnerService.hide();
      }, error => {
        this.spinnerService.hide();
      }, () => {
        this.spinnerService.hide();
      });
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }

  ngAfterViewInit(): void {

    // this.carouselRef.changes.pipe(takeUntil(this.destroyed$)).subscribe((w) => {
    //   setTimeout(_ => {
    //     const dataElement: any = this.carouselRef._results;
    //     const carouselRoot = dataElement[0].el.nativeElement;
    //     const nextIcon = carouselRoot.getElementsByClassName('carousel-control-next-icon');
    //     const prevIcon = carouselRoot.getElementsByClassName('carousel-control-prev-icon');
    //     if (nextIcon) {
    //       this.renderer.setStyle(nextIcon[0], 'background-color', 'black');
    //     }
    //     if (prevIcon) {
    //       this.renderer.setStyle(prevIcon[0], 'background-color', 'black');
    //     }
    //   }, 0);
    // });
    //
    // setTimeout(() => {
    //   // tslint:disable-next-line:only-arrow-functions
    //   $('span[asid="statistics"]').click(function() {
    //     $('.carousel-control-next-icon').css('background-color', 'black');
    //     $('.carousel-control-prev-icon').css('background-color', 'black');
    //   });
    // }, 0);


    setTimeout(_ => {
      window.dispatchEvent(new Event('resize'));
    }, 0);

  }

}

export function trnslitName(name) {
  let trName = '';
  switch (name) {
    case 'query_property':
      trName = 'Недвижимость';
      break;
    case 'tbl_query_auto':
      trName = 'Авто';
      break;
    case 'query_other':
      trName = 'Другое';
      break;
    case 'query_work':
      trName = 'Работа';
      break;
    case 'query_legal':
      trName = 'Право';
      break;
    case 'query_quest':
      trName = 'Квест';
      break;
    default :
      trName = 'не определена запись';
      break;
  }
  return trName;
}
