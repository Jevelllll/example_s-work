import {AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Balance, Transactions} from '../../../services/pay/amount-intervace';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AmountService} from '../../../services/pay/amount.service';
import {ToastrService} from 'ngx-toastr';
import {SpinnerService} from '../../../services/spinner.service';
import {Observable, Subject} from 'rxjs';
import {delay, first, takeUntil} from 'rxjs/operators';
import {TRANSLATION} from '../../../shared/translation-pay';

@Component({
  selector: 'app-fill-balance',
  templateUrl: './fill-balance.component.html',
  styleUrls: ['./fill-balance.component.scss']
})
export class FillBalanceComponent implements OnInit, AfterViewInit, OnDestroy {
  destroyed$ = new Subject<void>();
  @ViewChild('sysPay', {static: true}) sysPay: ElementRef;
  TRANSLATION = TRANSLATION;

  public displayedColumns = ['id', 'service', 'status', 'date_create', 'currency', 'amount'];
  public displayedColumnsTransactions = ['id', 'date_create', 'amount', 'order'];
  public balance: Balance;
  public historyPay: History[] = [];
  public historyTransactionsPay: Transactions[] = [];
  public Number: any = Number;
  public payServices: any = [];
  public payForm: FormGroup;
  public selectPayTittle = 'Выберите способ пополнения'.toUpperCase();
  public wayOfReplenishment = '';

  public payContent: any;

  constructor(private fb: FormBuilder, private amountService: AmountService,
              private toaster: ToastrService,
              private spinnerService: SpinnerService) {
    this.mainForm();
  }

  ngOnInit() {

    /*********************история оплат********************************************/
    this.amountService.historyPayObs
      .pipe(takeUntil(this.destroyed$), first())
      .subscribe(histPay => {
        if (histPay) {
          this.historyPay = histPay;
        } else {
          try {
            this.amountService.getHistoryPay()
              .pipe(takeUntil(this.destroyed$))
              .subscribe(hP => {
                this.historyPay = hP;
              }, error => {

              });
          } catch (e) {
          }
        }
      }, error => {
        const history: any = new History();
        Object.keys(history).forEach((historyParameters) => {
          history[historyParameters] = null;
        });
        this.historyPay = [history];
      });
    /*********************транзакции***************************************/
    this.amountService.historyTransactionsPayObs
      .pipe(takeUntil(this.destroyed$))
      .subscribe(transactions => {
        if (transactions) {

          this.historyTransactionsPay = transactions;
        } else {
          try {
            this.amountService.getHistoryTransactionsPay()
              .pipe(takeUntil(this.destroyed$))
              .subscribe(ht => {
                this.historyTransactionsPay = ht;
              }, error => this.toaster.error('Error'));
          } catch (e) {
          }
        }
      });
    /*********************баланс*********************************************/
    this.amountService.balanceObs
      .pipe(takeUntil(this.destroyed$))
      .subscribe(result => {
        if (result !== null) {
          this.balance = result;
        } else {
          try {
            this.amountService.getCurrentBalance()
              .pipe(takeUntil(this.destroyed$))
              .subscribe(value => {
                this.balance = value;
              }, error => this.toaster.error('Error'));
          } catch (e) {
          }
        }
      });
    /*********************список сервисов**************************************/
    this.amountService.serviceListObs
      .pipe(takeUntil(this.destroyed$))
      .subscribe(result => {
        if (result) {
          this.payServices = result;
        } else {
          this.amountService.getServiceList()
            .pipe(takeUntil(this.destroyed$))
            .subscribe(rN => {
            }, error1 => this.toaster.error('Error'));
        }
      }, error1 => this.toaster.error('Error'));
  }

  onSubmit() {
    if (this.payForm.valid) {
      this.amountService.newPayContent(this.payForm.value)
        .pipe(takeUntil(this.destroyed$), delay(400))
        .subscribe(amountContent => {
          this.payContent = amountContent;
          this.scrollToSystemPay();
        }, error => {
          this.toaster.error('Error');
        }, () =>  {});
    }
  }

  private scrollToSystemPay() {
    function fieldDelays(timeVal: number): Observable<any> {
      return Observable.create((obs) => {
        obs.next(true);
      }).pipe(delay(timeVal));
    }

    const element = this.sysPay.nativeElement;
    fieldDelays(10)
      .pipe(takeUntil(this.destroyed$))
      .subscribe(() => element.scrollIntoView({behavior: 'smooth', block: 'end'}));

  }

  private mainForm() {

    this.payForm = this.fb.group({
      amount: ['', Validators.required],
      service: ['', Validators.required],
      is_wallet: [1],
      redirect_to: (window.location.href.indexOf('money') === -1) ? window.location.href + '/money' : window.location.href
    });
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }

  ngAfterViewInit(): void {
  }

}
