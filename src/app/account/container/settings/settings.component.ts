import {Component, OnDestroy, OnInit} from '@angular/core';
import {AccountUsers} from '../../../services/accounts/accountIntarface';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {LoginService} from '../../../services/auth/login.service';
import {ToastrService} from 'ngx-toastr';
import {AccountService} from '../../../services/accounts/accounts.service';
import {SpinnerService} from '../../../services/spinner.service';
import * as moment from 'moment';
import {Subject} from 'rxjs';
import {delay, takeUntil} from 'rxjs/operators';
import {MustMatch} from '../../../shared/other-help';
import {clearObject} from '../../../shared/helper-container-leave';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit, OnDestroy {
  destroyed$ = new Subject<void>();
  public areaCodeMask = ['(', /[0-9]/, /\d/, /\d/, ')', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
  public regExpPhone = new RegExp(/^(\([0-9]{3}\)\s*|[0-9]{3}\-)[0-9]{3}-[0-9]{4}$/);
  private regExp = new RegExp(/[-()/\\]/g);
  public userInfo: AccountUsers;
  private regExpNumber = new RegExp(/^\d+$/);
  public btnSettingsConfirmName = 'Применить';
  private user: any;
  passFlag = true;
  dataChange = true;

  public successMsg = '';
  minDate = new Date(1944, 9, 2);
  startDate = new Date();
  // tslint:disable-next-line:variable-name
  date_year: any;

  private preDataEqually = {};

  formPhone: FormGroup;
  formEmail: FormGroup;
  formPassword: FormGroup;
  formFullName: FormGroup;
  formDate: FormGroup;

  // tslint:disable-next-line:variable-name
  constructor(private _fB: FormBuilder, private loginService: LoginService,
              private spinnerService: SpinnerService,
              private toster: ToastrService,
              public accountService: AccountService
  ) {

  }

  ngOnInit() {
    this.formInit();
    this.getAccountInfo();
  }

  get phone() {
    return this.formPhone.get('phone');
  }

  get mail() {
    return this.formEmail.get('email');
  }


  get password() {
    return this.formPassword.get('password');
  }

  get first_name() {
    return this.formFullName.get('first_name');
  }

  get last_name() {
    return this.formFullName.get('last_name');
  }

  get confirmPassword() {
    return this.formPassword.get('confirmPassword');
  }

  changeDate(event) {
    this.dataChange = false;
    this.startDate = new Date(event);
  }

  onSubmit(form: FormGroup) {

    const preForm: any = form.value;
    delete preForm.confirmPassword;

    if (form.controls.password) {
      this.passFlag = true;
    }
    if (form.controls.phone) {
      preForm.phone = preForm.phone.replace(this.regExp, '');
    }
    if (form.controls.date_year) {
      preForm.date_year = moment(new Date(preForm.date_year)).format('MM-DD-YYYY');
      this.dataChange = true;
    }
    this.spinnerService.show();
    this.accountService.tokenDataUsIdObserable
      .pipe(takeUntil(this.destroyed$))
      .subscribe(value => {
        this.accountService.userEditUpdate(value.user_id, value.token, preForm)
          .pipe(takeUntil(this.destroyed$))
          .subscribe(
            response => {
              this.toster.info(`Успешно обновлено! ${this.successMsg}`);
              this.spinnerService.hide();
            },
            error1 => this.toster.error('Ошибка сервера')
            , () => this.spinnerService.hide());
      });
  }

  /**********************наполнение инфой**********************************************************/
  getAccountInfo() {
    this.spinnerService.show();
    this.user = this.read<string>('currentUser');
    this.accountService.obsUserInfo
      .pipe(takeUntil(this.destroyed$))
      .subscribe(valResp => {
        if (valResp) {
          valResp = clearObject(valResp);
          Object.keys(valResp).forEach((field): any => {
            this.preDataEqually[field] = valResp[field];
          });
          this.userInfo = valResp;
          this.formPhone.get('phone').setValue(valResp.phone, {emitEvent: false});
          this.formEmail.get('email').setValue(valResp.email, {emitEvent: false});
          this.formFullName.patchValue({
            first_name: valResp.first_name,
            last_name: valResp.last_name
          });
          this.startDate = new Date(valResp.date_year);
          this.formDate.get('date_year').setValue(this.startDate, {emitEvent: false});
          this.spinnerService.hide();
        } else {
          /***********выпонить гаполнение данными*********************/
          setTimeout(function() {
            this.accountService.getPersonalInfo(this.user.user_id, this.user.token)
              .pipe(takeUntil(this.destroyed$))
              .subscribe(query => {
                  this.accountUsers = query;
                  try {
                    this.formDataInstance(valResp);
                  } catch (e) {
                  }
                  this.spinnerService.hide();
                },
                error => {
                  try {
                    if (error.auth === false) {
                      // this.loginService.logout();
                      // this.router.navigate(['/']);
                    }
                  } catch (e) {
                  }
                  this.spinnerService.hide();
                },
                () => {
                  this.spinnerService.hide();
                }
              );
          }.bind(this), 700);
        }
      });
  }

  changeMailValidation(control: AbstractControl): any {
    try {
      const formGroup = control.parent.controls;
      const controlName = Object.keys(formGroup).find(name => control === formGroup[name]) || null;
      if (control.value === this.preDataEqually[controlName]) {
        return {ee: true};
      }
    } catch (e) {

    }
    return null;
  }

  existUser(control: AbstractControl): any {
    const regExp = new RegExp(/[-_()/\\]/ig);
    const phoneValue = control.value.replace(regExp, '');
    const phoneLength = phoneValue.length;
    if (phoneLength === 10 && control.pristine === false) {
      this.loginService.checkPhome(phoneValue, true)
        .pipe(
          takeUntil(this.destroyed$),
          delay(3000))
        .subscribe((valResp): any => {
          if (valResp['status'] === 200 && valResp['message'] === 'User not exists') {
            return null;
          } else {
            this.formPhone.controls.phone.setErrors({incorrect: true});
          }
        }, error1 => {
          // this.toster.warning('Данный номер существует в базе данных');
          this.formPhone.controls.phone.setErrors({incorrect: true});
        }, () => {
        });
    } else {
      return {incorrect: true};
    }

  }

  read<T>(key: string): T {
    const value: string = localStorage.getItem(key);

    if (value && value != 'undefined' && value != 'null') {
      return JSON.parse(value) as T;
    }

    return null;
  }

  formInit() {
    this.formPhone = this._fB.group({
      phone: ['', Validators.compose([
        Validators.minLength(5),
        Validators.pattern(this.regExpPhone),
        countSimbol.bind(this),
        this.existUser.bind(this)])],
    });
    this.formFullName = this._fB.group({
      first_name: ['', [Validators.minLength(5), countSimbol, Validators.maxLength(144), this.changeMailValidation.bind(this)]],
      last_name: ['', [Validators.minLength(5), countSimbol, Validators.maxLength(144), this.changeMailValidation.bind(this)]]
    });
    this.formEmail = this._fB.group({
      email: ['', Validators.compose([Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,3}$'),
        countSimbol, this.changeMailValidation.bind(this)])]
    });
    this.formPassword = this._fB.group({
      password: ['', Validators.compose([Validators.minLength(5), countSimbol,
        Validators.required,
        Validators.maxLength(144)])],
      confirmPassword: ['', Validators.compose([Validators.minLength(5), countSimbol, Validators.required,
        Validators.maxLength(144)])]
    }, {
      validator: MustMatch('password', 'confirmPassword')
    });
    this.formPassword.valueChanges
      .pipe(takeUntil(this.destroyed$))
      .subscribe(() => this.passFlag = false);
    this.formDate = this._fB.group({
      date_year: ['']
    });
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }

}

export function countSimbol(control: AbstractControl): any {
  try {
    if (control.value.toString().length === 0) {
      return {countS: true};
    }
    return null;
  }catch (e) {

  }


}
