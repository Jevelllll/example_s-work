import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {WebGameService, WebPoint} from '../../services/web-game.service';
import {LegalCategory} from '../../services/priority/resModel';
import {Observable} from 'rxjs/internal/Observable';
import {Subject} from 'rxjs/internal/Subject';
import {takeUntil} from 'rxjs/internal/operators/takeUntil';
import {ToastrService} from 'ngx-toastr';
import {map} from 'rxjs/internal/operators/map';

@Component({
  selector: 'app-web-game-form',
  templateUrl: './web-game-form.component.html',
  styleUrls: ['./web-game-form.component.scss']
})
export class WebGameFormComponent implements OnInit, OnDestroy {
  destroyed$ = new Subject<void>();
  public webGameForm: FormGroup;
  private user;
  public webGameProcess: Observable<LegalCategory[]>;

  public webGamePoint = 0;

  private areaCodeMask = ['(', /[0-9]/, /\d/, /\d/, ')', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
  private areaCodeDisk = [/\d/, /\d/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, /\d/];
  public regExpPhone = new RegExp(/^(\([0-9]{3}\)\s*|[0-9]{3}\-)[0-9]{3}-[0-9]{4}$/);
  public regExpCredit = new RegExp(/^[0-9]{4}\s[0-9]{4}\s[0-9]{4}\s[0-9]{4}$/);
  public currentMask = {placeholder: '(___)-___-___', mask: this.areaCodeMask};
  public currentPattern = this.regExpPhone;

  // tslint:disable-next-line:variable-name
  constructor(private _fb: FormBuilder, private webGameService: WebGameService, private toaster: ToastrService) {
    try {
      this.user = JSON.parse(localStorage.getItem('currentUser'));
    } catch (e) {
    }
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
  }

  ngOnInit(): void {
    this.formInit();
    this.webGameService.obsWebPoint.pipe(takeUntil(this.destroyed$)).subscribe((data) => {
        if (data) {
          this.webGamePoint = Number(data.game_points);
          this.webGameForm.get('amount').setValidators(Validators.compose(
            [
              Validators.required,
              Validators.pattern('^[0-9]*$'),
              Validators.minLength(1),
              Validators.max(this.webGamePoint),
              Validators.min(2)
            ]));
        }
    }, error => {
      this.webGamePoint = 0;
      this.webGameForm.get('amount').setValidators(Validators.compose(
        [
          Validators.required,
          Validators.pattern('^[0-9]*$'),
          Validators.minLength(1),
          Validators.max(this.webGamePoint),
          Validators.min(2)
        ]));
    });
    this.webGameProcess = this.webGameService.obsWbProcess;
  }

  public save(val) {
    if (val) {
      this.webGameService.webWithdrawal(val).pipe(takeUntil(this.destroyed$)).subscribe((result) => {
        this.webGameForm.reset();
        this.toaster.success('Операция успешна', '', {timeOut: 17000});
      }, error => {
        this.toaster.success('Ошибка повторите попытку позже ', '', {timeOut: 17000});
      });
    } else {
      return false;
    }
  }

  changeProcess(event): void {
    this.webGameForm.get('data').reset();
    switch (event) {
      case 1:
        this.currentMask = {
          placeholder: '(___)-___-___',
          mask: this.areaCodeMask
        };
        this.currentPattern = this.regExpPhone;
        break;
      case 2:
        this.currentMask = {
          placeholder: '____ ____ ____ ____',
          mask: this.areaCodeDisk
        };
        this.currentPattern = this.regExpCredit;
        break;
    }
    this.webGameForm.get('data').setValidators([Validators.compose([Validators.pattern(this.currentPattern), Validators.required])]);
  }

  private formInit() {
    this.webGameForm = this._fb.group({
      amount: ['', Validators.compose(
        [
          Validators.required,
          Validators.pattern('^[0-9]*$'),
          Validators.minLength(1),
        ]),
      ],
      process_type: ['', Validators.required],
      data: ['', Validators.required],
      user_id: [Number(this.user.user_id)]
    });
  }
}
