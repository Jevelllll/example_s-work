import {Directive, ElementRef, Input, Renderer2} from '@angular/core';

@Directive({
  selector: '[appBaseShorten]'
})
export class BaseShortenDirective {

  @Input() bellElementS: HTMLElement;
  private readonly currentElement: HTMLElement;

  constructor(private element: ElementRef, private renderer: Renderer2) {
    this.currentElement = this.element.nativeElement;
  }

  @Input()
  // tslint:disable-next-line:variable-name
  set countText(number: number) {
    setTimeout(() => {
      const meaningLength = number.toString().length;
      if (meaningLength === 2) {
        this.renderer.setStyle(this.currentElement, 'right', '4px');
        this.renderer.setStyle(this.currentElement, 'bottom', '1px');
        this.renderer.setStyle(this.currentElement, 'font-size', '15px');
      } else if (meaningLength === 1) {
        this.renderer.setStyle(this.currentElement, 'right', '10px');
        this.renderer.setStyle(this.currentElement, 'bottom', '-2px');
        this.renderer.setStyle(this.currentElement, 'font-size', '23px');
      } else if (meaningLength === 3) {
        this.renderer.setStyle(this.currentElement, 'right', '7px');
        this.renderer.setStyle(this.currentElement, 'bottom', '1px');
        this.renderer.setStyle(this.currentElement, 'font-size', '15px');
        this.renderer.setStyle(this.bellElementS, 'font-size', '2.4rem');
      } else {
        if (this.bellElementS) {
          this.renderer.setStyle(this.bellElementS, 'font-size', 'xx-large');
        }
      }
    }, 15);

  }

}
