import {
  AfterViewInit,
  Component,
  ComponentFactoryResolver,
  ElementRef,
  HostListener,
  Inject,
  OnDestroy,
  OnInit,
  Renderer2,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import {BehaviorSubject, Observable, Subject, timer} from 'rxjs';
import {AccountService} from '../../services/accounts/accounts.service';
import {ToastrService} from 'ngx-toastr';
import {ActivatedRoute, Router} from '@angular/router';
import {MatDialog} from '@angular/material/dialog';
import {Title} from '@angular/platform-browser';
import {LoginService} from '../../services/auth/login.service';
import {MessengersMService} from '../../services/accounts/messengers/messengers-m.service';
import {PersonalInformationComponent} from '../container/personal-information/personal-information.component';
import {MessengersComponent} from '../messengers/messengers.component';
import {LeftRequestsComponent} from '../container/left-requests/left-requests.component';
import {SettingsComponent} from '../container/settings/settings.component';
import {StatisticsComponent} from '../container/statistics/statistics.component';
import {MessengersTComponent} from '../container/messengers-t/messengers-t.component';
import {FillBalanceComponent} from '../container/fill-balance/fill-balance.component';
import {authExit} from '../../shared/authExist';
import {takeUntil, combineLatest} from 'rxjs/operators';
import * as moment from 'moment';
import {DOCUMENT} from '@angular/common';
import {NgxSpinnerService} from 'ngx-spinner';
import {PromoComponent} from '../container/promo/promo.component';
import {WebGameFormComponent} from '../web-game-form/web-game-form.component';
import {WebGameService} from '../../services/web-game.service';

declare var $: any;

@Component({
  selector: 'app-base-account',
  templateUrl: './base-account.component.html',
  styleUrls: ['./base-account.component.scss']
})
export class BaseAccountComponent implements OnInit, AfterViewInit, OnDestroy {
  destroyed$ = new Subject<void>();
  public lkt = 'Личная информация';
  public lrt = 'Оставленные заявки';
  public settingst = 'Настройки';
  public statistic = 'Статистика';
  public moneyCab = 'Пополнение счета';
  public messagesTitle = 'Messengers';
  public messagePromo = 'Промокод';
  public webGame = 'Веб игра';
  private characteristicUrl = null;
  public soundOfOnFlag = true;
  public soundName: any = {true: 'Отключить звук', false: 'Включить звук'};
  // tslint:disable-next-line:variable-name
  alive = true;
  public countViewNotRead: any;
  public oldCountViewNotRead: any[] = [];
  public countViewNotReadTest = 222;
  time: Date;
  private user: any;
  // tslint:disable-next-line:variable-name
  private fields_title: any[];
  @ViewChild('money', {static: true}) money: ElementRef;
  @ViewChild('dynamicAccount', {static: true, read: ViewContainerRef}) dynamicAccount: ViewContainerRef;
  @ViewChild('lAside', {static: true}) lAside: ElementRef;
  @ViewChild('bellElement', {static: true}) bellElement: ElementRef;
  private nameActiveClass = 'search_landing__aside__active';
  clickedElement: any;

  public innerWidth: any = 0;

  // tslint:disable-next-line:variable-name
  public __behSMainAudio: BehaviorSubject<any>;
  // tslint:disable-next-line:variable-name
  public __obsMainAudio: Observable<any> = new Observable<any>();
  // tslint:disable-next-line:variable-name
  public __bSubject: BehaviorSubject<any>;
  // tslint:disable-next-line:variable-name
  public __obsResizeWidth: Observable<any> = new Observable();

  private dataMessengers: any;

  constructor(
    @Inject(DOCUMENT) private document: Document,
    private renderer: Renderer2, private accountService: AccountService,
    private componentFactoryResolver: ComponentFactoryResolver,
    private spinnerService: NgxSpinnerService,
    private toster: ToastrService, private route: ActivatedRoute,
    public dialog: MatDialog,
    private messengersMService: MessengersMService,
    private webGameService: WebGameService,
    private titleService: Title, private loginService: LoginService, private router: Router) {

    this.titleService.setTitle('??? account');

    this.__behSMainAudio = new BehaviorSubject<any>(false);
    this.__obsMainAudio = this.__behSMainAudio.asObservable();

    this.innerWidth = window.innerWidth;
    this.__bSubject = new BehaviorSubject<any>(this.innerWidth);
    this.__obsResizeWidth = this.__bSubject.asObservable();
  }

  private customFactory(dynamic: any) {
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(dynamic);
    this.dynamicAccount.clear();
    this.dynamicAccount.createComponent(componentFactory);
  }

  private personalInfo(): void {
    this.customFactory(PersonalInformationComponent);
  }

  openDialog(): void {
    let isSelectOneMessenger;
    if (Array.isArray(this.dataMessengers)) {
      isSelectOneMessenger = this.dataMessengers.filter((it) => it.click);
    }

    if (isSelectOneMessenger.length > 0) {
    } else {
      const dialogRef = this.dialog.open(MessengersComponent, {
        width: '600px',
        minHeight: '170px',
        disableClose: true,
        data: this.dataMessengers
      });

      dialogRef.afterClosed().subscribe(dataRes => {
        if (dataRes) {
          if (dataRes === 'close') {
            this.toster.warning('Рекомендовано подключить мессенджер');
          } else {
            window.open(`${dataRes}${this.user.token}`);
          }
          // this.messengersMService.messengersList(this.user.user_id).pipe(takeUntil(componentDestroyed(this)))
          //   .subscribe(() => {
          //   }, error => {
          //   });
          // this.router.navigate([]).then((result): any => {
          //   window.open(dataRes, '_blank');
          // });

        }
      });
    }
  }


  private leftRequest(): void {
    this.customFactory(LeftRequestsComponent);
  }

  private settings(): void {
    this.customFactory(SettingsComponent);
  }

  private statisticsC(): void {
    this.customFactory(StatisticsComponent);
  }

  private messagesC(): void {
    this.customFactory(MessengersTComponent);
  }

  private promoC(): void {
    this.customFactory(PromoComponent);
  }

  private webGameC(): void {
    this.customFactory(WebGameFormComponent);
  }

  private fillBalace(): void {
    this.customFactory(FillBalanceComponent);
  }

  cleanClickedElement() {
    for (const entry of this.lAside.nativeElement.children) {
      this.renderer.removeClass(entry, this.nameActiveClass);
    }
  }

  focusFunction(element: any) {
    this.cleanClickedElement();
    // let target = element.target || element.srcElement || element.currentTarget;
    const target = element;
    const someattribute = target.getAttribute('aSid');
    switch (someattribute) {
      case 'lK':
        this.personalInfo();
        break;
      case 'lR':
        this.leftRequest();
        break;
      case 'settings':
        this.settings();
        break;
      case 'statistics':
        this.statisticsC();
        break;
      case 'money':
        this.fillBalace();
        break;
      case 'messages':
        this.messagesC();
        break;
      case 'promo':
        this.promoC();
        break;
      case 'webgame':
        this.webGameC();
        break;
      default:
        this.personalInfo();
    }
    this.clickedElement = target;
    this.renderer.addClass(target, this.nameActiveClass);

    // this.renderer.addClass(target, this.nameActiveClass);
  }

  ngOnInit() {
    this.webGameService.getGProcess().pipe(takeUntil(this.destroyed$), combineLatest(this.webGameService.gamePoint()))
      .subscribe((d) => {}, error => {});
    try {
      this.soundOfOnFlag = JSON.parse(localStorage.getItem('sound'));
    } catch (e) {

    }
    this.viewRead();
    try {
      this.characteristicUrl = this.route.snapshot.paramMap.get('characteristic');
    } catch (e) {

    }

    if (this.characteristicUrl) {
      if (this.characteristicUrl === 'money') {
        this.focusFunction(this.money.nativeElement);
      } else {
        this.leftRequest();
        // this.personalInfo();
      }
    } else {
      this.leftRequest();
      // this.personalInfo();
    }

    authExit(this.loginService, this.router, true);
    // this.messengersMService.__obsMessengers.pipe(takeUntil(componentDestroyed(this))).subscribe((data) => {
    //   if (data) {
    //     this.dataMessengers = data;
    //     this.openDialog();
    //   } else {
    this.messengersMService.messengersList(this.user.user_id).pipe(takeUntil(this.destroyed$))
      .subscribe((data) => {
        this.dataMessengers = data;
        this.openDialog();
      }, error => {
      });
    // }
    // }, error => {
    // });

  }

  private viewRead() {
    const reducer = (accumulator, currentValue) => accumulator + currentValue;
    this.user = this.read<string>('currentUser');
    if (typeof this.user === 'object') {
      try {
        this.accountService.viewNotRead(this.user.user_id, this.user.token)
          .pipe(takeUntil(this.destroyed$))
          .subscribe(resultRes => {
            if (resultRes.status === 200) {
              this.formationCountRead(resultRes, reducer);
            }
          }, error => this.toster.error('Ошибка сервера!'));
        timer(0, 4000)
          .pipe(takeUntil(this.destroyed$))
          .subscribe(() => {

            this.accountService.timeUpdate(this.user.user_id, this.user.token)
              .pipe(takeUntil(this.destroyed$))
              .subscribe(resultRes => {
                if (resultRes) {
                  this.formationCountRead(resultRes, reducer);
                }
              });
          });
      } catch (e) {

      }


      // this.accountService.viewNotRead(this.user.user_id, this.user.token)
      //   .pipe(takeUntil(componentDestroyed(this)))
      //   .subscribe(resultRes => {
      //     if (resultRes.status === 200) {
      //       this.formationCountRead(resultRes, reducer);
      //     }
      //   }, error => this.toster.error('Ошибка сервера!'));
    }
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.innerWidth = event.target.innerWidth;
    this.__bSubject.next(this.innerWidth);
  }


  private formationCountRead(resultRes, reducer) {

    if (typeof resultRes.data === 'object') {
      const data = resultRes.data;
      this.countViewNotRead = Object.keys(data).map(field => {
        const dataField = data[field];
        if (Array.isArray(dataField)) {
          return dataField.length;
        }
      }).reduce(reducer);

      /*****************под звуковой сигнал проверка на новые данные*****/
      const resT = [];
      Object.keys(resultRes.data).forEach(data => {
        if (Array.isArray(resultRes.data[data])) {
          resultRes.data[data].forEach(item => {
            try {
              resT.push(JSON.stringify(item));
            } catch (e) {
            }
          });
        }
      });
      if (Array.isArray(resT)) {

        if (JSON.stringify(resT) < JSON.stringify(this.oldCountViewNotRead)) {
          if (this.soundOfOnFlag) {
            BaseAccountComponent.mainAudio();
          }
        } else {
        }
        this.oldCountViewNotRead = resT;
        // resT.forEach((item) => {
        //   if (this.oldCountViewNotRead.indexOf(item) === -1) {
        //     if (this.soundOfOnFlag) {
        //       BaseAccountComponent.mainAudio();
        //       // this.__behSMainAudio.next(true);
        //     }
        //     this.oldCountViewNotRead.push(item);
        //   }
        // });
      }
      /**********************/
    }
  }

  soundOfOn(): void {

    this.soundOfOnFlag = !this.soundOfOnFlag;
    localStorage.setItem('sound', JSON.stringify(this.soundOfOnFlag));
  }

  private static mainAudio(): void {
    const audio = new Audio();
    // tslint:disable-next-line:variable-name
    let number = moment(new Date()).unix();
    audio.src = './../../../../assets/audio/questy_bird_sms-namobilu.com.mp3#' + number;
    // audio.load();
    try {
      audio.play();
    } catch (e) {
    }
  }

  public read<T>(key: string): T {
    const value: string = localStorage.getItem(key);
    if (value && value != 'undefined' && value != 'null') {
      return <T> JSON.parse(value);
    }
    return null;
  }

  ngAfterViewInit() {

    this.__obsResizeWidth
      .pipe(takeUntil(this.destroyed$))
      .subscribe(value => this.accountService.setSubjectWidth(value));
    window.scrollTo(0, 67);
    // $('body,html').animate({
    //   scrollTop: 67
    // }, 800);
    // $(window).resize(function() {
    //     var w = $(window).width();
    //     if (w < 500) {
    //         for ( const child of children) {
    //             child.style.fontSize = '0px';
    //             child.querySelector('i').style.fontSize = '20px';
    //         }
    //     } else {
    //         for ( const child of children) {
    //             child.style.fontSize = '20px';
    //             child.querySelector('i').style.fontSize = '';
    //         }
    //     }
    // });
  }

  ngOnDestroy(): void {
    this.alive = false; // switches your IntervalObservable off
    this.destroyed$.next();
  }

}
