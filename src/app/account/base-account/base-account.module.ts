import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {BaseAccountRoutingModule} from './base-account-routing.module';
import {BaseShortenDirective} from './base-shorten.directive';
import {BaseAccountComponent} from './base-account.component';
import {ShortenTextPipe} from './shorten-text.pipe';
import {MessengersMService} from '../../services/accounts/messengers/messengers-m.service';
import {PersonalInformationComponent} from '../container/personal-information/personal-information.component';
import {MessengersComponent} from '../messengers/messengers.component';
import {LeftRequestsComponent} from '../container/left-requests/left-requests.component';
import {SettingsComponent} from '../container/settings/settings.component';
import {StatisticsComponent} from '../container/statistics/statistics.component';
import {MessengersTComponent} from '../container/messengers-t/messengers-t.component';
import {FillBalanceComponent} from '../container/fill-balance/fill-balance.component';
import {MyDateAdapter} from '../../leave-request/container/tariff/my-date-adapter';
import {DateAdapter, MAT_DATE_LOCALE, MatNativeDateModule} from '@angular/material/core';
import {MatDialogModule} from '@angular/material/dialog';
import {MatTabsModule} from '@angular/material/tabs';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatSelectModule} from '@angular/material/select';
import {ReactiveFormsModule} from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import {ExpPipeModule} from '../../services/pipe_export/exp-pipe.module';
import {MatTableModule} from '@angular/material/table';
import {MatButtonModule} from '@angular/material/button';
import {MDBBootstrapModule} from 'angular-bootstrap-md';
import {disabledMaterialInputsDirective} from '../../services/directivs/disabled-materialInputs-directive';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {TextMaskModule} from 'angular2-text-mask';
import {GroupingService} from '../../services/accounts/grouping.service';
import {GroupCategoryComponent} from '../container/left-requests/grouping/group-category/group-category.component';
// import {GroupingComponent} from '../container/left-requests/grouping/grouping.component';
import {EmptyCategoryComponent} from '../container/left-requests/empty-category/empty-category.component';
import {NgxPaginationModule} from 'ngx-pagination';
import {SearchModule} from '../../leave-request/container/search/search.module';
import {OnHoverHelpersComponent} from '../container/left-requests/on-hover-helpers/on-hover-helpers.component';
import {StatisticsService} from '../../services/accounts/statistics.service';
import {ShowFoundHelpComponent} from '../container/left-requests/on-hover-helpers/show-found-help/show-found-help.component';
import {InfoHelpComponent} from '../container/left-requests/on-hover-helpers/info-help/info-help.component';
import {ExtendRequestComponent} from '../container/left-requests/on-hover-helpers/extend-request/extend-request.component';
import {ActivateModalComponent} from '../container/left-requests/on-hover-helpers/activate-modal/activate-modal.component';
import {ConfirmModalComponent} from '../container/left-requests/on-hover-helpers/confirm-modal/confirm-modal.component';
import {PriceAppModule} from '../../topbar/price-app/price-app.module';
import {NgPipesModule} from 'ngx-pipes';
import {NgxChartsModule} from '@swimlane/ngx-charts';
import {ShowCoastPipe} from '../container/left-requests/on-hover-helpers/show-found-help/show-coastshow-coast.pipe';
import {MatExpansionModule} from '@angular/material/expansion';
import {NgxSpinnerModule} from 'ngx-spinner';
import {OrderModule} from 'ngx-order-pipe';
import {PromoComponent} from '../container/promo/promo.component';
import {WebGameFormComponent} from '../web-game-form/web-game-form.component';


@NgModule({
  declarations: [
    BaseShortenDirective,
    BaseAccountComponent,
    ShortenTextPipe,
    PersonalInformationComponent,
    MessengersComponent,
    LeftRequestsComponent,
    SettingsComponent,
    StatisticsComponent,
    MessengersTComponent,
    FillBalanceComponent,
    disabledMaterialInputsDirective,
    GroupCategoryComponent,
    // GroupingComponent,
    EmptyCategoryComponent,
    OnHoverHelpersComponent,
    ShowFoundHelpComponent,
    InfoHelpComponent,
    ExtendRequestComponent,
    ActivateModalComponent,
    ConfirmModalComponent,
    ShowCoastPipe,
    PromoComponent,
    WebGameFormComponent
  ],
  imports: [
    MatDialogModule,
    CommonModule,
    MatNativeDateModule,
    BaseAccountRoutingModule,
    MatTabsModule,
    MatTooltipModule,
    MatSelectModule,
    ReactiveFormsModule,
    MatInputModule,
    ExpPipeModule,
    MatTableModule,
    MatButtonModule,
    MDBBootstrapModule,
    MatDatepickerModule,
    TextMaskModule,
    NgxPaginationModule,
    SearchModule,
    PriceAppModule,
    NgPipesModule,
    NgxChartsModule,
    MatExpansionModule,
    NgxSpinnerModule,
    OrderModule
  ],
  exports: [
    disabledMaterialInputsDirective
  ],
  providers: [
    {provide: DateAdapter, useClass: MyDateAdapter},
    {provide: MAT_DATE_LOCALE, useValue: 'ru-Ru'},
    MessengersMService,
    GroupingService,
    StatisticsService
  ]
})
export class BaseAccountModule {
}
