import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {BaseAccountComponent} from './base-account.component';
import {AuthGuardService} from '../../services/login/auth-guard.service';


const routes: Routes = [{ path: '', component: BaseAccountComponent, canActivate: [AuthGuardService]}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BaseAccountRoutingModule { }
