import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import * as moment from 'moment';

@Component({
  selector: 'app-sishik-footer-app',
  templateUrl: './sishik-footer-app.component.html',
  styleUrls: ['./sishik-footer-app.component.scss']
})
export class SishikFooterAppComponent implements OnInit {

  public nowYear: any;

  constructor(  private router: Router) { }

  ngOnInit() {
    this.nowYear = moment(new Date()).format('YYYY');
  }
  public sent_quest() {
    this.router.navigate(['/contact_us']);
  }

}
