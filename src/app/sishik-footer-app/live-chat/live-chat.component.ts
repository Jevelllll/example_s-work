import {Component, ElementRef, EventEmitter, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import * as moment from 'moment';
import {clearObject} from '../../shared/helper-container-leave';
import {ChatRoomService} from '../../services/chat-room/chat-room.service';
import {timer} from 'rxjs';

@Component({
  selector: 'app-live-chat',
  templateUrl: './live-chat.component.html',
  styleUrls: ['./live-chat.component.scss']
})
export class LiveChatComponent implements OnInit, OnDestroy {

  // tslint:disable-next-line:variable-name
  constructor(private _fb: FormBuilder, private chatRoomService: ChatRoomService) {
  }

  @ViewChild('wrHistory', {static: true}) wrHistory: ElementRef<any>;
  @Output('sendAc') public sendAc = new EventEmitter<boolean>();
  public filePreFormData = {value: null, fileName: '', name: 'live'};
  isSaveMail = false;
  isQestion = false;
  isDowload = false;
  isUpdlod = false;
  isAppC;
  previewLive = '';
  historyContent: any[] = [];
  private titleOne = 'Напишите нам и мы Вам ответим!';
  private titleTwo = 'Напишите ваше сообщение';
  public title = '';
  public liveForm: FormGroup;

   static checkExtension(c: any) {
    const valToLower = c;
    const regex = new RegExp('(.*?)\.(jpg|png|pdf|docx|txt|gif|jpeg)$'); // add or remove required extensions here
    const regexTest = regex.test(valToLower);
    return !regexTest ? true : false;
  }

  ngOnInit(): void {
    this.initForm();
    this.chatRoomService.getChatRoomState()
      // .pipe(takeUntil(componentDestroyed(this)))
      .subscribe((chatState) => {
        this.title = (chatState) ? this.titleTwo : this.titleOne;
        this.isAppC = chatState;

        if (!chatState) {
          this.isDowload = chatState;
        } else if (chatState && this.isQestion) {
          this.isDowload = chatState;
        }
      });

  }

  private scrollToBotHistory() {
    const element = this.wrHistory.nativeElement;
    element.scrollTop = element.scrollHeight - element.clientHeight;
  }

  isQestionM() {
    const key = this.liveForm.controls.key.value;
    this.chatRoomService.viewNotRead(key)
      // .pipe(takeUntil(componentDestroyed(this)))
      .subscribe(value => {});
    timer(0, 40000)
    // .pipe(takeUntil(componentDestroyed(this)))
    .subscribe(() => {
    this.chatRoomService.timeUpdate(key)
      // .pipe(takeUntil(componentDestroyed(this)))
      .subscribe(resultRes => {
        if (resultRes.msg === 'not_result') {
        } else {

          this.historyContent = resultRes.data.map(item => item).reverse();
          this.scrollToBotHistory();
        }
      });
    });
    this.isQestion = !this.isQestion;
    this.isDowload = this.isQestion;
  }

  onSubmit() {

    const formData: FormData = new FormData();
    formData.append('data', JSON.stringify(clearObject(this.liveForm.value)));

    if (this.filePreFormData.value === '' || this.filePreFormData.value === null) {
      this.reset();
    } else {
      try {
        formData.append(`${this.filePreFormData.name}`, this.filePreFormData.value,
          `${moment(new Date()).unix()}${this.filePreFormData.fileName}`);
      } catch (e) {
      }
    }

    this.chatRoomService.sendMsh(formData)
      // .pipe(takeUntil(componentDestroyed(this)))
      .subscribe(result => {
        this.chatRoomService.timeUpdate(this.liveForm.controls.key.value)
          // .pipe(takeUntil(componentDestroyed(this)))
          .subscribe((resultRes) => {
            if (resultRes.msg == 'not_result') {
            }else {
              this.historyContent = resultRes.data.map(item => item).reverse();
              this.scrollToBotHistory();
            }
          });
        this.reset(true);
      }, error => {
        this.reset(true);
      });

  }

  private reset(isForm = false) {
    if (isForm) {
      this.liveForm.controls.message.patchValue('');
    }

    Object.keys(this.filePreFormData).map((field) => {
      delete this.filePreFormData[field];
      this.previewLive = '';
    });
  }
  isUpdlodW(state) {
    document.getElementById('fileCl').click();
    this.isUpdlod = state;
  }
  isApp(state) {
    this.chatRoomService.setChatRoomState(state);
  }
  initForm() {

    const unCode = Math.round(Math.random() * 10) + moment(new Date()).unix();
    this.liveForm = this._fb.group({
      user_mail: [null, Validators.compose([Validators.required, Validators.email])],
      isCustomer: [1],
      message: ['', Validators.required],
      key: [unCode],
      worker_id: [6]
    });
  }

  cleanPreviewLive() {
    delete this.filePreFormData.fileName;
    delete this.filePreFormData.name;
    delete this.filePreFormData.value;
    this.previewLive = '';
  }
  public fileEvent(event: any) {

    const fileSelected: File = event.target.files[0];

    if (!LiveChatComponent.checkExtension(fileSelected.name)) {
      this.previewLive = fileSelected.name;
    } else {
      this.filePreFormData.value = '';
      return;
    }
    this.filePreFormData = {
      name: 'live',
      fileName: fileSelected.name,
      value: fileSelected
    };
  }
  get user_mail() {
    return this.liveForm.get('user_mail');
  }
  ngOnDestroy(): void {
  }

}
