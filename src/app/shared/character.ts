export class CharacteristicList {

  validate(name) {
    return !!characteristicsDataList[name];
  }
}


const characteristicsDataList = {
  auto: {val: true},
  property: {val: true},
  other: {val: true},
  autoproducts: {val: true},
  work: {val: true},
  legal: {val: true},
  quest: {val: true}
};
