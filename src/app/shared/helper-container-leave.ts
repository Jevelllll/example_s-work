import {FormArray, FormGroup} from '@angular/forms';


import {takeUntil} from 'rxjs/operators';
import {errorAssocc} from './fieldErrortranslation';
import {extraFields} from '../services/aditionals/electronicsmodel';

const USD = 1;
const EUR = 2;
const UAH = 3;

export function translitCurrency(idCurrency: number): any {

  let currentCurrency;
  if (Number(idCurrency) === USD) {
    currentCurrency = 'USD';
  } else if (Number(idCurrency) === EUR) {
    currentCurrency = 'EUR';
  } else if (Number(idCurrency) === UAH) {
    currentCurrency = 'UAH';
  } else {
    return false;
  }

  return currentCurrency;
}

export function preRegAuth() {
  const regExp = new RegExp(/[-()/\\]/g);
  this.toValidservice.getDataContactForm().subscribe(user => {
    this.loginService.login(user[0].phone.replace(regExp, ''), user[0].password).subscribe(result => {
      if (result === true) {
        this.router.navigate(['/account']);
      }
    }, () => {

    }, () => {
      this.loginService.setIsLogin(true);
      setTimeout(() => {
      }, 150);
    });
  });
}

export function correctFileldsAndView(isValid: boolean, form: any, toster) {
  const errror = [];
  if (isValid) {
  } else {
    const controls = form.controls;
    if (form.errors) {
      if (form.errors.raceQuery) {
        toster.error(`"Пробег, тыс. км/ ОТ больше чем ДО"`, '', {timeOut: 9000});
        errror.push(form.errors.raceQuery);
      }
    }
    Object.keys(controls)
      .forEach(controlName => {
        const control = form.controls[controlName];

        if (control.status === 'INVALID') {
          toster.error(`Проверьте поле "${errorAssocc[controlName].ru.toUpperCase()}"`, '', {timeOut: 9000});
          errror.push(control.status);
        }
      });
  }
  return errror.length;
}

export function goNextCategory(template: any) {
  // this.destroyAdditional();
  this.disabled(false);
  const numberSt = this.numberSt(this.back, 'up');
  this.numberS = numberSt;
  const bolean = (numberSt === 0) ? true : false;
  this.cleanActionDown(bolean);
  this.hideShowService.change(bolean);
  this.service.setEventS((this.checkRegistration) ? 2 : numberSt);
  if (this.checkRegistration) {
    if (numberSt === 1) {
      this.complete_flag = false;
      this.goCategoryTariff();
      this.titleNext = 'Создать заявку'.trim();
    } else if (numberSt === 2) {
      if (this.complete_flag === false) {
        this.toValidservice.setCommonForm(true);
        this.search_application_isReg_user();
      } else {
        this.renderer.removeClass(this.goNext.nativeElement, 'c-btn_primary');
        this.openModal(template);
      }
    }
  } else {
    if (this.getBack() === 0) {
      this.goContact();
    } else if (this.getBack() === 1) {
      this.complete_flag = false;
      this.goCategoryTariff();
      this.titleNext = 'Создать заявку'.trim();
    } else if (this.getBack() === 2) {
      if (this.complete_flag === false) {
        this.toValidservice.setCommonForm(true);
        this.toValidservice.getDataContactForm()
          // .pipe(takeUntil(componentDestroyed(this)))
          .subscribe(x => this.postContact(x, true));
      } else {
        this.renderer.removeClass(this.goNext.nativeElement, 'c-btn_primary');
        this.openModal(template);
      }
    }
  }
}


export function clearObject(commonObject) {
  const activityModel: any = commonObject;
  for (const key in activityModel) {
    if (activityModel[key] === null || activityModel[key] === '' || activityModel[key] === undefined
      || activityModel[key].length == 0
      || activityModel[key] === 'null' || activityModel[key] === false) {
      delete activityModel[key];
    }
  }
  return activityModel;
}


export const titleButtonSkip = 'Оплатить позже';

export function extraAdditional() {
  const queryExtra = this.myFormGroup.controls.query_extra as FormArray;
  queryExtra.controls = [];
  const dataParamsFields: any = {};
  this.additionalCam = [];
  if (Array.isArray(this.extraAdditionalFieldModel)) {

    this.extraAdditionalFieldModel.map((values: extraFields) => {
      if (values.type === 'text') {
        if (Boolean(values.is_from_to)) {
          if (values.valRia) {

            dataParamsFields[values.tbl_name] = this.fb.group({
              [values.valRia + '[from]']: '',
              [values.valRia + '[to]']: ''
            });
            this.additionalCam[values.tbl_name] = [{
              label: values.name + ' от',
              val: [values.valRia + '[from]'],
              type: values.type
            },
              {
                label: values.name + ' до',
                val: [values.valRia + '[to]'],
                type: values.type
              }
            ];
            queryExtra.push(this.fb.group(dataParamsFields));
          }
        }
      } else if (values.type === 'check' || values.type === 'check_one_select') {
        let valRiaName = '';
        let valR: any;
        try {
          valR = JSON.parse(values.valRia);
          // tslint:disable-next-line:forin
          for (const vN in valR) {
            valRiaName = vN;
          }

        } catch (e) {
          valRiaName = '';
        }
        if (valRiaName !== '') {

          dataParamsFields[values.tbl_name] = this.fb.group({[valRiaName]: ''});
          queryExtra.push(this.fb.group(dataParamsFields));

          this.additionalCam[values.tbl_name] = [
            {
              label: values.name,
              val: [valRiaName],
              type: values.type,
              data: valR[valRiaName]
            }
          ];

        }

      } else if (values.type === 'check_one') {

        dataParamsFields[values.tbl_name] = this.fb.group({[values.valRia]: ''});

        queryExtra.push(this.fb.group(dataParamsFields));
        const dataVal = [{
          id: values.valRia.replace(/[^0-9]/g, ''),
          name_ru: values.name
        }];
        this.additionalCam[values.tbl_name] = [{
          label: values.name,
          val: [values.valRia],
          type: values.type,
          data: dataVal
        }];
      }
    });

  }
}

export function eventPriceC(sliderPriceMinE, sliderPriceMaxE, form = null) {
  // tslint:disable-next-line:one-variable-per-declaration
  let valMin = form.controls.query_price_min.value,
    valMax = form.controls.query_price_max.value,
    // tslint:disable-next-line:prefer-const
    modelPrice: any = {query_price_min: valMin, query_price_max: valMax};

  if (sliderPriceMinE) {
    valMin = sliderPriceMinE;
    form.patchValue({query_price_min: valMin});
  }
  if (sliderPriceMaxE) {
    valMax = sliderPriceMaxE;
    form.patchValue({query_price_max: valMax});
  }

  if (valMin && valMax) {
    this.query_price_max = valMax;
    this.query_price_min = valMin;
    if (valMin > valMax) {
      this.query_price_max = valMin;
      this.query_price_min = valMax;
      modelPrice.query_price_max = valMin;
      modelPrice.query_price_min = valMax;
      form.patchValue(modelPrice);
    }
  }
}

export function flattenDeep(arr1) {
  return arr1.reduce((acc, val) => Array.isArray(val) ? acc.concat(flattenDeep(val)) : acc.concat(val), []);
}

export function urlRusLat(str) {

  if (str === null || str === undefined || str === 'null') {
    return false;
  }

  str = str.toString().toLowerCase(); // все в нижний регистр
  const cyr2latChars = new Array(
    ['а', 'a'], ['б', 'b'], ['в', 'v'], ['г', 'g'],
    ['д', 'd'], ['е', 'e'], ['ё', 'yo'], ['ж', 'zh'], ['з', 'z'],
    ['и', 'i'], ['й', 'y'], ['к', 'k'], ['л', 'l'],
    ['м', 'm'], ['н', 'n'], ['о', 'o'], ['п', 'p'], ['р', 'r'],
    ['с', 's'], ['т', 't'], ['у', 'u'], ['ф', 'f'],
    ['х', 'h'], ['ц', 'c'], ['ч', 'ch'], ['ш', 'sh'], ['щ', 'shch'],
    ['ъ', ''], ['ы', 'y'], ['ь', ''], ['э', 'e'], ['ю', 'yu'], ['я', 'ya'],

    ['А', 'A'], ['Б', 'B'], ['В', 'V'], ['Г', 'G'],
    ['Д', 'D'], ['Е', 'E'], ['Ё', 'YO'], ['Ж', 'ZH'], ['З', 'Z'],
    ['И', 'I'], ['Й', 'Y'], ['К', 'K'], ['Л', 'L'],
    ['М', 'M'], ['Н', 'N'], ['О', 'O'], ['П', 'P'], ['Р', 'R'],
    ['С', 'S'], ['Т', 'T'], ['У', 'U'], ['Ф', 'F'],
    ['Х', 'H'], ['Ц', 'C'], ['Ч', 'CH'], ['Ш', 'SH'], ['Щ', 'SHCH'],
    ['Ъ', ''], ['Ы', 'Y'],
    ['Ь', ''],
    ['Э', 'E'],
    ['Ю', 'YU'],
    ['Я', 'YA'],

    ['a', 'a'], ['b', 'b'], ['c', 'c'], ['d', 'd'], ['e', 'e'],
    ['f', 'f'], ['g', 'g'], ['h', 'h'], ['i', 'i'], ['j', 'j'],
    ['k', 'k'], ['l', 'l'], ['m', 'm'], ['n', 'n'], ['o', 'o'],
    ['p', 'p'], ['q', 'q'], ['r', 'r'], ['s', 's'], ['t', 't'],
    ['u', 'u'], ['v', 'v'], ['w', 'w'], ['x', 'x'], ['y', 'y'],
    ['z', 'z'],

    ['A', 'A'], ['B', 'B'], ['C', 'C'], ['D', 'D'], ['E', 'E'],
    ['F', 'F'], ['G', 'G'], ['H', 'H'], ['I', 'I'], ['J', 'J'], ['K', 'K'],
    ['L', 'L'], ['M', 'M'], ['N', 'N'], ['O', 'O'], ['P', 'P'],
    ['Q', 'Q'], ['R', 'R'], ['S', 'S'], ['T', 'T'], ['U', 'U'], ['V', 'V'],
    ['W', 'W'], ['X', 'X'], ['Y', 'Y'], ['Z', 'Z'],

    [' ', ''], ['0', '0'], ['1', '1'], ['2', '2'], ['3', '3'],
    ['4', '4'], ['5', '5'], ['6', '6'], ['7', '7'], ['8', '8'], ['9', '9'],
    ['-', '-']
  );

  let newStr = new String();

  for (let i = 0; i < str.length; i++) {

    const ch = str.charAt(i);
    let newCh = '';

    for (let j = 0; j < cyr2latChars.length; j++) {
      if (ch === cyr2latChars[j][0]) {
        newCh = cyr2latChars[j][1];

      }
    }
    // Если найдено совпадение, то добавляется соответствие, если нет - пустая строка
    newStr += newCh;

  }
  // Удаляем повторяющие знаки - Именно на них заменяются пробелы.
  // Так же удаляем символы перевода строки, но это наверное уже лишнее
  return newStr.replace(/[_]{2,}/gim, '_').replace(/\n/gim, '');
}

/****************поиск дочернего элемнента по классу********************/
export function findFirstChildByClass(element, className) {
  // tslint:disable-next-line:prefer-const
  let foundElement = null, found;

  // tslint:disable-next-line:no-shadowed-variable
  function recurse(element, className, found) {
    for (let i = 0; i < element.childNodes.length && !found; i++) {
      const el = element.childNodes[i];
      const classes = el.className != undefined ? el.className.split(' ') : [];
      for (let j = 0, jl = classes.length; j < jl; j++) {
        if (classes[j] == className) {
          found = true;
          foundElement = element.childNodes[i];
          break;
        }
      }
      if (found) {
        break;
      }
      recurse(element.childNodes[i], className, found);
    }
  }

  recurse(element, className, false);
  return foundElement;
}

export function transformRequest(obj) {
  const str = [];
  // tslint:disable-next-line:forin
  for (const p in obj) {
    str.push(p + '=' + obj[p]);
  }
  return str.join('&');
}

declare var $: any;
declare var EnjoyHint: any;

export function commonHindHelper(myForm, split = false, contentSplit = [], toster = null) {
  // tslint:disable-next-line:variable-name
  const enjoyhint_script_steps: any = [];

  document.getElementsByClassName('goNext')[0].addEventListener('click', () => {
    this.hintsService.installLeaveRequesState();
  }, false);

  this.hintsService.__obsLeaveRequestState
    // .pipe(takeUntil(componentDestroyed(this)))
    .subscribe((hintLeave) => {
    if (hintLeave) {
    } else {
      let localHintHome;
      try {
        localHintHome = JSON.parse(localStorage.getItem('hint-leave-request'));
      } catch (e) {
      }
      if (localHintHome) {
      } else {
        if ([].slice.call(document.getElementsByClassName('services-active')).length == 0) {
          enjoyhint_script_steps.push({
            'click .main-help-service': 'Выберите один или несколько сайтов сервисов',
            skipButton: {className: 'mySkip', text: 'Знаю'},
          });
        }
        const elementName = (className) => {
          const name: any = document.getElementsByClassName(className)[0].querySelector('label') || false;
          if (name) {
            return name.innerText.toLowerCase();
          } else {
            return className;
          }
        };
        const formDataList = Object.keys(myForm.controls);
        const rootFieldName = formDataList[0];
        formDataList.map((fields) => {
          if (myForm.controls[fields].errors) {
            const preObjField = `change .${fields}`;
            if (fields === 'query_currency') {
              enjoyhint_script_steps.push({
                [preObjField]: 'Выберите "Валюту"',
                skipButton: {className: 'mySkip', text: 'Закрыть'},
                scrollAnimationSpeed: 1000,
              });
              enjoyhint_script_steps.push({
                'key .query_price_min': '"Укажите минимальную сумму" и нажмите *ENTER*',
                keyCode: 13,
                skipButton: {className: 'mySkip', text: 'Закрыть'},
                scrollAnimationSpeed: 1000,
              });
              enjoyhint_script_steps.push({
                'key .query_price_max': '"Укажите максимальную сумму" и нажмите *ENTER*',
                keyCode: 13,
                skipButton: {className: 'mySkip', text: 'Закрыть'},
                scrollAnimationSpeed: 1000,
              });

            } else if (fields === rootFieldName) {
              if (!this.typeId) {
                enjoyhint_script_steps.push({
                  [preObjField]: `Укажите "${elementName(fields)}"`,
                  skipButton: {className: 'mySkip', text: 'Закрыть'},
                  scrollAnimationSpeed: 1000
                });
              }
            } else {
              enjoyhint_script_steps.push({
                [preObjField]: `Укажите "${elementName(fields)}"`,
                skipButton: {className: 'mySkip', text: 'Закрыть'},
                scrollAnimationSpeed: 1000,
              });
            }
          }
        });

        if (split) {
          contentSplit.forEach((data) => {
            enjoyhint_script_steps.push(data);
          });
        } else {
          enjoyhint_script_steps.push({
            'click .confirmbtn>.c-btn_primary': 'Подтвердить и перейти дальше.',
            skipButton: {className: 'mySkip', text: 'Закрыть'},
          });
        }
        let objEnf = {};
        if (toster) {
          objEnf = {
            onEnd() {
              toster.success('Продолжайте сами вы хорошо справляетесь!');
            }
          };
        }

        const hintsService = this.hintsService;
        $(document).ready(function() {
          // tslint:disable-next-line:variable-name
          const enjoyhint_instance: any = new EnjoyHint(objEnf);

          const enjoyhint = document.getElementsByClassName('enjoyhint');
          const eventFunc = (event) => {
            if ([].slice.call(event.target.classList).includes('mySkip')) {
              hintsService.installLeaveRequesState();
            }
          };
          enjoyhint[0].addEventListener('click', eventFunc, false);
          setTimeout(() => {
            try {
              enjoyhint_instance.set(enjoyhint_script_steps);
              try {
                enjoyhint_instance.run();
              } catch (e) {
              }
            } catch (e) {
            }
          }, 200);
        });
      }
    }

  }, error => {});

}
