
import {Stepsmodel} from './stepsmodel';

export const DATASTEPS: Stepsmodel[] = [
    {
        title: 'Шаг 1',
        text: 'Выбрать категорию',
        active: true,
    },
     {
        title: 'Шаг 2',
        text: 'Оставить контактные данные',
        active: false,
    },
    {
        title: 'Шаг 3',
        text: 'Оплата заявки',
        active: false,
    }
];