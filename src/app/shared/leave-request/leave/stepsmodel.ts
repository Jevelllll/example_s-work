export class Stepsmodel {
    title: string;
    text: string;
    active: boolean;
}

export class FeaturesModel {

    constructor(public icons: any,
                public title: string,
                // tslint:disable-next-line:variable-name
                public description_text: string,
                public active: boolean,
                // tslint:disable-next-line:variable-name
                public img_src: any) {
    }
}
