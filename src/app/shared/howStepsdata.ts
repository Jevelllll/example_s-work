import {FeaturesModel, Stepsmodel} from './leave-request/leave/stepsmodel';


export const HOWSTEPSDATA: Stepsmodel[] = [
    {
        title: 'Шаг',
        text: 'Выбрать категорию',
        active: true,
    },
     {
        title: 'Шаг',
        text: 'Выбор ресурсов',
        active: false,
    },
    {
        title: 'Шаг',
        text: 'Оставить контактные данные',
        active: false,
    },
    {
        title: 'Шаг',
        text: 'Оплата',
        active: false,
    },
];

export const FEATURENEWSDATA: FeaturesModel[] = [

    {
        icons: './../../../assets/icons/checked.png',
        title: 'Уникальность',
        description_text: 'Единственный сайт в Украине помогающий в поиске товаров',
        active: true,
        img_src: 'uniqueness.png'
    },
    {
        icons: './../../../assets/icons/settings.png',
        title: 'Гибкая настройка',
        description_text: 'Используя дополнительные параметры сузьте поиск товара подходящего под ваши критерии',
        active: false,
        img_src: 'flexible_setup.png'
    },
    {
        icons: './../../../assets/icons/mail.png',
        title: 'Оповещение',
        description_text: 'Получайте информацию о найденных товарах сразу на телефон это позволит вам сразу связаться с продавцом',
        active: false,
        img_src: 'sms_notif.png'
    },
    {
        icons: './../../../assets/icons/clock.png',
        title: 'Экономия времени',
        description_text: 'Вам нет необходимости посещать множество интернет магазинов и искать в них необходимый товар, мы это сделаем за вас',
        active: false,
        img_src: 'saving_time.png'
    }
];


