

export class HelpModel {
    title: string;
    description: string;
}

export class RulesModel {
    title: string;
    data: [];
}

export const DataHelp: HelpModel[] = [
    {
        title: 'Как зарегистрироваться?',
        description: 'Регистрация производится автоматически во время подачи заявки.'
    },
    {
        title: 'Как подать заявку?',
        description: 'Для подачи заявки выберите нужную вам категорию товаров или услуг на главной странице сайта и заполните форму.'
    },
    {
        title: 'Как оплатить?',
        description: 'После заполнения формы нужно нажать кнопку оплатить, после чего вы автоматически перенаправляетесь на сервис оплаты.'
    },
    {
        title: 'Сколько товаров можно выбрать в одной заявке?',
        description: 'Для каждого товара создается одна заявка. Все свои заявки вы можете просмотреть в личном кабинете.'
    },
    {
        title: 'Как быстро будет найден нужный товар?',
        description: 'Оповещение о найденном товаре придет вам сразу же после того как он появится на одном из сайтов'
    },
    {
        title: 'Сколько я буду получать СМС в день?',
        description: 'Вы сами выбираете для себя тарифный план с СМС сообщениями. Минимальный пакет - 5 бесплатных СМС в день.'
    },
    {
        title: 'В какое время будет приходить СМС?',
        description: 'Сообщения будут приходить ежедневно с 9.00-18.00.'
    },
    {
        title: 'Как отключить СМС информирование, если товар найден?',
        description: 'Отключить СМС информирование вы можете в своем личном кабинете.'
    },
    {
        title: 'Сколько действует заявка?',
        description: 'Заявка действует в течении месяца с момента оплаты.'
    },
];

export const SELECTEDHERO: SelectedHero[] = [
  // { options: 'Транспорт', value: 'transport'},
  // { options: 'Электроника', value: 'electronics', src: ''},
  { options: 'Недвижимость', value: 'property', src: '../../../../assets/images/category/hero/mainproperty.png'},
  { options: 'Транспорт', value: 'auto', src: '../../../../assets/images/category/hero/maintransport.png'},
  { options: 'Юридические услуги', value: 'legal', src: '../../../../assets/images/category/hero/legal.png'},
  { options: 'Работа', value: 'work', src: '../../../../assets/images/category/hero/query_work.png'},
  { options: 'Другое', value: 'other', src: '../../../../assets/images/category/hero/other.png'},
  { options: 'СТО (СПРАВКА)', value: 'sto', src: '../../../../assets/images/category/hero/sto.png'},
  { options: 'Крафт', value: 'kraft', src: '../../../../assets/images/category/hero/kraft.png'},
  { options: 'Квест', value: 'quest', src: '../../../../assets/images/category/hero/quest.png'},
  // { options: 'Автотовары', value: 'autoproducts', src: '../../../../assets/images/landing/mainautoproperty.png'},
  // { options: 'Работа', value: 'job'},
  // { options: 'Билеты, отели', value: 'tickets'},
  // { options: 'Услуги', value: 'services'},
  // { options: 'Прочее', value: 'other'}
] ;
export class SelectedHero {
  options: string;
  value: string;
  src: string;
}
