export class TariffModel {
  // title: string;
  // countSmsDay: number;
  // priceDay: number;
  // priceMonth: number;
  // select: boolean;
  // // discount: number;
  id: number;
  // tslint:disable-next-line:variable-name
  count_sms: number;
  discount: number;
  // tslint:disable-next-line:variable-name
  sum_day: number;
  // tslint:disable-next-line:variable-name
  sum_month: number;
  select: boolean;
  // tslint:disable-next-line:variable-name
  tariff_name: string;
  typeTitle: string;
  // tslint:disable-next-line:variable-name
  isCust_quest: number;
  // discount: number;
}

export class DataPayModel {
  // tslint:disable-next-line:variable-name
  constructor(public id: number, public query_tariff_end: any, public query_tariff_start: any) {
  }
}
