export const TRANSLATION = {
  id: 'id',
  service: 'Сервис',
  date_create: 'Создан',
  currency: 'Валюта',
  amount: 'Сумма (UAH)',
  status: 'Статус',
  null: 'Нет данных',
  sandbox: 'Песочница',
  success: 'Успешный',
  error: 'Неуспешный',
  failure: 'Неуспешный',
  reversed: 'Платеж возвращен',
  subscribed: 'Подписка успешно оформлена',
  unsubscribed: 'Подписка успешно деактивирована',
  1: 'liqPay',
  order: 'Заявка'
};
