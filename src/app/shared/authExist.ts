export function authExit(loginService, router, isRedirect = true) {
  choice(isRedirect);
  // tslint:disable-next-line:no-shadowed-variable
  function choice(isRedirect: boolean) {
    loginService.checksession(null, null).subscribe(resp => {
      if (!resp) {
        loginService.logout();
        if (isRedirect) {
          router.navigate(['/']);
        }
      }
    }, error => {
      loginService.logout();
      if (isRedirect) {
        router.navigate(['/']);
      }
    });
  }
}
